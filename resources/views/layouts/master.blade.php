<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="title" content="{{ $seoSetting->meta_title }}">
    <meta name="description" content="{{ $seoSetting->meta_description }}">
    <meta name="author" content="{{ $seoSetting->meta_author }}">
    <meta name="keywords" content="{{ $seoSetting->meta_keyword }}">
    <meta name="robots" content="all">
    <title>Ecommerce | @yield('title')</title>
    <x-head />
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <script>
        {!! $seoSetting->google_analytics !!}
    </script>
    @stack('head')
</head>

<body class="cnt-home">
    <!-- ============================================== HEADER ============================================== -->
    <x-header />
    <!-- ============================================== HEADER : END ============================================== -->
    @yield('content')
    <!-- /#top-banner-and-menu -->
    <!-- ============================================================= FOOTER ============================================================= -->
    <x-footer />
    <!-- ============================================================= FOOTER : END============================================================= -->
    <!-- JavaScripts placed at the end of the document so the pages load faster -->
    <x-scripts />
    <!-- Modal -->
    <div class="modal fade" id="addToCartModal" tabindex="-1" role="dialog" aria-labelledby="addToCartModalTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle"><strong><span id="pname"></span> </strong></h5>
                    <button type="button" class="close" data-dismiss="modal" id='closeModal' aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card" style="width: 18rem;">
                                <img src=" " class="card-img-top" alt="..." style="height: 200px; width: 200px;"
                                    id="pimage">
                            </div>
                        </div><!-- // end col md -->
                        <div class="col-md-4">
                            <ul class="list-group">
                                <li class="list-group-item">Product Price: <strong class="text-danger">$<span
                                            id="pprice"></span></strong>
                                    <del id="oldprice">$</del>
                                </li>
                                <li class="list-group-item">Product Code: <strong id="pcode"></strong></li>
                                <li class="list-group-item">Category: <strong id="pcategory"></strong></li>
                                <li class="list-group-item">Brand: <strong id="pbrand"></strong></li>
                                <li class="list-group-item">Stock: <span class="badge badge-pill badge-success"
                                        id="aviable" style="background: green; color: white;"></span>
                                    <span class="badge badge-pill badge-danger" id="stockout"
                                        style="background: red; color: white;"></span>
                                </li>
                            </ul>
                        </div><!-- // end col md -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="color">Choose Color</label>
                                <select class="form-control" id="color" name="color">
                                </select>
                            </div> <!-- // end form group -->
                            <div class="form-group" id="sizeArea">
                                <label for="size">Choose Size</label>
                                <select class="form-control" id="size" name="size">
                                    <option>1</option>
                                </select>
                            </div> <!-- // end form group -->
                            <div class="form-group">
                                <label for="qty">Quantity</label>
                                <input type="number" class="form-control" id="qty" value="1" min="1">
                            </div> <!-- // end form group -->
                            <input type="hidden" id="product_id">
                            <button type="submit" class="btn btn-primary mb-2" onclick="addToCart()">Add to
                                Cart</button>
                        </div><!-- // end col md -->
                    </div> <!-- // end row -->
                </div>
            </div>
        </div>
    </div>

    <!-- Order Traking Modal -->
    <div class="modal fade" id="orderTrackingModal" tabindex="-1" aria-labelledby="orderTrackingModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Track Your Order </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form method="get" action="{{ route('user.order.tracking') }}">
                        <div class="modal-body">
                            <label>Invoice Code</label>
                            <input type="text" name="invoice_no" required="" class="form-control"
                                placeholder="Your Order Invoice Number" autofocus>
                        </div>

                        <button class="btn btn-danger" type="submit" style="margin-left: 17px;"> Track Now </button>

                    </form>


                </div>

            </div>
        </div>
    </div>
    @stack('scripts')
    <script src="{{ mix('js/app.js') }}"></script>
    <script>
        @if (session('success'))
            $.toast({
            text: "{{ session('success') }}",
            position: 'top-right',
            loaderBg: '#ff6849',
            icon: 'success',
            hideAfter: 3500,
            stack: 6
            });
        @elseif(session('error'))
            $.toast({
            text: "{{ session('error') }}",
            position: 'top-right',
            loaderBg: '#ff6849',
            icon: 'error',
            hideAfter: 3500
            });
        @endif
    </script>
</body>

</html>
