@extends('layouts.master')
@section('title', 'Dashboard')
@section('content')
    <div class="container">
        <div class="row">
            <!-- col md 2 -->
            <x-user.sidebar />
            <div class="col-md-10">
                <div class="card">
                    <h3 class="text-center">
                        <span class="text-danger">Hi....</span>
                        <strong>{{ auth()->user()->name }}</strong>
                        Welcome To Easy Online Shop
                    </h3>
                </div>
            </div>
            <div class="col-md-2">

            </div>
        </div>
    </div>
@endsection
