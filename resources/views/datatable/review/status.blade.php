@php
$badgeColor = $review->status === 1 ? 'success' : 'danger';
$statusText = $review->status === 1 ? 'Publish' : 'Pending';
@endphp
<span class="badge badge-pill badge-{{ $badgeColor }} mr-2">{{ $statusText }}</span>
