@if ($review->status === 0)
    <a href="{{ route('admin.reviews.approve', $review->id) }}" class="btn btn-danger btn-sm ml-2"
        title="Review Publish">Publish</a>
@endif
<a href="#" id='delete' class="btn btn-danger btn-sm ml-2" title="Review Delete"><i class="fa fa-trash"></i></a>
<form class='btn-block d-none' id='formDelete' action="{{ route('admin.reviews.destroy', $review->id) }}"
    method="POST">
    @csrf
    @method('delete')
    <button type='submit' class="btn btn-danger btn-sm fa fa-trash">
    </button>
</form>
