<a href="{{ route('admin.permissions.edit', $permission->id) }}" class="btn btn-info btn-sm" title="Edit Permission"><i
        class="fa fa-pencil"></i> </a>
<a href="#" id='delete' class="btn btn-danger btn-sm ml-2" title="Permission Delete"><i class="fa fa-trash"></i></a>
<form class='btn-block d-none' id='formDelete' action="{{ route('admin.permissions.destroy', $permission->id) }}" method="POST">
    @csrf
    @method('delete')
    <button type='submit' class="btn btn-danger btn-sm fa fa-trash">
    </button>
</form>
