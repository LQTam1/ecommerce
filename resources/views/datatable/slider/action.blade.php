<a href="{{ route('admin.sliders.toggle-status', $slider->id) }} "
    class="btn btn-sm btn-{{ $slider->status === 1 ? 'danger' : 'success' }}"
    title="Switch Status {{ $slider->status === 1 ? 'Off' : 'On' }}"><i
        class="fa fa-toggle-{{ $slider->status === 1 ? 'off' : 'on' }}"></i> </a>
<a href="{{ route('admin.sliders.edit', $slider->id) }} " class="btn btn-sm btn-info" title="Slider Edit Data"><i
        class="fa fa-pencil"></i> </a>
<a href="#" id='delete' title='Delete Slider' class="btn btn-danger btn-sm fa fa-trash">
</a>
<form class='btn-block d-none' id='formDelete' action="{{ route('admin.sliders.destroy', $slider->id) }}"
    method="POST">
    @csrf
    @method('delete')
    <button type='submit' class="btn btn-danger btn-sm fa fa-trash">
    </button>
</form>
