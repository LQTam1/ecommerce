<img width="{{ $item['width'] ?? 60 }}" height="{{ $item['height'] ?? 60 }}"
    src="{{ request()->action === 'pdf' ? public_path($item['src']) : asset($item['src']) }}"
    alt="{{ $item['name'] }}" />
