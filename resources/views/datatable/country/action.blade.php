<a href="{{ route('admin.countries.edit', $country->id) }}" title="Edit Country" class="btn btn-primary btn-sm fa fa-pencil">
</a>
<a href="#" id='delete' title='Delete Country' class="btn btn-primary btn-sm fa fa-trash">
</a>
<form class='btn-block d-none' id='formDelete' action="{{ route('admin.countries.destroy', $country->id) }}" method="POST">
    @csrf
    @method('delete')
    <button type='submit' class="btn btn-danger btn-sm fa fa-trash">
    </button>
</form>
