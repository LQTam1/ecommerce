<a href="{{ route('admin.blog.postCategories.edit', $postCategory->id) }}" class="btn btn-info btn-sm"
    title="Post Category Edit Data"><i class="fa fa-pencil"></i> </a>
<a href="#" id='delete' title='Delete Brand' class="btn btn-primary btn-sm fa fa-trash">
</a>
<form class='btn-block d-none' id='formDelete' action="{{ route('admin.blog.postCategories.destroy', $postCategory->id) }}"
    method="POST">
    @csrf
    @method('delete')
    <button type='submit' class="btn btn-danger btn-sm fa fa-trash">
    </button>
</form>
