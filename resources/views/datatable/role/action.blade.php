<a href="{{ route('admin.roles.edit', $role->id) }}" class="btn btn-info btn-sm" title="Edit Role"><i
        class="fa fa-pencil"></i> </a>
<a href="#" id='delete' class="btn btn-danger btn-sm ml-2" title="Role Delete"><i class="fa fa-trash"></i></a>
<form class='btn-block d-none' id='formDelete' action="{{ route('admin.roles.destroy', $role->id) }}" method="POST">
    @csrf
    @method('delete')
    <button type='submit' class="btn btn-danger btn-sm fa fa-trash">
    </button>
</form>
