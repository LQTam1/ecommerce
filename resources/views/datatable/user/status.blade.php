@php
$lastActivity = \Carbon\Carbon::parse($user->last_seen)->diffForHumans();
@endphp
@if ($user->isUserOnline())
    <span class="badge badge-pill badge-success">Active Now</span>
@else
    <span class="badge badge-pill badge-danger">{{ $lastActivity }}</span>
@endif
