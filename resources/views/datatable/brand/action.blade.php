<a href="{{ route('admin.brands.edit', $brand->id) }}" title="Edit Brand" class="btn btn-primary btn-sm fa fa-pencil">
</a>
<a href="#" id='delete' title='Delete Brand' class="btn btn-primary btn-sm fa fa-trash">
</a>
<form class='btn-block d-none' id='formDelete' action="{{ route('admin.brands.destroy', $brand->id) }}" method="POST">
    @csrf
    @method('delete')
    <button type='submit' class="btn btn-danger btn-sm fa fa-trash">
    </button>
</form>
