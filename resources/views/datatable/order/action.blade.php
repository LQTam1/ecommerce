<a href="{{ route('admin.orders.show',$order->id) }} " class="btn btn-sm btn-warning" title="Order View Data"><i
        class="fa fa-eye"></i> </a>
@if ($order->status !== 'pending')
    <a target="_blank" href="{{ route('admin.order.invoice.download', $order->id) }}" class="btn btn-danger ml-2"
        title="Order Invoice Download"><i class="fa fa-download"></i> </a>
@elseif ($order->return_order === 1)
    <a href="{{ route('admin.return.order.approve', $order->id) }}" class="btn btn-danger ml-2"
        title="Return Approve">Return Approve</a>
@endif