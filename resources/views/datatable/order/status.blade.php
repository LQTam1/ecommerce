@switch($order->status)
    @case ('delivered')
        <span class="badge badge-pill badge-success {{ $order->return_order === 2 ? 'd-none' : '' }} mr-2">
            {{ $order->status }}
            ({{ $order->delivered_date }}) </span>
    @break;
    @case ('confirm')
        <span class="badge badge-pill badge-info {{ $order->return_order === 2 ? 'd-none' : '' }} mr-2">
            {{ $order->status }}
            ({{ $order->confirmed_date }}) </span>
    @break;
    @case ('processing')
        <span class="badge badge-pill badge-warning {{ $order->return_order === 2 ? 'd-none' : '' }} mr-2">
            {{ $order->status }}
            ({{ $order->processing_date }}) </span>
    @break;
    @case ('picked')
        <span class="badge badge-pill badge-secondary {{ $order->return_order === 2 ? 'd-none' : '' }} mr-2">
            {{ $order->status }}
            ({{ $order->picked_date }}) </span>
    @break;
    @case ('shipped')
        <span class="badge badge-pill badge-light {{ $order->return_order === 2 ? 'd-none' : '' }} mr-2">
            {{ $order->status }}
            ({{ $order->shipped_date }}) </span>
    @break;
    @case ('cancel')
        <span class="badge badge-pill badge-danger {{ $order->return_order === 2 ? 'd-none' : '' }} mr-2">
            {{ $order->status }}
            ({{ $order->cancel_date }}) </span>
    @break;
    @default
        <span class="badge badge-pill badge-primary {{ $order->return_order === 2 ? 'd-none' : '' }} mr-2">
            {{ $order->status }}
            ({{ $order->order_date }}) </span>
@endswitch

@if ($order->return_order === 1)
    <span class="badge badge-pill badge-danger"> Return Requested </span>
@elseif ($order->return_order === 2)
    <span class="badge badge-pill badge-success"> Return Success ({{ $order->return_date }})</span>
@endif
