<a href="{{ route('admin.admins.show', $admin->id) }}" class="btn btn-sm btn-warning" title="Admin View Data"><i
        class="fa fa-eye"></i> </a>
<a href="{{ route('admin.admins.edit', $admin->id) }}" class="btn btn-sm btn-info ml-2" title="Admin Edit Data"><i
        class="fa fa-pencil"></i> </a>
<a href="#" id='delete' class="btn btn-danger btn-sm ml-2" title="Role Delete"><i class="fa fa-trash"></i></a>
<form class='btn-block d-none' id='formDelete' action="{{ route('admin.admins.destroy', $admin->id) }}" method="POST">
    @csrf
    @method('delete')
    <button type='submit' class="btn btn-danger btn-sm fa fa-trash">
    </button>
</form>
