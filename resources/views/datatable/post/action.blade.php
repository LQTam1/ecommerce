<a href="{{ route('admin.blog.posts.edit', $post->id) }} " class="btn btn-sm btn-info" title="Post Edit Data"><i
        class="fa fa-pencil"></i> </a>
<a href="#" id='delete' title='Delete Brand' class="btn btn-primary btn-sm fa fa-trash">
</a>
<form class='btn-block d-none' id='formDelete' action="{{ route('admin.blog.posts.destroy', $post->id)}}"
    method="POST">
    @csrf
    @method('delete')
    <button type='submit' class="btn btn-danger btn-sm fa fa-trash">
    </button>
</form>
