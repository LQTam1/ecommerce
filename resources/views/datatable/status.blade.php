@if ($item->status == 1)
    <span class="badge badge-pill badge-success" data-status='1'> Active </span>
@else
    <span class="badge badge-pill badge-danger" data-status='0'> InActive </span>
@endif
