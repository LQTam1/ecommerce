@if ($product->discount_price == null)
    <span class="badge badge-pill badge-danger" data-discount-price="null">No Discount</span>
@else
    <span class="badge badge-pill badge-danger" data-discount-price="{{ $product->discount_price }}">
        {{ $product->getDiscountPercentage() }}%</span>
@endif
