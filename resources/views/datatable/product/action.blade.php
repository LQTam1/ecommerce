<a href="{{ route('admin.products.toggle-status', $product->id) }} "
    class="btn btn-sm btn-{{ $product->status === 1 ? 'danger' : 'success' }}"
    title="Switch Status {{ $product->status === 1 ? 'Off' : 'On' }}"><i
        class="fa fa-toggle-{{ $product->status === 1 ? 'off' : 'on' }}"></i> </a>
<a href="{{ route('admin.products.edit', $product->id) }} " class="btn btn-sm btn-info" title="Product Edit Data"><i
        class="fa fa-pencil"></i> </a>
<a id="deleteRow" data-href="{{ route('admin.products.destroy', $product->id) }} " class="btn btn-sm btn-danger"
    title="Product Delete Data"><i class="fa fa-trash"></i> </a>
