@component('mail::message')
# Order Paid

Your order has been placed!

@component('mail::table')
    |   Invoice No  |     Amount    |   Name   |    Email   |
    | ------------- |:-------------:| --------:|-----------:|
    | {{ $invoice['invoice_no'] }} | ${{ $invoice['amount'] }} | {{ $invoice['name'] }} | {{ $invoice['email'] }} |
@endcomponent

@component('mail::button', ['url' => route('user.orders.show',$invoice['order_id'])])
    View Order
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
