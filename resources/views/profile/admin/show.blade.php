@extends('layouts.admin.master')
@section('title', 'Profile')
@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <section class="content">
                <div class="row">
                    <div class="col-12">
                        <!-- /.box -->
                        <div class="box box-widget widget-user">
                            <!-- Add the bg color to the header using any of the bg-* classes -->
                            <div class="widget-user-header bg-black"
                                style="background: url('{{ asset('backend/assets/images/gallery/full/10.jpg') }}') center center;">
                                <h3 class="widget-user-username">Name: {{ auth('admin')->user()->name }}</h3>
                                {{-- <button id='editBtn' class="pull-right btn btn-rounded btn-success mb-5" data-toggle="modal" data-target="#modal-center">Edit Profile</button> --}}
                                <h6 class="widget-user-desc">Email: {{ auth('admin')->user()->email }}</h6>
                            </div>
                            <div class="widget-user-image">
                                <img class="rounded-circle"
                                    src="{{ auth('admin')->user()->profile_photo_url }}"
                                    alt="User Avatar">
                            </div>
                            <div class="box-footer">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="description-block">
                                            <h5 class="description-header">12K</h5>
                                            <span class="description-text">FOLLOWERS</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4 br-1 bl-1">
                                        <div class="description-block">
                                            <h5 class="description-header">550</h5>
                                            <span class="description-text">FOLLOWERS</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4">
                                        <div class="description-block">
                                            <h5 class="description-header">158</h5>
                                            <span class="description-text">TWEETS</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <!-- /.box -->
                        <div class="box box-widget widget-user p-5">
                            <h3>Profile Information</h3>
                            <!-- Add the bg color to the header using any of the bg-* classes -->
                            <form method='post' action="{{ route('admin.profile-information.update') }}" enctype="multipart/form-data">
                                @csrf
                                @method('put')
                                <div class="form-group @error('name') is-invalid @enderror">
                                    <h5>Name <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="name" id="name" class="form-control"
                                            value="{{ auth('admin')->user()->name }}" required="">
                                        <div class="help-block"></div>
                                    </div>
                                    @error('name')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group @if ($errors->first('email')) is-invalid @endif">
                                    <h5>Email <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="email" id="email" name="email" class="form-control" required=""
                                            value="{{ auth('admin')->user()->email }}">
                                    </div>
                                    @if ($errors->first('email'))
                                        <div class="text-danger">{{ $errors->first('email') }}</div>
                                    @endif
                                </div>

                                <div class="form-group @error('photo') is-invalid @enderror">
                                    <h5>Avatar</h5>
                                    <div class="controls">
                                        <input type="file" id="photo" name="photo" class="form-control">
                                    </div>
                                    @error('photo')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                    <img class="border rounded-circle" id="showAvatar"
                                        src="{{ auth('admin')->user()->profile_photo_url }}"
                                        height="120"
                                        alt="User Avatar">
                                </div>
                                <div class="text-xs-right">
                                    <button type="submit"
                                        class="btn btn-rounded btn-primary mb-5 pull-right">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-12">
                        <!-- /.box -->
                        <div class="box box-widget widget-user p-5">
                            <h3>Update Password</h3>
                            <!-- Add the bg color to the header using any of the bg-* classes -->
                            <form method='post' action="{{ route('admin.password.update') }}">
                                @csrf
                                @method('put')
                                <div class="form-group @error('current_password') invalid @enderror">
                                    <h5>Current Password</h5>
                                    <div class="controls">
                                        <input type="password" name="current_password" id="current_password"
                                            class="form-control">
                                    </div>
                                    @error('current_password', 'updatePassword')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="form-group @error('password') invalid @enderror">
                                    <h5>New Password</h5>
                                    <div class="controls">
                                        <input type="password" name="password" id="password" class="form-control">
                                    </div>
                                    @error('password', 'updatePassword')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="form-group @error('password_confirmation') invalid @enderror">
                                    <h5>Password Confirm</h5>
                                    <div class="controls">
                                        <input type="password" name="password_confirmation" id="password_confirmation"
                                            class="form-control">
                                    </div>
                                    @error('password_confirmation', 'updatePassword')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="text-xs-right">
                                    <button type="submit"
                                        class="btn btn-rounded btn-primary mb-5 pull-right">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
    </div>
@endsection
@push('scripts')
    {{-- <script src="{{asset('backend/assets/js/pages/validation.js')}}"></script>
<script src="{{asset('backend/assets/js/pages/form-validation.js')}}"></script> --}}
    <script type="text/javascript">
        $('#photo').change(function(e) {
            let render = new FileReader();
            render.onload = function(e) {
                $('#showAvatar').attr('src', e.target.result);
            }
            render.readAsDataURL((e.target.files[0]))
        })
    </script>
@endpush
