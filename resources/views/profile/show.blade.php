{{-- <x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Profile') }}
        </h2>
    </x-slot>

    <div>
        <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
            @if (Laravel\Fortify\Features::canUpdateProfileInformation())
                @livewire('profile.update-profile-information-form')

                <x-jet-section-border />
            @endif

            @if (Laravel\Fortify\Features::enabled(Laravel\Fortify\Features::updatePasswords()))
                <div class="mt-10 sm:mt-0">
                    @livewire('profile.update-password-form')
                </div>

                <x-jet-section-border />
            @endif

            @if (Laravel\Fortify\Features::canManageTwoFactorAuthentication())
                <div class="mt-10 sm:mt-0">
                    @livewire('profile.two-factor-authentication-form')
                </div>

                <x-jet-section-border />
            @endif

            <div class="mt-10 sm:mt-0">
                @livewire('profile.logout-other-browser-sessions-form')
            </div>

            @if (Laravel\Jetstream\Jetstream::hasAccountDeletionFeatures())
                <x-jet-section-border />

                <div class="mt-10 sm:mt-0">
                    @livewire('profile.delete-user-form')
                </div>
            @endif
        </div>
    </div>
</x-app-layout> --}}

@extends('layouts.master')
@section('title', 'Profile')
@section('content')

    <div class="body-content">
        <div class="container">
            <div class="row">

                <x-user.sidebar />

                <div class="col-md-10">
                    <div class="row">
                        <div class="col-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="text-center">
                                        Profile Information
                                    </h3>
                                </div>
                                <div class="panel-body">
                                    <form method="post" action="{{ route('user.profile.update') }}"
                                        enctype="multipart/form-data">
                                        @csrf
                                        @method('put')
                                        <div class="form-group @error('name') is-invalid @enderror">
                                            <label class="info-title" for="name">Name <span> </span></label>
                                            <input type="text" name="name" class="form-control"
                                                value="{{ auth()->user()->name }}">
                                            @error('name')
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <div class="form-group @error('email') is-invalid @enderror">
                                            <label class="info-title" for="email">Email <span> </span></label>
                                            <input type="email" name="email" class="form-control"
                                                value="{{ auth()->user()->email }}">
                                            @error('email')
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <div class="form-group @error('photo') is-invalid @enderror">
                                            <label class="info-title" for="photo">Avatar <span> </span></label>
                                            <input type="file" id='photo' name="photo" class="form-control">
                                            @error('photo')
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                            <img class="card-img-top" id='showAvatar'
                                                style="margin-top: 5px;border-radius: 50%"
                                                src="{{ auth()->user()->profile_photo_url }}"
                                                height="120" width="120">
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-danger">Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="text-center">
                                        Update Password
                                    </h3>
                                </div>
                                <div class="panel-body">
                                    <form method='post' action="{{ route('user.password.update') }}">
                                        @csrf
                                        @method('put')
                                        <div class="form-group @error('current_password') invalid @enderror">
                                            <h5>Current Password</h5>
                                            <div class="controls">
                                                <input type="password" name="current_password" id="current_password"
                                                    class="form-control">
                                            </div>
                                            @error('current_password', 'updatePassword')
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <div class="form-group @error('password') invalid @enderror">
                                            <h5>New Password</h5>
                                            <div class="controls">
                                                <input type="password" name="password" id="password" class="form-control">
                                            </div>
                                            @error('password', 'updatePassword')
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <div class="form-group @error('password_confirmation') invalid @enderror">
                                            <h5>Password Confirm</h5>
                                            <div class="controls">
                                                <input type="password" name="password_confirmation"
                                                    id="password_confirmation" class="form-control">
                                            </div>
                                            @error('password_confirmation', 'updatePassword')
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="text-xs-right">
                                            <button type="submit"
                                                class="btn btn-rounded btn-primary mb-5 pull-right">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- // end col md 10 -->
            </div> <!-- // end row -->
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $('#photo').change(function(e) {
            let render = new FileReader();
            render.onload = function(e) {
                $('#showAvatar').attr('src', e.target.result);
            }
            render.readAsDataURL((e.target.files[0]))
        })
    </script>
@endpush
