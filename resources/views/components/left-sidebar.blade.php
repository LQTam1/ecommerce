@props(['specialDeals', 'specialOffer', 'hotDeals'])
<div class="col-xs-12 col-sm-12 col-md-3 sidebar">
    <x-sidebar.category />

    <x-sidebar.hot-deals />

    <x-sidebar.special-offer :specialOffer="$specialOffer" />

    <x-product-tags />

    <x-sidebar.special-deals :specialDeals="$specialDeals" />

    <x-sidebar.newsletter />

    <x-sidebar.testimonials />

    <div class="home-banner">
        <img src="{{ asset('assets/images/banners/LHS-banner.jpg') }}" alt="Image" width='100%' height='100%'>
    </div>
</div>
