<header class="header-style-1">

    <!-- ============================================== TOP MENU ============================================== -->
    <div class="top-bar animate-dropdown">
        <div class="container">
            <div class="header-top-inner">
                <div class="cnt-account">
                    <ul class="list-unstyled">
                        <li><a href="{{ route('wishlist.index') }}"><i class="icon fa fa-heart"></i>Wishlist</a></li>
                        <li><a href="{{ route('carts.index') }}"><i class="icon fa fa-shopping-cart"></i>My Cart</a>
                        </li>
                        <li><a href='#' type='button' data-toggle="modal" data-target="#orderTrackingModal">
                                <i class="icon fa fa-truck"></i>Order Tracking</a></li>
                        @auth
                            <li><a href="{{ route('user.checkout.view') }}">Checkout</a>
                            </li>
                            <li class="dropdown dropdown-small"> <a href="#" class="dropdown-toggle" data-hover="dropdown"
                                    data-toggle="dropdown"><span class="value">My Account </span><b
                                        class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    @guest
                                        <li><a href="{{ route('login') }}">Login</a></li>
                                    @endguest
                                    <li><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
                                    <li>
                                        <form class='btn-block' method="POST" action="{{ route('logout') }}"
                                            id="user-logout">
                                            @csrf
                                            <a class=" btn-sm btn-block" href="{{ route('logout') }}"
                                                onclick="event.preventDefault();document.querySelector('#user-logout').submit()">{{ __('Logout') }}
                                            </a>
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @else
                            <li><a href="{{ route('login') }}"><i class="icon fa fa-lock"></i>Login</a></li>
                        @endauth
                    </ul>
                </div>
                <!-- /.cnt-account -->

                <div class="cnt-block">
                    <ul class="list-unstyled list-inline">
                        <li class="dropdown dropdown-small"> <a href="#" class="dropdown-toggle" data-hover="dropdown"
                                data-toggle="dropdown"><span class="value">USD </span><b
                                    class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">USD</a></li>
                                <li><a href="#">INR</a></li>
                                <li><a href="#">GBP</a></li>
                            </ul>
                        </li>
                        <li class="dropdown dropdown-small">
                            <a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown"><span
                                    class="value">{{ LaravelLocalization::getCurrentLocaleNative() }}
                                </span><b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                @foreach (LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                    <li>
                                        <a rel="alternate" hreflang="{{ $localeCode }}"
                                            href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                            {{ $properties['native'] }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>

                    </ul>
                    <!-- /.list-unstyled -->
                </div>
                <!-- /.cnt-cart -->
                <div class="clearfix"></div>
            </div>
            <!-- /.header-top-inner -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.header-top -->
    <!-- ============================================== TOP MENU : END ============================================== -->
    <div class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-3 logo-holder">
                    <!-- ============================================================= LOGO ============================================================= -->
                    <div class="logo"> <a href="/"> <img src="{{ asset($siteSetting->logo) }}" alt="logo">
                        </a>
                    </div>
                    <!-- /.logo -->
                    <!-- ============================================================= LOGO : END ============================================================= -->
                </div>
                <!-- /.logo-holder -->

                <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder">
                    <!-- /.contact-row -->
                    <!-- ============================================================= SEARCH AREA ============================================================= -->
                    <div class="search-area">
                        <form method="get" action="{{ route('products.search') }}">
                            <div class="control-group">
                                <ul class="categories-filter animate-dropdown">
                                    <input type="hidden" name="menu_item_id" id="menu_item_id">
                                    <li class="dropdown">
                                        <a href="#" class="link-control">{{ str_split('All Category', 12)[0] }}</a>
                                        <ul class="dropdown-menu" role="menu">
                                            @foreach ($mainCategories as $category)
                                                <li role="presentation" class='p-0 mb-2 text-xl'
                                                    data-id="{{ $category['id'] }}"
                                                    data-slug="{{ $category['slug'] }}">-{{ $category['label'] }}
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                </ul>
                                <div class='hidden' id='products_search_dropdown'
                                    data-href="{{ route('products.search-dropdown') }}"></div>
                                <input class="search-field" onfocus="searchResultShow()" onblur="searchResultHide()"
                                    name='keyword' autocomplete="on" placeholder="Search here..." />
                                <button type="submit" class="search-button"></button>
                            </div>
                        </form>
                        <div id="productSearchDropdown"></div>
                    </div>
                    <!-- /.search-area -->
                    <!-- ============================================================= SEARCH AREA : END ============================================================= -->
                </div>
                <!-- /.top-search-holder -->

                <div class="col-xs-12 col-sm-12 col-md-2 animate-dropdown top-cart-row">
                    <!-- ============================================================= SHOPPING CART DROPDOWN ============================================================= -->

                    <div class="dropdown dropdown-cart"> <a href="#" class="dropdown-toggle lnk-cart"
                            data-toggle="dropdown">
                            <div class="items-cart-inner">
                                <div class="basket"> <i class="glyphicon glyphicon-shopping-cart"></i> </div>
                                <div class="basket-item-count"><span class="count" id="cartQty">0</span></div>
                                <div class="total-price-basket"> <span class="lbl">cart -</span> <span
                                        class="total-price"> <span class="sign"></span><span
                                            class="value"></span> </span> </div>
                            </div>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <div id="miniCart"></div>

                                <!-- /.cart-item -->
                                <div class="clearfix"></div>
                                <div class="clearfix cart-total">
                                    @auth
                                        <div class="pull-right"> <span class="text">Sub Total :</span><span
                                                class='price' id="cartSubTotal">$0</span> </div>
                                        <div class="clearfix"></div>
                                        <a href="{{ route('user.checkout.view') }}"
                                            class="btn btn-upper btn-primary btn-block m-t-20">Checkout</a>
                                    @else
                                        <a href="{{ route('login') }}">Login to see your cart.</a>
                                    @endauth
                                </div>
                                <!-- /.cart-total-->

                            </li>
                        </ul>
                        <!-- /.dropdown-menu-->
                    </div>
                    <!-- /.dropdown-cart -->

                    <!-- ============================================================= SHOPPING CART DROPDOWN : END============================================================= -->
                </div>
                <!-- /.top-cart-row -->
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->

    </div>
    <!-- /.main-header -->

    <!-- ============================================== NAVBAR ============================================== -->
    <div class="header-nav animate-dropdown">
        <div class="container">
            <div class="yamm navbar navbar-default" role="navigation">
                <div class="navbar-header">
                    <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse"
                        class="navbar-toggle collapsed" type="button">
                        <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span
                            class="icon-bar"></span> <span class="icon-bar"></span> </button>
                </div>
                <div class="nav-bg-class">
                    <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
                        <div class="nav-outer">
                            <ul class="nav navbar-nav">
                                @foreach ($mainCategories as $category)
                                    @if ($category['link'] === '/')
                                        <li class="active dropdown yamm-fw"> <a href="/" data-hover="dropdown"
                                                class="dropdown-toggle" data-toggle="dropdown">Home</a> </li>
                                    @else
                                        <li class="dropdown yamm mega-menu"> <a
                                                href="{{ LaravelLocalization::localizeURL(route('product.category', $category['slug'])) }}"
                                                data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown">
                                                {{ $category['label'] }}
                                            </a>
                                            <ul class="dropdown-menu container">
                                                <li>
                                                    <div class="yamm-content ">
                                                        <div class="row">
                                                            @foreach ($category['child'] as $categoryChild)
                                                                <div class="col-xs-12 col-sm-6 col-md-2 col-menu">
                                                                    <h2 class="title">
                                                                        {{ $categoryChild['label'] }}
                                                                    </h2>
                                                                    <ul class="links">
                                                                        @foreach ($categoryChild['child'] as $subChildren)
                                                                            <li><a
                                                                                    href="{{ LaravelLocalization::localizeURL(route('product.category', $subChildren['slug'])) }}">
                                                                                    {{ $subChildren['label'] }}
                                                                                </a>
                                                                            </li>
                                                                        @endforeach
                                                                    </ul>
                                                                </div>
                                                            @endforeach

                                                            <div
                                                                class="col-xs-12 col-sm-6 col-md-4 col-menu banner-image">
                                                                <img class="img-responsive"
                                                                    src="{{ asset('assets/images/banners/top-menu-banner.jpg') }}"
                                                                    alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif
                                @endforeach
                                <li class="dropdown  navbar-right special-menu"> <a
                                        href="{{ route('shop') }}">Shop</a> </li>
                                        <li class="dropdown  navbar-right special-menu"> <a
                                        href="{{ route('blog.index') }}">Blog</a> </li>
                                <li class="dropdown  navbar-right special-menu"> <a href="#">Todays offer</a> </li>
                            </ul>
                            <!-- /.navbar-nav -->
                            <div class="clearfix"></div>
                        </div>
                        <!-- /.nav-outer -->
                    </div>
                    <!-- /.navbar-collapse -->

                </div>
                <!-- /.nav-bg-class -->
            </div>
            <!-- /.navbar-default -->
        </div>
        <!-- /.container-class -->

    </div>
    <!-- /.header-nav -->
    <!-- ============================================== NAVBAR : END ============================================== -->

</header>
