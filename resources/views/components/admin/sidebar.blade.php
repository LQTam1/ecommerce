@php
$route = request()->route();
@endphp
<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">

        <div class="user-profile">
            <div class="ulogo">
                <a href="{{ route('admin.dashboard') }}">
                    <!-- logo for regular state and mobile devices -->
                    <div class="d-flex align-items-center justify-content-center">
                        <img src="{{ asset('backend/assets/images/logo-dark.png') }}" alt="">
                        <h3><b>Sunny</b> Admin</h3>
                    </div>
                </a>
            </div>
        </div>

        <!-- sidebar menu-->
        <ul class="sidebar-menu" data-widget="tree">

            <li class="{{ $route->named('admin.dashboard') ? 'active' : '' }}">
                <a href="{{ route('admin.dashboard') }}">
                    <i data-feather="pie-chart"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <li class="treeview {{ $route->named('admin.roles*') ? 'active' : '' }}">
                <a href="">
                    <i data-feather="message-circle"></i>
                    <span>Role Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.roles.index') }}"><i class="ti-more"></i>All Role</a></li>
                </ul>
            </li>

            <li class="treeview {{ $route->named('admin.permissions*') ? 'active' : '' }}">
                <a href="">
                    <i data-feather="message-circle"></i>
                    <span>Permission Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.permissions.index') }}"><i class="ti-more"></i>All
                            Permission</a></li>
                </ul>
            </li>

            <li class="treeview {{ $route->named('admin.brands*') ? 'active' : '' }}">
                <a href="">
                    <i data-feather="message-circle"></i>
                    <span>Brand Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.brands.index') }}"><i class="ti-more"></i>All Brand</a></li>
                </ul>
            </li>

            <li class="treeview {{ $route->named('admin.categories*') ? 'active' : '' }}">
                <a href="">
                    <i data-feather="mail"></i> <span>Category Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.categories.index') }}"><i class="ti-more"></i>All
                            Category</a>
                    </li>
                </ul>
            </li>

            <li class="{{ $route->named('admin.menu*') ? 'active' : '' }}">
                <a href="{{ route('admin.menu') }}">
                    <i data-feather="pie-chart"></i>
                    <span>Menu</span>
                </a>
            </li>

            @can('update category_products_tab_home')
                <li class="{{ $route->named('admin.product.category.tabs') ? 'active' : '' }}">
                    <a href="{{ route('admin.product.category.tabs') }}">
                        <i data-feather="pie-chart"></i>
                        <span>Product Category Tabs</span>
                    </a>
                </li>
            @endcan

            <li
                class="treeview  {{ $route->named('admin.products.index') || $route->named('admin.products.create') || $route->named('admin.products.edit') ? 'active' : '' }}">
                <a href="">
                    <i data-feather="file"></i>
                    <span>Product Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.products.index') }}"><i class="ti-more"></i>All Product</a>
                    </li>
                </ul>
            </li>

            <li class="treeview  {{ $route->named('admin.products.stock.list') ? 'active' : '' }}">
                <a href="">
                    <i data-feather="file"></i>
                    <span>Stock Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('admin.products.stock.list') }}"><i class="ti-more"></i>Product
                            Stock</a>
                    </li>
                </ul>
            </li>

            <li class="treeview  {{ $route->named('admin.orders*') ? 'active' : '' }}">
                <a href="">
                    <i data-feather="file"></i>
                    <span>Order Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('admin.orders.index') }}"><i class="ti-more"></i>All Order</a>
                    </li>
                </ul>
            </li>

            <li class="treeview  {{ $route->named('admin.reports*') ? 'active' : '' }}">
                <a href="">
                    <i data-feather="file"></i>
                    <span>Reports Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('admin.reports.index') }}"><i class="ti-more"></i>All Reports</a>
                    </li>
                </ul>
            </li>

            <li class="treeview  {{ $route->named('admin.users*') ? 'active' : '' }}">
                <a href="">
                    <i data-feather="file"></i>
                    <span>User Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('admin.users.index') }}"><i class="ti-more"></i>All User</a>
                    </li>
                </ul>
            </li>

            <li class="treeview  {{ $route->named('admin.admins*') ? 'active' : '' }}">
                <a href="">
                    <i data-feather="file"></i>
                    <span>Admin Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('admin.admins.index') }}"><i class="ti-more"></i>All Admin</a>
                    </li>
                </ul>
            </li>

            <li class="treeview {{ $route->named('admin.sliders*') ? 'active' : '' }}">
                <a href="">
                    <i data-feather="file"></i>
                    <span>Slider Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.sliders.index') }}"><i class="ti-more"></i>All Slider</a>
                    </li>
                </ul>
            </li>

            <li class="treeview {{ $route->named('admin.coupons*') ? 'active' : '' }}">
                <a href="">
                    <i data-feather="file"></i>
                    <span>Coupon Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.coupons.index') }}"><i class="ti-more"></i>All Coupon</a>
                    </li>
                </ul>
            </li>

            <li class="treeview {{ $route->named('admin.countries*') ? 'active' : '' }}">
                <a href="">
                    <i data-feather="file"></i>
                    <span>Country Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.countries.index') }}"><i class="ti-more"></i>All Country</a>
                    </li>
                </ul>
            </li>

            <li class="treeview {{ $route->named('admin.blog*') ? 'active' : '' }}">
                <a href="">
                    <i data-feather="file"></i>
                    <span>Blog Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('admin.blog.postCategories.index') }}"><i class="ti-more"></i>All
                            Post Category</a>
                    </li>
                    <li>
                        <a href="{{ route('admin.blog.posts.index') }}"><i class="ti-more"></i>All Post</a>
                    </li>
                    @can('create posts')
                        <li>
                            <a href="{{ route('admin.blog.posts.create') }}"><i class="ti-more"></i>Create
                                Post</a>
                        </li>
                    @endcan
                </ul>
            </li>

            <li class="treeview {{ $route->named('admin.settings*') ? 'active' : '' }}">
                <a href="">
                    <i data-feather="file"></i>
                    <span>Setting Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('update siteSetting')
                        <li>
                            <a href="{{ route('admin.settings.siteSetting.edit', 1) }}"><i
                                    class="ti-more"></i>Site
                                Setting</a>
                        </li>
                    @endcan
                    @can('update seoSetting')
                        <li>
                            <a href="{{ route('admin.settings.seoSetting.edit', 1) }}"><i class="ti-more"></i>Seo
                                Setting</a>
                        </li>
                    @endcan
                </ul>
            </li>

            <li class="treeview {{ $route->named('admin.reviews*') ? 'active' : '' }}">
                <a href="">
                    <i data-feather="file"></i>
                    <span>Review Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('admin.reviews.index') }}"><i class="ti-more"></i>All Review</a>
                    </li>
                </ul>
            </li>
        </ul>
    </section>

    <div class="sidebar-footer">
        <!-- item-->
        <a href="javascript:void(0)" class="link" data-toggle="tooltip" title=""
            data-original-title="Settings" aria-describedby="tooltip92529"><i class="ti-settings"></i></a>
        <!-- item-->
        <a href="mailbox_inbox.html" class="link" data-toggle="tooltip" title=""
            data-original-title="Email"><i class="ti-email"></i></a>
        <!-- item-->
        <a href="javascript:void(0)" class="link" data-toggle="tooltip" title=""
            data-original-title="Logout"><i class="ti-lock"></i></a>
    </div>
</aside>
