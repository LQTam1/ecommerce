<script src="{{ asset('backend/assets/js/vendors.min.js') }}"></script>
<script src="{{ asset('backend/assets/icons/feather-icons/feather.min.js') }}"></script>
<script src="{{ asset('backend/assets/vendor_components/easypiechart/dist/jquery.easypiechart.js') }}"></script>
<script src="{{ asset('backend/assets/vendor_components/apexcharts-bundle/irregular-data-series.js') }}"></script>

<!-- Sunny Admin App -->
<script src="{{ asset('backend/assets/js/template.js') }}"></script>
<script src="{{ asset('backend/assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.js') }}"></script>
<script src="{{ asset('backend/assets/vendor_components/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('backend/assets/js/functions.js') }}"></script>

{{-- JQueryDatatable --}}
<script src="{{ asset('backend/assets/vendor_components/datatable/datatables.min.js') }}"></script>
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>
