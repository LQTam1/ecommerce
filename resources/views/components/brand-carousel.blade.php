<!-- ============================================== BRANDS CAROUSEL ============================================== -->
<div id="brands-carousel" class="logo-slider wow fadeInUp">
    <div class="logo-slider-inner">
        <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
            @foreach ($brands as $brand)
                <div class="item m-t-15"><a href="#" class="image"> <img
                            data-echo="{{ asset($brand->image) }}" width='100%' height='110'
                            src="{{ asset('assets/images/blank.gif') }}" alt=""> </a></div>
                <!--/.item-->
            @endforeach
        </div>
        <!-- /.owl-carousel #logo-slider -->
    </div>
    <!-- /.logo-slider-inner -->
</div>
<!-- /.logo-slider -->
<!-- ============================================== BRANDS CAROUSEL : END ============================================== -->
