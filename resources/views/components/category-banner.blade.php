<!-- == ==== SECTION – HERO === ====== -->
<div id="category" class="category-carousel hidden-xs">
    <div class="item">
        <div class="image"> <img src="{{ asset('assets/images/banners/cat-banner-1.jpg') }}" width='100%'
                alt="" class="img-responsive"> </div>
        <div class="container-fluid">
            <div class="caption vertical-top text-left">
                <div class="big-text"> Big Sale </div>
                <div class="excerpt hidden-sm hidden-md"> Save up to 49% off </div>
                <div class="excerpt-normal hidden-sm hidden-md"> Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit </div>
            </div>
            <!-- /.caption -->
        </div>
        <!-- /.container-fluid -->
    </div>
</div>
