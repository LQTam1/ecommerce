<div class="clearfix filters-container m-t-10">
    <form method="get" action="{{ request()->getRequestUri() }}">
        <div class="row">
            <div class="col col-sm-6 col-md-2">
                <div class="filter-tabs">
                    <ul id="filter-tabs" class="nav nav-tabs nav-tab-box nav-tab-fa-icon">
                        <li class="active"> <a data-toggle="tab" href="#grid-container"><i
                                    class="icon fa fa-th-large"></i>Grid</a> </li>
                        <li><a data-toggle="tab" href="#list-container"><i class="icon fa fa-th-list"></i>List</a></li>
                    </ul>
                </div>
                <!-- /.filter-tabs -->
            </div>
            <!-- /.col -->
            <div class="col col-sm-12 col-md-6">
                <div class="col col-sm-3 col-md-6 no-padding">
                    <div class="lbl-cnt"> <span class="lbl">Sort by</span>
                        <div class="fld inline">
                            <div class="dropdown dropdown-small dropdown-med dropdown-white inline">
                                {{-- onclick="this.form.submit()" --}}
                                <button data-toggle="dropdown" type="button"
                                    class="btn dropdown-toggle text-capitalize">
                                    {{ request()->sort ?? 'Price:Lowest first' }} <span class="caret"></span>
                                </button>
                                <input type='hidden' name='sort' value="name" />
                                <ul role="menu" class="dropdown-menu">
                                    <li role="presentation"><a
                                            href="{{ request()->fullUrlWithQuery(['sortKey' => 'selling_price', 'order' => 'asc']) }}">Price:Lowest
                                            first</a></li>
                                    <li role="presentation"><a
                                            href="{{ request()->fullUrlWithQuery(['sortKey' => 'selling_price', 'order' => 'desc']) }}">Price:HIghest
                                            first</a></li>
                                    <li role="presentation"><a
                                            href="{{ request()->fullUrlWithQuery(['sortKey' => 'name', 'order' => 'asc']) }}">Product
                                            Name:A to Z</a></li>
                                    <li role="presentation"><a
                                            href="{{ request()->fullUrlWithQuery(['sortKey' => 'name', 'order' => 'desc']) }}">Product
                                            Name:Z to A</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- /.fld -->
                    </div>
                    <!-- /.lbl-cnt -->
                </div>
                <!-- /.col -->
                <div class="col col-sm-3 col-md-6 no-padding">
                    <div class="lbl-cnt"> <span class="lbl">Show</span>
                        <div class="fld inline">
                            <div class="dropdown dropdown-small dropdown-med dropdown-white inline">
                                <button data-toggle="dropdown" type="button" class="btn dropdown-toggle">
                                    {{ request()->length ?? 6 }}
                                    <span class="caret"></span> </button>
                                <input type='hidden' name='length' value="3" />
                                <ul role="menu" class="dropdown-menu">
                                    <li role="presentation"><a
                                            href="{{ request()->fullUrlWithQuery(['length' => '6']) }}">1</a></li>
                                    <li role="presentation"><a
                                            href="{{ request()->fullUrlWithQuery(['length' => '12']) }}">12</a></li>
                                    <li role="presentation"><a
                                            href="{{ request()->fullUrlWithQuery(['length' => '18']) }}">18</a></li>
                                    <li role="presentation"><a
                                            href="{{ request()->fullUrlWithQuery(['length' => '24']) }}">24</a></li>
                                    <li role="presentation"><a
                                            href="{{ request()->fullUrlWithQuery(['length' => '30']) }}">30</a></li>
                                    <li role="presentation"><a
                                            href="{{ request()->fullUrlWithQuery(['length' => '36']) }}">36</a></li>
                                    <li role="presentation"><a
                                            href="{{ request()->fullUrlWithQuery(['length' => '42']) }}">42</a></li>
                                    <li role="presentation"><a
                                            href="{{ request()->fullUrlWithQuery(['length' => '48']) }}">48</a></li>
                                    <li role="presentation"><a
                                            href="{{ request()->fullUrlWithQuery(['length' => '54']) }}">54</a></li>
                                    <li role="presentation"><a
                                            href="{{ request()->fullUrlWithQuery(['length' => '60']) }}">60</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- /.fld -->
                    </div>
                    <!-- /.lbl-cnt -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.col -->
            <div class="col col-sm-6 col-md-4 text-right">
                {{ $products->links() }}
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </form>
</div>
