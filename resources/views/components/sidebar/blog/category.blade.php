@props(['postCategories'])
<!-- ======== ====CATEGORY======= === -->
<div class="sidebar-widget outer-bottom-xs wow fadeInUp">
    <h3 class="section-title">Blog Category</h3>
    <div class="sidebar-widget-body m-t-10">
        <div class="accordion">
            @foreach ($postCategories as $category)
                <ul class="list-group">
                    <a href="{{ route('blog.post.category', $category->id) }}">
                        <li class="list-group-item">{{ $category->name }}</li>
                    </a>
                </ul>
            @endforeach
        </div><!-- /.accordion -->
    </div><!-- /.sidebar-widget-body -->
</div><!-- /.sidebar-widget -->
<!-- ===== ======== CATEGORY : END ==== = -->
