<!-- ================================== TOP NAVIGATION ================================== -->
<div class="side-menu animate-dropdown outer-bottom-xs">
    <div class="head"><i class="icon fa fa-align-justify fa-fw"></i> Categories</div>
    <nav class="yamm megamenu-horizontal">
        <ul class="nav">
            @foreach ($mainCategories as $category)
                @if ($category['link'] !== '/')
                    <li class="dropdown menu-item"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                class="icon {{ $category['class'] }}" aria-hidden="true"></i>
                            {{ $category['label'] }}
                        </a>
                        <ul class="dropdown-menu mega-menu">
                            <li class="yamm-content">
                                <div class="row">
                                    @foreach ($category['child'] as $categoryChild)
                                        <div class="col-sm-12 col-md-3">
                                            <h2 class="title">
                                                {{ $categoryChild['label'] }}
                                            </h2>
                                            <ul class="links list-unstyled">
                                                @foreach ($categoryChild['child'] as $subChild)
                                                    <li><a
                                                            href="{{ LaravelLocalization::localizeURL(route('product.category', $subChild['slug'])) }} ">
                                                            {{ $subChild['label'] }}
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endforeach
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </li>
                            <!-- /.yamm-content -->
                        </ul>
                        <!-- /.dropdown-menu -->
                    </li>
                @endif
            @endforeach
            <!-- /.menu-item -->
        </ul>
        <!-- /.nav -->
    </nav>
    <!-- /.megamenu-horizontal -->
</div>
<!-- /.side-menu -->
<!-- ================================== TOP NAVIGATION : END ================================== -->
