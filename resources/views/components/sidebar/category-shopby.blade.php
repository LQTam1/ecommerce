 <!-- = ==== TOP NAVIGATION : END === ===== -->
 <div class="sidebar-module-container">
     <div class="sidebar-filter">
         @if (!empty($_GET['filters']))
             @php
                 $filters = $_GET['filters'];
             @endphp
         @endif
         {{-- <form method="get" action="{{ route(Route::currentRouteName(), request()->tag) }}"> --}}
             
         <form method="get" action="{{ route('shop', request()->query()) }}">

             <!-- ============================================== SIDEBAR CATEGORY ============================================== -->
             <div class="sidebar-widget wow fadeInUp">
                 <h3 class="section-title">shop by</h3>
                 <div class="widget-header">
                     <h4 class="widget-title">Category</h4>
                 </div>
                 <div class="sidebar-widget-body">
                     <div class="accordion">
                         @foreach ($mainCategories as $category)
                             <div class="accordion-group">
                                 <div class="accordion-heading">
                                     <label class="form-check-label">
                                         <input type="checkbox" class="form-check-input" name="filters[category][]"
                                             value="{{ $category['id'] }}" @if (!empty($filters['category']) && in_array($category['id'], $filters['category'])) checked @endif>
                                         {{ $category['label'] }}
                                     </label>
                                 </div>
                                 <!-- /.accordion-body -->
                             </div>
                             <!-- /.accordion-group -->
                         @endforeach
                     </div>
                     <!-- /.accordion -->
                 </div>
                 <!-- /.sidebar-widget-body -->
             </div>
             <!-- /.sidebar-widget -->
             <!-- ============================================== SIDEBAR CATEGORY : END ============================================== -->
             <!-- ============================================== PRICE SILDER============================================== -->
             <div class="sidebar-widget wow fadeInUp">
                 <div class="widget-header">
                     <h4 class="widget-title">Price Slider</h4>
                 </div>
                 <div class="sidebar-widget-body m-t-10">
                     @php
                         $minPrice = \App\Models\Product::min('selling_price');
                         $maxPrice = \App\Models\Product::max('selling_price');
                         $value = '100,600';
                         if(isset($filters['price'])){
                             $value = $filters['price'];
                         }
                     @endphp

                     <div class="price-range-holder"> <span class="min-max"> <span
                                 class="pull-left" id='min-price' data-price="{{$minPrice}}">${{ $minPrice }}</span> <span
                                 class="pull-right" id='max-price' data-price="{{$maxPrice}}">${{ $maxPrice }}</span>
                         </span>
                         <input type="text" id="amount"
                             style="border:0; color:#666666; font-weight:bold;text-align:center;">
                         <input type="text" class="price-slider" name="filters[price]" value="{{$value}}" data="value: '{{$value}}'" style="display: none;">
                     </div>
                     <!-- /.price-range-holder -->
                 </div>
                 <!-- /.sidebar-widget-body -->
             </div>
             <!-- /.sidebar-widget -->
             <!-- ============================================== PRICE SILDER : END ============================================== -->
             <!-- ============================================== MANUFACTURES============================================== -->
             <div class="sidebar-widget wow fadeInUp">
                 <div class="widget-header">
                     <h4 class="widget-title">Manufactures</h4>
                 </div>
                 <div class="sidebar-widget-body">
                     <div class="accordion">
                         @foreach ($brands as $brand)
                             <div class="accordion-group">
                                 <div class="accordion-heading">
                                     <label class="form-check-label">
                                         <input type="checkbox" class="form-check-input" name="filters[brand][]"
                                             value="{{ $brand->id }}" @if (!empty($filters['brand']) && in_array($brand->id, $filters['brand'])) checked @endif>
                                         {{ $brand->name }}
                                     </label>
                                 </div>
                                 <!-- /.accordion-body -->
                             </div>
                             <!-- /.accordion-group -->
                         @endforeach
                     </div>
                     <!-- /.accordion -->
                 </div>
                 <!-- /.sidebar-widget-body -->
             </div>
             <!-- /.sidebar-widget -->
             <!-- ============================================== MANUFACTURES: END ============================================== -->
             <!-- ============================================== COLOR============================================== -->
             <div class="sidebar-widget wow fadeInUp">
                 <div class="widget-header">
                     <h4 class="widget-title">Colors</h4>
                 </div>
                 <div class="sidebar-widget-body">
                     <div class="accordion">
                         @php
                             $colors = App\Models\Product::active()->latest()->take(6)->pluck('color')->unique()
                         @endphp

                         @foreach ($colors as $color)
                             <div class="accordion-group">
                                 <div class="accordion-heading">
                                     <label class="form-check-label">
                                         <input type="checkbox" class="form-check-input" name="filters[color][]"
                                             value="{{ $color }}" @if (!empty($filters['color']) && in_array($color, $filters['color'])) checked @endif>
                                         {{ $color }}
                                     </label>
                                 </div>
                                 <!-- /.accordion-body -->
                             </div>
                             <!-- /.accordion-group -->
                         @endforeach
                     </div>
                     <!-- /.accordion -->
                     <button type='submit' class="lnk btn btn-primary">Show Now</button>
                 </div>
                 <!-- /.sidebar-widget-body -->
             </div>
             <!-- /.sidebar-widget -->
             <!-- ============================================== COLOR: END ============================================== -->
             <button type='submit' class='hidden btn btn-primary'>Submit</button>
         </form>

         <!-- == ======= COMPARE==== ==== -->
         <div class="sidebar-widget wow fadeInUp outer-top-vs">
             <h3 class="section-title">Compare products</h3>
             <div class="sidebar-widget-body">
                 <div class="compare-report">
                     <p>You have no <span>item(s)</span> to compare</p>
                 </div>
                 <!-- /.compare-report -->
             </div>
             <!-- /.sidebar-widget-body -->
         </div>
         <!-- /.sidebar-widget -->
         <!-- ============================================== COMPARE: END ============================================== -->
         <!-- == ====== PRODUCT TAGS ==== ======= -->
         <x-product-tags />
         <!-- /.sidebar-widget -->
         <!-- == ====== END PRODUCT TAGS ==== ======= -->
         <x-sidebar.testimonials />

         <div class="home-banner"> <img src="{{ asset('assets/images/banners/LHS-banner.jpg') }}" width='100%'
                 alt="Image">
         </div>
     </div>
     <!-- /.sidebar-filter -->
 </div>
 <!-- /.sidebar-module-container -->
