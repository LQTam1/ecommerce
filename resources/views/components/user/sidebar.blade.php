<div class="col-md-2">
    <div style='margin:15px 0'>
        <img class="card-img-top" style="border-radius: 50%" src="{{ auth()->user()->profile_photo_url }}"
            height="100%" width="100%">
    </div>

    <ul class="list-group list-group-flush">
        <a href="{{ route('user.dashboard') }}" class="btn btn-primary btn-sm btn-block">Home</a>

        <a href="{{ route('user.profile.show') }}" class="btn btn-primary btn-sm btn-block">Profile Update</a>

        <a href="{{ route('user.orders.index') }}" class="btn btn-primary btn-sm btn-block">My Orders</a>

        <a href="#" class="btn btn-primary btn-sm btn-block">Cancel Orders</a>

        <form class='btn-block' method="POST" action="{{ route('logout') }}" id="user-logout">
            @csrf
            <a class="btn btn-danger btn-sm btn-block" href="{{ route('logout') }}"
                onclick="event.preventDefault();document.querySelector('#user-logout').submit()">{{ __('Logout') }}
            </a>
        </form>
    </ul>
</div>
