@extends('layouts.master')
@section('title', 'Blog Post Category List')
@section('content')
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="#">Home</a></li>
                    <li class='w-40'><a href="{{ route('blog.index') }}">All Blog Post</a></li>
                    <li class='active'>Blog Post Category</li>
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->
    <div class="body-content">
        <div class="container">
            <div class="row">
                <div class="blog-page">
                    <div class="col-md-9">
                        <h1 class='text-5xl mb-5'>
                            Category: {{ $post_category->name }}
                        </h1>
                        @foreach ($posts as $blog)
                            <div class="blog-post  wow fadeInUp">
                                <a href="{{ route('blog.details', ['slug' => $blog->slug, 'id' => $blog->id]) }}"><img
                                        class="img-responsive" src="{{ asset($blog->image) }}" alt=""></a>
                                <h1><a href="{{ route('blog.details', ['slug' => $blog->slug, 'id' => $blog->id]) }}">{{ $blog->title }}
                                    </a></h1>
                                <span class="date-time">
                                    {{ $blog->created_at->diffForHumans() }}</span>
                                <p>{!! Str::limit($blog->details, 200) !!} </p>
                                <a href="{{ route('blog.details', ['slug' => $blog->slug, 'id' => $blog->id]) }}"
                                    class="btn btn-upper btn-primary read-more">read more</a>
                            </div>
                        @endforeach
                        <div class="clearfix blog-pagination filters-container  wow fadeInUp"
                            style="padding:0px; background:none; box-shadow:none; margin-top:15px; border:none">
                            <div class="text-right">
                                <div class="pagination-container">
                                    <ul class="list-inline list-unstyled">
                                        <li class="prev"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                                        <li><a href="#">1</a></li>
                                        <li class="active"><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li class="next"><a href="#"><i class="fa fa-angle-right"></i></a></li>
                                    </ul><!-- /.list-inline -->
                                </div><!-- /.pagination-container -->
                            </div><!-- /.text-right -->
                        </div><!-- /.filters-container -->
                    </div>
                    <div class="col-md-3 sidebar">
                        <div class="sidebar-module-container">
                            <div class="search-area outer-bottom-small">
                                <form>
                                    <div class="control-group">
                                        <input placeholder="Type to search" class="search-field">
                                        <a href="#" class="search-button"></a>
                                    </div>
                                </form>
                            </div>
                            <div class="home-banner outer-top-n outer-bottom-xs">
                                <img src="{{ asset('assets/images/banners/LHS-banner.jpg') }} " width='100%' alt="Image">
                            </div>
                            
                            <x-sidebar.blog.category :postCategories='$postCategories' />

                            <x-product-tags />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
