@extends('layouts.master')
@section('title', 'Checkout')
@section('content')
    <div id='app'>
        <x-breadcrumb>
            <li><a href="#">Home</a></li>
            <li class='active'>Checkout</li>
        </x-breadcrumb>
        <div class="body-content">
            <div class="container">
                <div class="checkout-box ">
                    <div class="row">
                        <div class="col-md-8">
                            <validation-observer v-slot="{passes}">
                                <form @submit.prevent="passes(nextStep)" class="form">
                                    <div class="panel-group checkout-steps" id="accordion">
                                        <div class="panel panel-default checkout-step-02">
                                            <div class="panel-heading">
                                                <h4 class="unicase-checkout-title">
                                                    <a class="collapsed" href="#collapseTwo">
                                                        <span>@{{ activeStep + 1 }}</span>@{{ currentStep . title }}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse ">
                                                <div class="panel-body" v-if='activeStep === 0 '>
                                                    <div class="form-group">
                                                        <div class="row align-items-center">
                                                            <div class="col-md-6 mb-3">
                                                                <validation-provider name="First Name"
                                                                    rules="required|max:30" v-slot="{ errors }" persist>
                                                                    <label for="f_name">First Name:</label>
                                                                    <input type="text" name="fname"
                                                                        v-model='billing_info.fname' class="form-control"
                                                                        id="f_name" placeholder="First Name" />
                                                                    <div class="text-danger">@{{ errors[0] }}</div>
                                                                </validation-provider>
                                                                <!-- Input type text -->
                                                            </div>
                                                            <div class="col-md-6 mb-3">
                                                                <validation-provider name="Last Name"
                                                                    rules="required|max:30" v-slot="{ errors }" persist>
                                                                    <label for="l_name">Last Name:</label>
                                                                    <input type=" text" name="lname"
                                                                        v-model='billing_info.lname' class="form-control"
                                                                        id="l_name" placeholder="Last Name" />
                                                                    <div class="text-danger">@{{ errors[0] }}</div>
                                                                </validation-provider>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row align-items-center">
                                                            <div class="col-md-6 mb-3">
                                                                <validation-provider name="Address Line 1" rules="required"
                                                                    v-slot="{ errors }" persist>
                                                                    <label for="line1">Address Line 1:</label>
                                                                    <textarea class="form-control" rows="2" name="line1"
                                                                        v-model='billing_info.line1'
                                                                        id="address_line_1"></textarea>
                                                                    <small class="text-muted form-text">Street address,
                                                                        P.O
                                                                        box,
                                                                        company name, c/o</small>
                                                                    <div class="text-danger">@{{ errors[0] }}</div>
                                                                </validation-provider>
                                                            </div>
                                                            <div class="col-md-6 mb-3">
                                                                <validation-provider name="Address Line 2" rules="required"
                                                                    v-slot="{ errors }" persist>
                                                                    <!-- textarea -->
                                                                    <label for="address_line_2">Address Line 2:</label>
                                                                    <textarea class="form-control" rows="2" name="line2"
                                                                        v-model='billing_info.line2'
                                                                        id="address_line_2"></textarea>
                                                                    <small class="text-muted form-text">Apartment,
                                                                        suite,
                                                                        unit,
                                                                        building, floor, etc.</small>
                                                                    <div class="text-danger">@{{ errors[0] }}</div>
                                                                </validation-provider>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row align-items-center">
                                                            <div class="col-md-5 mb-3">
                                                                <validation-provider name="City" rules="required|max:30"
                                                                    v-slot="{ errors }" persist>
                                                                    <!-- Input type text -->
                                                                    <label for="input_id_20">City</label>
                                                                    <input type="text" class="form-control" name="city"
                                                                        v-model='billing_info.city' id="input_id_20"
                                                                        placeholder="City">
                                                                    <div class="text-danger">@{{ errors[0] }}</div>
                                                                </validation-provider>
                                                            </div>
                                                            <div class="col-md-4 mb-3">
                                                                <validation-provider name="State/Province/Region"
                                                                    rules="required|max:30" v-slot="{ errors }" persist>
                                                                    <!-- Input type text -->
                                                                    <label
                                                                        for="state_province_region1">State/Province/Region</label>
                                                                    <input type="text" class="form-control" name="state"
                                                                        v-model='billing_info.state'
                                                                        id="state_province_region1" placeholder="Province"
                                                                        required>
                                                                    <div class="text-danger">@{{ errors[0] }}</div>
                                                                </validation-provider>
                                                            </div>
                                                            <div class="col-md-3 mb-3">
                                                                <validation-provider name="Zip/Postal Code"
                                                                    rules="required|numeric|max:10" v-slot="{ errors }"
                                                                    persist>
                                                                    <!-- Input type text -->
                                                                    <label for="zip">Zip/Postal Code</label>
                                                                    <input type="number" class="form-control" name="zip"
                                                                        id="zip" v-model='billing_info.zip'
                                                                        placeholder="Zip" />
                                                                    <div class="text-danger">@{{ errors[0] }}</div>
                                                                </validation-provider>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row align-items-center">
                                                            <div class="col-md-6 mb-3">
                                                                <validation-provider name="Country" rules="required|max:30"
                                                                    v-slot="{ errors }" persist>
                                                                    <!-- select -->
                                                                    <label for="country">Country</label>
                                                                    <select class="custom-select form-control"
                                                                        name="country" v-model='billing_info.country'
                                                                        id="country">
                                                                        <option value="">---Select Country---</option>
                                                                        @foreach ($countries as $country)
                                                                            <option value="{{ $country->id }}">
                                                                                {{ $country->country_name }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                    <div class="text-danger">@{{ errors[0] }}</div>
                                                                </validation-provider>
                                                            </div>
                                                            <div class="col-md-6 mb-3">
                                                                <validation-provider name="Phone Number" rules="required"
                                                                    v-slot="{ errors }" persist>
                                                                    <!-- Input type text -->
                                                                    <label for="phone_number">Phone Number</label>
                                                                    <input type="text" class="form-control" name="phone"
                                                                        v-model='billing_info.phone' id="phone_number"
                                                                        required>
                                                                    <div class="text-danger">@{{ errors[0] }}</div>
                                                                </validation-provider>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-body" v-else-if='activeStep === 1 '>
                                                    <div class="form-group">
                                                        <div class="row align-items-center">
                                                            <div class="col-md-4 mb-3">
                                                                <validation-provider name="First Name"
                                                                    rules="required|max:30" v-slot="{ errors }" persist>
                                                                    <label for="f_name">First Name:</label>
                                                                    <input type="text" name="fname"
                                                                        v-model='shipping_info.fname' class="form-control"
                                                                        id="f_name" placeholder="First Name" />
                                                                    <div class="text-danger">@{{ errors[0] }}</div>
                                                                </validation-provider>
                                                                <!-- Input type text -->
                                                            </div>
                                                            <div class="col-md-4 mb-3">
                                                                <validation-provider name="Last Name"
                                                                    rules="required|max:30" v-slot="{ errors }" persist>
                                                                    <label for="l_name">Last Name:</label>
                                                                    <input type=" text" name="lname"
                                                                        v-model='shipping_info.lname' class="form-control"
                                                                        id="l_name" placeholder="Last Name" />
                                                                    <div class="text-danger">@{{ errors[0] }}</div>
                                                                </validation-provider>
                                                            </div>
                                                            <div class="col-md-4 mb-3">
                                                                <validation-provider name="Email" rules="email|max:191"
                                                                    v-slot="{ errors }" persist>
                                                                    <label for="f_name">Email (option):</label>
                                                                    <input type="email" name="email"
                                                                        v-model='shipping_info.email' class="form-control"
                                                                        id="f_name" placeholder="Email" />
                                                                    <small class="text-muted form-text">We'll send email
                                                                        using this email address. Otherwise, we'll using
                                                                        your email account.</small>
                                                                    <div class="text-danger">@{{ errors[0] }}</div>
                                                                </validation-provider>
                                                                <!-- Input type text -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row align-items-center">
                                                            <div class="col-md-6 mb-3">
                                                                <validation-provider name="Address Line 1" rules="required"
                                                                    v-slot="{ errors }" persist>
                                                                    <label for="line1">Address Line 1:</label>
                                                                    <textarea class="form-control" rows="2" name="line1"
                                                                        v-model='shipping_info.line1'
                                                                        id="address_line_1"></textarea>
                                                                    <small class="text-muted form-text">Street address,
                                                                        P.O
                                                                        box,
                                                                        company name, c/o</small>
                                                                    <div class="text-danger">@{{ errors[0] }}</div>
                                                                </validation-provider>
                                                            </div>
                                                            <div class="col-md-6 mb-3">
                                                                <validation-provider name="Address Line 2" rules="required"
                                                                    v-slot="{ errors }" persist>
                                                                    <!-- textarea -->
                                                                    <label for="address_line_2">Address Line 2:</label>
                                                                    <textarea class="form-control" rows="2" name="line2"
                                                                        v-model='shipping_info.line2'
                                                                        id="address_line_2"></textarea>
                                                                    <small class="text-muted form-text">Apartment,
                                                                        suite,
                                                                        unit,
                                                                        building, floor, etc.</small>
                                                                    <div class="text-danger">@{{ errors[0] }}</div>
                                                                </validation-provider>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row align-items-center">
                                                            <div class="col-md-5 mb-3">
                                                                <validation-provider name="City" rules="required|max:30"
                                                                    v-slot="{ errors }" persist>
                                                                    <!-- Input type text -->
                                                                    <label for="input_id_20">City</label>
                                                                    <input type="text" class="form-control" name="city"
                                                                        v-model='shipping_info.city' id="input_id_20"
                                                                        placeholder="City">
                                                                    <div class="text-danger">@{{ errors[0] }}</div>
                                                                </validation-provider>
                                                            </div>
                                                            <div class="col-md-4 mb-3">
                                                                <validation-provider name="State/Province/Region"
                                                                    rules="required|max:30" v-slot="{ errors }" persist>
                                                                    <!-- Input type text -->
                                                                    <label
                                                                        for="state_province_region1">State/Province/Region</label>
                                                                    <input type="text" class="form-control" name="state"
                                                                        v-model='shipping_info.state'
                                                                        id="state_province_region1" placeholder="Province"
                                                                        required>
                                                                    <div class="text-danger">@{{ errors[0] }}</div>
                                                                </validation-provider>
                                                            </div>
                                                            <div class="col-md-3 mb-3">
                                                                <validation-provider name="Zip/Postal Code"
                                                                    rules="required|numeric|max:10" v-slot="{ errors }"
                                                                    persist>
                                                                    <!-- Input type text -->
                                                                    <label for="zip">Zip/Postal Code</label>
                                                                    <input type="number" class="form-control" name="zip"
                                                                        id="zip" v-model='shipping_info.zip'
                                                                        placeholder="Zip" />
                                                                    <div class="text-danger">@{{ errors[0] }}</div>
                                                                </validation-provider>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row align-items-center">
                                                            <div class="col-md-6 mb-3">
                                                                <div class="form-group">
                                                                    <validation-provider name="Country"
                                                                        rules="required|max:30" v-slot="{ errors }" persist>
                                                                        <!-- select -->
                                                                        <label for="country">Country</label>
                                                                        <select class="custom-select form-control"
                                                                            name="country" v-model='shipping_info.country'
                                                                            id="country">
                                                                            <option value="">---Select Country---</option>
                                                                            @foreach ($countries as $country)
                                                                                <option value="{{ $country->id }}">
                                                                                    {{ $country->country_name }}
                                                                                </option>
                                                                            @endforeach
                                                                        </select>
                                                                        <div class="text-danger">@{{ errors[0] }}
                                                                        </div>
                                                                    </validation-provider>
                                                                </div>
                                                                <div class="form-group">
                                                                    <validation-provider name="Phone Number"
                                                                        rules="required" v-slot="{ errors }" persist>
                                                                        <!-- Input type text -->
                                                                        <label for="phone_number">Phone Number</label>
                                                                        <input type="text" class="form-control"
                                                                            name="phone" v-model='shipping_info.phone'
                                                                            id="phone_number" required>
                                                                        <div class="text-danger">@{{ errors[0] }}
                                                                        </div>
                                                                    </validation-provider>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 mb-3">
                                                                <!-- textarea -->
                                                                <label for="note">Note:</label>
                                                                <textarea class="form-control" rows="4" name="note"
                                                                    v-model='shipping_info.note' id="note"></textarea>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="panel-body" v-else>
                                                    <div class="row">
                                                        <div class="col-sm-12 col-md-12">
                                                            <div class="wrap-address-billing mb-10"
                                                                v-if="payment_method === 'stripe'">
                                                                <div class='form-group'>
                                                                    <validation-provider name="Card Name"
                                                                        rules="required|max:60" v-slot="{ errors }" persist>
                                                                        <label for="card_no">Card
                                                                            Name:</label>
                                                                        <input type="text" class='form-control'
                                                                            placeholder="Card Name"
                                                                            v-model='payment_info.card_name'>
                                                                        <div class="text-danger">
                                                                            @{{ errors[0] }}
                                                                        </div>
                                                                    </validation-provider>
                                                                </div>
                                                                <div class='form-group'>
                                                                    <validation-provider name="Card Number"
                                                                        rules="required|length:16|numeric"
                                                                        v-slot="{ errors }" persist>
                                                                        <label for="card_no">Card
                                                                            Number:</label>
                                                                        <input type="number" class='form-control'
                                                                            placeholder="4242424242424242"
                                                                            v-model='payment_info.card_no'>
                                                                        <div class="text-danger">
                                                                            @{{ errors[0] }}
                                                                        </div>
                                                                    </validation-provider>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                        <div class='form-group'>
                                                                            <validation-provider name="Expiry Month"
                                                                                rules="required|numeric|length:2"
                                                                                v-slot="{ errors }" persist>
                                                                                <label class='w-1/4' for="exp_month">Expiry
                                                                                    Month:</label>
                                                                                <input type="number" placeholder="MM"
                                                                                    v-model='payment_info.exp_month'>
                                                                                <div class="text-danger">
                                                                                    @{{ errors[0] }}
                                                                                </div>
                                                                            </validation-provider>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                        <div class='form-group'>
                                                                            <validation-provider name="Expiry Year"
                                                                                rules="required|numeric|length:2"
                                                                                v-slot="{ errors }" persist>
                                                                                <label class='w-1/4' for="exp_year">Expiry
                                                                                    Year:</label>
                                                                                <input type="number" placeholder="YY"
                                                                                    v-model='payment_info.exp_year'>
                                                                                <div class="text-danger">
                                                                                    @{{ errors[0] }}
                                                                                </div>
                                                                            </validation-provider>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                        <div class='form-group'>
                                                                            <validation-provider name="CVC"
                                                                                rules="required|numeric|length:3"
                                                                                v-slot="{ errors }" persist>
                                                                                <label class='w-1/4' for="cvc">CVC:</label>
                                                                                <input type="password" placeholder="CVC"
                                                                                    v-model='payment_info.cvc'>
                                                                                <div class="text-danger">
                                                                                    @{{ errors[0] }}
                                                                                </div>
                                                                            </validation-provider>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="">Stripe</label>
                                                                <input style='margin-top:0;margin-left:8px' type="radio"
                                                                    name="payment_method" v-model='payment_method'
                                                                    :checked="payment_method === 'stripe'" value="stripe">
                                                                <img src="{{ asset('assets/images/payments/4.png') }}">
                                                            </div> <!-- end col md 4 -->
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="">Card</label>
                                                                <input style='margin-top:0;margin-left:8px' type="radio"
                                                                    name="payment_method" v-model='payment_method'
                                                                    :checked="payment_method === 'card'" value="card">
                                                                <img src="{{ asset('assets/images/payments/3.png') }}">
                                                            </div> <!-- end col md 4 -->
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="">Cash</label>
                                                                <input style='margin-top:0;margin-left:8px' type="radio"
                                                                    name="payment_method" v-model='payment_method'
                                                                    :checked="payment_method === 'cash'" value="cash">
                                                                <img src="{{ asset('assets/images/payments/6.png') }}">
                                                            </div> <!-- end col md 4 -->
                                                        </div>
                                                    </div> <!-- // end row  -->
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.checkout-steps -->
                                    <div class='pull-right'>
                                        <button v-if="activeStep > 0"
                                            class='btn bg-gray-700 text-white hover:text-white hover:bg-gray-400'
                                            type="button" @click="activeStep--">Previous</button>
                                        <button class='btn btn-primary'
                                            :disabled="submitting">@{{ isLastStep ? 'Submit' : 'Next' }}</button>
                                    </div>
                                </form>
                            </validation-observer>
                            <div class='clear-both panel-group checkout-steps'>
                                <!-- checkout-step-01  -->
                                <div class="panel panel-default checkout-step-01">
                                    <!-- panel-heading -->
                                    <div class="panel-heading">
                                        <h4 class="unicase-checkout-title">
                                            <a class="___class_+?13___" href="#collapseOne">
                                                <span>1</span>Your Cart
                                            </a>
                                        </h4>
                                    </div>
                                    <!-- panel-heading -->
                                    <div id="collapseOne" class="panel-collapse ">
                                        <!-- panel-body  -->
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th class="cart-description item">Image</th>
                                                                    <th class="cart-product-name item">Product Name</th>
                                                                    <th class="cart-qty item">Quantity</th>
                                                                    <th class="cart-edit item">Price</th>
                                                                    <th class="cart-sub-total item">Subtotal</th>
                                                                </tr>
                                                            </thead><!-- /thead -->
                                                            <tbody>
                                                                <tr v-for="item in items" :key="item.id">
                                                                    <td class="cart-image">
                                                                        <a class="entry-thumbnail"
                                                                            :href="`/details/${item.associatedModel.slug}`">
                                                                            <img :src="item.attributes.image"
                                                                                alt="Product Image" width='120'
                                                                                height='120'>
                                                                        </a>
                                                                    </td>
                                                                    <td class="cart-product-name-info">
                                                                        <h4 class='cart-product-description'><a
                                                                                :href="`/details/${item.associatedModel.slug}`">@{{ item . name }}</a>
                                                                        </h4>
                                                                        <div class="row">
                                                                            <div class="col-sm-4">
                                                                                <div class="rating rateit-small"></div>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <div class="reviews">
                                                                                    (06 Reviews)
                                                                                </div>
                                                                            </div>
                                                                        </div><!-- /.row -->
                                                                        <div class="cart-product-info">
                                                                            <div class="product-attribute product-color">
                                                                                COLOR: <span
                                                                                    class="text-green-400">@{{ item . attributes . color }}</span>
                                                                            </div>
                                                                            <div class="product-attribute product-size">
                                                                                SIZE: <span
                                                                                    class="text-green-400">@{{ item . attributes . size }}</span>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    </td>
                                                                    <td class="cart-product-quantity">
                                                                        <div class="quant-input">
                                                                            @{{ item . quantity }}</div>
                                                                    </td>
                                                                    <td>
                                                                        <span>
                                                                            $@{{ item . price }}
                                                                        </span>
                                                                    </td>
                                                                    <td class="cart-product-sub-total">
                                                                        <span
                                                                            class="cart-sub-total-price">$@{{ item . quantity * item . price }}</span>
                                                                    </td>
                                                                </tr>
                                                            </tbody><!-- /tbody -->
                                                        </table><!-- /table -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- panel-body  -->
                                    </div><!-- row -->
                                </div>
                                <!-- checkout-step-01  -->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-sm-12 col-md-12" v-if='errors.length > 0'>
                                    <div class="panel-group">
                                        <div class="panel panel-default">
                                            <ul>
                                                <li class="text-red-400" v-for="error in errors" :key='error'>
                                                    @{{ error }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12">
                                    <!-- checkout-progress-sidebar -->
                                    <div class="checkout-progress-sidebar ">
                                        <div class="panel-group">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="unicase-checkout-title">Your Checkout Progress</h4>
                                                </div>
                                                <div class="___class_+?77___">
                                                    <ul class="nav nav-checkout-progress list-unstyled">
                                                        <li><a href="javascript:avoid(0)"
                                                                class='font-semibold italic'>Billing
                                                                Address</a>
                                                            <div v-show="billing_info.fname" class='px-5'>
                                                                <h4 class='font-semibold'>
                                                                    @{{ billing_info . fname + ' ' + billing_info . lname }}
                                                                </h4>
                                                                <p>@{{ billing_info . line1 + ', ' + billing_info . line2 }}
                                                                </p>
                                                                <p>@{{ billing_info . city + ', ' + billing_info . state + ', ' + countries[billing_info . country] + ', ' + billing_info . zip }}
                                                                </p>
                                                                <p>@{{ billing_info . phone }}</p>
                                                            </div>
                                                        </li>
                                                        <li><a href="javascript:avoid(0)"
                                                                class='font-semibold italic'>Shipping
                                                                Address</a>
                                                            <div v-show="shipping_info.fname" class='px-5'>
                                                                <h4 class='font-semibold'>
                                                                    @{{ shipping_info . fname + ' ' + shipping_info . lname }}
                                                                </h4>
                                                                <p>@{{ shipping_info . line1 + ', ' + shipping_info . line2 }}
                                                                </p>
                                                                <p>@{{ shipping_info . city + ', ' + shipping_info . state + ', ' + countries[shipping_info . country] + ', ' + shipping_info . zip }}
                                                                </p>
                                                                <p>@{{ shipping_info . phone }}</p>
                                                            </div>
                                                        </li>
                                                        <li><a href="javascript:avoid(0)"
                                                                class='font-semibold italic'>Payment
                                                                Method:
                                                                @{{ payment_method }}</a></li>
                                                        <li><a href="javascript:avoid(0)" class='font-semibold italic'>Order
                                                                Detail: </a>
                                                            <div class='px-4'>
                                                                <div class="cart-sub-total">
                                                                    Subtotal<span
                                                                        class="inner-left-md">@{{ '$' + details . sub_total }}</span>
                                                                </div>
                                                                <template v-if='details.cart_total_conditions'>
                                                                    <div
                                                                        v-for="(condition,name) in details.cart_total_conditions">
                                                                        <div class="cart-sub-total capitalize">
                                                                            @{{ condition . type }}
                                                                            <span
                                                                                class="inner-left-md">@{{ name }}
                                                                                (@{{ condition . value }})</span>
                                                                            <button v-if="condition.type !== 'tax'"
                                                                                type="submit" @click="couponRemove(name)"><i
                                                                                    class="text-red-400 fa fa-times"
                                                                                    title="Remove coupon"></i>
                                                                            </button>
                                                                        </div>
                                                                        <div class="cart-sub-total capitalize">
                                                                            @{{ condition . type }} Amount: <span
                                                                                class="inner-left-md">$
                                                                                @{{ condition . discount }}</span>
                                                                        </div>
                                                                    </div>
                                                                </template>
                                                                <div class="cart-grand-total">
                                                                    Grand Total<span
                                                                        class="inner-left-md">@{{ '$' + details . total }}</span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                            @if ($shippings->count() > 0)
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="unicase-checkout-title">Shipping Address</h4>
                                                    </div>
                                                    <div class="___class_+?77___">
                                                        <ul class="nav nav-checkout-progress list-unstyled">
                                                            @foreach ($shippings as $shipping)
                                                                <li class='flex items-center mb-6' style='display:flex'>
                                                                    <input type="radio" class='w-5 h-5'
                                                                        value="{{ $shipping->id }}"
                                                                        v-model='shipping_id'>
                                                                    <div class='px-5'>
                                                                        <h4 class='font-semibold'>
                                                                            {{ $shipping->fname . ' ' . $shipping->lname }}
                                                                        </h4>
                                                                        <p>{{ $shipping->line1 . ', ' . $shipping->line2 }}
                                                                        </p>
                                                                        <p>{{ $shipping->city . ', ' . $shipping->state . ', ' . $shipping->country . ', ' . $shipping->zip }}
                                                                        </p>
                                                                        <p>{{ $shipping->phone }}</p>
                                                                    </div>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="unicase-checkout-title">Payment Method</h4>
                                                    </div>
                                                    <div class="___class_+?77___">
                                                        <div class="row">
                                                            <div
                                                                class="col-sm-12 col-md-12 sm:mx-auto sm:w-1/2 sm:float-none md:w-full">
                                                                <div class="wrap-address-billing mb-10"
                                                                    v-if="payment_method === 'stripe'">
                                                                    <div class='form-group'>
                                                                        <validation-provider name="Card Name"
                                                                            rules="required|max:60" v-slot="{ errors }"
                                                                            persist class='flex flex-col'>
                                                                            <div>
                                                                                <label class='w-1/4' for="card_no">Card
                                                                                    Name:</label>
                                                                                <input type="text" placeholder="Card Name"
                                                                                    v-model='payment_info.card_name'>
                                                                            </div>
                                                                            <div class="text-danger">
                                                                                @{{ errors[0] }}
                                                                            </div>
                                                                        </validation-provider>
                                                                    </div>
                                                                    <div class='form-group'>
                                                                        <validation-provider name="Card Number"
                                                                            rules="required|length:16|numeric"
                                                                            v-slot="{ errors }" persist
                                                                            class='flex flex-col'>
                                                                            <div>
                                                                                <label class='w-1/4' for="card_no">Card
                                                                                    Number:</label>
                                                                                <input type="number"
                                                                                    placeholder="4242424242424242"
                                                                                    v-model='payment_info.card_no'>
                                                                            </div>
                                                                            <div class="text-danger">
                                                                                @{{ errors[0] }}
                                                                            </div>
                                                                        </validation-provider>
                                                                    </div>
                                                                    <div>
                                                                        <div class='form-group'>
                                                                            <validation-provider name="Expiry Month"
                                                                                rules="required|numeric|length:2"
                                                                                v-slot="{ errors }" persist
                                                                                class='flex flex-col'>
                                                                                <div>
                                                                                    <label class='w-1/4'
                                                                                        for="exp_month">Expiry
                                                                                        Month:</label>
                                                                                    <input type="number" placeholder="MM"
                                                                                        v-model='payment_info.exp_month'>
                                                                                </div>
                                                                                <div class="text-danger">
                                                                                    @{{ errors[0] }}
                                                                                </div>
                                                                            </validation-provider>
                                                                        </div>

                                                                        <div class='form-group'>
                                                                            <validation-provider name="Expiry Year"
                                                                                rules="required|numeric|length:2"
                                                                                v-slot="{ errors }" persist
                                                                                class='flex flex-col'>
                                                                                <div>
                                                                                    <label class='w-1/4'
                                                                                        for="exp_year">Expiry
                                                                                        Year:</label>
                                                                                    <input type="number" placeholder="YY"
                                                                                        v-model='payment_info.exp_year'>
                                                                                </div>
                                                                                <div class="text-danger">
                                                                                    @{{ errors[0] }}
                                                                                </div>
                                                                            </validation-provider>
                                                                        </div>

                                                                        <div class='form-group'>
                                                                            <validation-provider name="CVC"
                                                                                rules="required|numeric|length:3"
                                                                                v-slot="{ errors }" persist
                                                                                class='flex flex-col'>
                                                                                <div>
                                                                                    <label class='w-1/4'
                                                                                        for="cvc">CVC:</label>
                                                                                    <input type="password" placeholder="CVC"
                                                                                        v-model='payment_info.cvc'>
                                                                                </div>
                                                                                <div class="text-danger">
                                                                                    @{{ errors[0] }}
                                                                                </div>
                                                                            </validation-provider>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                    <label for="">Stripe</label>
                                                                    <input style='margin-top:0;margin-left:8px' type="radio"
                                                                        name="payment_method" v-model='payment_method'
                                                                        :checked="payment_method === 'stripe'"
                                                                        value="stripe">
                                                                    <img
                                                                        src="{{ asset('assets/images/payments/4.png') }}">
                                                                </div> <!-- end col md 4 -->
                                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                    <label for="">Card</label>
                                                                    <input style='margin-top:0;margin-left:8px' type="radio"
                                                                        name="payment_method" v-model='payment_method'
                                                                        :checked="payment_method === 'card'" value="card">
                                                                    <img
                                                                        src="{{ asset('assets/images/payments/3.png') }}">
                                                                </div> <!-- end col md 4 -->
                                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                    <label for="">Cash</label>
                                                                    <input style='margin-top:0;margin-left:8px' type="radio"
                                                                        name="payment_method" v-model='payment_method'
                                                                        :checked="payment_method === 'cash'" value="cash">
                                                                    <img
                                                                        src="{{ asset('assets/images/payments/6.png') }}">
                                                                </div> <!-- end col md 4 -->
                                                            </div>
                                                        </div> <!-- // end row  -->
                                                    </div>
                                                </div>
                                                <div class='pull-right mt-5'>
                                                    <button class='btn btn-primary' :disabled="submitting"
                                                        @click='submit'>Payment</button>
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                    <!-- checkout-progress-sidebar -->
                                </div>
                            </div>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.checkout-box -->
                <x-brand-carousel />
            </div><!-- /.container -->
        </div><!-- /.data-content -->
    </div>
@endsection
@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"
        integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://unpkg.com/vee-validate@3.4.12/dist/vee-validate.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vee-validate@3.4.12/dist/rules.umd.js"></script>
    <script>
        $(function($) {
            var _token = '<?php echo csrf_token(); ?>';
            $(document).ready(function() {
                VeeValidate.localize({
                    vi: {
                        "messages": {
                            "alpha": "{_field_} chỉ có thể chứa các kí tự chữ",
                            "alpha_dash": "{_field_} có thể chứa các kí tự chữ (A-Z a-z), số (0-9), gạch ngang (-) và gạch dưới (_)",
                            "alpha_num": "{_field_} chỉ có thể chứa các kí tự chữ và số",
                            "alpha_spaces": "{_field_} chỉ có thế chứa các kí tự và khoảng trắng",
                            "between": "{_field_} phải có giá trị nằm trong khoảng giữa {min} và {max}",
                            "confirmed": "{_field_} khác với {target}",
                            "digits": "Trường {_field_} chỉ có thể chứa các kí tự số và bắt buộc phải có độ dài là {length}",
                            "dimensions": "{_field_} phải có chiều rộng {width} pixels và chiều cao {height} pixels",
                            "email": "{_field_} phải là một địa chỉ email hợp lệ",
                            "excluded": "{_field_} không phải là một giá trị hợp lệ",
                            "ext": "{_field_} phải là một tệp",
                            "image": "Trường {_field_} phải là một ảnh",
                            "oneOf": "{_field_} không phải là một giá trị hợp lệ",
                            "max": "{_field_} không thể có nhiều hơn {length} kí tự",
                            "max_value": "{_field_} phải nhỏ hơn hoặc bằng {max}",
                            "mimes": "{_field_} phải chứa kiểu tệp phù hợp",
                            "min": "{_field_} phải chứa ít nhất {length} kí tự",
                            "min_value": "{_field_} phải lớn hơn hoặc bằng {min}",
                            "numeric": "{_field_} chỉ có thể có các kí tự số",
                            "regex": "{_field_} có định dạng không đúng",
                            "required": "{_field_} là bắt buộc",
                            "required_if": "{_field_} là bắt buộc",
                            "size": "{_field_} chỉ có thể chứa tệp nhỏ hơn {size}KB",
                            "double": "{_field_} phải là một số thập phân hợp lệ"
                        }
                    },
                    en: {
                        "messages": {
                            "alpha": "The {_field_} field may only contain alphabetic characters",
                            "alpha_num": "The {_field_} field may only contain alpha-numeric characters",
                            "alpha_dash": "The {_field_} field may contain alpha-numeric characters as well as dashes and underscores",
                            "alpha_spaces": "The {_field_} field may only contain alphabetic characters as well as spaces",
                            "between": "The {_field_} field must be between {min} and {max}",
                            "confirmed": "The {_field_} field confirmation does not match",
                            "digits": "The {_field_} field must be numeric and exactly contain {length} digits",
                            "dimensions": "The {_field_} field must be {width} pixels by {height} pixels",
                            "email": "The {_field_} field must be a valid email",
                            "excluded": "The {_field_} field is not a valid value",
                            "ext": "The {_field_} field is not a valid file",
                            "image": "The {_field_} field must be an image",
                            "integer": "The {_field_} field must be an integer",
                            "length": "The {_field_} field must be {length} long",
                            "max_value": "The {_field_} field must be {max} or less",
                            "max": "The {_field_} field may not be greater than {length} characters",
                            "mimes": "The {_field_} field must have a valid file type",
                            "min_value": "The {_field_} field must be {min} or more",
                            "min": "The {_field_} field must be at least {length} characters",
                            "numeric": "The {_field_} field may only contain numeric characters",
                            "oneOf": "The {_field_} field is not a valid value",
                            "regex": "The {_field_} field format is invalid",
                            "required_if": "The {_field_} field is required",
                            "required": "The {_field_} field is required",
                            "size": "The {_field_} field size must be less than {size}KB",
                            "double": "The {_field_} field must be a valid decimal"
                        }
                    },
                });
                VeeValidate.localize("{{ LaravelLocalization::getCurrentLocale() }}");
                Object.keys(VeeValidateRules).forEach(rule => {
                    if (rule !== 'default')
                        VeeValidate.extend(rule, VeeValidateRules[rule]);
                });
                Vue.component('validation-observer', VeeValidate.ValidationObserver)
                Vue.component('validation-provider', VeeValidate.ValidationProvider)

                const app = new Vue({
                    el: '#app',
                    data() {
                        const countries = {!! $countries->pluck('country_name', 'id') !!}
                        return {
                            shipping_id: null,
                            countries,
                            activeStep: 0,
                            checkoutSchema: [{
                                    title: 'Billing Information',
                                },
                                {
                                    title: 'Shipping Information',
                                }, {
                                    title: 'Payment Information',
                                },
                            ],
                            submitting: false,
                            payment_method: 'cash',
                            payment_info: {
                                card_name: null,
                                card_no: null,
                                exp_month: null,
                                exp_year: null,
                                cvc: null,
                            },
                            billing_info: {
                                fname: '',
                                lname: '',
                                line1: '',
                                line2: '',
                                city: '',
                                state: '',
                                zip: null,
                                country: '',
                                phone: null
                            },
                            shipping_info: {
                                fname: '',
                                lname: '',
                                email: '',
                                line1: '',
                                line2: '',
                                city: '',
                                state: '',
                                zip: null,
                                country: '',
                                phone: null
                            },
                            errors: [],
                            toastOptions: {
                                text: "Oops! Something went wrong!",
                                position: "top-right",
                                loaderBg: "#ff6849",
                                icon: "error",
                                hideAfter: 3500,
                                stack: 6,
                            },
                            details: {
                                sub_total: 0,
                                total: 0,
                                total_quantity: 0,
                                cart_total_conditions: null
                            },
                            itemCount: 0,
                            items: [],
                            item: {
                                id: '',
                                name: '',
                                price: 0.00,
                                quantity: 1
                            },
                        }
                    },
                    computed: {
                        currentStep() {
                            return this.checkoutSchema[this.activeStep];
                        },
                        isLastStep() {
                            return this.activeStep === 2;
                        }
                    },
                    mounted: function() {
                        this.loadItems();
                    },
                    methods: {
                        submit() {
                            // You could also validate manually like this.
                            // this.$refs.registerForm.validate(); // this is 'async' use `await` or `then`.
                            let {
                                shipping_info,
                                payment_info,
                                billing_info,
                                payment_method,
                                shipping_id
                            } = this;
                            let _this = this;
                            _this.submitting = true;
                            axios.post("{{ route('user.checkout.store') }}", {
                                    _token,
                                    shipping_info,
                                    billing_info,
                                    payment_info,
                                    payment_method,
                                    shipping_id
                                })
                                .then(response => {
                                    $.toast({
                                        ..._this.toastOptions,
                                        icon: "success",
                                        text: response.data.success,
                                    })
                                    _this.submitting = false
                                    setTimeout(() => {
                                        window.location.href = "{{ route('home') }}"
                                    }, 2000);
                                }).catch(error => {
                                    $.toast({
                                        ..._this.toastOptions,
                                        text: error.response.data.error,
                                    })
                                    _this.submitting = false
                                })
                        },
                        nextStep() {
                            if (this.isLastStep) {
                                return this.submit();
                            }
                            this.activeStep++;
                        },
                        payment() {
                            console.log(this.shipping_info);
                        },
                        couponRemove(coupon_name) {
                            let _this = this;
                            axios.post(`/carts/coupon-remove/${coupon_name}`, {
                                _token: _token,
                            }).then(function(success) {
                                $.toast({
                                    ..._this.toastOptions,
                                    icon: "success",
                                    text: success.data.message,
                                })
                                _this.loadItems();
                            }, function(error) {
                                $.toast({
                                    ..._this.toastOptions,
                                    text: success.data.message,
                                })
                            });
                        },
                        loadItems: function() {
                            var _this = this;
                            axios.get('/carts', {
                                params: {
                                    limit: 10
                                }
                            }).then(function(success) {
                                _this.items = success.data.data;
                                _this.itemCount = success.data.data.length;
                                _this.loadCartDetails();
                            }, function(error) {
                                console.log(error);
                            });
                        },
                        loadCartDetails: function() {
                            var _this = this;
                            axios.get('/carts/details').then(function(success) {
                                let {
                                    data
                                } = success.data
                                _this.details = data;
                            }, function(error) {
                                console.log(error);
                            });
                        },
                    }
                });
            });
        });
    </script>
@endpush
