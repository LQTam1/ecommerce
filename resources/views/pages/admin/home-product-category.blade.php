@extends('layouts.admin.master')
@section('title', 'Product Category Manager')
@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        Admin
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Product All</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <form
                                    action="{{ $categoryProduct === null ? route('admin.product.category.store') : route('admin.product.category.update', $categoryProduct->id) }}"
                                    method="post">
                                    @csrf
                                    @if ($categoryProduct !== null)
                                        @method('put')
                                    @endif
                                    @php
                                        $selectCategories = [];
                                        $productsToShow = 1;
                                        if ($categoryProduct !== null) {
                                            $selectCategories = array_map(fn($item) => $item->id, $categoryProduct->select_categories);
                                            $productsToShow = $categoryProduct->products_to_show;
                                        }
                                    @endphp
                                    <div class="form-group @error('select_categories') is-invalid @enderror">
                                        <label>Select Categories</label>
                                        <select class="form-control select2" multiple="multiple" name='select_categories[]'
                                            data-placeholder="Select a State" style="width: 100%;">
                                            @foreach ($categories as $category)
                                                <option value="{{ $category['id'] }}" @if (in_array($category['id'], $selectCategories)) selected @endif>
                                                    {{ $category['label'] }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('select_categories')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group @error('products_to_show') is-invalid @enderror">
                                        <label>Products To Show</label>
                                        <input type='number' class="form-control" value="{{ $productsToShow }}"
                                            name='products_to_show' placeholder="Products To Show" min="1" max='50' />
                                        @error('products_to_show')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <!-- /.form-group -->

                                    <div class="form-group">
                                        <label></label>
                                        <input value='Save' type='submit' class="btn btn-primary" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection

@push('scripts')
    {{-- <script src="{{ asset('backend/assets/vendor_components/bootstrap-select/dist/js/bootstrap-select.js') }}"></script> --}}
    <script src="{{ asset('backend/assets/vendor_components/select2/dist/js/select2.full.js') }}"></script>
    <script>
        $('.select2').select2();
    </script>
@endpush
