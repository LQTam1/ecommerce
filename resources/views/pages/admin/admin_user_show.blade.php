@extends('layouts.admin.master')
@section('title', 'View Admin User')
@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        <a href="{{ route('admin.admins.index') }}">All User Admin</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">View</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box mb-0">
                            <!-- /.box-header -->
                            <div class="box box-widget widget-user p-5 mb-0">
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <div class="user-details text-center">
                                    <img id="previewImage" src="{{ $admin->profile_photo_url }}"
                                        style="width: 100px; height: 100px;">
                                    <h3>Admin User Name: {{ $admin->name }}</h3>
                                    <h5>Admin Email: {{ $admin->email }} </h5>
                                    <hr>
                                </div>
                                <div class="row">
                                    <div class="col-12 container m-auto text-center">
                                        <div class="row">
                                            @foreach ($permissionGroup as $resource => $permission)
                                                <div class="col-sm-6 col-md-4 col-lg-3">
                                                    <div class="form-group ">
                                                        <div class="controls ">
                                                            <fieldset>
                                                                <h5 class='text-capitalize'>{{ $resource }}
                                                                    Management</h5>
                                                            </fieldset>
                                                            @foreach ($permission as $action)
                                                                <fieldset>
                                                                    <label for="checkbox_{{ $action->id }}"
                                                                        class='text-capitalize'>{{ $action->name }}
                                                                    </label>
                                                                </fieldset>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </section>
        </div>
    </div>
@endsection
