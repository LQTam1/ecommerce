@extends('layouts.admin.master')
@section('title', 'Country Management')

@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        Admin
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Country All</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-md-8">
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                    {{ $dataTable->table([], true) }}
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box box-widget widget-user p-5">
                                <h3>Add Country</h3>
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <form method='post' action="{{ route('admin.countries.store') }}">
                                    @csrf
                                    <div class="form-group @error('country_code') is-invalid @enderror">
                                        <h5>Code <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="country_code" value="{{ old('country_code') }}"
                                                class="form-control">
                                        </div>
                                        @error('country_code')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group @error('country_name') is-invalid @enderror">
                                        <h5>Name <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="country_name" value="{{ old('country_name') }}"
                                                class="form-control">
                                        </div>
                                        @error('country_name')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group @error('phone_code') is-invalid @enderror">
                                        <h5>Phone Code <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="number" name="phone_code" value="{{ old('phone_code') }}"
                                                class="form-control">
                                        </div>
                                        @error('phone_code')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group @error('alpha_3') is-invalid @enderror">
                                        <h5>Alpha 3</h5>
                                        <div class="controls">
                                            <input type="text" name="alpha_3" value="{{ old('alpha_3') }}"
                                                class="form-control">
                                        </div>
                                        @error('alpha_3')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group @error('continent_code') is-invalid @enderror">
                                        <h5>Continent Code <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="continent_code" value="{{ old('continent_code') }}"
                                                class="form-control">
                                        </div>
                                        @error('continent_code')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group @error('continent_name') is-invalid @enderror">
                                        <h5>Continent Name <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="continent_name" value="{{ old('continent_name') }}"
                                                class="form-control">
                                        </div>
                                        @error('continent_name')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="text-xs-right">
                                        <button type="submit"
                                            class="btn btn-rounded btn-primary mb-5 pull-right">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </section>
        </div>
    </div>
@endsection
@push('scripts')
    {!! $dataTable->scripts() !!}
@endpush
