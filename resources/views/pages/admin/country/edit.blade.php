@extends('layouts.admin.master')
@section('title', 'Country Edit')

@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        <a href="{{ route('admin.countries.index') }}">Country All</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box box-widget widget-user p-5">
                                <h3>Edit Country</h3>
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <form method='post' action="{{ route('admin.countries.update', $country->id) }}">
                                    @csrf
                                    @method('put')
                                    <div class="form-group @error('country_code') is-invalid @enderror">
                                        <h5>Code <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="country_code" value="{{ $country->country_code }}"
                                                class="form-control">
                                        </div>
                                        @error('country_code')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group @error('country_name') is-invalid @enderror">
                                        <h5>Name <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="country_name" value="{{ $country->country_name }}"
                                                class="form-control">
                                        </div>
                                        @error('country_name')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group @error('phone_code') is-invalid @enderror">
                                        <h5>Phone Code <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="number" name="phone_code" value="{{ $country->phone_code }}"
                                                class="form-control">
                                        </div>
                                        @error('phone_code')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group @error('alpha_3') is-invalid @enderror">
                                        <h5>Alpha 3</h5>
                                        <div class="controls">
                                            <input type="text" name="alpha_3" value="{{ $country->alpha_3 }}"
                                                class="form-control">
                                        </div>
                                        @error('alpha_3')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group @error('continent_code') is-invalid @enderror">
                                        <h5>Continent Code <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="continent_code"
                                                value="{{ $country->continent_code }}" class="form-control">
                                        </div>
                                        @error('continent_code')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group @error('continent_name') is-invalid @enderror">
                                        <h5>Continent Name <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="continent_name"
                                                value="{{ $country->continent_name }}" class="form-control">
                                        </div>
                                        @error('continent_name')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="text-xs-right">
                                        <button type="submit"
                                            class="btn btn-rounded btn-primary mb-5 pull-right">Update</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </section>
        </div>
    </div>
@endsection
