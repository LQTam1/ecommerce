@extends('layouts.admin.master')
@section('title', 'All Report')
@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.reports.index') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        Admin
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">All Report</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="row">
                    <!--   ------------ Add Search Page -------- -->
                    <div class="col-4">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Search By Date </h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                    <div class="form-group">
                                        <h5>Select Date <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="date" name="date" id='date' class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <div class="col-4">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Search By Month </h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                    <div class="form-group">
                                        <h5>Select Month <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <select name="month" class="form-control">
                                                <option label="Choose One"></option>
                                                <option value="January">January</option>
                                                <option value="February">February</option>
                                                <option value="March">March</option>
                                                <option value="April">April</option>
                                                <option value="May">May</option>
                                                <option value="Jun">Jun</option>
                                                <option value="July">July</option>
                                                <option value="August">August</option>
                                                <option value="September">September</option>
                                                <option value="October">October</option>
                                                <option value="November">November</option>
                                                <option value="December">December</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5>Select Year <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <select name="year_name" class="form-control">
                                                <option label="Choose One"></option>
                                                <option value="2020">2020</option>
                                                <option value="2021">2021</option>
                                                <option value="2022">2022</option>
                                                <option value="2023">2023</option>
                                                <option value="2024">2024</option>
                                                <option value="2025">2025</option>
                                                <option value="2026">2026</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="text-xs-right">
                                        <input type="button" class="btn btn-rounded btn-primary mb-5 " id='searchByMonthBtn'
                                            value="Search">
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <div class="col-4">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Select Year </h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                    <div class="form-group">
                                        <h5>Select Year <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <select name="year" class="form-control">
                                                <option label="Choose One"></option>
                                                <option value="2020">2020</option>
                                                <option value="2021">2021</option>
                                                <option value="2022">2022</option>
                                                <option value="2023">2023</option>
                                                <option value="2024">2024</option>
                                                <option value="2025">2025</option>
                                                <option value="2026">2026</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!--   ------------End  Add Search Page -------- -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Order List</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table id="report-table" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Date </th>
                                                <th>Invoice </th>
                                                <th>Amount </th>
                                                <th>Payment </th>
                                                <th>Status </th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot></tfoot>
                                    </table>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
            </section>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('backend/assets/vendor_components/datatable/datatables.min.js') }}"></script>
    <script>
        $(function() {
            let table = $('#report-table').DataTable({
                dom: 'Bfrtip',
                lengthMenu: [
                    [10, 25, 50, -1],
                    ['10 rows', '25 rows', '50 rows', 'Show all']
                ],
                buttons: [
                    'pageLength',
                ],
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.orders.index') }}",
                searchDelay: 500,
                lengthMenu: [10, 25, 50, 75, 100, 250, 500],
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'id'
                    },
                    {
                        data: 'order_date',
                        name: 'order_date',

                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'invoice_no',
                        name: 'invoice_no',
                        orderable: true,
                    },
                    {
                        data: 'amount',
                        name: 'amount',
                        orderable: true,
                    },
                    {
                        data: 'payment_method',
                        name: 'payment_method',
                        orderable: true,
                    },
                    {
                        data: 'status',
                        name: 'status',
                        searchDelay: 500,
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('.dt-buttons.btn-group').append(`
                <button class="btn btn-secondary buttons-reset" tabindex="0" aria-controls="orders-table">
                    <span><i class="fa fa-undo"></i> Reset</span>
                </button>
            `)

            $('.buttons-reset').on('click', function() {
                table.search('').clear().draw();
                table.ajax.reload();
            })


            $('#report-table_filter').parent().prepend(`
                <div class="d-inline pull-right pt-5">
                    <label>Sort by:</label>
                    <select class="form-control d-inline" style="width: inherit;" id='sortByStatus'>
                        <option value='' >---Select status---</option>
                        <option value='pending' >Pending</option>
                        <option value='confirm'>Confirm</option>
                        <option value='processing'>Processing</option>
                        <option value='picked'>Picked</option>
                        <option value='shipped'>Shipped</option>
                        <option value='delivered'>Delivered</option>
                        <option value='cancel'>Cancel</option>
                    </select>
                </div>
            `)

            $('#sortByStatus').on('change', function() {
                table.search(this.value).draw();
            })

            $('#date').on('change', function() {
                table.search(formatDatedFY(this.value)).draw();
            })

            $('select[name=year]').on('change', function() {
                table.search(this.value).draw();
            })

            $('#searchByMonthBtn').click(function() {
                let month = $('select[name=month]').val()
                let yearName = $('select[name=year_name]').val()
                let searchVal = `${month} ${yearName}`
                table.search(searchVal).draw();
            })
        })
    </script>
@endpush
