@extends('layouts.admin.master')
@section('title', 'Order Details')
@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item">
                                        <a href="{{ route('admin.orders.index') }}">All Order</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Order Details</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-md-6 col-12">
                        <div class="box box-bordered border-primary">
                            <div class="box-header with-border">
                                <h4 class="box-title"><strong>Shipping Details</strong> </h4>
                            </div>
                            <table class="table">
                                <tr>
                                    <th> Shipping Name : </th>
                                    <th> {{ $order->name }} </th>
                                </tr>
                                <tr>
                                    <th> Shipping Phone : </th>
                                    <th> {{ $order->phone }} </th>
                                </tr>
                                <tr>
                                    <th> Shipping Email : </th>
                                    <th> {{ $order->email }} </th>
                                </tr>
                                <tr>
                                    <th> Address : </th>
                                    <th>
                                        <p>{{ $shipping->line1 }}, {{ $shipping->line2 }}</p>
                                        <p>{{ $shipping->city . ', ' . $shipping->state . ', ' . $shipping->countryModel->country_name . ', ' . $shipping->zip }}
                                        </p>
                                    </th>
                                </tr>
                                <tr>
                                    <th> Order Date : </th>
                                    <th> {{ $order->order_date }} </th>
                                </tr>
                            </table>
                        </div>
                    </div> <!--  // cod md -6 -->
                    <div class="col-md-6 col-12">
                        <div class="box box-bordered border-primary">
                            <div class="box-header with-border">
                                <h4 class="box-title"><strong>Order Details</strong><span class="text-danger">
                                        Invoice :
                                        {{ $order->invoice_no }}</span></h4>
                            </div>
                            <table class="table">
                                <tr>
                                    <th> Name : </th>
                                    <th> {{ $order->name }} </th>
                                </tr>
                                <tr>
                                    <th> Phone : </th>
                                    <th> {{ $order->phone }} </th>
                                </tr>
                                <tr>
                                    <th> Payment Type : </th>
                                    <th> {{ $order->payment_method }} </th>
                                </tr>
                                @if ($order->transaction_id)
                                <tr>
                                    <th> Tranx ID : </th>
                                    <th> {{ $order->transaction_id }} </th>
                                </tr>
                                @endif
                                <tr>
                                    <th> Invoice : </th>
                                    <th class="text-danger"> {{ $order->invoice_no }} </th>
                                </tr>
                                <tr>
                                    <th> Order Total : </th>
                                    <th>${{ $order->amount }} </th>
                                </tr>
                                <tr>
                                    <th> Order : </th>
                                    <th>
                                        @if ($order->return_order == 0)
                                            <span class="badge badge-pill badge-warning"
                                                style="background: #418DB9;">{{ $order->status }} </span>
                                        @elseif($order->return_order == 1)
                                            <span class="badge badge-pill badge-warning" style="background: #800000;">
                                                Return Pending ({{ $order->return_date }})</span>
                                        @elseif($order->return_order == 2)
                                            <span class="badge badge-pill badge-warning" style="background: #008000;">Return
                                                Success ({{ $order->return_date }}) </span>
                                        @endif
                                    </th>
                                </tr>
                                <tr>
                                    <th> </th>
                                    <th>
                                        @if ($order->status == 'pending')
                                            <a href="{{ route('admin.orders.updateStatus', [$order->id, 'status' => 'confirm']) }}"
                                                onclick="if(confirm('Are you sure want to update this order to confirm?')) return true;else return false"
                                                class="btn btn-block btn-success" id="confirm">Confirm Order</a>
                                        @elseif($order->status == 'confirm')
                                            <a href="{{ route('admin.orders.updateStatus', [$order->id, 'status' => 'processing']) }}"
                                                onclick="if(confirm('Are you sure want to update this order to processing?')) return true;else return false"
                                                class="btn btn-block btn-success" id="processing">Processing
                                                Order</a>
                                        @elseif($order->status == 'processing')
                                            <a href="{{ route('admin.orders.updateStatus', [$order->id, 'status' => 'picked']) }}"
                                                onclick="if(confirm('Are you sure want to update this order to picked?')) return true;else return false"
                                                class="btn btn-block btn-success" id="picked">Picked Order</a>
                                        @elseif($order->status == 'picked')
                                            <a href="{{ route('admin.orders.updateStatus', [$order->id, 'status' => 'shipped']) }}"
                                                onclick="if(confirm('Are you sure want to update this order to shipped?')) return true;else return false"
                                                class="btn btn-block btn-success" id="shipped">Shipped Order</a>
                                        @elseif($order->status == 'shipped')
                                            <a href="{{ route('admin.orders.updateStatus', [$order->id, 'status' => 'delivered']) }}"
                                                onclick="if(confirm('Are you sure want to update this order to delivered?')) return true;else return false"
                                                class="btn btn-block btn-success" id="delivered">Delivered Order</a>
                                        @elseif ($order->return_order === 1)
                                            <a href="{{route('admin.return.order.approve', $order->id)}}" onclick="if(confirm('Are you sure want to approve this order to be return?')) return true;else return false"
                                                class="btn btn-block btn-danger" title="Return Approve">Return Approve</a>
                                        @endif
                                    </th>
                                </tr>
                            </table>
                        </div>
                    </div> <!--  // cod md -6 -->
                    <div class="col-md-12 col-12">
                        <div class="box box-bordered border-primary">
                            <div class="box-header with-border">
                            </div>
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td width="10%">
                                            <label for=""> Image</label>
                                        </td>
                                        <td width="20%">
                                            <label for=""> Product Name </label>
                                        </td>
                                        <td width="10%">
                                            <label for=""> Product Code</label>
                                        </td>
                                        <td width="10%">
                                            <label for=""> Color </label>
                                        </td>
                                        <td width="10%">
                                            <label for=""> Size </label>
                                        </td>
                                        <td width="10%">
                                            <label for=""> Quantity </label>
                                        </td>
                                        <td width="10%">
                                            <label for=""> Price </label>
                                        </td>
                                    </tr>
                                    @foreach ($order->items as $item)
                                        <tr>
                                            <td width="10%">
                                                <label for=""><img src="{{ asset($item->product->product_thumbnail) }}"
                                                        height="50px;" width="50px;"> </label>
                                            </td>
                                            <td width="20%">
                                                <label for=""> {{ $item->product->name }}</label>
                                            </td>
                                            <td width="10%">
                                                <label for=""> {{ $item->product->product_code }}</label>
                                            </td>
                                            <td width="10%">
                                                <label for=""> {{ $item->color }}</label>
                                            </td>
                                            <td width="10%">
                                                <label for=""> {{ $item->size }}</label>
                                            </td>
                                            <td width="10%">
                                                <label for=""> {{ $item->qty }}</label>
                                            </td>
                                            <td width="10%">
                                                <label for=""> ${{ $item->price }} ( $ {{ $item->price * $item->qty }}
                                                    )
                                                </label>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div> <!--  // cod md -12 -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
    </div>
@endsection
