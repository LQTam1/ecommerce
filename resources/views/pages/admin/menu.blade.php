@extends('layouts.admin.master')
@section('title', 'Menu Builder')
@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <section class='content'>
                {!! Menu::render() !!}
            </section>
        </div>
    </div>
@endsection
@push('scripts')
    {!! Menu::scripts() !!}
@endpush
