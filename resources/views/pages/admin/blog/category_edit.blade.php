@extends('layouts.admin.master')
@section('title', 'Post Category Edit')

@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        <a href="{{ route('admin.blog.postCategories.index') }}">Post Category All</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box box-widget widget-user p-5">
                                <h3>Edit Post Category</h3>
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <form method='post' action="{{ route('admin.blog.postCategories.update', $postCategory->id) }}"
                                    enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    @foreach (LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                        <div class="form-group @error("name.$localeCode") is-invalid @enderror">
                                            <h5>Category Name {{ $properties['native'] }} <span
                                                    class="text-danger">*</span>
                                            </h5>
                                            <div class="controls">
                                                <input type="text" name="name[{{ $localeCode }}]"
                                                    value="{{ $postCategory->getTranslation('name',$localeCode) }}" class="form-control">
                                            </div>

                                            @error("name.$localeCode")
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    @endforeach

                                    @foreach (LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                        <div class="form-group @error("slug.$localeCode") is-invalid @enderror">
                                            <h5>Category Slug {{ $properties["native"] }} <span
                                                    class="text-danger">*</span>
                                            </h5>
                                            <div class="controls">
                                                <input type="text" name="slug[{{ $localeCode }}]"
                                                    value="{{ $postCategory->getTranslation('slug',$localeCode) }}" class="form-control">
                                            </div>
                                            @error("slug.$localeCode")
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    @endforeach
                                    <div class="text-xs-right">
                                        <button type="submit"
                                            class="btn btn-rounded btn-primary mb-5 pull-right">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </section>
        </div>
    </div>
@endsection