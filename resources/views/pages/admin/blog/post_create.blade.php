@extends('layouts.admin.master')
@section('title', 'Post Create')
@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item">
                                        <a href="{{ route('admin.blog.posts.index') }}">All Blog Post</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Post Create</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <!-- Basic Forms -->
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">Add Post </h4>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col">
                                <form method="post" action="{{ route('admin.blog.posts.store') }}"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row">
                                                <!-- start 1st row  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <h5>Image <span class="text-danger">*</span></h5>
                                                            <div class="controls">
                                                                <input type="file" name="image" class="form-control"
                                                                    onChange="imageUrl(this)">
                                                                @error('image')
                                                                    <span class="text-danger">{{ $message }}</span>
                                                                @enderror
                                                                <img src="" id="image">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> <!-- end col md 4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h5>Post Category Select <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <select name="post_category_id" class="form-control">
                                                                @foreach ($postCategories as $postCategory)
                                                                    <option value="{{ $postCategory['id'] }}"
                                                                        @if (old('post_category_id') === $postCategory['name']) selected @endif>
                                                                        {{ $postCategory['name'] }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                            @error('post_category_id')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div> <!-- end col md 4 -->
                                            </div> <!-- end 1st row  -->
                                            <div class="row">
                                                <!-- start 2nd row  -->
                                                @foreach (LaravelLocalization::getSupportedLocales() as $localeCode => $lang)
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <h5>Post Title {{ $lang['name'] }} <span
                                                                    class="text-danger">*</span></h5>
                                                            <div class="controls">
                                                                <input type="text" name="title[{{ $localeCode }}]"
                                                                    class="form-control"
                                                                    value="{{ old("title.$localeCode") }}" />
                                                                @error("title.$localeCode")
                                                                    <span class="text-danger">{{ $message }}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div> <!-- end col md 4 -->
                                                @endforeach
                                                @foreach (LaravelLocalization::getSupportedLocales() as $localeCode => $lang)
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <h5>Post Slug<span class="text-danger">*</span></h5>
                                                            <div class="controls">
                                                                <input type="text" name="slug[{{ $localeCode }}]"
                                                                    value="{{ old("slug.$localeCode") }}"
                                                                    class="form-control">
                                                                @error("slug.$localeCode")
                                                                    <span class="text-danger">{{ $message }}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div> <!-- end col md 4 -->
                                                @endforeach
                                            </div> <!-- end 2nd row  -->
                                            <div class="row">
                                                <!-- start 7th row  -->
                                                @foreach (LaravelLocalization::getSupportedLocales() as $localeCode => $lang)
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <h5>Details {{ $lang['name'] }} <span
                                                                    class="text-danger">*</span>
                                                            </h5>
                                                            <div class="controls">
                                                                <textarea name="details[{{ $localeCode }}]"
                                                                    value="{{ old("details.$localeCode") }}"
                                                                    class="form-control textarea" required
                                                                    placeholder="Textarea text">
                                                                            {{ old("details.$localeCode") }}
                                                                            </textarea>
                                                                @error("details.$localeCode")
                                                                    <span class="text-danger">{{ $message }}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div> <!-- end col md 6 -->
                                                @endforeach
                                            </div> <!-- end 7th row  -->
                                            <hr>
                                            <div class="text-xs-right">
                                                <input type="submit" class="btn btn-rounded btn-primary mb-5"
                                                    value="Add Post">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
    </div>
@endsection
@push('scripts')
    {{-- <script src="{{ asset('backend/assets/vendor_components/ckeditor/ckeditor.js') }}"></script> --}}
    <script src="{{ asset('backend/assets/vendor_components/ckeditor_4.16.2_501b131964df/ckeditor.js') }}"></script>
    <script type="text/javascript">
        function imageUrl(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#image').attr('src', e.target.result).width(80).height(80);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script>
        $(document).ready(function() {
            CKEDITOR.replaceAll('textarea');
            $('.textarea').ckeditor();
            $('#multiImg').on('change', function() { //on file input change
                if (window.File && window.FileReader && window.FileList && window
                    .Blob) //check File API supported browser
                {
                    var data = $(this)[0].files; //this file data
                    $.each(data, function(index, file) { //loop though each file
                        if (/(\.|\/)(gif|jpe?g|png)$/i.test(file
                                .type)) { //check supported file type
                            var fRead = new FileReader(); //new filereader
                            fRead.onload = (function(file) { //trigger function on successful read
                                return function(e) {
                                    var img = $('<img/>').addClass('thumb').attr('src',
                                            e.target.result).width(80)
                                        .height(80); //create image element
                                    $('#preview_img').append(
                                        img); //append image to output element
                                };
                            })(file);
                            fRead.readAsDataURL(file); //URL representing the file's data.
                        }
                    });
                } else {
                    alert("Your browser doesn't support File API!"); //if File API is absent
                }
            });
        });
    </script>
@endpush
