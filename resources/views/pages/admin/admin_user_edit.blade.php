@extends('layouts.admin.master')
@section('title', 'Update Admin User')
@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        <a href="{{ route('admin.admins.index') }}">All User Admin</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Update</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box box-widget widget-user p-5">
                                <h3>Update Admin</h3>
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <form method='post' action="{{ route('admin.admins.update', $admin->id) }}"
                                    enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group @error('name') is-invalid @enderror">
                                                        <h5>Admin User Name <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <input type="text" name="name" value="{{ $admin->name }}"
                                                                class="form-control">
                                                            @error('name')
                                                                <div class="text-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div> <!-- end cold md 4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group  @error('email') is-invalid @enderror">
                                                        <h5>Admin Email <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <input type="email" name="email" value="{{ $admin->email }}"
                                                                class="form-control">
                                                            @error('email')
                                                                <div class="text-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div> <!-- end cold md 4 -->
                                            </div> <!-- end row 	 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group  @error('newProfilePhoto') is-invalid @enderror">
                                                        <h5>Admin User Image <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <input type="file" name="newProfilePhoto" class="form-control"
                                                                id="image">
                                                            @error('newProfilePhoto')
                                                                <div class="text-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div><!-- end cold md 6 -->
                                                <div class="col-md-6">
                                                    <img id="previewImage" src="{{ $admin->profile_photo_url }}"
                                                        style="width: 100px; height: 100px;">
                                                </div><!-- end cold md 6 -->
                                            </div><!-- end row 	 -->
                                            <hr>
                                            <div class="row">
                                                @foreach ($permissionGroup as $resource => $permission)
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <div class="controls">
                                                                <fieldset>
                                                                    <h5 class='text-capitalize'>{{ $resource }}
                                                                        Management</h5>
                                                                </fieldset>
                                                                @foreach ($permission as $action)
                                                                    <fieldset>
                                                                        <input type="checkbox"
                                                                            id="checkbox_{{ $action->id }}"
                                                                            name="permissions[{{ $resource }}][]"
                                                                            value="{{ $action->name }}"
                                                                            @if ($admin->hasDirectPermission($action->name)) checked @endif>
                                                                        <label for="checkbox_{{ $action->id }}"
                                                                            class='text-capitalize'>{{ $action->name }}
                                                                        </label>
                                                                    </fieldset>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="text-xs-right">
                                                <input type="submit" class="btn btn-rounded btn-primary mb-5"
                                                    value="Update Admin User">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </section>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $('#newProfilePhoto').change(function(e) {
            let render = new FileReader();
            render.onload = function(e) {
                $('#previewImage').attr('src', e.target.result);
            }
            render.readAsDataURL((e.target.files[0]))
        })
    </script>
@endpush
