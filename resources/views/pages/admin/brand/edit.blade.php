@extends('layouts.admin.master')
@section('title', 'Edit Brand')

@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        <a href="{{ route('admin.brands.index') }}">Brand All</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            {{-- <div class="box-header with-border">
                            <h3 class="box-title">Data Table With Full Features</h3>
                        </div> --}}
                            <!-- /.box-header -->
                            <div class="box box-widget widget-user p-5">
                                <h3>Edit Brand</h3>
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <form method='post' action="{{ route('admin.brands.update', $brand->id) }}"
                                    enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    <div class="form-group @error('name.en') is-invalid @enderror">
                                        <h5>Name EN <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="name[en]"
                                                value="{{ $brand->getTranslation('name', 'en') }}" class="form-control">
                                        </div>
                                        @error('name.en')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group @error('name.vi') is-invalid @enderror">
                                        <h5>Name VI <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="name[vi]"
                                                value="{{ $brand->getTranslation('name', 'vi') }}" class="form-control">
                                        </div>
                                        @error('name.vi')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group @if ($errors->first('slug')) is-invalid @endif">
                                        <h5>Slug <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" id="slug" name="slug" value="{{ $brand->slug }}"
                                                class=" form-control">
                                        </div>
                                        @if ($errors->first('slug'))
                                            <div class="text-danger">{{ $errors->first('slug') }}</div>
                                        @endif
                                    </div>

                                    <div class="form-group @error('image') is-invalid @enderror">
                                        <h5>Image</h5>
                                        <div class="controls mb-2">
                                            <input type="file" id="image" name="image" class="form-control">
                                        </div>
                                        @error('image')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                        <img class="border rounded-circle" id="previewImage"
                                            src="{{ asset($brand->image) }}" width='100' height='100' alt="Brand name">
                                    </div>
                                    <div class="text-xs-right">
                                        <button type="submit"
                                            class="btn btn-rounded btn-primary mb-5 pull-right">Update</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </section>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $('#image').change(function(e) {
            let render = new FileReader();
            render.onload = function(e) {
                $('#previewImage').attr('src', e.target.result);
            }
            render.readAsDataURL((e.target.files[0]))
        })
    </script>
@endpush
