@extends('layouts.admin.master')
@section('title', 'Dashboard')

@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        Admin
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Brand All</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-md-8">
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                    {{ $dataTable->table([], true) }}
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box box-widget widget-user p-5">
                                <h3>Add Brand</h3>
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <form method='post' action="{{ route('admin.brands.store') }}"
                                    enctype="multipart/form-data">
                                    @csrf
                                    @foreach (LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                        <div class="form-group @error('name.{{ $localeCode }}') is-invalid @enderror">
                                            <h5>Name {{ $properties['native'] }} <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="name[{{ $localeCode }}]"  value="{{ old("name.$localeCode") }}" class="form-control">
                                            </div>
                                            @error('name.{{ $localeCode }}')
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    @endforeach
                                    <div class="form-group @if ($errors->first('slug')) is-invalid @endif">
                                        <h5>Slug <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" id="slug" name="slug"  value="{{ old("slug") }}" class="form-control">
                                        </div>
                                        @if ($errors->first('slug'))
                                            <div class="text-danger">{{ $errors->first('slug') }}</div>
                                        @endif
                                    </div>

                                    <div class="form-group @error('image') is-invalid @enderror">
                                        <h5>Image</h5>
                                        <div class="controls mb-2">
                                            <input type="file" id="image" name="image" class="form-control">
                                        </div>
                                        @error('image')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                        <img class="border rounded-circle" id="previewImage"
                                            src="{{ asset('backend/assets/images/no_image.jpg') }}" width='100'
                                            height='100' alt="Brand name">
                                    </div>
                                    <div class="text-xs-right">
                                        <button type="submit"
                                            class="btn btn-rounded btn-primary mb-5 pull-right">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </section>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $('#image').change(function(e) {
            let render = new FileReader();
            render.onload = function(e) {
                $('#previewImage').attr('src', e.target.result);
            }
            render.readAsDataURL((e.target.files[0]))
        })
    </script>
    {!! $dataTable->scripts() !!}
@endpush
