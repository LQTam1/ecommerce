@extends('layouts.admin.master')
@section('title', 'Category Management')

@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        Admin
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Category All</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-md-8">
                        <div class="box">
                            {{-- <div class="box-header with-border">
                            <h3 class="box-title">Data Table With Full Features</h3>
                        </div> --}}
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Name en</th>
                                                <th>Name vi</th>
                                                <th>Slug</th>
                                                <th>Icon</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($categories as $category)
                                                @include('pages.admin.category.indexRow', compact('category'))
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Name en</th>
                                                <th>Name vi</th>
                                                <th>Slug</th>
                                                <th>Icon</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box box-widget widget-user p-5">
                                <h3>Add Category</h3>
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <form method='post' action="{{ route('admin.categories.store') }}"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group @error('name.en') is-invalid @enderror">
                                        <h5>Name EN <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="name[en]" class="form-control">
                                        </div>
                                        @error('name.en')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group @error('name.vi') is-invalid @enderror">
                                        <h5>Name VI <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="name[vi]" class="form-control">
                                        </div>
                                        @error('name.vi')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group @if ($errors->first('slug')) is-invalid @endif">
                                        <h5>Slug <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" id="slug" name="slug" class="form-control">
                                        </div>
                                        @if ($errors->first('slug'))
                                            <div class="text-danger">{{ $errors->first('slug') }}</div>
                                        @endif
                                    </div>

                                    <div class="form-group @error('icon') is-invalid @enderror">
                                        <h5>Icon</h5>
                                        <div class="controls mb-2">
                                            <input type="text" id="icon" name="icon" class="form-control">
                                        </div>
                                        @error('icon')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="text-xs-right">
                                        <button type="submit"
                                            class="btn btn-rounded btn-primary mb-5 pull-right">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </section>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('backend/assets/vendor_components/datatable/datatables.min.js') }}"></script>
    <script>
        $('#example1').DataTable();
    </script>
@endpush
