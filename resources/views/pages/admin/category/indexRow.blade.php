 <tr>
     <td>{{ $category->getTranslation('name', 'en') }}</td>
     <td>{{ $category->getTranslation('name', 'vi') }}</td>
     <td>{{ $category->slug }}</td>
     <td><i class="{{ $category->icon }}"></i></td>
     <td>
         <a href="{{ route('admin.categories.edit', $category->id) }}" title="Edit Category"
             class="btn btn-primary btn-sm fa fa-pencil">
         </a>
         <a href="#" id='delete' title='Delete Category' class="btn btn-primary btn-sm delete-confirm fa fa-trash">
         </a>
         <form class='btn-block d-none' id='formDelete'
             action="{{ route('admin.categories.destroy', $category->id) }}" method="POST">
             @csrf
             @method('delete')
             <button type='submit' class="btn btn-danger btn-sm fa fa-trash">
             </button>
         </form>
     </td>
 </tr>
