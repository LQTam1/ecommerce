@extends('layouts.admin.master')
@section('title', 'Create Admin User')
@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        <a href="{{ route('admin.admins.index') }}">All User Admin</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Create</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box box-widget widget-user p-5">
                                <h3>Add Admin</h3>
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                @error('permissions')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                                <div class="text-danger"></div>
                                <form method='post' action="{{ route('admin.admins.store') }}"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group @error('name') is-invalid @enderror">
                                                        <h5>Admin User Name <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <input type="text" name="name" value="{{ old('name') }}"
                                                                class="form-control">
                                                            @error('name')
                                                                <div class="text-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div> <!-- end cold md 4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group  @error('email') is-invalid @enderror">
                                                        <h5>Admin Email <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <input type="email" name="email" value="{{ old('email') }}"
                                                                class="form-control">
                                                            @error('email')
                                                                <div class="text-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div> <!-- end cold md 4 -->
                                            </div> <!-- end row 	 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div
                                                        class="form-group  @error('profile_photo_path') is-invalid @enderror">
                                                        <h5>Admin User Image <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <input type="file" name="profile_photo_path"
                                                                class="form-control" id="image">
                                                            @error('profile_photo_path')
                                                                <div class="text-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div><!-- end cold md 6 -->
                                                <div class="col-md-6">
                                                    <img id="previewImage"
                                                        src="{{ asset('assets/images/no-avatar.jpg') }}"
                                                        style="width: 100px; height: 100px;">
                                                </div><!-- end cold md 6 -->
                                            </div><!-- end row 	 -->
                                            <hr>
                                            <div class="row">
                                                @foreach ($permissionGroup as $resource => $permission)
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <div class="controls">
                                                                <fieldset>
                                                                    <h5 class='text-capitalize'>{{ $resource }}
                                                                        Management</h5>
                                                                </fieldset>
                                                                @foreach ($permission as $key => $action)
                                                                    <fieldset>
                                                                        <input type="checkbox"
                                                                            id="checkbox_{{ $action->id }}"
                                                                            name="permissions[{{ $resource }}][]"
                                                                            value="{{ $action->name }}"
                                                                            @if (old("permissions.$resource.$key")) checked @endif>
                                                                        <label for="checkbox_{{ $action->id }}"
                                                                            class='text-capitalize'>{{ $action->name }}
                                                                        </label>
                                                                    </fieldset>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="text-xs-right">
                                                <input type="submit" class="btn btn-rounded btn-primary mb-5"
                                                    value="Add Admin User">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </section>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $('#image').change(function(e) {
            let render = new FileReader();
            render.onload = function(e) {
                $('#previewImage').attr('src', e.target.result);
            }
            render.readAsDataURL((e.target.files[0]))
        })
    </script>
@endpush
