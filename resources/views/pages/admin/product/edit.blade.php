@extends('layouts.admin.master')
@section('title', 'Dashboard')
@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        <a href="{{ route('admin.products.index') }}">Product All</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <!-- Basic Forms -->
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">Edit Product </h4>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col">
                                <form method="post" action="{{ route('admin.products.update', $product->id) }}"
                                    enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h5>Status <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <input type="checkbox" id="md_checkbox_21" name='status'
                                                                class="filled-in chk-col-primary" @if ($product->status === 1) checked @endif>
                                                            <label for="md_checkbox_21"></label>
                                                            @error('status')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div> <!-- end col md 4 -->
                                                <!-- start 1st row  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h5>Brand Select <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <select name="brand_id" class="form-control" required="">
                                                                <option value="" selected="" disabled="">Select Brand
                                                                </option>
                                                                @foreach ($brands as $brand)
                                                                    <option value="{{ $brand->id }}"
                                                                        {{ $product->brand_id === $brand->id ? 'selected' : '' }}>
                                                                        {{ $brand->name }}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('brand_id')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div> <!-- end col md 4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h5>Category Select <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <select name="menu_item_id" class="form-control" required="">
                                                                @foreach ($categories as $menu)
                                                                    <option value="{{ $menu['id'] }}"
                                                                        {{ $product->menu_item_id === $menu['id'] ? 'selected' : '' }}>
                                                                        {{ $menu['label'] }}
                                                                    </option>
                                                                    @foreach ($menu['child'] as $child)
                                                                        @include('pages.admin.product.menu-item-option',['menu'
                                                                        =>
                                                                        $child,'parent' => $menu,'product' => $product])
                                                                    @endforeach
                                                                @endforeach
                                                            </select>
                                                            @error('menu_item_id')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div> <!-- end col md 4 -->
                                            </div> <!-- end 1st row  -->
                                            <div class="row">
                                                <!-- start 2nd row  -->
                                                @foreach (LaravelLocalization::getSupportedLocales() as $key => $lang)
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <h5>Product Name {{ $lang['name'] }} <span
                                                                    class="text-danger">*</span></h5>
                                                            <div class="controls">
                                                                <input type="text"
                                                                    value="{{ $product->getTranslation('name', $key) }}"
                                                                    name="name[{{ $key }}]" class="form-control"
                                                                    required="">
                                                                @error('name.{{ $key }}')
                                                                    <span class="text-danger">{{ $message }}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div> <!-- end col md 4 -->
                                                @endforeach
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h5>Product Slug<span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <input type="text" name="slug" value="{{ $product->slug }}"
                                                                class="form-control" required="">
                                                            @error('slug')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div> <!-- end col md 4 -->
                                            </div> <!-- end 2nd row  -->
                                            <div class="row">
                                                <!-- start 3RD row  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h5>Product Code <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <input type="text" name="product_code"
                                                                value="{{ $product->product_code }}"
                                                                class="form-control" required="">
                                                            @error('product_code')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div> <!-- end col md 4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h5>Product Quantity <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <input type="text" name="product_qty"
                                                                value="{{ $product->product_qty }}"
                                                                class="form-control" required="">
                                                            @error('product_qty')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div> <!-- end col md 4 -->
                                                @foreach (LaravelLocalization::getSupportedLocales() as $key => $lang)
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <h5>Product Tags {{ $lang['name'] }} <span
                                                                    class="text-danger">*</span></h5>
                                                            <div class="controls">
                                                                <input type="text"
                                                                    value="{{ $product->getTranslation('tags', $key) }}"
                                                                    name="tags[{{ $key }}]" class="form-control"
                                                                    data-role="tagsinput" required="">
                                                                @error('tags.{{ $key }}')
                                                                    <span class="text-danger">{{ $message }}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div> <!-- end col md 4 -->
                                                @endforeach
                                            </div> <!-- end 3RD row  -->
                                            <div class="row">
                                                <!-- start 4th row  -->
                                                @foreach (LaravelLocalization::getSupportedLocales() as $key => $lang)
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <h5>Product Size {{ $lang['name'] }} <span
                                                                    class="text-danger">*</span></h5>
                                                            <div class="controls">
                                                                <input type="text"
                                                                    value="{{ $product->getTranslation('size', $key) }}"
                                                                    name="size[{{ $key }}]" class="form-control"
                                                                    value="Small,Midium,Large" data-role="tagsinput"
                                                                    required="">
                                                                @error('size.{{ $key }}')
                                                                    <span class="text-danger">{{ $message }}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div> <!-- end col md 4 -->
                                                @endforeach
                                            </div> <!-- end 4th row  -->
                                            <div class="row">
                                                <!-- start 5th row  -->
                                                @foreach (LaravelLocalization::getSupportedLocales() as $key => $lang)
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <h5>Product Color {{ $lang['name'] }} <span
                                                                    class="text-danger">*</span></h5>
                                                            <div class="controls">
                                                                <input type="text"
                                                                    value="{{ $product->getTranslation('color', $key) }}"
                                                                    name="color[{{ $key }}]"
                                                                    class="form-control" value="red,Black,Amet"
                                                                    data-role="tagsinput" required="">
                                                                @error('color.{{ $key }}')
                                                                    <span class="text-danger">{{ $message }}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div> <!-- end col md 4 -->
                                                @endforeach
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h5>Product Selling Price <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <input type="text" value="{{ $product->selling_price }}"
                                                                name="selling_price" class="form-control" required="">
                                                            @error('selling_price')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div> <!-- end col md 4 -->
                                            </div> <!-- end 5th row  -->
                                            <div class="row">
                                                <!-- start 6th row  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h5>Product Discount Price <span class="text-danger">*</span>
                                                        </h5>
                                                        <div class="controls">
                                                            <input type="text" value="{{ $product->discount_price }}"
                                                                name="discount_price" class="form-control">
                                                            @error('discount_price')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div> <!-- end col md 4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h5>Main Thumbnail <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <input type="file" name="product_thumbnail"
                                                                class="form-control" onChange="mainThumbUrl(this)">
                                                            @error('product_thumbnail')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                            <img width='80' height='80'
                                                                src="{{ asset($product->product_thumbnail) }}"
                                                                id="mainProdThumb">
                                                        </div>
                                                    </div>
                                                </div> <!-- end col md 4 -->

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h5>More Gallery Images <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <input type="file" name="images[]" class="form-control"
                                                                multiple="" id="multiImg">
                                                            @error('images')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                            <div class="row" id="preview_img"></div>

                                                        </div>
                                                    </div>
                                                </div> <!-- end col md 4 -->
                                            </div> <!-- end 6th row  -->
                                            <div class="row">
                                                <!-- start 7th row  -->
                                                @foreach (LaravelLocalization::getSupportedLocales() as $key => $lang)
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <h5>Short Description {{ $lang['name'] }} <span
                                                                    class="text-danger">*</span>
                                                            </h5>
                                                            <div class="controls">
                                                                <textarea name="short_description[{{ $key }}]"
                                                                    value="{{ $product->getTranslation('short_description', $key) }}"
                                                                    id="textarea" class="form-control" required
                                                                    placeholder="Textarea text"> {{ $product->getTranslation('short_description', $key) }}</textarea>
                                                            </div>
                                                        </div>
                                                    </div> <!-- end col md 6 -->
                                                @endforeach
                                            </div> <!-- end 7th row  -->
                                            <div class="row">
                                                <!-- start 8th row  -->
                                                @foreach (LaravelLocalization::getSupportedLocales() as $key => $lang)
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <h5>Description {{ $lang['name'] }} <span
                                                                    class="text-danger">*</span>
                                                            </h5>
                                                            <div class="controls">
                                                                <textarea id="editor1"
                                                                    value="{{ $product->getTranslation('description', $key) }}"
                                                                    name="description[{{ $key }}]" rows="10"
                                                                    cols="80"
                                                                    required="">{{ $product->getTranslation('description', $key) }}</textarea>
                                                            </div>
                                                        </div>
                                                    </div> <!-- end col md 6 -->
                                                @endforeach
                                            </div> <!-- end 8th row  -->
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <fieldset>
                                                                <input type="checkbox" id="checkbox_2" name="hot_deals"
                                                                    {{ $product->hot_deals == 1 ? 'checked' : '' }}
                                                                    value="1">
                                                                <label for="checkbox_2">Hot Deals</label>
                                                            </fieldset>
                                                            <fieldset>
                                                                <input type="checkbox" id="checkbox_3" name="featured"
                                                                    {{ $product->featured == 1 ? 'checked' : '' }}
                                                                    value="1">
                                                                <label for="checkbox_3">Featured</label>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <fieldset>
                                                                <input type="checkbox" id="checkbox_4" name="special_offer"
                                                                    {{ $product->special_offer == 1 ? 'checked' : '' }}
                                                                    value="1">
                                                                <label for="checkbox_4">Special Offer</label>
                                                            </fieldset>
                                                            <fieldset>
                                                                <input type="checkbox" id="checkbox_5" name="special_deals"
                                                                    {{ $product->special_deals == 1 ? 'checked' : '' }}
                                                                    value="1">
                                                                <label for="checkbox_5">Special Deals</label>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <h5>Digital Product <span class="text-danger">pdf,xlx,csv*</span>
                                                    </h5>
                                                    <div class="controls">
                                                        <input type="file" name="digital_file" class="form-control">
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->
                                            <div class="text-xs-right">
                                                <input type="submit" class="btn btn-rounded btn-primary mb-5"
                                                    value="Update Product">
                                            </div>
                                </form>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
            <!-- ///////////////// Start Multiple Image Update Area ///////// -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box bt-3 border-info">
                            <div class="box-header">
                                <h4 class="box-title">Product Multiple Image <strong>Update</strong></h4>
                            </div>
                            <form method="post" action="{{ route('admin.products.images.update') }}"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="row row-sm">
                                    @foreach ($product->images as $img)
                                        <div class="col-md-3">
                                            <div class="card">
                                                <img src="{{ asset($img->photo_name) }}" class="card-img-top"
                                                    style="height: 130px; object-fit:contain">
                                                <div class="card-body">
                                                    <h5 class="card-title">
                                                        <a href="{{ route('admin.products.image.delete', $img->id) }}"
                                                            class="btn btn-sm btn-danger"
                                                            onclick="if(confirm('Are you sure want to delete this image')) return true;else return false"
                                                            title="Delete Data"><i class="fa fa-trash"></i> </a>
                                                    </h5>
                                                    <p class="card-text">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Change Image <span
                                                                class="tx-danger">*</span></label>
                                                        <input class="form-control" type="file"
                                                            name="multi_img[{{ $img->id }}]">
                                                    </div>
                                                    </p>
                                                </div>
                                            </div>
                                        </div><!--  end col md 3		 -->
                                    @endforeach
                                </div>
                                <div class="text-xs-right px-20">
                                    <input type="submit" class="btn btn-rounded btn-primary mb-5" value="Update Image">
                                </div>
                                <br><br>
                            </form>
                        </div>
                    </div>
                </div> <!-- // end row  -->
            </section>
            <!-- ///////////////// End Start Multiple Image Update Area ///////// -->
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('backend/assets/vendor_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.js') }}">
    </script>
    <script type="text/javascript">
        function mainThumbUrl(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#mainProdThumb').attr('src', e.target.result).width(80).height(80);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script>
        $(document).ready(function() {
            $('#multiImg').on('change', function() { //on file input change
                if (window.File && window.FileReader && window.FileList && window
                    .Blob) //check File API supported browser
                {
                    var data = $(this)[0].files; //this file data
                    $.each(data, function(index, file) { //loop though each file
                        if (/(\.|\/)(gif|jpe?g|png)$/i.test(file
                                .type)) { //check supported file type
                            var fRead = new FileReader(); //new filereader
                            fRead.onload = (function(file) { //trigger function on successful read
                                return function(e) {
                                    var img = $('<img/>').addClass('thumb').attr('src',
                                            e.target.result).width(80)
                                        .height(80); //create image element 
                                    $('#preview_img').append(
                                        img); //append image to output element
                                };
                            })(file);
                            fRead.readAsDataURL(file); //URL representing the file's data.
                        }
                    });
                } else {
                    alert("Your browser doesn't support File API!"); //if File API is absent
                }
            });
        });
    </script>
@endpush
