@extends('layouts.admin.master')
@section('title', 'Product Create')
@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        Admin
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Product Create</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">

                <!-- Basic Forms -->
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">Add Product </h4>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col">
                                <form method="post" action="{{ route('admin.products.store') }}"
                                    enctype="multipart/form-data">
                                    @csrf

                                    <div class="row">
                                        <div class="col-12">


                                            <div class="row">
                                                <div class="col-md-4">

                                                    <div class="form-group">
                                                        <h5>Status <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <input type="checkbox" id="md_checkbox_21" name='status'
                                                                class="filled-in chk-col-primary" @if (old('status') === 'on') checked @endif>
                                                            <label for="md_checkbox_21"></label>
                                                            @error('status')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                </div> <!-- end col md 4 -->
                                                <!-- start 1st row  -->
                                                <div class="col-md-4">

                                                    <div class="form-group">
                                                        <h5>Brand Select <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <select name="brand_id" class="form-control" required="">
                                                                <option value="" disabled="">Select Brand
                                                                </option>
                                                                @foreach ($brands as $brand)
                                                                    <option value="{{ $brand->id }}"
                                                                        @if (old('brand_id') === $brand->id) selected @endif>
                                                                        {{ $brand->name }}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('brand_id')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                </div> <!-- end col md 4 -->

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h5>Category Select <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <select name="menu_item_id" class="form-control" required="">
                                                                @foreach ($categories as $menu)
                                                                    <option value="{{ $menu['id'] }}" @if(old('menu_item_id') == $menu['id']) selected @endif>
                                                                        {{ $menu['label'] }}
                                                                    </option>
                                                                    @foreach ($menu['child'] as $child)
                                                                        @include('pages.admin.product.menu-item-option',
                                                                        [
                                                                        'menu' => $child,
                                                                        'parent' => $menu,
                                                                        'product' => null,
                                                                        ])
                                                                    @endforeach
                                                                @endforeach
                                                            </select>
                                                            @error('menu_item_id')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div> <!-- end col md 4 -->
                                            </div> <!-- end 1st row  -->


                                            <div class="row">
                                                <!-- start 2nd row  -->
                                                @foreach (LaravelLocalization::getSupportedLocales() as $key => $lang)
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <h5>Product Name {{ $lang['name'] }} <span
                                                                    class="text-danger">*</span></h5>
                                                            <div class="controls">
                                                                <input type="text" name="name[{{ $key }}]"
                                                                    value="{{ old('name.' . $key) }}"
                                                                    class="form-control" required="">
                                                                @error('name.{{ $key }}')
                                                                    <span class="text-danger">{{ $message }}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div> <!-- end col md 4 -->
                                                @endforeach
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h5>Product Slug<span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <input type="text" name="slug" value="{{ old('slug') }}"
                                                                class="form-control" required="">
                                                            @error('slug')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div> <!-- end col md 4 -->
                                            </div> <!-- end 2nd row  -->

                                            <div class="row">
                                                <!-- start 3RD row  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h5>Product Code <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <input type="text" name="product_code"
                                                                value="{{ old('product_code') }}" class="form-control"
                                                                required="">
                                                            @error('product_code')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div> <!-- end col md 4 -->

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h5>Product Quantity <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <input type="text" name="product_qty"
                                                                value="{{ old('product_qty') }}" class="form-control"
                                                                required="">
                                                            @error('product_qty')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div> <!-- end col md 4 -->

                                                @foreach (LaravelLocalization::getSupportedLocales() as $key => $lang)
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <h5>Product Tags {{ $lang['name'] }} <span
                                                                    class="text-danger">*</span></h5>
                                                            <div class="controls">
                                                                <input type="text" name="tags[{{ $key }}]"
                                                                    class="form-control"
                                                                    value="{{ old('tags.' . $key) }}"
                                                                    data-role="tagsinput" required="">
                                                                @error('tags.{{ $key }}')
                                                                    <span class="text-danger">{{ $message }}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div> <!-- end col md 4 -->
                                                @endforeach
                                            </div> <!-- end 3RD row  -->

                                            <div class="row">
                                                <!-- start 4th row  -->
                                                @foreach (LaravelLocalization::getSupportedLocales() as $key => $lang)
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <h5>Product Size {{ $lang['name'] }} <span
                                                                    class="text-danger">*</span></h5>
                                                            <div class="controls">
                                                                <input type="text" name="size[{{ $key }}]"
                                                                    class="form-control"
                                                                    value="{{ old('size.' . $key) }}"
                                                                    data-role="tagsinput" required="">
                                                                @error('size.{{ $key }}')
                                                                    <span class="text-danger">{{ $message }}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div> <!-- end col md 4 -->
                                                @endforeach
                                            </div> <!-- end 4th row  -->

                                            <div class="row">
                                                <!-- start 5th row  -->
                                                @foreach (LaravelLocalization::getSupportedLocales() as $key => $lang)
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <h5>Product Color {{ $lang['name'] }} <span
                                                                    class="text-danger">*</span></h5>
                                                            <div class="controls">
                                                                <input type="text" name="color[{{ $key }}]"
                                                                    class="form-control"
                                                                    value="{{ old('color.' . $key) }}"
                                                                    data-role="tagsinput" required="">
                                                                @error('color.{{ $key }}')
                                                                    <span class="text-danger">{{ $message }}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div> <!-- end col md 4 -->
                                                @endforeach

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h5>Product Selling Price <span class="text-danger">*</span>
                                                        </h5>
                                                        <div class="controls">
                                                            <input type="text" name="selling_price"
                                                                value="{{ old('selling_price') }}" class="form-control"
                                                                required="">
                                                            @error('selling_price')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div> <!-- end col md 4 -->
                                            </div> <!-- end 5th row  -->

                                            <div class="row">
                                                <!-- start 6th row  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h5>Product Discount Price <span class="text-danger">*</span>
                                                        </h5>
                                                        <div class="controls">
                                                            <input type="text" name="discount_price"
                                                                value="{{ old('discount_price') }}"
                                                                class="form-control" required="">
                                                            @error('discount_price')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div> <!-- end col md 4 -->

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h5>Main Thambnail <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <input type="file" name="product_thumbnail"
                                                                class="form-control" onChange="mainThamUrl(this)">
                                                            @error('product_thumbnail')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                            <img src="" id="mainThmb">
                                                        </div>
                                                    </div>
                                                </div> <!-- end col md 4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h5>Multiple Image <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <input type="file" name="images[]" class="form-control"
                                                                multiple="" id="multiImg">
                                                            @error('images')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                            <div class="row" id="preview_img"></div>

                                                        </div>
                                                    </div>
                                                </div> <!-- end col md 4 -->
                                            </div> <!-- end 6th row  -->
                                            <div class="row">
                                                <!-- start 7th row  -->
                                                @foreach (LaravelLocalization::getSupportedLocales() as $key => $lang)
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <h5>Short Description {{ $lang['name'] }} <span
                                                                    class="text-danger">*</span>
                                                            </h5>
                                                            <div class="controls">
                                                                <textarea name="short_description[{{ $key }}]"
                                                                    id="textarea" class="form-control" required
                                                                    placeholder="Textarea text">{{ old("short_description.$key") }}</textarea>
                                                            </div>
                                                        </div>
                                                    </div> <!-- end col md 6 -->
                                                @endforeach
                                            </div> <!-- end 7th row  -->

                                            <div class="row">
                                                <!-- start 8th row  -->
                                                @foreach (LaravelLocalization::getSupportedLocales() as $key => $lang)
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <h5>Description {{ $lang['name'] }} <span
                                                                    class="text-danger">*</span>
                                                            </h5>
                                                            <div class="controls">
                                                                <textarea id="editor1"
                                                                    name="description[{{ $key }}]" rows="10"
                                                                    cols="80"
                                                                    required="">{{ old("description.$key") }}</textarea>
                                                            </div>
                                                        </div>
                                                    </div> <!-- end col md 6 -->
                                                @endforeach
                                            </div> <!-- end 8th row  -->

                                            <hr>

                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">

                                                        <div class="controls">
                                                            <fieldset>
                                                                <input type="checkbox" id="checkbox_2" name="hot_deals"
                                                                    @if (old('hot_deals') === 'on') checked @endif>
                                                                <label for="checkbox_2">Hot Deals
                                                                    {{ old('hot_deals') }}</label>
                                                            </fieldset>
                                                            <fieldset>
                                                                <input type="checkbox" id="checkbox_3" name="featured"
                                                                    @if (old('featured') === 'on') checked @endif>
                                                                <label for="checkbox_3">Featured
                                                                    {{ old('featured') }}</label>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">

                                                        <div class="controls">
                                                            <fieldset>
                                                                <input type="checkbox" id="checkbox_4" name="special_offer"
                                                                    @if (old('special_offer') === 'on') checked @endif>
                                                                <label for="checkbox_4">Special Offer</label>
                                                            </fieldset>
                                                            <fieldset>
                                                                <input type="checkbox" id="checkbox_5" name="special_deals"
                                                                    @if (old('special_deals') === 'on') checked @endif>
                                                                <label for="checkbox_5">Special Deals</label>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <h5>Digital Product <span class="text-danger">pdf,xlx,csv*</span>
                                                    </h5>
                                                    <div class="controls">
                                                        <input type="file" name="digital_file" class="form-control">
                                                    </div>
                                                </div>


                                            </div> <!-- end col md 4 -->

                                            <div class="text-xs-right">
                                                <input type="submit" class="btn btn-rounded btn-primary mb-5"
                                                    value="Add Product">
                                            </div>
                                </form>

                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </section>
            <!-- /.content -->
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        function mainThamUrl(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#mainThmb').attr('src', e.target.result).width(80).height(80);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

    <script>
        $(document).ready(function() {
            $('#multiImg').on('change', function() { //on file input change
                if (window.File && window.FileReader && window.FileList && window
                    .Blob) //check File API supported browser
                {
                    var data = $(this)[0].files; //this file data

                    $.each(data, function(index, file) { //loop though each file
                        if (/(\.|\/)(gif|jpe?g|png)$/i.test(file
                                .type)) { //check supported file type
                            var fRead = new FileReader(); //new filereader
                            fRead.onload = (function(file) { //trigger function on successful read
                                return function(e) {
                                    var img = $('<img/>').addClass('thumb').attr('src',
                                            e.target.result).width(80)
                                        .height(80); //create image element
                                    $('#preview_img').append(
                                        img); //append image to output element
                                };
                            })(file);
                            fRead.readAsDataURL(file); //URL representing the file's data.
                        }
                    });

                } else {
                    alert("Your browser doesn't support File API!"); //if File API is absent
                }
            });
        });
    </script>
@endpush
