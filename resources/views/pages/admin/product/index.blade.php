@extends('layouts.admin.master')
@section('title', 'Product Management')
@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        Admin
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Product All</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <a href="{{route('admin.products.delete-all')}}" class="btn btn-danger">Delete</a>
                                <div class="pull-left">
                                    <form action="{{ route('admin.products.import-data') }}" method="post"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row">
                                            <div class="col-lg-12">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name='import_file'
                                                        id="customFile">
                                                    <label class="custom-file-label text-white" for="customFile">Import Data
                                                        File</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-10">
                                                <input type="checkbox" name='header' id="basic_checkbox_1" checked />
                                                <label for="basic_checkbox_1">File contains header row?</label>
                                            </div>
                                        </div>
                                        <button type='submit' style='height:calc(1.5em + 0.75rem + 2px);line-height:0'
                                            class='btn btn-secondary'>Import</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                    {!! $dataTable->table([], true) !!}
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
    </div>
@endsection
@push('scripts')
    {!! $dataTable->scripts() !!}
@endpush
