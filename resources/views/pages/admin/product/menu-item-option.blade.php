<option value="{{ $menu['id'] }}" @if (!is_null($product))
    {{ $product->menu_item_id === $menu['id'] ? 'selected' : '' }}
@else {{ old('menu_item_id') == $menu['id'] ? 'selected' : '' }}
    @endif>
    @if ($parent)
        {{ $parent['label'] }}
        /
    @endif
    {{ $menu['label'] }}
</option>

@if ($menu['child'])
    @foreach ($menu['child'] as $child)
        @include('pages.admin.product.menu-item-option', ['menu' => $child,'parent' => $menu,'product' => $product])
    @endforeach
@endif
