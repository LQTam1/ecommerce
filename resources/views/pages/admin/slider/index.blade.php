@extends('layouts.admin.master')
@section('title', 'Slider Manager')

@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        Admin
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Slider All</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-md-8">
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                    {{ $dataTable->table([], true) }}
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box box-widget widget-user p-5">
                                <h3>Add Slider</h3>
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <form method='post' action="{{ route('admin.sliders.store') }}"
                                    enctype="multipart/form-data">
                                    @csrf
                                    @foreach (LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                        <div class="form-group @error('title.{{ $localeCode }}') is-invalid @enderror">
                                            <h5>Title {{ $properties['native'] }} <span class="text-danger">*</span>
                                            </h5>
                                            <div class="controls">
                                                <input type="text" name="title[{{ $localeCode }}]"
                                                    value="{{ old("title.$localeCode") }}" class="form-control">
                                            </div>
                                            @error('title.{{ $localeCode }}')
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    @endforeach
                                    @foreach (LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                        <div class="form-group @if ('description.{{ $localeCode }}')) is-invalid @endif">
                                            <h5>Description {{ $properties['native'] }} <span
                                                    class="text-danger">*</span>
                                            </h5>
                                            <div class="controls">
                                                <textarea type="text" id="description"
                                                    value="{{ old("description.$localeCode") }}"
                                                    name="description[{{ $localeCode }}]"
                                                    class="form-control">{{ old("description.$localeCode") }}</textarea>
                                            </div>
                                            @error('description.{{ $localeCode }}')
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    @endforeach

                                    <div class="form-group @error('slider_img') is-invalid @enderror">
                                        <h5>Image</h5>
                                        <div class="controls mb-2">
                                            <input type="file" id="image" name="slider_img" class="form-control">
                                        </div>
                                        @error('slider_img')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                        <img class="border rounded-circle" id="previewImage"
                                            src="{{ asset('backend/assets/images/no_image.jpg') }}" width='100'
                                            height='100' alt="Slider name">
                                    </div>
                                    <div class="text-xs-right">
                                        <button type="submit"
                                            class="btn btn-rounded btn-primary mb-5 pull-right">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </section>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $('#image').change(function(e) {
            let render = new FileReader();
            render.onload = function(e) {
                $('#previewImage').attr('src', e.target.result);
            }
            render.readAsDataURL((e.target.files[0]))
        })
    </script>
    {!! $dataTable->scripts() !!}
@endpush
