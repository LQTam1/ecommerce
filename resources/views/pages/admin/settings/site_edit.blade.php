@extends('layouts.admin.master')
@section('title', 'Update Site Setting')
@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.reports.index') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        Admin
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Update Site Setting</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col">
                        <form method="post" action="{{ route('admin.settings.siteSetting.update', $siteSetting->id) }}"
                            enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <h5>Site Logo <span class="text-danger"> </span></h5>
                                        <div class="controls">
                                            <input type="file" name="logo" id='logo' class="form-control">
                                            <img class='mt-4' src="{{asset($siteSetting->logo)}}" id='previewLogo' width='139' height='36'/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <h5>Phone One <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="phone1" class="form-control"
                                                value="{{ $siteSetting->phone1 }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <h5>Phone Two <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="phone2" class="form-control"
                                                value="{{ $siteSetting->phone2 }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <h5>Email <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="email" name="email" class="form-control"
                                                value="{{ $siteSetting->email }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <h5>Company Name <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="company_name" class="form-control"
                                                value="{{ $siteSetting->company_name }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <h5>Company Address <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="company_address" class="form-control"
                                                value="{{ $siteSetting->company_address }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <h5>Facebook <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="facebook" class="form-control"
                                                value="{{ $siteSetting->facebook }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <h5>Twitter <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="twitter" class="form-control"
                                                value="{{ $siteSetting->twitter }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <h5>Linkedin <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="linkedin" class="form-control"
                                                value="{{ $siteSetting->linkedin }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <h5>Youtube <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="youtube" class="form-control"
                                                value="{{ $siteSetting->youtube }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 d-flex align-items-center">
                                    <div class="form-group mb-0">
                                        <h5></h5>
                                        <div class="controls">
                                            <input type="submit" class="btn btn-rounded btn-primary" value="Update">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </section>
        </div>
    </div>
@endsection
@push('scripts')
<script>
     $('#logo').change(function(e) {
            let render = new FileReader();
            render.onload = function(e) {
                $('#previewLogo').attr('src', e.target.result);
            }
            render.readAsDataURL((e.target.files[0]))
        })
</script>
@endpush