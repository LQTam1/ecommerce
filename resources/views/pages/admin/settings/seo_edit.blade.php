@extends('layouts.admin.master')
@section('title', 'Update Seo Setting')
@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.reports.index') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        Admin
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Update Seo Setting</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col">
                        <form method="post" action="{{ route('admin.settings.seoSetting.update', $siteSetting->id) }}"
                            enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <h5>Meta Title <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="meta_title" class="form-control"
                                                value="{{ $seoSetting->meta_title }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <h5>Meta Author <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="meta_author" class="form-control"
                                                value="{{ $seoSetting->meta_author }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <h5>Meta Keyword <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="meta_keyword" class="form-control"
                                                value="{{ $seoSetting->meta_keyword }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <h5>Meta Description <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <textarea rows='5' name="meta_description" class="form-control" required placeholder="Textarea text">{{ $seoSetting->meta_description }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <h5>Google Analytics <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <textarea rows='5' name="google_analytics" class="form-control" required placeholder="Textarea text">{{ $seoSetting->google_analytics }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 d-flex align-items-center">
                                    <div class="form-group mb-0">
                                        <h5></h5>
                                        <div class="controls">
                                            <input type="submit" class="btn btn-rounded btn-primary" value="Update">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </section>
        </div>
    </div>
@endsection