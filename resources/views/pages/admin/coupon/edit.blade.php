@extends('layouts.admin.master')
@section('title', 'Coupon Edit')

@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        <a href="{{ route('admin.coupons.index') }}">Coupon All</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box box-widget widget-user p-5">
                                <h3>Edit Coupon</h3>
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <form method='post' action="{{ route('admin.coupons.update', $coupon->id) }}">
                                    @csrf
                                    @method('put')
                                    <div class="form-group @error('status') is-invalid @enderror">
                                        <h5>Active <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="checkbox" {{ $coupon->status === 1 ? 'checked' : '' }}
                                                name='status' id="checkbox_123">
                                            <label for="checkbox_123" class="block"></label>
                                        </div>
                                        @error('status')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group @error('name') is-invalid @enderror">
                                        <h5>Name <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="name" value="{{ $coupon->name }}"
                                                class="form-control">
                                        </div>
                                        @error('name')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group @error('type') is-invalid @enderror">
                                        <h5>Type <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="type" value="{{ $coupon->type }}"
                                                class="form-control">
                                        </div>
                                        @error('type')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group @error('value') is-invalid @enderror">
                                        <h5>Value <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" id="value" name="value" value="{{ $coupon->value }}"
                                                class=" form-control">
                                        </div>
                                        @error('value')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group @error('expires') is-invalid @enderror">
                                        <h5>Expires <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="date" name="expires" class="form-control"
                                                value="{{ $coupon->expires }}"
                                                min="{{ Carbon\Carbon::now()->format('Y-m-d') }}">
                                        </div>
                                        @error('expires')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="text-xs-right">
                                        <button type="submit"
                                            class="btn btn-rounded btn-primary mb-5 pull-right">Update</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </section>
        </div>
    </div>
@endsection
