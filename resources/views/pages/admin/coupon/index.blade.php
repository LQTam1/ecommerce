@extends('layouts.admin.master')
@section('title', 'Coupon Management')

@section('content')
    <div class="content-wrapper">
        <div class="container-full">
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        Admin
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Coupon All</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-md-8">
                        <div class="box">
                            {{-- <div class="box-header with-border">
                            <h3 class="box-title">Data Table With Full Features</h3>
                        </div> --}}
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <th>Value</th>
                                                <th>Expires</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($coupons as $coupon)
                                                <tr>
                                                    <td>{{ $coupon->name }}</td>
                                                    <td>{{ $coupon->type }}</td>
                                                    <td>{{ $coupon->value }}</td>
                                                    <td>
                                                        @if ($coupon->expires >= Carbon\Carbon::now()->format('Y-m-d'))
                                                            <span class="badge badge-pill badge-success"> Valid </span>
                                                        @else
                                                            <span class="badge badge-pill badge-danger"> Invalid </span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($coupon->status === 1)
                                                            <span class="badge badge-pill badge-success"> Active </span>
                                                        @else
                                                            <span class="badge badge-pill badge-danger"> InActive </span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('admin.coupons.toggle-status', $coupon->id) }}"
                                                            class="btn btn-sm btn-{{ $coupon->status === 1 ? 'danger' : 'success' }}"
                                                            title="Switch Status {{ $coupon->status === 1 ? 'off' : 'on' }}"><i
                                                                class="fa fa-toggle-{{ $coupon->status === 1 ? 'off' : 'on' }}"></i>
                                                        </a>
                                                        <a href="{{ route('admin.coupons.edit', $coupon->id) }}"
                                                            title="Edit Coupon" class="btn btn-primary btn-sm fa fa-pencil">
                                                        </a>
                                                        <a href="#" id='delete' title='Delete Coupon'
                                                            class="btn btn-danger btn-sm fa fa-trash">
                                                        </a>
                                                        <form class='btn-block d-none' id='formDelete'
                                                            action="{{ route('admin.coupons.destroy', $coupon->id) }}"
                                                            method="POST">
                                                            @csrf
                                                            @method('delete')
                                                            <button type='submit' class="btn btn-danger btn-sm fa fa-trash">
                                                            </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <th>Value</th>
                                                <th>Expires</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    {{ $coupons->links() }}
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box box-widget widget-user p-5">
                                <h3>Add Coupon</h3>
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <form method='post' action="{{ route('admin.coupons.store') }}">
                                    @csrf
                                    <div class="form-group @error('status') is-invalid @enderror">
                                        <h5>Active <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="checkbox" name='status' id="checkbox_123">
                                            <label for="checkbox_123" class="block"></label>
                                        </div>
                                        @error('status')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group @error('name') is-invalid @enderror">
                                        <h5>Name <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="name" value="{{ old('name') }}"
                                                class="form-control">
                                        </div>
                                        @error('name')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group @error('type') is-invalid @enderror">
                                        <h5>Type <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="type" value="{{ old('type') }}"
                                                class="form-control">
                                        </div>
                                        @error('type')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group @if ($errors->first('value')) is-invalid @endif">
                                        <h5>Value <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" id="value" name="value" value="{{ old('value') }}"
                                                class="form-control">
                                        </div>
                                        @if ($errors->first('value'))
                                            <div class="text-danger">{{ $errors->first('value') }}</div>
                                        @endif
                                    </div>

                                    <div class="form-group @if ($errors->first('expires')) is-invalid @endif">
                                        <h5>Expires <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="date" name="expires" value="{{ old('expires') }}"
                                                class="form-control" min="{{ Carbon\Carbon::now()->format('Y-m-d') }}">
                                        </div>
                                        @if ($errors->first('expires'))
                                            <div class="text-danger">{{ $errors->first('expires') }}</div>
                                        @endif
                                    </div>

                                    <div class="text-xs-right">
                                        <button type="submit"
                                            class="btn btn-rounded btn-primary mb-5 pull-right">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </section>
        </div>
    </div>
@endsection
