@extends('layouts.master')
@section('title', 'Order Details')
@section('content')
    <div class="body-content">
        <div class="container">
            <div class="row">
                <x-user.sidebar />
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class='text-4xl py-3'>Shipping Details</h4>
                                </div>
                                <hr>
                                <div class="card-body" style="background: #E9EBEC;">
                                    <table class="table">
                                        <tr>
                                            <th> Shipping Name : </th>
                                            <th> {{ $order->name }} </th>
                                        </tr>
                                        <tr>
                                            <th> Shipping Phone : </th>
                                            <th> {{ $order->phone }} </th>
                                        </tr>
                                        <tr>
                                            <th> Shipping Email : </th>
                                            <th> {{ $order->email }} </th>
                                        </tr>
                                        <tr>
                                            <th> Address : </th>
                                            <th>
                                                <p>{{ $shipping->line1 }}</p>
                                                <p>{{ $shipping->city . ', ' . $shipping->state . ', ' . $shipping->country . ', ' . $shipping->zip }}
                                                </p>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th> Order Date : </th>
                                            <th> {{ $order->order_date }} </th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- // end col md -5 -->
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class='text-4xl py-3'>Order Details
                                        <span class="text-danger"> Invoice : {{ $order->invoice_no }}</span>
                                    </h4>
                                </div>
                                <hr>
                                <div class="card-body" style="background: #E9EBEC;">
                                    <table class="table">
                                        <tr>
                                            <th> Name : </th>
                                            <th> {{ $order->name }} </th>
                                        </tr>
                                        <tr>
                                            <th> Phone : </th>
                                            <th> {{ $order->phone }} </th>
                                        </tr>
                                        <tr>
                                            <th> Payment Type : </th>
                                            <th> {{ $order->payment_method }} </th>
                                        </tr>
                                        @if ($order->transaction_id)
                                            <tr>
                                                <th> Tranx ID : </th>
                                                <th> {{ $order->transaction_id }} </th>
                                            </tr>
                                        @endif
                                        <tr>
                                            <th> Invoice : </th>
                                            <th class="text-danger"> {{ $order->invoice_no }} </th>
                                        </tr>
                                        <tr>
                                            <th> Order Total : </th>
                                            <th>${{ $order->amount }} </th>
                                        </tr>
                                        <tr>
                                            <th> Order : </th>
                                            <th>
                                                <span class="badge badge-pill badge-warning"
                                                    style="background: #418DB9;">{{ $order->status }} </span>
                                            </th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- // 2ND end col md -5 -->
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr style="background: #e2e2e2;">
                                        <td>
                                            <label for=""> Image</label>
                                        </td>
                                        <td>
                                            <label for=""> Product Name </label>
                                        </td>
                                        <td>
                                            <label for=""> Product Code</label>
                                        </td>
                                        <td>
                                            <label for=""> Color </label>
                                        </td>
                                        <td>
                                            <label for=""> Size </label>
                                        </td>
                                        <td>
                                            <label for=""> Quantity </label>
                                        </td>
                                        <td>
                                            <label for=""> Price </label>
                                        </td>
                                        <td>
                                            <label for=""> Download </label>
                                        </td>
                                    </tr>
                                    @foreach ($order->items as $item)
                                        <tr>
                                            <td>
                                                <label for=""><img src="{{ asset($item->product->product_thumbnail) }}"
                                                        height="50px;" width="50px;"> </label>
                                            </td>
                                            <td>
                                                <label for=""> {{ $item->product->name }}</label>
                                            </td>
                                            <td>
                                                <label for=""> {{ $item->product->product_code }}</label>
                                            </td>
                                            <td>
                                                <label for=""> {{ $item->color }}</label>
                                            </td>
                                            <td>
                                                <label for=""> {{ $item->size }}</label>
                                            </td>
                                            <td>
                                                <label for=""> {{ $item->qty }}</label>
                                            </td>
                                            <td>
                                                <label for=""> ${{ $item->price }} ( ${{ $item->price * $item->qty }}
                                                    ) </label>
                                            </td>
                                            <td>
                                                @if ($order->status === 'pending')
                                                    <strong>
                                                        <span class="badge badge-pill badge-success"
                                                            style="background: #418DB9;"> No File</span> </strong>
                                                @else
                                                    <a target="_blank" href="{{ asset($item->product->digital_file) }}">
                                                        <strong>
                                                            <span class="badge badge-pill badge-success"
                                                                style="background: #FF0000;"> Download Ready</span>
                                                        </strong> </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div> <!-- // END ORDER ITEM ROW -->
                    <div class="row mb-10">
                        @if ($order->status === 'delivered')
                            @if ($order->return_reason === null)
                                <form action="{{ route('user.order.return', $order->id) }}" method="post">
                                    @csrf
                                    <div class="form-group @if ('return_reason') is-invalid @endif">
                                        <label for="label"> Order Return Reason:</label>
                                        <textarea name="return_reason" id="" class="form-control" cols="30"
                                            rows="05">Return Reason</textarea>
                                        @error('return_reason')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <button type="submit" class="btn btn-danger">Order Return</button>
                                </form>
                            @else
                                <span class="badge badge-pill badge-warning" style="background: red">You Have send return
                                    request
                                    for this product</span>
                            @endif
                        @endif
                    </div>
                </div> <!-- // end col md 10 -->
            </div> <!-- // end row -->
        </div>
    </div>
@endsection
