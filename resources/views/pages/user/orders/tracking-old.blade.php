@extends('layouts.master')
@section('title', 'Tracking Order Page')
@push('head')
    <style>
        .step>.icon {
            width: 10px
        }

        .step>.icon>i {
            left: -11px;
            transform: translateY(-50%)
        }

        .step>.icon:after {
            content: "";
            position: absolute;
            left: 0;
            bottom: 41px;
            width: 100%;
            height: 10px;
            background: rgba(156, 163, 175, var(--tw-bg-opacity))
        }

        .step.active>.icon:after {
            background: rgba(59, 130, 246, var(--tw-bg-opacity))
        }

        @media screen and (min-width: 768px) {
            .step>.icon {
                width: 100%
            }

            .step>.icon>i {
                left: 0;
            }
        }

    </style>
@endpush
@section('content')
    <div class="container">
        <article class="card ">
            <header class="px-3 py-5 bg-white border-b-2 text-2xl"> <b> Orders / Tracking </b> </header>
            <div class="card-body">
                <div class="container track flex bg-white py-10 flex-col items-start md:items-center   md:flex-row">
                    <div
                        class="step w-full text-left pl-2 md:pl-0 sm:text-center xs:w-1/2 sm:w-1/3 md:w-1/4  relative   {{ $trackOrder->order_date ? 'active' : '' }}">
                        <div class="step-info pl-11 md:pl-0 h-40 md:h-44 lg:h-40">
                            <b> Invoice Number </b><br>
                            {{ $trackOrder->invoice_no }}
                        </div> <!-- // end col md 2 -->
                        <span
                            class="icon absolute md:static left-2  h-full top-0 md:w-10 md:h-10 {{ $trackOrder->order_date ? 'bg-blue-500 md:bg-transparent' : 'bg-gray-400 md:bg-transparent' }} md:rounded-full md:p-3">
                            <i class="relative top-4 rounded-full bg-blue-500 p-4 fa z-10 fa-check "></i></span>
                        <div class="text mt-2 pl-11 md:pl-0">Order Pending</div>
                    </div>
                    <div
                        class="step w-full text-left pl-2 md:pl-0 sm:text-center xs:w-1/2 sm:w-1/3 md:w-1/4  relative  {{ $trackOrder->confirmed_date ? 'active' : '' }}">
                        <div class="step-info pl-11 md:pl-0 h-40 md:h-44 lg:h-40">
                            <b> Order Date </b><br>
                            {{ $trackOrder->order_date }}
                        </div> <!-- // end col md 2 -->

                        <span
                            class="icon absolute md:static left-2  h-full top-0 md:w-10 md:h-10 {{ $trackOrder->confirmed_date ? 'bg-blue-500 md:bg-transparent' : 'bg-gray-400 md:bg-transparent' }} md:rounded-full md:p-3">
                            <i class="relative top-4 rounded-full bg-blue-500 p-4 fa z-10 fa-check "></i> </span>
                        <div class="text mt-2 pl-11 md:pl-0"> Order Confirmed</div>
                    </div>
                    <div
                        class="step w-full text-left pl-2 md:pl-0 sm:text-center xs:w-1/2 sm:w-1/3 md:w-1/4  relative  {{ $trackOrder->processing_date ? 'active' : '' }}">
                        <div class="step-info pl-11 md:pl-0 h-40 md:h-44 lg:h-40">
                            <b> Shipping By - {{ $trackOrder->name }} </b><br>
                            {{ $trackOrder->shipping->city . ', ' . $trackOrder->shipping->state . ', ' . $trackOrder->shipping->countryModel->country_name }}
                        </div> <!-- // end col md 2 -->

                        <span
                            class="icon absolute md:static left-2  h-full top-0 md:w-10 md:h-10 {{ $trackOrder->processing_date ? 'bg-blue-500 md:bg-transparent' : 'bg-gray-400 md:bg-transparent' }} md:rounded-full md:p-3"><i
                                class="relative top-4 rounded-full bg-blue-500 p-4 fa z-10 fa-check "></i> </span>
                        <div class="text mt-2 pl-11 md:pl-0"> Order Processing </div>
                    </div>
                    <div
                        class="step w-full text-left pl-2 md:pl-0 sm:text-center xs:w-1/2 sm:w-1/3 md:w-1/4  relative  {{ $trackOrder->picked_date ? 'active' : '' }}">
                        <div class="step-info pl-11 md:pl-0 h-40 md:h-44 lg:h-40">
                            <b> User Mobile Number </b><br>
                            {{ $trackOrder->phone }}
                        </div> <!-- // end col md 2 -->

                        <span
                            class="icon absolute md:static left-2  h-full top-0 md:w-10 md:h-10 {{ $trackOrder->picked_date ? 'bg-blue-500 md:bg-transparent' : 'bg-gray-400 md:bg-transparent' }} md:rounded-full md:p-3">
                            <i class="relative top-4 rounded-full bg-blue-500 p-4 fa z-10 fa-check "></i> </span>
                        <div class="text mt-2 pl-11 md:pl-0">Order Picked</div>
                    </div>
                    <div
                        class="step w-full text-left pl-2 md:pl-0 sm:text-center xs:w-1/2 sm:w-1/3 md:w-1/4  relative  {{ $trackOrder->shipped_date ? 'active' : '' }}">
                        <div class="step-info pl-11 md:pl-0 h-40 md:h-44 lg:h-40">
                            <b> Payment Method </b><br>
                            {{ $trackOrder->payment_method }} <span
                                class="label label-success">{{ $trackOrder->transaction_id ? 'Paid' : '' }}</span>
                        </div> <!-- // end col md 2 -->

                        <span
                            class="icon absolute md:static left-2  h-full top-0 md:w-10 md:h-10 {{ $trackOrder->shipped_date ? 'bg-blue-500 md:bg-transparent' : 'bg-gray-400 md:bg-transparent' }} md:rounded-full md:p-3">
                            <i class="relative top-4 rounded-full bg-blue-500 p-4 fa z-10 fa-check "></i> </span>
                        <div class="text mt-2 pl-11 md:pl-0">Order Shipped </div>
                    </div>
                    <div
                        class="step w-full text-left pl-2 md:pl-0 sm:text-center xs:w-1/2 sm:w-1/3 md:w-1/4  relative  {{ $trackOrder->delivered_date ? 'active' : '' }}">
                        <div class="step-info pl-11 md:pl-0 h-40 md:h-44 lg:h-40">
                            <b> Total Amount </b><br>
                            $ {{ $trackOrder->amount }}
                        </div> <!-- // end col md 2 -->
                        <span
                            class="icon absolute md:static left-2 w-1.5 h-full top-0 md:w-10 md:h-10 {{ $trackOrder->delivered_date ? 'bg-blue-500 md:bg-transparent' : 'bg-gray-400 md:bg-transparent' }} md:rounded-full md:p-3"><i
                                class="relative top-4 rounded-full bg-blue-500 p-4 fa z-10 fa-check "></i> </span>
                        <div class="text mt-2 pl-11 md:pl-0">Delivered </div>
                    </div>
                </div> <!-- // end track  -->
                <hr><br><br>
            </div>
        </article>
    </div>
@endsection
