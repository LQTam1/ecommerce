@extends('layouts.master')
@section('title', 'Tracking Order Page')
@push('head')
    <style>
        @import url('https://fonts.googleapis.com/css?family=Open+Sans&display=swap');

        body {
            background-color: #eeeeee;
            font-family: 'Open Sans', serif
        }

        .card {
            word-wrap: break-word;
        }

        .track .step {
            -webkit-box-flex: 1;
            -ms-flex-positive: 1;
            margin-top: -18px;
        }

        .track .step.active:before {
            background: #FF5722
        }

        .track .step::before {
            height: 7px;
            position: absolute;
            content: "";
            width: 100%;
            left: 0;
            top: 18px
        }

        .track .step.active .icon {
            background: #ee5435;
            color: #fff
        }

        .track .icon {
            display: inline-block;
            width: 40px;
            height: 40px;
            line-height: 40px;
            position: relative;
            border-radius: 100%;
            background: #ddd
        }

        .track .step.active .text {
            font-weight: 400;
            color: #000
        }

        .track .text {
            display: block;
            margin-top: 7px
        }

        .btn-warning {
            color: #ffffff;
            background-color: #ee5435;
            border-color: #ee5435;
            border-radius: 1px
        }

        .btn-warning:hover {
            color: #ffffff;
            background-color: #ff2b00;
            border-color: #ff2b00;
            border-radius: 1px
        }

        @media screen and (max-width: 430px) {
            .list-items {
                flex-wrap: wrap
            }
        }

    </style>
@endpush
@section('content')
    <div class="container py-16">
        <article class="card relative flex flex-col bg-white min-w-0 bg-clip-border border">
            <header class="card-header text-3xl p-5 bg-white border-b"> My Orders / Tracking </header>
            <div class="card-body px-5">
                <h6 class='text-2xl py-5'>Order ID: {{ $trackOrder->order_number }}</h6>
                <article class="card border py-5 px-5">
                    <div class="card-body flex justify-around flex-wrap ">
                        <div class="col w-1/2 sm:w-1/4"> <strong>Estimated Delivery time:</strong> <br>29 nov 2019 </div>
                        <div class="col w-1/2 sm:w-1/4"> <strong>Shipping BY:</strong> <br>
                            {{ $shipping->fullName }} | <i class="fa fa-phone"></i>
                            {{ $shipping->phone }} </div>
                        <div class="col w-1/2 sm:w-1/4"> <strong>Status:</strong> <br> Picked by the courier </div>
                        <div class="col w-1/2 sm:w-1/4"> <strong>Tracking #:</strong> <br> {{$trackOrder->invoice_no}} </div>
                    </div>
                </article>
                <div class="track relative bg-gray-400 h-2 flex mb-28 mt-16">
                    <div
                        class="step {{ $trackOrder->confirmed_date ? 'active' : '' }} flex-grow w-1/4 text-center relative">
                        <span class="icon {{ $trackOrder->confirmed_date ? 'bg-red-500' : 'bg-gray-400' }}   text-white">
                            <i class="fa fa-check"></i> </span> <span class="text">Order confirmed</span>
                    </div>
                    <div class="step {{ $trackOrder->picked_date ? 'active' : '' }} flex-grow w-1/4 text-center relative">
                        <span class="icon {{ $trackOrder->picked_date ? 'bg-red-500' : 'bg-gray-400' }}  text-white"> <i
                                class="fa fa-user"></i> </span> <span class="text"> Picked by courier</span>
                    </div>
                    <div
                        class="step {{ $trackOrder->shipped_date ? 'active' : '' }} flex-grow w-1/4 text-center relative">
                        <span class="icon {{ $trackOrder->shipped_date ? 'bg-red-500' : 'bg-gray-400' }} text-white"> <i
                                class="fa fa-truck"></i> </span> <span class="text"> On the way </span> </div>
                    <div
                        class="step {{ $trackOrder->delivered_date ? 'active' : '' }} flex-grow w-1/4 text-center relative">
                        <span class="icon {{ $trackOrder->delivered_date ? 'bg-red-500' : 'bg-gray-400' }}text-white"> <i
                                class="fa fa-archive {{ $trackOrder->delivered_date ? 'text-white' : 'text-black' }}"></i>
                        </span> <span class="text">Ready for pickup</span> </div>
                </div>
                <hr>
                <ul class="list-items flex py-5">
                    @foreach ($trackOrder->items as $order)
                        <li class="col col-xs-6 col-sm-6 col-md-3 col-lg-3 px-0">
                            <figure class="itemside mb-3 flex flex-col sm:flex-row">
                                <div class="aside"><img src="{{ asset($order->product->product_thumbnail) }}"
                                        alt="{{ $order->product->name }}" class="w-32 h-w-32 p-2 border">
                                </div>
                                <figcaption class="info align-self-center pl-0 sm:pl-4">
                                    <p class="title">{{ $order->product->name }} <br>
                                        {{ $order->product->color }} - {{ $order->product->size }}<br>
                                        <span class="text-muted">${{ $order->product->selling_price }} </span>
                                </figcaption>
                            </figure>
                        </li>
                    @endforeach
                </ul>
                <hr> <a href="{{route('user.orders.index')}}" class="my-5 btn btn-warning" data-abc="true"> <i class="fa fa-chevron-left"></i> Back to
                    orders</a>
            </div>
        </article>
    </div>
@endsection
