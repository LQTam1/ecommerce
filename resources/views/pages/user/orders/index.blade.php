@extends('layouts.master')
@section('title', 'My Orders')
@section('content')
    <div class="body-content">
        <div class="container">
            <div class="row">
                <x-user.sidebar />
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="text-center text-4xl">
                                        Order List
                                    </h3>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                                <tr style="background: #e2e2e2;">
                                                    <td class="col-md-1">
                                                        <label for=""> Order Date</label>
                                                    </td>
                                                    <td class="col-md-3">
                                                        <label for=""> Total</label>
                                                    </td>
                                                    <td class="col-md-3">
                                                        <label for=""> Payment</label>
                                                    </td>
                                                    <td class="col-md-2">
                                                        <label for=""> Invoice</label>
                                                    </td>
                                                    <td class="col-md-2">
                                                        <label for=""> Order Status</label>
                                                    </td>
                                                    <td class="col-md-1">
                                                        <label for=""> Action </label>
                                                    </td>
                                                </tr>
                                                @foreach ($orders as $order)
                                                    <tr>
                                                        <td class="col-md-1">
                                                            <label for="">{{ $order->order_date }}</label>
                                                        </td>
                                                        <td class="col-md-3">
                                                            <label for=""> ${{ $order->amount }}</label>
                                                        </td>
                                                        <td class="col-md-3">
                                                            <label for=""> {{ $order->payment_method }}</label>
                                                        </td>
                                                        <td class="col-md-2">
                                                            <label for=""> {{ $order->invoice_no }}</label>
                                                        </td>
                                                        <td class="col-md-2">
                                                            <label for="">
                                                                @switch ($order->status)
                                                                    @case( 'delivered')
                                                                        @if ($order->return_order === 1)
                                                                            <span class="badge badge-pill badge-warning"
                                                                                style="background:red;">Return
                                                                                Requested
                                                                                ({{ $order->return_date }})</span>
                                                                        @elseif ($order->return_order ==
                                                                            2)
                                                                            <span class="badge badge-pill badge-success">Return
                                                                                Approved
                                                                                ({{ $order->return_date }})
                                                                            </span>
                                                                        @endif
                                                                        @php
                                                                            $badgeColor = '#008000';
                                                                            $statusDateColumn = 'delivered_date';
                                                                        @endphp
                                                                    @break;
                                                                    @case( 'confirm')
                                                                        @php
                                                                            $badgeColor = '#0000FF';
                                                                            $statusDateColumn = 'confirmed_date';
                                                                        @endphp
                                                                    @break;
                                                                    @case( 'processing')
                                                                        @php
                                                                            $badgeColor = '#FFA500;';
                                                                            $statusDateColumn = 'processing_date';
                                                                        @endphp
                                                                    @break;
                                                                    @case( 'picked')
                                                                        @php
                                                                            $badgeColor = '#808000;';
                                                                            $statusDateColumn = 'picked_date';
                                                                        @endphp
                                                                    @break;
                                                                    @case( 'shipped')
                                                                        @php
                                                                            $badgeColor = '#808080;';
                                                                            $statusDateColumn = 'shipped_date';
                                                                        @endphp
                                                                    @break;
                                                                    @case( 'cancel')
                                                                        @php
                                                                            $badgeColor = '#FF0000;';
                                                                            $statusDateColumn = 'cancel_date';
                                                                        @endphp
                                                                    @break;
                                                                    @default:
                                                                        @php
                                                                            $badgeColor = '#800080;';
                                                                            $statusDateColumn = 'order_date';
                                                                        @endphp
                                                                @endswitch
                                                                <span class="badge badge-pill badge-warning"
                                                                    style="background: {{ $badgeColor }}">
                                                                    {{ ucfirst($order->status) }}
                                                                    ({{ $order->$statusDateColumn }})</span>
                                                            </label>
                                                        </td>
                                                        <td class="col-md-1">
                                                            @if ($order->isDelivering())
                                                                <form method="get" action="{{ route('user.order.tracking') }}">
                                                                    <input type="hidden" name='invoice_no'
                                                                        value="{{ $order->invoice_no }}">
                                                                    <button type='submit' title='Tracking Order'>
                                                                        <img src="{{ asset('assets/images/payments/6.png') }}"
                                                                            alt='Tracking Order' />
                                                                    </button>
                                                                </form>
                                                            @endif
                                                            <a href="{{ route('user.orders.show', $order->id) }}"
                                                                class="btn btn-sm btn-warning" title='View Order'>
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                                                            <a target="_blank"
                                                                href="{{ route('user.order.invoice.download', $order->id) }}"
                                                                class="btn btn-sm btn-danger" style="margin-top: 5px;"
                                                                title='Download Invoice'><i class="fa fa-download"
                                                                    style="color: white;"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- // end col md 10 -->
            </div> <!-- // end row -->
        </div>
    </div>
@endsection
