@extends('layouts.master')
@section('title', 'Wishlist')
@section('content')
    <div id="wishlist">
        <x-breadcrumb>
            <li><a href="/">Home</a></li>
            <li class='active'>Wishlist</li>
        </x-breadcrumb>

        <div class="body-content">
            <div class="container">
                @auth
                    <div class="my-wishlist-page" v-if='items.length > 0'>
                        <div class="row">
                            <div class="col-md-12 my-wishlist">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th colspan="4" class="heading-title">My Wishlist</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="item in items" :key="item.id">
                                                <td class="col-md-2"><img :src="item.attributes.image" :alt="item.name">
                                                </td>
                                                <td class="col-md-7">
                                                    <div class="product-name"><a
                                                            :href="`/details/${item.associatedModel.slug}`">@{{ item . name }}</a>
                                                    </div>
                                                    <div class="rating">
                                                        <i class="fa fa-star rate"></i>
                                                        <i class="fa fa-star rate"></i>
                                                        <i class="fa fa-star rate"></i>
                                                        <i class="fa fa-star rate"></i>
                                                        <i class="fa fa-star non-rate"></i>
                                                        <span class="review">( 06 Reviews )</span>
                                                    </div>
                                                    <div class="price">
                                                        <div v-if='item.associatedModel.discount_price === null'>
                                                            $@{{ item . price }}</div>
                                                        <div v-else>$@{{ item . associatedModel . discount_price }} <span
                                                                class='line-though'>$@{{ item . price }}</span></div>
                                                    </div>
                                                </td>
                                                <td class="col-md-2">
                                                    <a href="javascript:avoid(0)" class="btn-upper btn btn-primary"
                                                        @click="addToCart(item)">Add
                                                        to cart</a>
                                                </td>
                                                <td class="col-md-1 close-btn">
                                                    <a href="javascript:avoid(0)" @click="removeItem(item.id)"
                                                        class="___class_+?26___"><i class="fa fa-times"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!-- /.row -->
                    </div><!-- /.sigin-in-->

                    <div class="text-center" v-else>
                        <h3>No Cart Items</h3>
                        <a href="/">Continues Shopping</a>
                        <hr>
                    </div>
                @else
                    <div class='text-center'>
                        <h3>
                            <a href="{{ route('login') }}">Login</a> to see yout cart.
                        </h3>
                        <hr>
                    </div>
                @endauth
                <x-brand-carousel />
            </div><!-- /.container -->
        </div>
    </div>
@endsection
@push('scripts')
    <script src="https://unpkg.com/vue"></script>
    <script src="https://cdn.jsdelivr.net/vue.resource/1.3.1/vue-resource.min.js"></script>
    <script>
        $(function() {
            const _token = $("meta[name=csrf-token]").attr('content')
            const wishlist = new Vue({
                el: '#wishlist',
                data: {
                    toastOptions: {
                        text: "Oops! Something went wrong!",
                        position: "top-right",
                        loaderBg: "#ff6849",
                        icon: "error",
                        hideAfter: 3500,
                        stack: 6,
                    },
                    itemCount: 0,
                    items: [],
                    item: {
                        id: '',
                        name: '',
                        price: 0.00,
                        quantity: 1
                    }
                },
                mounted: function() {
                    this.loadItems();
                },
                methods: {
                    addItem: function() {
                        var _this = this;
                        this.$http.post('/wishlist', {
                            _token: _token,
                            id: _this.item.id,
                            name: _this.item.name,
                            price: _this.item.price,
                            quantity: _this.item.quantity
                        }).then(function(success) {
                            _this.loadItems();
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    addToCart({
                        id,
                        name,
                        quantity,
                        attributes
                    }) {
                        const _this = this;
                        this.$http.post('/carts', {
                            _token: _token,
                            color: attributes.size,
                            size: attributes.color,
                            quantity,
                            name,
                            id,
                        }).then(function(success) {
                            _this.removeItem(id);
                            _this.loadItems();
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    removeItem: function(id) {
                        var _this = this;
                        this.$http.delete('/wishlist/' + id, {
                            params: {
                                _token: _token
                            }
                        }).then(function(success) {
                            $.toast({
                                ..._this.toastOptions,
                                icon: "success",
                                text: success.body.message,
                            })
                            _this.loadItems();
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadItems: function() {
                        var _this = this;
                        this.$http.get('/wishlist', {
                            params: {
                                limit: 10
                            }
                        }).then(function(success) {
                            _this.items = success.body.data;
                            _this.itemCount = success.body.data.length;
                        }, function(error) {
                            console.log(error);
                        });
                    },
                }
            })
        })
    </script>
@endpush
