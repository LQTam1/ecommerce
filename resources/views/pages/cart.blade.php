@extends('layouts.master')
@section('title', 'Shopping Cart')
@section('content')
    <div id='app'>
        <x-breadcrumb>
            <li><a href="/">Home</a></li>
            <li class='active'>Shopping Cart</li>
        </x-breadcrumb>

        <div class="body-content outer-top-xs">
            <div class="container">
                <div class="row ">
                    @auth
                        <div class="shopping-cart" v-if="items.length > 0">
                            <div class="shopping-cart-table">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="cart-romove item">Remove</th>
                                                <th class="cart-description item">Image</th>
                                                <th class="cart-product-name item">Product Name</th>
                                                <th class="cart-qty item">Quantity</th>
                                                <th class="cart-edit item">Update</th>
                                                <th>Price</th>
                                                <th class="cart-sub-total item">Subtotal</th>
                                            </tr>
                                        </thead><!-- /thead -->
                                        <tfoot>
                                            <tr>
                                                <td colspan="7">
                                                    <div class="shopping-cart-btn">
                                                        <span class="___class_+?14___">
                                                            <a href="/" class="btn btn-upper btn-primary outer-left-xs">Continue
                                                                Shopping</a>
                                                            <a href="#"
                                                                class="btn btn-upper btn-primary pull-right outer-right-xs">Update
                                                                shopping cart</a>
                                                        </span>
                                                    </div><!-- /.shopping-cart-btn -->
                                                </td>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <tr v-for="item in items" :key="item.id">
                                                <td class="romove-item"><a href="#" title="cancel" class="icon"
                                                        @click.prevent="removeItem(item.id)"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                                <td class="cart-image">
                                                    <a class="entry-thumbnail" :href="`/details/${item.associatedModel.slug}`">
                                                        <img :src="item.attributes.image" alt="">
                                                    </a>
                                                </td>
                                                <td class="cart-product-name-info">
                                                    <h4 class='cart-product-description'><a
                                                            :href="`/details/${item.associatedModel.slug}`">@{{ item . name }}</a>
                                                    </h4>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="rating rateit-small"></div>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <div class="reviews">
                                                                (06 Reviews)
                                                            </div>
                                                        </div>
                                                    </div><!-- /.row -->
                                                    <div class="cart-product-info">
                                                        <div class="product-attribute product-color">COLOR:
                                                            <select class="text-2xl unicase-form-control w-52"
                                                                v-model='item.attributes.color'>
                                                                <option v-for="color in item.associatedModel.color"
                                                                    :selected='color === item.attributes.color' :value='color'>
                                                                    @{{ color }}</option>
                                                            </select>
                                                        </div>
                                                        <div class="product-attribute product-size">SIZE:
                                                            <select class="text-2xl unicase-form-control w-52"
                                                                v-model='item.attributes.size'>
                                                                <option v-for="size in item.associatedModel.size"
                                                                    :selected='size === item.attributes.size' :value='size'>
                                                                    @{{ size }}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </td>
                                                </td>
                                                <td class="cart-product-quantity">
                                                    <div class="quant-input">
                                                        <input type="number" v-model='item.quantity'
                                                            @change="quantityChange(item)">
                                                    </div>
                                                </td>
                                                <td class="cart-product-edit"><a href="#" @click.prevent='updateItem(item)'
                                                        class="product-edit btn btn-primary text-capitalize"
                                                        style='color: white'>update</a></td>
                                                <td>
                                                    <span>
                                                        $@{{ item . price }}
                                                    </span>
                                                </td>
                                                <td class="cart-product-sub-total">
                                                    <span
                                                        class="cart-sub-total-price">$@{{ item . quantity * item . price }}</span>
                                                </td>
                                            </tr>
                                        </tbody><!-- /tbody -->
                                    </table><!-- /table -->
                                </div>
                            </div><!-- /.shopping-cart-table -->

                            <div class="col-md-4 col-sm-12 estimate-ship-tax">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>
                                                <span class="estimate-title">Estimate shipping and tax</span>
                                                <p>Enter your destination to get shipping and tax.</p>
                                            </th>
                                        </tr>
                                    </thead><!-- /thead -->
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label class="info-title control-label">Country <span>*</span></label>
                                                    <select class="form-control unicase-form-control selectpicker">
                                                        <option>--Select options--</option>
                                                        <option>India</option>
                                                        <option>SriLanka</option>
                                                        <option>united kingdom</option>
                                                        <option>saudi arabia</option>
                                                        <option>united arab emirates</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="info-title control-label">State/Province
                                                        <span>*</span></label>
                                                    <select class="form-control unicase-form-control selectpicker">
                                                        <option>--Select options--</option>
                                                        <option>TamilNadu</option>
                                                        <option>Kerala</option>
                                                        <option>Andhra Pradesh</option>
                                                        <option>Karnataka</option>
                                                        <option>Madhya Pradesh</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="info-title control-label">Zip/Postal Code</label>
                                                    <input type="text" class="form-control unicase-form-control text-input"
                                                        placeholder="">
                                                </div>
                                                <div class="pull-right">
                                                    <button type="submit" class="btn-upper btn btn-primary">GET A QOUTE</button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div><!-- /.estimate-ship-tax -->

                            <div class="col-md-4 col-sm-12 estimate-ship-tax" v-if='details.cart_total_conditions.length === 0'>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>
                                                <span class="estimate-title">Discount Code</span>
                                                <p>Enter your coupon code if you have one..</p>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <input type="text" class="form-control unicase-form-control text-input"
                                                        placeholder="You Coupon.." v-model='coupon_name'
                                                        @keyup.enter="applyCoupon">
                                                </div>
                                                <div class="clearfix pull-right">
                                                    <button type="submit" class="btn-upper btn btn-primary"
                                                        @click="applyCoupon">APPLY
                                                        COUPON</button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody><!-- /tbody -->
                                </table><!-- /table -->
                            </div><!-- /.estimate-ship-tax -->

                            <div class="col-md-4 col-sm-12 cart-shopping-total pull-right">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>
                                                <div class="cart-sub-total">
                                                    Subtotal<span
                                                        class="inner-left-md">@{{ '$' + details . sub_total }}</span>
                                                </div>
                                                <template v-if='details.cart_total_conditions'>
                                                    <div v-for="(condition,name) in details.cart_total_conditions">
                                                        <div class="cart-sub-total capitalize">
                                                            @{{ condition . type }}
                                                            <span class="inner-left-md">@{{ name }}
                                                                (@{{ condition . value }})</span>
                                                            <button v-if="condition.type !== 'tax'" type="submit"
                                                                @click="couponRemove(name)"><i class="text-red-400 fa fa-times"
                                                                    title="Remove coupon"></i>
                                                            </button>
                                                        </div>
                                                        <div class="cart-sub-total capitalize">
                                                            @{{ condition . type }} Amount: <span class="inner-left-md">$
                                                                @{{ condition . discount }}</span>
                                                        </div>
                                                    </div>
                                                </template>
                                                <div class="cart-grand-total">
                                                    Grand Total<span
                                                        class="inner-left-md">@{{ '$' + details . total }}</span>
                                                </div>
                                            </th>
                                        </tr>
                                    </thead><!-- /thead -->
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="cart-checkout-btn pull-right">
                                                    <a href="{{ route('user.checkout.view') }}"
                                                        class="btn btn-primary checkout-btn">PROCCED TO
                                                        CHEKOUT</a>
                                                    <span class="___class_+?69___">Checkout with multiples address!</span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody><!-- /tbody -->
                                </table><!-- /table -->
                            </div><!-- /.cart-shopping-total -->

                        </div><!-- /.shopping-cart -->
                        <div class="text-center" v-else>
                            <h3>No Cart Items</h3>
                            <a href="/">Continues Shopping</a>
                            <hr>
                        </div>
                    @else
                        <div class='text-center'>
                            <h3>
                                <a href="{{ route('login') }}">Login</a> to see yout cart.
                            </h3>
                            <hr>
                        </div>
                    @endauth
                </div> <!-- /.row -->

                <x-brand-carousel />
            </div><!-- /.container -->
        </div>
    </div>
@endsection


@push('scripts')
    <script src="https://unpkg.com/vue"></script>
    <script src="https://cdn.jsdelivr.net/vue.resource/1.3.1/vue-resource.min.js"></script>
    <script>
        $(function($) {
            var _token = '<?php echo csrf_token(); ?>';
            var userId = '<?php echo auth()->id(); ?>';
            console.log(userId);
            $(document).ready(function() {
                var app = new Vue({
                    el: '#app',
                    data: {
                        coupon_name: '',
                        toastOptions: {
                            text: "Oops! Something went wrong!",
                            position: "top-right",
                            loaderBg: "#ff6849",
                            icon: "error",
                            hideAfter: 3500,
                            stack: 6,
                        },
                        details: {
                            sub_total: 0,
                            total: 0,
                            total_quantity: 0,
                            cart_total_conditions: [],
                        },
                        itemCount: 0,
                        items: [],
                        item: {
                            id: '',
                            name: '',
                            price: 0.00,
                            quantity: 1
                        },
                        cartCondition: {
                            name: '',
                            type: '',
                            target: '',
                            value: '',
                            attributes: {
                                description: 'Value Added Tax'
                            }
                        },
                        options: {
                            target: [{
                                    label: 'Apply to SubTotal',
                                    key: 'subtotal'
                                },
                                {
                                    label: 'Apply to Total',
                                    key: 'total'
                                }
                            ]
                        }
                    },
                    mounted: function() {
                        this.loadItems();
                    },
                    methods: {
                        quantityChange(item) {
                            if (item.quantity <= 0) {
                                if (window.confirm(
                                        "Quantity less was than 1. Are you want to delete it?")) {
                                    this.removeItem(item.id);
                                } else {
                                    let index = this.getCartItemIndex(item.id);
                                    this.items[index].quantity = 1;
                                }
                            }
                        },
                        couponRemove(coupon_name) {
                            let _this = this;
                            this.$http.delete(`/carts/coupon/${coupon_name}?_token=${_token}`).then(
                                function(success) {
                                    $.toast({
                                        ..._this.toastOptions,
                                        icon: "success",
                                        text: success.body.message,
                                    })
                                    _this.loadItems();
                                },
                                function(error) {
                                    $.toast({
                                        ..._this.toastOptions,
                                        text: success.body.message,
                                    })
                                });
                        },
                        applyCoupon() {
                            let _this = this;
                            this.$http.post('/carts/coupon', {
                                _token: _token,
                                coupon_name: _this.coupon_name
                            }).then(function(success) {
                                $.toast({
                                    ..._this.toastOptions,
                                    icon: "success",
                                    text: success.body.message,
                                })
                                _this.loadItems();
                            }, function(error) {
                                $.toast({
                                    ..._this.toastOptions,
                                    text: error.body.message,
                                })
                            });
                        },
                        addItem: function() {
                            var _this = this;
                            this.$http.post('/cart', {
                                _token: _token,
                                id: _this.item.id,
                                name: _this.item.name,
                                price: _this.item.price,
                                quantity: _this.item.quantity
                            }).then(function(success) {
                                _this.loadItems();
                            }, function(error) {
                                console.log(error);
                            });
                        },
                        getCartItemIndex(_id) {
                            return this.items.findIndex(({
                                id
                            }) => id == _id)
                        },
                        updateItem(item) {
                            var _this = this;
                            this.$http.put(`/carts/${item.id}`, {
                                _token: _token,
                                ...item
                            }).then(function(success) {
                                let {
                                    sub_total,
                                    total,
                                } = success.body.data
                                $.toast({
                                    ..._this.toastOptions,
                                    icon: "success",
                                    text: success.body.message,
                                })
                                _this.details.sub_total = sub_total
                                _this.details.total = total
                            }, function(error) {
                                console.log(error);
                            });
                        },
                        removeItem: function(id) {
                            var _this = this;
                            this.$http.delete('/carts/' + id, {
                                params: {
                                    _token: _token
                                }
                            }).then(function(success) {
                                $.toast({
                                    ..._this.toastOptions,
                                    icon: "success",
                                    text: success.body.success,
                                })
                                _this.loadItems();
                            }, function(error) {
                                console.log(error);
                            });
                        },
                        loadItems: function() {
                            var _this = this;
                            this.$http.get('/carts', {
                                params: {
                                    limit: 10
                                }
                            }).then(function(success) {
                                _this.items = success.body.data;
                                _this.itemCount = success.body.data.length;
                                _this.loadCartDetails();
                            }, function(error) {
                                console.log(error);
                            });
                        },
                        loadCartDetails: function() {
                            var _this = this;
                            this.$http.get(`/carts/${userId}`).then(function(success) {
                                _this.details = success.body.data;
                            }, function(error) {
                                console.log(error);
                            });
                        }
                    }
                });
            });
        });
    </script>
@endpush
