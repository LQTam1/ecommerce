@extends('layouts.master')
@section('title', 'Home')
@section('content')
    <div class="body-content outer-top-xs" id="top-banner-and-menu">
        <div class="container">
            <div class="row">
                <!-- ============================================== SIDEBAR ============================================== -->
                <x-left-sidebar :specialDeals="$specialDeals" :specialOffer="$specialOffer" />
                <!-- /.sidemenu-holder -->
                <!-- ============================================== SIDEBAR : END ============================================== -->

                <!-- ============================================== CONTENT ============================================== -->
                <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder">
                    <!-- ========================================== SECTION – HERO ========================================= -->

                    <div id="hero">
                        <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
                            @foreach ($sliders as $slider)
                                <div class="item"
                                    style="background-image: url({{ asset($slider->slider_img) }});">
                                    <div class="container-fluid">
                                        <div class="caption bg-color vertical-center text-left">
                                            <div class="big-text fadeInDown-1"> New Collections</div>
                                            <div class="excerpt fadeInDown-2 hidden-xs">
                                                <span>{{ $slider->description }}</span>
                                            </div>
                                            <div class="button-holder fadeInDown-3"><a href="index.php?page=single-product"
                                                    class="btn-lg btn btn-uppercase btn-primary shop-now-button">Shop
                                                    Now</a></div>
                                        </div>
                                        <!-- /.caption -->
                                    </div>
                                    <!-- /.container-fluid -->
                                </div>
                                <!-- /.item -->
                            @endforeach
                        </div>
                        <!-- /.owl-carousel -->
                    </div>

                    <!-- ========================================= SECTION – HERO : END ========================================= -->

                    <!-- ============================================== INFO BOXES ============================================== -->
                    <div class="info-boxes wow fadeInUp">
                        <div class="info-boxes-inner">
                            <div class="row">
                                <div class="col-md-6 col-sm-4 col-lg-4">
                                    <div class="info-box">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <h4 class="info-box-heading green">money back</h4>
                                            </div>
                                        </div>
                                        <h6 class="text">30 Days Money Back Guarantee</h6>
                                    </div>
                                </div>
                                <!-- .col -->

                                <div class="hidden-md col-sm-4 col-lg-4">
                                    <div class="info-box">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <h4 class="info-box-heading green">free shipping</h4>
                                            </div>
                                        </div>
                                        <h6 class="text">Shipping on orders over $99</h6>
                                    </div>
                                </div>
                                <!-- .col -->

                                <div class="col-md-6 col-sm-4 col-lg-4">
                                    <div class="info-box">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <h4 class="info-box-heading green">Special Sale</h4>
                                            </div>
                                        </div>
                                        <h6 class="text">Extra $5 off on all items </h6>
                                    </div>
                                </div>
                                <!-- .col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.info-boxes-inner -->

                    </div>
                    <!-- /.info-boxes -->
                    <!-- ============================================== INFO BOXES : END ============================================== -->

                    <!-- ============================================== FEATURED PRODUCTS ============================================== -->
                    <section class="section featured-product wow fadeInUp">
                        <h3 class="section-title">Lastest Products</h3>
                        <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
                            @foreach ($latestProducts as $product)
                                <div class="item item-carousel">
                                    <div class="products">
                                        <div class="product">
                                            <div class="product-image">
                                                <div class="image"><a
                                                        href="{{ route('product.details', $product->slug) }}"><img
                                                            src="{{ asset($product->product_thumbnail) }}" alt=""></a>
                                                </div>
                                                <!-- /.image -->
                                                @if ($product->discount_price === null)
                                                    <div class="tag new"><span>new</span></div>
                                                @else
                                                    <div class="tag sale">
                                                        <span>{{ $product->getDiscountPercentage() }}%</span>
                                                    </div>
                                                @endif
                                            </div>
                                            <!-- /.product-image -->

                                            <div class="product-info text-left">
                                                <h3 class="name"><a
                                                        href="{{ route('product.details', $product->slug) }}">{{ $product->name }}</a>
                                                </h3>
                                                <div class="rating rateit-small"></div>
                                                <div class="description"></div>
                                                <div class="product-price">
                                                    @if ($product->discount_price === null)
                                                        <span class="price">
                                                            ${{ $product->selling_price }}
                                                        </span>
                                                    @else
                                                        <span class="price">
                                                            ${{ $product->discount_price }}
                                                        </span>
                                                        <span class="price-before-discount">$
                                                            {{ $product->selling_price }}</span>
                                                    @endif
                                                </div>
                                                <!-- /.product-price -->

                                            </div>
                                            <!-- /.product-info -->
                                            <div class="cart clearfix animate-effect">
                                                <div class="action">
                                                    <ul class="list-unstyled">
                                                        <li class="add-cart-button btn-group">
                                                            <button class="btn btn-primary icon" data-toggle="modal"
                                                                id="{{ $product->id }}" onclick="productView(this.id)"
                                                                data-target="#addToCartModal" type="button"><i
                                                                    class="fa fa-shopping-cart"></i>
                                                            </button>
                                                            <button class="btn btn-primary cart-btn" type="button">Add to
                                                                cart
                                                            </button>
                                                        </li>
                                                        <li class="lnk wishlist"><a class="add-to-cart"
                                                                href="javascript:avoid(0)" id='{{ $product->id }}'
                                                                onclick="add2Wishlist(this.id)" title=" Wishlist"> <i
                                                                    class="icon fa fa-heart"></i>
                                                            </a>
                                                        </li>
                                                        <li class="lnk"><a class="add-to-cart"
                                                                href="{{ route('product.details', $product->slug) }}"
                                                                title="Compare"> <i class="fa fa-signal"
                                                                    aria-hidden="true"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <!-- /.action -->
                                            </div>
                                            <!-- /.cart -->
                                        </div>
                                        <!-- /.product -->

                                    </div>
                                    <!-- /.products -->
                                </div>
                            @endforeach
                            <!-- /.item -->
                        </div>
                        <!-- /.home-owl-carousel -->
                    </section>
                    <!-- /.section -->

                    <!-- ============================================== SCROLL TABS ============================================== -->
                    @if ($categoryProduct !== null)
                        <div id="product-tabs-slider" class="scroll-tabs outer-top-vs wow fadeInUp">
                            <div class="more-info-tab clearfix ">
                                <h3 class="new-product-title pull-left">New Products</h3>
                                <ul class="nav nav-tabs nav-tab-line pull-right" id="new-products-1">
                                    @foreach ($categoryProduct->select_categories as $key => $category)
                                        <li class="{{ $key === 0 ? 'active' : '' }}"><a data-transition-type="backSlide"
                                                href="#category-{{ $category->id }}" data-toggle="tab"
                                                style="{{ $key === 0 ? 'color:white;background-color:lightblue' : '' }}">{{ $category->label }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                                <!-- /.nav-tabs -->
                            </div>
                            <div class="tab-content outer-top-xs">
                                @foreach ($categoryProduct->select_categories as $key => $category)
                                    <div class="tab-pane in {{ $key === 0 ? 'active' : '' }}"
                                        id="category-{{ $category->id }}">
                                        <div class="product-slider">
                                            <div class="owl-carousel home-owl-carousel custom-carousel owl-theme"
                                                data-item="4">
                                                @php
                                                    $catWiseProducts = \App\Models\Product::active()
                                                        ->where('menu_item_id', $category->id)
                                                        ->take($categoryProduct->products_to_show)
                                                        ->get();
                                                @endphp
                                                @if ($catWiseProducts->count() > 0)
                                                    @foreach ($catWiseProducts as $product)
                                                        <div class="item item-carousel">
                                                            <div class="products">
                                                                <div class="product">
                                                                    <div class="product-image">
                                                                        <div class="image"><a
                                                                                href="{{ route('product.details', $product->slug) }}"><img
                                                                                    src="{{ asset($product->product_thumbnail) }}"
                                                                                    alt="{{ $product->name }}"></a></div>
                                                                        <!-- /.image -->
                                                                        @if ($product->discount_price === null)
                                                                            <div class="tag new"><span>new</span>
                                                                            </div>
                                                                        @else
                                                                            <div class="tag sale">
                                                                                <span>{{ $product->getDiscountPercentage() }}%</span>
                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                    <!-- /.product-image -->

                                                                    <div class="product-info text-left">
                                                                        <h3 class="name"><a
                                                                                href="{{ route('product.details', $product->slug) }}">{{ $product->name }}</a>
                                                                        </h3>
                                                                        <div class="rating rateit-small"></div>
                                                                        <div class="description"></div>
                                                                        <div class="product-price">
                                                                            @if ($product->discount_price === null)
                                                                                <span class="price">
                                                                                    ${{ $product->selling_price }}
                                                                                </span>
                                                                            @else
                                                                                <span class="price">
                                                                                    ${{ $product->discount_price }}
                                                                                </span>
                                                                                <span class="price-before-discount">$
                                                                                    {{ $product->selling_price }}</span>
                                                                            @endif
                                                                        </div>
                                                                        <!-- /.product-price -->

                                                                    </div>
                                                                    <!-- /.product-info -->
                                                                    <div class="cart clearfix animate-effect">
                                                                        <div class="action">
                                                                            <ul class="list-unstyled">
                                                                                <li class="add-cart-button btn-group">
                                                                                    <button class="btn btn-primary icon"
                                                                                        data-toggle="modal"
                                                                                        id="{{ $product->id }}"
                                                                                        onclick="productView(this.id)"
                                                                                        data-target="#addToCartModal"
                                                                                        type="button" title="Add Cart"><i
                                                                                            class="fa fa-shopping-cart"></i></button>
                                                                                    <button class="btn btn-primary cart-btn"
                                                                                        type="button">Add to cart
                                                                                    </button>
                                                                                </li>
                                                                                <li class="lnk wishlist"><a
                                                                                        class="add-to-cart"
                                                                                        href="javascript:avoid(0)"
                                                                                        id='{{ $product->id }}'
                                                                                        onclick="add2Wishlist(this.id)"
                                                                                        title="Wishlist">
                                                                                        <i class="icon fa fa-heart"></i>
                                                                                    </a></li>
                                                                                <li class="lnk"><a
                                                                                        data-toggle="tooltip"
                                                                                        class="add-to-cart"
                                                                                        href="{{ route('product.details', $product->slug) }}"
                                                                                        title="Compare">
                                                                                        <i class="fa fa-signal"
                                                                                            aria-hidden="true"></i>
                                                                                    </a></li>
                                                                            </ul>
                                                                        </div>
                                                                        <!-- /.action -->
                                                                    </div>
                                                                    <!-- /.cart -->
                                                                </div>
                                                                <!-- /.product -->

                                                            </div>
                                                            <!-- /.products -->
                                                        </div>
                                                        <!-- /.item -->
                                                    @endforeach
                                                @endif
                                            </div>
                                            <!-- /.home-owl-carousel -->
                                        </div>
                                        <!-- /.product-slider -->
                                    </div>
                                @endforeach
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                    @endif
                    <!-- /.scroll-tabs -->
                    <!-- ============================================== SCROLL TABS : END ============================================== -->
                    <!-- ============================================== WIDE PRODUCTS ============================================== -->
                    <div class="wide-banners wow fadeInUp outer-bottom-xs">
                        <div class="row">
                            <div class="col-md-7 col-sm-7">
                                <div class="wide-banner cnt-strip">
                                    <div class="image"><img class="img-responsive"
                                            src="{{ asset('assets/images/banners/home-banner1.jpg') }}" alt=""
                                            width='100%' height='100%'></div>
                                </div>
                                <!-- /.wide-banner -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-5 col-sm-5">
                                <div class="wide-banner cnt-strip">
                                    <div class="image"><img class="img-responsive"
                                            src="{{ asset('assets/images/banners/home-banner2.jpg') }}" alt=""
                                            width='100%' height='100%'></div>
                                </div>
                                <!-- /.wide-banner -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.wide-banners -->

                    <!-- ============================================== WIDE PRODUCTS : END ============================================== -->
                    <!-- ============================================== FEATURED PRODUCTS ============================================== -->
                    <section class="section featured-product wow fadeInUp">
                        <h3 class="section-title">Featured products</h3>
                        <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
                            @foreach ($featured as $product)
                                <div class="item item-carousel">
                                    <div class="products">
                                        <div class="product">
                                            <div class="product-image">
                                                <div class="image"><a
                                                        href="{{ route('product.details', $product->slug) }}"><img
                                                            src="{{ asset($product->product_thumbnail) }}" alt=""></a>
                                                </div>
                                                <!-- /.image -->
                                                @if ($product->discount_price === null)
                                                    <div class="tag new"><span>new</span></div>
                                                @else
                                                    <div class="tag sale">
                                                        <span>{{ $product->getDiscountPercentage() }}%</span>
                                                    </div>
                                                @endif
                                            </div>
                                            <!-- /.product-image -->

                                            <div class="product-info text-left">
                                                <h3 class="name"><a
                                                        href="{{ route('product.details', $product->slug) }}">{{ $product->name }}</a>
                                                </h3>
                                                <div class="rating rateit-small"></div>
                                                <div class="description"></div>
                                                <div class="product-price">
                                                    @if ($product->discount_price === null)
                                                        <span class="price">
                                                            ${{ $product->selling_price }}
                                                        </span>
                                                    @else
                                                        <span class="price">
                                                            ${{ $product->discount_price }}
                                                        </span>
                                                        <span class="price-before-discount">$
                                                            {{ $product->selling_price }}</span>
                                                    @endif
                                                </div>
                                                <!-- /.product-price -->

                                            </div>
                                            <!-- /.product-info -->
                                            <div class="cart clearfix animate-effect">
                                                <div class="action">
                                                    <ul class="list-unstyled">
                                                        <li class="add-cart-button btn-group">
                                                            <button class="btn btn-primary icon" data-toggle="modal"
                                                                id="{{ $product->id }}" onclick="productView(this.id)"
                                                                data-target="#addToCartModal" type="button"><i
                                                                    class="fa fa-shopping-cart"></i>
                                                            </button>
                                                            <button class="btn btn-primary cart-btn" type="button">Add to
                                                                cart
                                                            </button>
                                                        </li>
                                                        <li class="lnk wishlist"><a class="add-to-cart"
                                                                href="javascript:avoid(0)" id='{{ $product->id }}'
                                                                onclick="add2Wishlist(this.id)" title=" Wishlist"> <i
                                                                    class="icon fa fa-heart"></i>
                                                            </a>
                                                        </li>
                                                        <li class="lnk"><a class="add-to-cart"
                                                                href="{{ route('product.details', $product->slug) }}"
                                                                title="Compare"> <i class="fa fa-signal"
                                                                    aria-hidden="true"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <!-- /.action -->
                                            </div>
                                            <!-- /.cart -->
                                        </div>
                                        <!-- /.product -->

                                    </div>
                                    <!-- /.products -->
                                </div>
                            @endforeach
                            <!-- /.item -->
                        </div>
                        <!-- /.home-owl-carousel -->
                    </section>
                    <!-- /.section -->
                    <!-- ============================================== FEATURED PRODUCTS : END ============================================== -->
                    <!-- ============================================== WIDE PRODUCTS ============================================== -->
                    <div class="wide-banners wow fadeInUp outer-bottom-xs">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="wide-banner cnt-strip">
                                    <div class="image"><img class="img-responsive"
                                            src="{{ asset('assets/images/banners/home-banner.jpg') }}" alt=""
                                            width='100%' height='100%'></div>
                                    <div class="strip strip-text">
                                        <div class="strip-inner">
                                            <h2 class="text-right">New Mens Fashion<br>
                                                <span class="shopping-needs">Save up to 40% off</span>
                                            </h2>
                                        </div>
                                    </div>
                                    <div class="new-label">
                                        <div class="text">NEW</div>
                                    </div>
                                    <!-- /.new-label -->
                                </div>
                                <!-- /.wide-banner -->
                            </div>
                            <!-- /.col -->

                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.wide-banners -->
                    <!-- ============================================== WIDE PRODUCTS : END ============================================== -->
                    <!-- ============================================== BEST SELLER ============================================== -->

                    <div class="best-deal wow fadeInUp outer-bottom-xs">
                        <h3 class="section-title">Best seller</h3>
                        <div class="sidebar-widget-body outer-top-xs">
                            <div class="owl-carousel best-seller custom-carousel owl-theme outer-top-xs">
                                @foreach ($bestSeller as $products)
                                    <div class="item">
                                        <div class="products best-product">
                                            @foreach ($products as $product)
                                                <div class="product">
                                                    <div class="product-micro">
                                                        <div class="row product-micro-row">
                                                            <div class="col col-xs-5">
                                                                <div class="product-image">
                                                                    <div class="image"> <a
                                                                            href="{{ route('product.details', $product->slug) }}">
                                                                            <img src="{{ asset($product->product_thumbnail) }}"
                                                                                alt="">
                                                                        </a>
                                                                    </div>
                                                                    <!-- /.image -->

                                                                </div>
                                                                <!-- /.product-image -->
                                                            </div>
                                                            <!-- /.col -->
                                                            <div class="col2 col-xs-7">
                                                                <div class="product-info">
                                                                    <h3 class="name"><a
                                                                            href="{{ route('product.details', $product->slug) }}">{{ $product->name }}</a>
                                                                    </h3>
                                                                    <div>
                                                                        @for ($i = 1; $i <= 5; $i++)
                                                                            <span
                                                                                class="fa fa-star {{ $i <= $product->reviews_avg_rating ? 'checked text-yellow-400' : 'text-gray-400' }}"></span>
                                                                        @endfor
                                                                    </div>
                                                                    <div class="product-price"> <span
                                                                            class="price">
                                                                            ${{ $product->selling_price }} </span> </div>
                                                                    <!-- /.product-price -->

                                                                </div>
                                                            </div>
                                                            <!-- /.col -->
                                                        </div>
                                                        <!-- /.product-micro-row -->
                                                    </div>
                                                    <!-- /.product-micro -->

                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- /.sidebar-widget-body -->
                    </div>
                    <!-- /.sidebar-widget -->
                    <!-- ============================================== BEST SELLER : END ============================================== -->

                    <!-- ============================================== BLOG SLIDER ============================================== -->
                    <section class="section latest-blog outer-bottom-vs wow fadeInUp">
                        <h3 class="section-title">latest from our blog</h3>
                        <div class="blog-slider-container outer-top-xs">
                            <div class="owl-carousel blog-slider custom-carousel">
                                @foreach ($posts as $post)
                                    <div class="item">
                                        <div class="blog-post">
                                            <div class="blog-post-image">
                                                <div class="image"><a
                                                        href="{{ route('blog.details', ['slug' => $post->slug, 'id' => $post->id]) }}"><img
                                                            src="{{ asset($post->image) }}" alt=""></a>
                                                </div>
                                            </div>
                                            <!-- /.blog-post-image -->

                                            <div class="blog-post-info text-left">
                                                <h3 class="name my-4">
                                                    <a
                                                        href="{{ route('blog.details', ['slug' => $post->slug, 'id' => $post->id]) }}">{{ $post->title }}</a>
                                                </h3>
                                                <span class="info">By {{ $post->author->name }} &nbsp;|&nbsp;
                                                    {{ $post->created_at->format('d F Y') }}
                                                </span>
                                                <p class="text">{!! Str::limit($post->details, 100) !!}</p>
                                                <a href="{{ route('blog.details', ['slug' => $post->slug, 'id' => $post->id]) }}"
                                                    class="lnk btn btn-primary">Read more</a>
                                            </div>
                                            <!-- /.blog-post-info -->

                                        </div>
                                        <!-- /.blog-post -->
                                    </div>
                                    <!-- /.item -->
                                @endforeach
                            </div>
                            <!-- /.owl-carousel -->
                        </div>
                        <!-- /.blog-slider-container -->
                    </section>
                    <!-- /.section -->
                    <!-- ============================================== BLOG SLIDER : END ============================================== -->

                    <!-- ============================================== FEATURED PRODUCTS ============================================== -->
                    <section class="section wow fadeInUp new-arriavls">
                        <h3 class="section-title">{{ $skip_brand_1->name }}</h3>
                        <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
                            @foreach ($skip_brand_product_1 as $product)
                                <div class="item item-carousel">
                                    <div class="products">
                                        <div class="product">
                                            <div class="product-image">
                                                <div class="image"><a
                                                        href="{{ route('product.details', $product->slug) }}"><img
                                                            src="{{ asset($product->product_thumbnail) }}" alt=""></a>
                                                </div>
                                                <!-- /.image -->
                                                @if ($product->discount_price === null)
                                                    <div class="tag new"><span>new</span></div>
                                                @else
                                                    <div class="tag sale">
                                                        <span>{{ $product->getDiscountPercentage() }}%</span>
                                                    </div>
                                                @endif
                                            </div>
                                            <!-- /.product-image -->

                                            <div class="product-info text-left">
                                                <h3 class="name"><a
                                                        href="{{ route('product.details', $product->slug) }}">{{ $product->name }}</a>
                                                </h3>
                                                <div class="rating rateit-small"></div>
                                                <div class="description"></div>
                                                <div class="product-price">
                                                    @if ($product->discount_price === null)
                                                        <span class="price">
                                                            ${{ $product->selling_price }}
                                                        </span>
                                                    @else
                                                        <span class="price">
                                                            ${{ $product->discount_price }}
                                                        </span>
                                                        <span class="price-before-discount">$
                                                            {{ $product->selling_price }}</span>
                                                    @endif
                                                </div>
                                                <!-- /.product-price -->

                                            </div>
                                            <!-- /.product-info -->
                                            <div class="cart clearfix animate-effect">
                                                <div class="action">
                                                    <ul class="list-unstyled">
                                                        <li class="add-cart-button btn-group">
                                                            <button class="btn btn-primary icon" data-toggle="modal"
                                                                id="{{ $product->id }}" onclick="productView(this.id)"
                                                                data-target="#addToCartModal" type="button"><i
                                                                    class="fa fa-shopping-cart"></i>
                                                            </button>
                                                            <button class="btn btn-primary cart-btn" type="button">Add to
                                                                cart
                                                            </button>
                                                        </li>
                                                        <li class="lnk wishlist"><a class="add-to-cart"
                                                                href="javascript:avoid(0)" id='{{ $product->id }}'
                                                                onclick="add2Wishlist(this.id)" title=" Wishlist"> <i
                                                                    class="icon fa fa-heart"></i>
                                                            </a>
                                                        </li>
                                                        <li class="lnk"><a class="add-to-cart"
                                                                href="{{ route('product.details', $product->slug) }}"
                                                                title="Compare"> <i class="fa fa-signal"
                                                                    aria-hidden="true"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <!-- /.action -->
                                            </div>
                                            <!-- /.cart -->
                                        </div>
                                        <!-- /.product -->

                                    </div>
                                    <!-- /.products -->
                                </div>
                            @endforeach
                            <!-- /.item -->
                        </div>
                        <!-- /.home-owl-carousel -->
                    </section>
                    <!-- /.section -->
                    <!-- ============================================== FEATURED PRODUCTS : END ============================================== -->

                </div>
                <!-- /.homebanner-holder -->
                <!-- ============================================== CONTENT : END ============================================== -->
            </div>
            <!-- /.row -->
            <x-brand-carousel />
        </div>
        <!-- /.container -->
    </div>
@endsection
