@if ($products->isEmpty())
    <h3 class="text-center text-danger">Product Not Found </h3>
@else
    <div class="container mt-1 absolute z-10 bg-white">
        <div class="row d-flex justify-content-center ">
            <div class="col">
                <div class="card p-4 border-none">
                    @foreach ($products as $product)
                        <a href="{{ route('product.details', $product->slug) }}">
                            <div class="list pt-5 pb-2 flex items-center border-b-2"> <img
                                    src="{{ asset($product->product_thumbnail) }}" style="width: 30px; height: 30px;">
                                <div class="d-flex flex-column ml-3" style="margin-left: 10px;">
                                    <span>{{ $product->name }} </span>
                                    <small class='text-gray-400'>${{ $product->selling_price }}</small>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endif
