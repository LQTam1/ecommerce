@extends('layouts.master')
@section('title', 'Product Detail')
@section('content')
    <x-breadcrumb>
        <li><a href="#">Home</a></li>
        <li><a href="#">Clothing</a></li>
        <li class='active'>{{ $product->name }}</li>
    </x-breadcrumb>
    <div class="body-content outer-top-xs">
        <div class='container'>
            <div class='row single-product'>
                <div class='col-md-3 sidebar'>
                    <div class="sidebar-module-container">
                        <div class="home-banner outer-top-n">
                            <img src="{{ asset('assets/images/banners/LHS-banner.jpg') }}" width='100%' alt="Image">
                        </div>

                        <x-sidebar.hot-deals />

                        <x-sidebar.newsletter />

                        <x-sidebar.testimonials />

                    </div>
                </div><!-- /.sidebar -->
                <div class='col-md-9'>
                    <div class="detail-block">
                        <div class="row  wow fadeInUp">

                            <div class="col-xs-12 col-sm-6 col-md-5 gallery-holder">
                                <div class="product-item-holder size-big single-product-gallery small-gallery">

                                    <div id="owl-single-product">
                                        @foreach ($product->images as $image)
                                            <div class="single-product-gallery-item" id="slide{{ $image->id }}">
                                                <a data-lightbox="image-1" data-title="Gallery"
                                                    href="{{ asset($image->photo_name) }}">
                                                    <img class="img-responsive" alt=""
                                                        src="{{ asset('assets/images/blank.gif') }}"
                                                        data-echo="{{ asset($image->photo_name) }}" />
                                                </a>
                                            </div><!-- /.single-product-gallery-item -->
                                        @endforeach
                                    </div><!-- /.single-product-slider -->


                                    <div class="single-product-gallery-thumbs gallery-thumbs">

                                        <div id="owl-single-product-thumbnails">
                                            @foreach ($product->images as $image)
                                                <div class="item">
                                                    <a class="horizontal-thumb active" data-target="#owl-single-product"
                                                        data-slide="{{ $image->id }}" href="#slide{{ $image->id }}">
                                                        <img class="img-responsive" width="85" alt=""
                                                            src="{{ asset('assets/images/blank.gif') }}"
                                                            data-echo="{{ asset($image->photo_name) }}" />
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div><!-- /#owl-single-product-thumbnails -->


                                    </div><!-- /.gallery-thumbs -->

                                </div><!-- /.single-product-gallery -->
                            </div><!-- /.gallery-holder -->
                            <div class='col-sm-6 col-md-7 product-info-block'>
                                <div class="product-info">
                                    <h1 class="name" id="pname">{{ $product->name }}</h1>

                                    <div class="rating-reviews m-t-20">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                @for ($i = 1; $i <= 5; $i++)
                                                    <span
                                                        class="fa fa-star {{ $i <= $product->reviews_avg_rating ? 'checked text-yellow-400' : 'text-gray-400' }}"></span>
                                                @endfor
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="reviews">
                                                    <a href="#review" onclick="document.querySelector('#reviewTab').click()"
                                                        class="lnk">({{ $product->reviews_count }}
                                                        Reviews)</a>
                                                </div>
                                            </div>
                                        </div><!-- /.row -->
                                    </div><!-- /.rating-reviews -->

                                    <div class="stock-container info-container m-t-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <div class="stock-box">
                                                    <span class="label">Availability :</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="stock-box">
                                                    <span class="value">In Stock</span>
                                                </div>
                                            </div>
                                        </div><!-- /.row -->
                                    </div><!-- /.stock-container -->

                                    <div class="description-container m-t-20">
                                        {!! $product->short_description !!}
                                    </div><!-- /.description-container -->

                                    <div class="price-container info-container m-t-20">
                                        <div class="row">


                                            <div class="col-sm-6">
                                                <div class="price-box">
                                                    @if ($product->discount_price === null)
                                                        <span
                                                            class="price">${{ $product->selling_price }}</span>
                                                    @else
                                                        <span
                                                            class="price">${{ $product->discount_price }}</span>
                                                        <span
                                                            class="price-strike">${{ $product->selling_price }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="favorite-button m-t-10">
                                                    <a class="btn btn-primary" data-toggle="tooltip" data-placement="right"
                                                        title="Wishlist" href="#">
                                                        <i class="fa fa-heart"></i>
                                                    </a>
                                                    <a class="btn btn-primary" data-toggle="tooltip" data-placement="right"
                                                        title="Add to Compare" href="#">
                                                        <i class="fa fa-signal"></i>
                                                    </a>
                                                    <a class="btn btn-primary" data-toggle="tooltip" data-placement="right"
                                                        title="E-mail" href="#">
                                                        <i class="fa fa-envelope"></i>
                                                    </a>
                                                </div>
                                            </div>

                                        </div><!-- /.row -->
                                    </div><!-- /.price-container -->

                                    <form
                                        action="{{ route('carts.store', ['id' => $product->id, 'name' => $product->name]) }}"
                                        method="post">
                                        @csrf
                                        <!--     /// Add Product Color And Product Size ///// -->
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    @if ($product->size !== null)
                                                        <label class="info-title control-label">Choose Color <span>
                                                            </span></label>
                                                        <select class="form-control unicase-form-control selectpicker"
                                                            name='color' style="display: none;" id="color" required>
                                                            <option value="" disabled>--Choose Color--</option>
                                                            @foreach (explode(',', $product->color) as $color)
                                                                <option value="{{ $color }}">
                                                                    {{ ucwords($color) }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    @endif
                                                </div> <!-- // end form group -->
                                            </div> <!-- // end col 6 -->
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    @if ($product->size !== null)
                                                        <label class="info-title control-label">Choose Size <span>
                                                            </span></label>
                                                        <select class="form-control unicase-form-control selectpicker"
                                                            name='size' style="display: none;" id="size" required>
                                                            <option value="" disabled>--Choose Size--</option>
                                                            @foreach (explode(',', $product->size) as $size)
                                                                <option value="{{ $size }}">{{ ucwords($size) }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    @endif
                                                </div> <!-- // end form group -->
                                            </div> <!-- // end col 6 -->
                                        </div><!-- /.row -->
                                        <!--     /// End Add Product Color And Product Size ///// -->

                                        <div class="quantity-container info-container">
                                            <div class="row">

                                                <div class="col-sm-2">
                                                    <span class="label">Qty :</span>
                                                </div>

                                                <div class="col-sm-2">
                                                    <div class="cart-quantity">
                                                        <div class="quant-input">
                                                            <div class="arrows">
                                                                <div class="arrow plus gradient" onclick="increaseQty()">
                                                                    <span class="ir"><i
                                                                            class="icon fa fa-sort-asc"></i></span>
                                                                </div>
                                                                <div class="arrow minus gradient" onclick="decreaseQty()">
                                                                    <span class="ir"><i
                                                                            class="icon fa fa-sort-desc"></i></span>
                                                                </div>
                                                            </div>
                                                            <input type="text" value="1" id='quantity' name='quantity'>
                                                        </div>
                                                    </div>
                                                </div>

                                                <input type="hidden" id="product_id" value="{{ $product->id }}" min="1">

                                                <div class="col-sm-7">
                                                    <button type='submit' href="javascript:avoid(0)"
                                                        class="btn btn-primary"><i
                                                            class="fa fa-shopping-cart inner-right-vs"></i>
                                                        ADD TO CART</button>
                                                </div>


                                            </div><!-- /.row -->
                                        </div><!-- /.quantity-container -->
                                    </form>

                                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
                                    <div class="addthis_inline_share_toolbox"></div>

                                </div><!-- /.product-info -->
                            </div><!-- /.col-sm-7 -->
                        </div><!-- /.row -->
                    </div>

                    <div class="product-tabs inner-bottom-xs  wow fadeInUp">
                        <div class="row">
                            <div class="col-sm-3">
                                <ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
                                    <li class="active"><a data-toggle="tab" href="#description">DESCRIPTION</a>
                                    </li>
                                    <li><a data-toggle="tab" id='reviewTab' href="#review">REVIEW</a></li>
                                    <li><a data-toggle="tab" href="#tags">TAGS</a></li>
                                </ul><!-- /.nav-tabs #product-tabs -->
                            </div>
                            <div class="col-sm-9">

                                <div class="tab-content">

                                    <div id="description" class="tab-pane in active">
                                        <div class="product-tab">
                                            <p class="text">{!! $product->description !!}</p>
                                        </div>
                                    </div><!-- /.tab-pane -->

                                    <div id="review" class="tab-pane">
                                        <div class="product-tab">

                                            <div class="product-reviews">
                                                <h4 class="title">Customer Reviews</h4>

                                                <div class="reviews">
                                                    @foreach ($product->reviews as $review)
                                                        <div class="review flex ">
                                                            <div class="user-avatar mr-10 flex flex-col items-center">
                                                                <img src="{{ $review->user->profile_photo_url }}"
                                                                    alt="{{ $review->user->name }}" width='80'
                                                                    height='80' />
                                                                <div class='mt-2'>{{ $review->user->name }}</div>
                                                            </div>
                                                            <div class="review-details">
                                                                <div class="rating flex">
                                                                    @for ($i = 1; $i <= 5; $i++)
                                                                        <span
                                                                            class="fa fa-star {{ $i <= $review->rating ? 'checked text-yellow-400' : 'text-gray-400' }}"></span>
                                                                    @endfor
                                                                </div>
                                                                <div class="review-title"><span
                                                                        class="summary">{{ $review->summary }}</span><span
                                                                        class="date"><i
                                                                            class="fa fa-calendar"></i><span>{{ $review->created_at->diffForHumans() }}</span></span>
                                                                </div>
                                                                <div class="text">
                                                                    <q>{{ $review->comment }}</q>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach

                                                </div><!-- /.reviews -->
                                            </div><!-- /.product-reviews -->

                                            <div class="product-add-review">
                                                <h4 class="title">Write your own review</h4>
                                                @if ($errors->first('rating'))
                                                    <div class="text-danger">{{ $errors->fist('rating') }}</div>
                                                @endif
                                                <div class="review-form">
                                                    @guest
                                                        <p> <b> For Add Product Review. You Need to Login First <a
                                                                    href="{{ route('login') }}">Login Here</a> </b> </p>
                                                    @else
                                                        <div class="form-container">
                                                            <form role="form" class="cnt-form" method="post"
                                                                action="{{ route('user.reviews.store') }}">
                                                                @csrf
                                                                <input type="hidden" name="product_id"
                                                                    value="{{ $product->id }}">
                                                                <div class="review-table">
                                                                    <div class="table-responsive">
                                                                        <table class="table">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th class="cell-label">&nbsp;</th>
                                                                                    <th>1 star</th>
                                                                                    <th>2 stars</th>
                                                                                    <th>3 stars</th>
                                                                                    <th>4 stars</th>
                                                                                    <th>5 stars</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="cell-label">Quality</td>
                                                                                    <td><input type="radio" name="rating"
                                                                                            @if (old('rating') == 1) checked @endif
                                                                                            class=" w-8 h-8" value="1">
                                                                                    </td>
                                                                                    <td><input type="radio" name="rating"
                                                                                            @if (old('rating') == 2) checked @endif
                                                                                            class=" w-8 h-8" value="2">
                                                                                    </td>
                                                                                    <td><input type="radio" name="rating"
                                                                                            @if (old('rating') == 3) checked @endif
                                                                                            class=" w-8 h-8" value="3">
                                                                                    </td>
                                                                                    <td><input type="radio" name="rating"
                                                                                            @if (old('rating') == 4) checked @endif
                                                                                            class=" w-8 h-8" value="4">
                                                                                    </td>
                                                                                    <td><input type="radio" name="rating"
                                                                                            @if (old('rating') == 5) checked @endif
                                                                                            class=" w-8 h-8" value="5">
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table><!-- /.table .table-bordered -->
                                                                    </div><!-- /.table-responsive -->
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <div
                                                                            class="form-group @error('summary') is-invalid @enderror">
                                                                            <label for="exampleInputSummary">Summary
                                                                                <span class="astk">*</span></label>
                                                                            <input type="text" name='summary'
                                                                                class="form-control txt"
                                                                                id="exampleInputSummary"
                                                                                value="{{ old('summary') }}" placeholder="">
                                                                            @error('summary')
                                                                                <div class="text-danger">{{ $message }}
                                                                                </div>
                                                                            @enderror
                                                                        </div><!-- /.form-group -->
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <div
                                                                            class="form-group @error('comment') is-invalid @enderror">
                                                                            <label for="exampleInputReview">Review <span
                                                                                    class="astk">*</span></label>
                                                                            <textarea class="form-control txt txt-review"
                                                                                name='comment' id="exampleInputReview" rows="4"
                                                                                placeholder="">{{ old('comment') }}</textarea>
                                                                            @error('comment')
                                                                                <div class="text-danger">{{ $message }}
                                                                                </div>
                                                                            @enderror
                                                                        </div><!-- /.form-group -->
                                                                    </div>
                                                                </div><!-- /.row -->

                                                                <div class="action text-right">
                                                                    <button class="btn btn-primary btn-upper">SUBMIT
                                                                        REVIEW
                                                                    </button>
                                                                </div><!-- /.action -->

                                                            </form><!-- /.cnt-form -->
                                                        </div><!-- /.form-container -->
                                                    @endguest
                                                </div><!-- /.review-form -->

                                            </div><!-- /.product-add-review -->

                                        </div><!-- /.product-tab -->
                                    </div><!-- /.tab-pane -->

                                    <div id="tags" class="tab-pane">
                                        <div class="product-tag">

                                            <h4 class="title">Product Tags</h4>
                                            <form role="form" class="form-inline form-cnt">
                                                <div class="form-container">

                                                    <div class="form-group">
                                                        <label for="exampleInputTag">Add Your Tags: </label>
                                                        <input type="email" id="exampleInputTag" class="form-control txt">


                                                    </div>

                                                    <button class="btn btn-upper btn-primary" type="submit">ADD TAGS
                                                    </button>
                                                </div><!-- /.form-container -->
                                            </form><!-- /.form-cnt -->

                                            <form role="form" class="form-inline form-cnt">
                                                <div class="form-group">
                                                    <label>&nbsp;</label>
                                                    <span class="text col-md-offset-3">Use spaces to separate tags. Use
                                                        single quotes (') for phrases.</span>
                                                </div>
                                            </form><!-- /.form-cnt -->

                                        </div><!-- /.product-tab -->
                                    </div><!-- /.tab-pane -->

                                </div><!-- /.tab-content -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.product-tabs -->

                    <!-- ============================================== Related products ============================================== -->
                    <section class="section featured-product wow fadeInUp">
                        <h3 class="section-title">Related products</h3>
                        <div class="owl-carousel home-owl-carousel upsell-product custom-carousel owl-theme outer-top-xs">
                            @foreach ($relatedProducts as $rproduct)
                                <div class="item item-carousel">
                                    <div class="products">

                                        <div class="product">
                                            <div class="product-image">
                                                <div class="image">
                                                    <a href="{{ route('product.details', $rproduct->slug) }}"><img
                                                            src="{{ asset($rproduct->product_thumbnail) }}" alt=""></a>
                                                </div><!-- /.image -->

                                                @if ($rproduct->discount_price === null)
                                                    <div class="tag new"><span>new</span></div>
                                                @else
                                                    <div class="tag sale">
                                                        <span>{{ $product->getDiscountPercentage() }}%</span>
                                                    </div>
                                                @endif
                                            </div><!-- /.product-image -->


                                            <div class="product-info text-left">
                                                <h3 class="name"><a
                                                        href="{{ route('product.details', $rproduct->slug) }}">{{ $rproduct->name }}</a>
                                                </h3>
                                                <div class="rating rateit-small"></div>
                                                <div class="description"></div>

                                                <div class="product-price">
                                                    @if ($rproduct->discount_price === null)
                                                        <span class="price">
                                                            ${{ $rproduct->selling_price }}
                                                        </span>
                                                    @else
                                                        <span class="price">
                                                            ${{ $rproduct->discount_price }}
                                                        </span>
                                                        <span class="price-before-discount">$
                                                            {{ $rproduct->selling_price }}</span>
                                                    @endif
                                                </div><!-- /.product-price -->

                                            </div><!-- /.product-info -->
                                            <div class="cart clearfix animate-effect">
                                                <div class="action">
                                                    <ul class="list-unstyled">
                                                        <li class="add-cart-button btn-group">
                                                            <button class="btn btn-primary icon" data-toggle="modal"
                                                                id="{{ $rproduct->id }}" onclick="productView(this.id)"
                                                                data-target="#addToCartModal" type="button"><i
                                                                    class="fa fa-shopping-cart"></i>
                                                                <button class="btn btn-primary cart-btn"
                                                                    data-toggle="modal" id="{{ $rproduct->id }}"
                                                                    onclick="productView(this.id)"
                                                                    data-target="#addToCartModal" type="button">Add to cart
                                                                </button>

                                                        </li>

                                                        <li class="lnk wishlist"><a class="add-to-cart"
                                                                href="javascript:avoid(0)" id='{{ $rproduct->id }}'
                                                                onclick="add2Wishlist(this.id)" title=" Wishlist"> <i
                                                                    class="icon fa fa-heart"></i>
                                                            </a>
                                                        </li>

                                                        <li class="lnk">
                                                            <a class="add-to-cart"
                                                                href="{{ route('product.details', $rproduct->slug) }}"
                                                                title="Compare">
                                                                <i class="fa fa-signal"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.action -->
                                            </div><!-- /.cart -->
                                        </div><!-- /.product -->

                                    </div><!-- /.products -->
                                </div><!-- /.item -->
                            @endforeach
                        </div><!-- /.home-owl-carousel -->
                    </section><!-- /.section -->
                    <!-- ============================================== UPSELL PRODUCTS : END ============================================== -->

                </div><!-- /.col -->
                <div class="clearfix"></div>
            </div><!-- /.row -->

            <x-brand-carousel />
        </div><!-- /.container -->
    </div><!-- /.body-content -->

@endsection
@push('scripts')
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-6140d5de19ff65b2"></script>
@endpush
