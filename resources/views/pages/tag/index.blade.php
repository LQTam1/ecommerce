@extends('layouts.master')
@section('title')
    Product Tag
@endsection
@section('content')
    <x-breadcrumb>
        <li><a href="#">Home</a></li>
        <li class='active capitalize'>{{ request()->tag }}</li>

    </x-breadcrumb>
    <div class="body-content outer-top-xs">
        <div class='container'>
            <div class='row'>
                <div class='col-md-3 sidebar'>
                    <!-- ===== == TOP NAVIGATION ======= ==== -->
                    <x-sidebar.category />

                    <x-sidebar.category-shopby />
                </div>
                <!-- /.sidebar -->
                <div class='col-md-9'>
                    <x-category-banner />

                    <x-head-filter-products :products="$products" />

                    <!--    //////////////////// START Product Grid View  ////////////// -->
                    <div class="search-result-container ">
                        <div id="myTabContent" class="tab-content category-list">
                            <div class="tab-pane active " id="grid-container">
                                <div class="category-product">
                                    <div class="row">
                                        @foreach ($products as $product)
                                            <div class="col-sm-6 col-md-4 wow fadeInUp">
                                                <div class="products">
                                                    <div class="product">
                                                        <div class="product-image">
                                                            <div class="image"> <a
                                                                    href="{{ route('product.details', $product->slug) }}"><img
                                                                        src="{{ asset($product->product_thumbnail) }}"
                                                                        alt=""></a> </div>
                                                            <!-- /.image -->
                                                            <div>
                                                                @if ($product->discount_price == null)
                                                                    <div class="tag new"><span>new</span></div>
                                                                @else
                                                                    <div class="tag hot">
                                                                        <span>{{ $product->getDiscountPercentage() }}%</span>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <!-- /.product-image -->
                                                        <div class="product-info text-left">
                                                            <h3 class="name"><a
                                                                    href="{{ route('product.details', $product->slug) }}">{{ $product->name }}</a>
                                                            </h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="description"></div>
                                                            @if ($product->discount_price == null)
                                                                <div class="product-price"> <span class="price">
                                                                        ${{ $product->selling_price }} </span> </div>
                                                            @else
                                                                <div class="product-price"> <span class="price">
                                                                        ${{ $product->discount_price }} </span> <span
                                                                        class="price-before-discount">$
                                                                        {{ $product->selling_price }}</span> </div>
                                                            @endif
                                                            <!-- /.product-price -->
                                                        </div>
                                                        <!-- /.product-info -->
                                                        <div class="cart clearfix animate-effect">
                                                            <div class="action">
                                                                <ul class="list-unstyled">
                                                                    <li class="add-cart-button btn-group">
                                                                        <button class="btn btn-primary icon"
                                                                            data-toggle="modal" id="{{ $product->id }}"
                                                                            onclick="productView(this.id)"
                                                                            data-target="#addToCartModal" type="button"><i
                                                                                class="fa fa-shopping-cart"></i>
                                                                        </button>
                                                                        <button class="btn btn-primary cart-btn"
                                                                            type="button">Add to
                                                                            cart
                                                                        </button>
                                                                    </li>
                                                                    <li class="lnk wishlist"><a class="add-to-cart"
                                                                            href="javascript:avoid(0)"
                                                                            id='{{ $product->id }}'
                                                                            onclick="add2Wishlist(this.id)"
                                                                            title=" Wishlist"> <i
                                                                                class="icon fa fa-heart"></i>
                                                                        </a>
                                                                    </li>>
                                                                    <li class="lnk"> <a class="add-to-cart"
                                                                            href="detail.html" title="Compare"> <i
                                                                                class="fa fa-signal"></i> </a> </li>
                                                                </ul>
                                                            </div>
                                                            <!-- /.action -->
                                                        </div>
                                                        <!-- /.cart -->
                                                    </div>
                                                    <!-- /.product -->
                                                </div>
                                                <!-- /.products -->
                                            </div>
                                            <!-- /.item -->
                                        @endforeach
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.category-product -->
                            </div>
                            <!-- /.tab-pane -->
                            <!--            //////////////////// END Product Grid View  ////////////// -->
                            <!--            //////////////////// Product List View Start ////////////// -->
                            <div class="tab-pane " id="list-container">
                                <div class="category-product">
                                    @foreach ($products as $product)
                                        <div class="category-product-inner wow fadeInUp">
                                            <div class="products">
                                                <div class="product-list product">
                                                    <div class="row product-list-row">
                                                        <div class="col col-sm-4 col-lg-4">
                                                            <div class="product-image">
                                                                <div class="image"> <img
                                                                        src="{{ asset($product->product_thumbnail) }}"
                                                                        alt=""> </div>
                                                            </div>
                                                            <!-- /.product-image -->
                                                        </div>
                                                        <!-- /.col -->
                                                        <div class="col col-sm-8 col-lg-8">
                                                            <div class="product-info">
                                                                <h3 class="name"><a
                                                                        href="{{ route('product.details', $product->slug) }}">{{ $product->name }}</a>
                                                                </h3>
                                                                <div class="rating rateit-small"></div>
                                                                @if ($product->discount_price == null)
                                                                    <div class="product-price"> <span
                                                                            class="price">
                                                                            ${{ $product->selling_price }} </span>
                                                                    </div>
                                                                @else
                                                                    <div class="product-price"> <span
                                                                            class="price">
                                                                            ${{ $product->discount_price }} </span>
                                                                        <span class="price-before-discount">$
                                                                            {{ $product->selling_price }}</span>
                                                                    </div>
                                                                @endif
                                                                <!-- /.product-price -->
                                                                <div class="description m-t-10">
                                                                    {{ $product->short_description }}</div>
                                                                <div class="cart clearfix animate-effect">
                                                                    <div class="action">
                                                                        <ul class="list-unstyled">
                                                                            <li class="add-cart-button btn-group">
                                                                                <button class="btn btn-primary icon"
                                                                                    data-toggle="modal"
                                                                                    id="{{ $product->id }}"
                                                                                    onclick="productView(this.id)"
                                                                                    data-target="#addToCartModal"
                                                                                    type="button"><i
                                                                                        class="fa fa-shopping-cart"></i>
                                                                                </button>
                                                                                <button class="btn btn-primary cart-btn"
                                                                                    type="button">Add to
                                                                                    cart
                                                                                </button>
                                                                            </li>
                                                                            <li class="lnk wishlist"><a
                                                                                    class="add-to-cart"
                                                                                    href="javascript:avoid(0)"
                                                                                    id='{{ $product->id }}'
                                                                                    onclick="add2Wishlist(this.id)"
                                                                                    title=" Wishlist"> <i
                                                                                        class="icon fa fa-heart"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li class="lnk"> <a
                                                                                    class="add-to-cart"
                                                                                    href="detail.html" title="Compare">
                                                                                    <i class="fa fa-signal"></i> </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <!-- /.action -->
                                                                </div>
                                                                <!-- /.cart -->
                                                            </div>
                                                            <!-- /.product-info -->
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                    <!-- /.product-list-row -->
                                                    <div>
                                                        @if ($product->discount_price == null)
                                                            <div class="tag new"><span>new</span></div>
                                                        @else
                                                            <div class="tag hot">
                                                                <span>{{ $product->getDiscountPercentage() }}%</span>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!-- /.product-list -->
                                            </div>
                                            <!-- /.products -->
                                        </div>
                                        <!-- /.category-product-inner -->
                                    @endforeach
                                    <!--            //////////////////// Product List View END ////////////// -->
                                </div>
                                <!-- /.category-product -->
                            </div>
                            <!-- /.tab-pane #list-container -->
                        </div>
                        <!-- /.tab-content -->
                        <div class="clearfix filters-container">
                            <div class="text-right">
                                <div class="pagination-container">
                                    <ul class="list-inline list-unstyled">
                                        {{ $products->links() }}
                                    </ul>
                                    <!-- /.list-inline -->
                                </div>
                                <!-- /.pagination-container -->
                            </div>
                            <!-- /.text-right -->
                        </div>
                        <!-- /.filters-container -->
                    </div>
                    <!-- /.search-result-container -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <!-- ============================================== BRANDS CAROUSEL ============================================== -->
            <x-brand-carousel />
            <!-- /.logo-slider -->
            <!-- ============================================== BRANDS CAROUSEL : END ============================================== -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.body-content -->
@endsection
