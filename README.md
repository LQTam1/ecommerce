## #Prerequisite Installed
- [Nginx](https://gist.github.com/nd3w/8017f2e0b8afb44188e733d2ec487deb)(Recommended) or Apache Webserver
- [PHP 7.4](https://gist.github.com/nd3w/8017f2e0b8afb44188e733d2ec487deb)
- [Composer](https://getcomposer.org/download/)
- [Git](https://git-scm.com/downloads)
- [Redis Server](https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-18-04)
## Install
Open Terminal on Linux and run:
```
git clone https://github.com/LQTam/ecommerce.git
cd ecommerce
cp .env.example .env
```

Open `.env` file and update these variables to fit to you.

```
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=ecommerce
DB_USERNAME=root
DB_PASSWORD=
```

Add your [mailtrap](https://mailtrap.io/) info
```
MAIL_MAILER=smtp
MAIL_HOST=mailhog
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"
```

Create [Stripe Account](https://stripe.com) and update `STRIPE_KEY` and `STRIPE_SECRET` in case you want to test **Stripe Payment**.
Go to `app/Providers/AppServiceProvider.php` and **Comment** all the `View::share`

And Run
```
composer install
php artisan key:generate
php artisan storage:link
php artisan migrate
php artisan queue:work redis
php artisan db:seed
```
Go to `app/Providers/AppServiceProvider.php` and **UnComment** all the `View::share`

## Test Account
**User Account**: user@gmail.com/user@gmail.com

**Admin Login Page**: _/admin/login_

**Admin Account**: super@gmail.com/super@gmail.com
