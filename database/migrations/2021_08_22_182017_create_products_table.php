<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('brand_id');
            $table->unsignedInteger('menu_item_id');
            $table->json('name');
            $table->string('slug')->unique();
            $table->string('product_code');
            $table->string('product_qty');
            $table->json('tags');
            $table->json('size')->nullable();
            $table->json('color')->nullable();
            $table->decimal('selling_price');
            $table->decimal('discount_price')->nullable();
            $table->json('short_description');
            $table->json('description');
            $table->string('product_thumbnail');
            $table->integer('hot_deals')->nullable();
            $table->integer('featured')->nullable();
            $table->integer('special_offer')->nullable();
            $table->integer('special_deals')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
