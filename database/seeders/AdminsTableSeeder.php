<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admins')->delete();
        
        \DB::table('admins')->insert(array (
            0 => 
            array (
                'id' => 2,
                'name' => 'Admin1',
                'email' => 'admin@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$qfg6DPHw9Gg2iKgXpBN6xuRhI6ef5YJFezAfchqD.wNSLwNG5am26',
                'remember_token' => 'jh3xXLtw3Puh9aGjrtxSW1ON08umY3dmEuIlxtVTTHwLOj7dEd44z5fzAkMp',
                'current_team_id' => NULL,
                'profile_photo_path' => NULL,
                'created_at' => '2021-09-13 19:28:37',
                'updated_at' => '2021-09-13 19:28:37',
            ),
            1 => 
            array (
                'id' => 3,
                'name' => 'Super Admin',
                'email' => 'super@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$JwdRyHxRy92lknigNRiewuiXgsFydwpELyYGERXqzxN5BTFmjRRIu',
                'remember_token' => 'V3MnqYaRzYgjXgBRh2s7eScbTSfhgNqNkH6AYyI94D33pXjgCbdJ52AjM8UN',
                'current_team_id' => NULL,
                'profile_photo_path' => 'profile-photos/CPYHRRR9BESHR4BkirRC4WJpWYVFbpaw2ZScv7sX.jpg',
                'created_at' => '2021-09-15 15:18:07',
                'updated_at' => '2021-09-22 08:43:39',
            ),
            2 => 
            array (
                'id' => 7,
                'name' => 'Dalton Stein',
                'email' => 'kybekohidy@mailinator.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$OUIA9v88mb/CoMrptIvB5esJbTUwWwPDX3HWiih4Ld72DN6Wjr4GO',
                'remember_token' => NULL,
                'current_team_id' => NULL,
                'profile_photo_path' => NULL,
                'created_at' => '2021-09-15 17:38:30',
                'updated_at' => '2021-09-15 17:38:30',
            ),
            3 => 
            array (
                'id' => 10,
                'name' => 'Fulton Goodman',
                'email' => 'xyjema@mailinator.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$kLO7A4Lssv8sQhHmauAIPO3U0SqagsCOAoJcJEodXHfIlN99/p4iO',
                'remember_token' => NULL,
                'current_team_id' => NULL,
                'profile_photo_path' => NULL,
                'created_at' => '2021-09-30 18:10:05',
                'updated_at' => '2021-09-30 18:10:05',
            ),
            4 => 
            array (
                'id' => 11,
                'name' => 'Kathleen Brady',
                'email' => 'qati@mailinator.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$qHsGteLA6y5CUkgfZNBXs.L10ISy4P1XjI6/H0.O7MH8avnhH0tzS',
                'remember_token' => NULL,
                'current_team_id' => NULL,
                'profile_photo_path' => NULL,
                'created_at' => '2021-09-30 18:11:03',
                'updated_at' => '2021-09-30 18:11:03',
            ),
            5 => 
            array (
                'id' => 12,
                'name' => 'Guinevere Carr',
                'email' => 'niretumo@mailinator.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$kAdBp2MY7FCzEoC.P6g/K.K1/Bq0QLjrlTWKRO6fG1K.9KmZ8UXDG',
                'remember_token' => NULL,
                'current_team_id' => NULL,
                'profile_photo_path' => NULL,
                'created_at' => '2021-09-30 18:13:35',
                'updated_at' => '2021-09-30 18:13:35',
            ),
            6 => 
            array (
                'id' => 14,
                'name' => 'Trevor Steele',
                'email' => 'qovevasoj@mailinator.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$1TUmCwdqI/v3N22kxThPxOlpgKkjLlYf6Tc7zhUg2iq4bklzI7XFS',
                'remember_token' => NULL,
                'current_team_id' => NULL,
                'profile_photo_path' => NULL,
                'created_at' => '2021-09-30 18:22:04',
                'updated_at' => '2021-09-30 18:22:04',
            ),
            7 => 
            array (
                'id' => 15,
                'name' => 'Lysandra Hanson',
                'email' => 'qycoqivop@mailinator.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$vF2Uokh.s8bYbOhxilLoRe34f1NlNKmZ5RrrD9eFm4b/TCw9v.BiK',
                'remember_token' => NULL,
                'current_team_id' => NULL,
                'profile_photo_path' => 'profile-photos/nZ9mbboBVtx8vqZ3SdVpANTJ8nnSi2FtYf0YEWP2.png',
                'created_at' => '2021-09-30 18:22:13',
                'updated_at' => '2021-09-30 18:25:12',
            ),
        ));
        
        
    }
}