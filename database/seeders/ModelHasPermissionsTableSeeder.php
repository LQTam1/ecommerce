<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ModelHasPermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('model_has_permissions')->delete();
        
        \DB::table('model_has_permissions')->insert(array (
            0 => 
            array (
                'permission_id' => 83,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            1 => 
            array (
                'permission_id' => 83,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            2 => 
            array (
                'permission_id' => 84,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            3 => 
            array (
                'permission_id' => 84,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            4 => 
            array (
                'permission_id' => 84,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            5 => 
            array (
                'permission_id' => 85,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            6 => 
            array (
                'permission_id' => 85,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            7 => 
            array (
                'permission_id' => 85,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            8 => 
            array (
                'permission_id' => 86,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            9 => 
            array (
                'permission_id' => 86,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            10 => 
            array (
                'permission_id' => 86,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            11 => 
            array (
                'permission_id' => 87,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            12 => 
            array (
                'permission_id' => 87,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            13 => 
            array (
                'permission_id' => 87,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            14 => 
            array (
                'permission_id' => 87,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            15 => 
            array (
                'permission_id' => 88,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            16 => 
            array (
                'permission_id' => 88,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            17 => 
            array (
                'permission_id' => 88,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            18 => 
            array (
                'permission_id' => 89,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            19 => 
            array (
                'permission_id' => 89,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            20 => 
            array (
                'permission_id' => 90,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            21 => 
            array (
                'permission_id' => 90,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            22 => 
            array (
                'permission_id' => 91,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            23 => 
            array (
                'permission_id' => 91,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            24 => 
            array (
                'permission_id' => 91,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            25 => 
            array (
                'permission_id' => 92,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            26 => 
            array (
                'permission_id' => 92,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            27 => 
            array (
                'permission_id' => 92,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            28 => 
            array (
                'permission_id' => 92,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            29 => 
            array (
                'permission_id' => 93,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            30 => 
            array (
                'permission_id' => 93,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            31 => 
            array (
                'permission_id' => 93,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            32 => 
            array (
                'permission_id' => 94,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            33 => 
            array (
                'permission_id' => 94,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            34 => 
            array (
                'permission_id' => 94,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            35 => 
            array (
                'permission_id' => 95,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            36 => 
            array (
                'permission_id' => 95,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            37 => 
            array (
                'permission_id' => 96,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            38 => 
            array (
                'permission_id' => 96,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            39 => 
            array (
                'permission_id' => 97,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            40 => 
            array (
                'permission_id' => 97,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            41 => 
            array (
                'permission_id' => 97,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            42 => 
            array (
                'permission_id' => 98,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            43 => 
            array (
                'permission_id' => 98,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            44 => 
            array (
                'permission_id' => 98,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            45 => 
            array (
                'permission_id' => 98,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            46 => 
            array (
                'permission_id' => 101,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            47 => 
            array (
                'permission_id' => 101,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            48 => 
            array (
                'permission_id' => 101,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            49 => 
            array (
                'permission_id' => 103,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            50 => 
            array (
                'permission_id' => 103,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            51 => 
            array (
                'permission_id' => 103,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            52 => 
            array (
                'permission_id' => 103,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            53 => 
            array (
                'permission_id' => 103,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            54 => 
            array (
                'permission_id' => 104,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            55 => 
            array (
                'permission_id' => 104,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            56 => 
            array (
                'permission_id' => 105,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            57 => 
            array (
                'permission_id' => 105,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            58 => 
            array (
                'permission_id' => 105,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            59 => 
            array (
                'permission_id' => 105,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            60 => 
            array (
                'permission_id' => 106,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            61 => 
            array (
                'permission_id' => 106,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            62 => 
            array (
                'permission_id' => 107,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            63 => 
            array (
                'permission_id' => 107,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            64 => 
            array (
                'permission_id' => 107,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            65 => 
            array (
                'permission_id' => 107,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            66 => 
            array (
                'permission_id' => 107,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            67 => 
            array (
                'permission_id' => 111,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            68 => 
            array (
                'permission_id' => 111,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            69 => 
            array (
                'permission_id' => 111,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            70 => 
            array (
                'permission_id' => 112,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            71 => 
            array (
                'permission_id' => 112,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            72 => 
            array (
                'permission_id' => 112,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            73 => 
            array (
                'permission_id' => 112,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            74 => 
            array (
                'permission_id' => 112,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            75 => 
            array (
                'permission_id' => 113,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            76 => 
            array (
                'permission_id' => 113,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            77 => 
            array (
                'permission_id' => 113,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            78 => 
            array (
                'permission_id' => 113,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            79 => 
            array (
                'permission_id' => 114,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            80 => 
            array (
                'permission_id' => 114,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            81 => 
            array (
                'permission_id' => 114,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            82 => 
            array (
                'permission_id' => 114,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            83 => 
            array (
                'permission_id' => 115,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            84 => 
            array (
                'permission_id' => 115,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            85 => 
            array (
                'permission_id' => 119,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            86 => 
            array (
                'permission_id' => 119,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            87 => 
            array (
                'permission_id' => 120,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            88 => 
            array (
                'permission_id' => 120,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            89 => 
            array (
                'permission_id' => 121,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            90 => 
            array (
                'permission_id' => 121,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            91 => 
            array (
                'permission_id' => 121,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            92 => 
            array (
                'permission_id' => 121,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            93 => 
            array (
                'permission_id' => 122,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            94 => 
            array (
                'permission_id' => 122,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            95 => 
            array (
                'permission_id' => 122,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            96 => 
            array (
                'permission_id' => 122,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            97 => 
            array (
                'permission_id' => 123,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            98 => 
            array (
                'permission_id' => 123,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            99 => 
            array (
                'permission_id' => 124,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            100 => 
            array (
                'permission_id' => 124,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            101 => 
            array (
                'permission_id' => 125,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            102 => 
            array (
                'permission_id' => 125,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            103 => 
            array (
                'permission_id' => 125,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            104 => 
            array (
                'permission_id' => 126,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            105 => 
            array (
                'permission_id' => 126,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            106 => 
            array (
                'permission_id' => 126,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            107 => 
            array (
                'permission_id' => 126,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            108 => 
            array (
                'permission_id' => 127,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            109 => 
            array (
                'permission_id' => 127,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            110 => 
            array (
                'permission_id' => 127,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            111 => 
            array (
                'permission_id' => 129,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            112 => 
            array (
                'permission_id' => 129,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            113 => 
            array (
                'permission_id' => 129,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            114 => 
            array (
                'permission_id' => 129,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            115 => 
            array (
                'permission_id' => 130,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            116 => 
            array (
                'permission_id' => 130,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            117 => 
            array (
                'permission_id' => 130,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            118 => 
            array (
                'permission_id' => 130,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            119 => 
            array (
                'permission_id' => 131,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            120 => 
            array (
                'permission_id' => 131,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            121 => 
            array (
                'permission_id' => 131,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            122 => 
            array (
                'permission_id' => 132,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            123 => 
            array (
                'permission_id' => 132,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            124 => 
            array (
                'permission_id' => 132,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            125 => 
            array (
                'permission_id' => 133,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            126 => 
            array (
                'permission_id' => 133,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            127 => 
            array (
                'permission_id' => 133,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            128 => 
            array (
                'permission_id' => 134,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            129 => 
            array (
                'permission_id' => 134,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            130 => 
            array (
                'permission_id' => 134,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            131 => 
            array (
                'permission_id' => 134,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            132 => 
            array (
                'permission_id' => 134,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            133 => 
            array (
                'permission_id' => 135,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            134 => 
            array (
                'permission_id' => 135,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            135 => 
            array (
                'permission_id' => 136,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            136 => 
            array (
                'permission_id' => 136,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            137 => 
            array (
                'permission_id' => 136,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            138 => 
            array (
                'permission_id' => 136,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            139 => 
            array (
                'permission_id' => 137,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            140 => 
            array (
                'permission_id' => 137,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            141 => 
            array (
                'permission_id' => 137,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            142 => 
            array (
                'permission_id' => 138,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            143 => 
            array (
                'permission_id' => 138,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            144 => 
            array (
                'permission_id' => 139,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            145 => 
            array (
                'permission_id' => 139,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            146 => 
            array (
                'permission_id' => 139,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            147 => 
            array (
                'permission_id' => 140,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            148 => 
            array (
                'permission_id' => 140,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            149 => 
            array (
                'permission_id' => 140,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            150 => 
            array (
                'permission_id' => 141,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            151 => 
            array (
                'permission_id' => 141,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            152 => 
            array (
                'permission_id' => 141,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            153 => 
            array (
                'permission_id' => 141,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            154 => 
            array (
                'permission_id' => 142,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            155 => 
            array (
                'permission_id' => 142,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            156 => 
            array (
                'permission_id' => 143,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            157 => 
            array (
                'permission_id' => 143,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            158 => 
            array (
                'permission_id' => 143,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            159 => 
            array (
                'permission_id' => 144,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            160 => 
            array (
                'permission_id' => 144,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            161 => 
            array (
                'permission_id' => 144,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            162 => 
            array (
                'permission_id' => 145,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            163 => 
            array (
                'permission_id' => 145,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            164 => 
            array (
                'permission_id' => 146,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            165 => 
            array (
                'permission_id' => 149,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            166 => 
            array (
                'permission_id' => 149,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            167 => 
            array (
                'permission_id' => 153,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            168 => 
            array (
                'permission_id' => 153,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            169 => 
            array (
                'permission_id' => 153,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 12,
            ),
            170 => 
            array (
                'permission_id' => 153,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            171 => 
            array (
                'permission_id' => 155,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 10,
            ),
            172 => 
            array (
                'permission_id' => 155,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 11,
            ),
            173 => 
            array (
                'permission_id' => 155,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            174 => 
            array (
                'permission_id' => 156,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            175 => 
            array (
                'permission_id' => 156,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            176 => 
            array (
                'permission_id' => 156,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            177 => 
            array (
                'permission_id' => 157,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 7,
            ),
            178 => 
            array (
                'permission_id' => 157,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
            179 => 
            array (
                'permission_id' => 157,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 15,
            ),
            180 => 
            array (
                'permission_id' => 158,
                'model_type' => 'App\\Models\\Admin',
                'model_id' => 14,
            ),
        ));
        
        
    }
}