<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CouponsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('coupons')->delete();
        
        \DB::table('coupons')->insert(array (
            0 => 
            array (
                'id' => 2,
                'name' => 'D25P',
                'type' => 'discount',
                'value' => '-25%',
                'expires' => '2021-09-22',
                'status' => 1,
                'created_at' => '2021-08-31 16:44:27',
                'updated_at' => '2021-09-23 08:48:39',
            ),
            1 => 
            array (
                'id' => 5,
                'name' => 'D20P',
                'type' => 'discount',
                'value' => '-20%',
                'expires' => '2021-09-22',
                'status' => 1,
                'created_at' => '2021-08-31 16:52:41',
                'updated_at' => '2021-09-09 17:48:34',
            ),
            2 => 
            array (
                'id' => 6,
                'name' => 'T10P',
                'type' => 'tax',
                'value' => '+10%',
                'expires' => '2021-09-03',
                'status' => 1,
                'created_at' => '2021-09-02 09:57:40',
                'updated_at' => '2021-09-02 10:01:19',
            ),
            3 => 
            array (
                'id' => 8,
                'name' => 'T15P',
                'type' => 'tax',
                'value' => '+15%',
                'expires' => '2021-09-03',
                'status' => 1,
                'created_at' => '2021-09-02 10:00:45',
                'updated_at' => '2021-09-23 08:48:25',
            ),
        ));
        
        
    }
}