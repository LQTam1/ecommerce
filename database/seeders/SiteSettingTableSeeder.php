<?php

namespace Database\Seeders;

use App\Models\SiteSetting;
use Illuminate\Database\Seeder;

class SiteSettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SiteSetting::create([
            'logo' => 'assets/images/logo.png',
            'phone1' => '+(888) 123-4567',
            'phone2' => '+(888) 456-7890',
            'email' => 'flipmart@themesground.com',
            'company_name' => 'ThemesGround',
            'company_address' => '789 Main rd, Anytown, CA 12345 USA',
            'facebook' => '#',
            'twitter' => '#',
            'linkedin' => '#',
            'youtube' => '#',
        ]);
    }
}
