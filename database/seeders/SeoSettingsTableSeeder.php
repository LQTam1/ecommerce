<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SeoSettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('seo_settings')->delete();
        
        \DB::table('seo_settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'meta_title' => 'Quis ab excepturi ac',
                'meta_author' => 'Officiis sit dolore',
                'meta_keyword' => 'Qui ipsam nulla cons',
                'meta_description' => 'Qui minim fugiat om',
                'google_analytics' => 'window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag(\'js\', new Date());

gtag(\'config\', \'UA-84816806-1\');',
                'created_at' => '2021-09-13 17:09:17',
                'updated_at' => '2021-09-13 17:21:18',
            ),
        ));
        
        
    }
}