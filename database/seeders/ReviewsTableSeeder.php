<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ReviewsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('reviews')->delete();
        
        \DB::table('reviews')->insert(array (
            0 => 
            array (
                'id' => 2,
                'product_id' => 226,
                'user_id' => 1,
                'comment' => 'Ipsa quia ipsum ve',
                'summary' => 'Neque nihil voluptat',
                'status' => 1,
                'rating' => 5,
                'created_at' => '2021-09-14 16:56:01',
                'updated_at' => '2021-09-14 16:56:33',
            ),
            1 => 
            array (
                'id' => 3,
                'product_id' => 226,
                'user_id' => 1,
                'comment' => 'Aliquam qui sequi ip',
                'summary' => 'Expedita velit eius',
                'status' => 1,
                'rating' => 3,
                'created_at' => '2021-09-17 20:05:15',
                'updated_at' => '2021-09-17 20:05:15',
            ),
            2 => 
            array (
                'id' => 4,
                'product_id' => 226,
                'user_id' => 1,
                'comment' => 'Porro quidem qui qua',
                'summary' => 'Irure repudiandae ni',
                'status' => 0,
                'rating' => 4,
                'created_at' => '2021-09-17 20:27:16',
                'updated_at' => '2021-09-17 20:27:16',
            ),
            3 => 
            array (
                'id' => 5,
                'product_id' => 266,
                'user_id' => 18,
                'comment' => 'All Good',
                'summary' => 'Nice Product',
                'status' => 1,
                'rating' => 3,
                'created_at' => '2021-09-21 17:41:15',
                'updated_at' => '2021-09-21 17:41:29',
            ),
            4 => 
            array (
                'id' => 6,
                'product_id' => 226,
                'user_id' => 18,
                'comment' => 'All Perfect',
                'summary' => 'Good product',
                'status' => 1,
                'rating' => 4,
                'created_at' => '2021-09-21 17:42:06',
                'updated_at' => '2021-09-21 17:42:18',
            ),
        ));
        
        
    }
}