<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class HomeCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('home_categories')->delete();
        
        \DB::table('home_categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'select_categories' => '[{"id":4,"label":"Clothing","slug":"clothing","link":"\\/clothing","parent":0,"sort":1,"class":"fa fa-shopping-bag","menu":1,"depth":0,"created_at":"2021-08-26T15:12:49.000000Z","updated_at":"2021-08-26T15:22:02.000000Z","role_id":0},{"id":6,"label":"Shoes","slug":"shoes","link":"\\/shoes","parent":0,"sort":7,"class":"fa fa-paw","menu":1,"depth":0,"created_at":"2021-08-26T15:14:42.000000Z","updated_at":"2021-08-27T16:58:33.000000Z","role_id":0},{"id":7,"label":"Electronics","slug":"electronics","link":"\\/electronics","parent":0,"sort":9,"class":"fa fa-laptop","menu":1,"depth":0,"created_at":"2021-08-26T15:15:11.000000Z","updated_at":"2021-08-27T16:58:33.000000Z","role_id":0},{"id":9,"label":"Jewellery","slug":"jewellery","link":"\\/jewellery","parent":0,"sort":6,"class":"fa fa-diamond","menu":1,"depth":0,"created_at":"2021-08-26T15:16:13.000000Z","updated_at":"2021-08-27T16:58:33.000000Z","role_id":0}]',
                'products_to_show' => 7,
                'created_at' => '2021-09-21 09:42:17',
                'updated_at' => '2021-09-30 15:18:01',
            ),
        ));
        
        
    }
}