<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SiteSettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('site_settings')->delete();
        
        \DB::table('site_settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'logo' => 'assets/images/logo/1712347761317985-logo.png',
            'phone1' => '+(888) 123-4567',
            'phone2' => '+(888) 456-7890',
                'email' => 'flipmart@themesground.com',
                'company_name' => 'ThemesGround',
                'company_address' => '789 Main rd, Anytown, CA 12345 USA',
                'facebook' => '#',
                'twitter' => '#',
                'linkedin' => '#',
                'youtube' => '#',
                'created_at' => '2021-09-13 08:23:07',
                'updated_at' => '2021-09-30 17:14:40',
            ),
        ));
        
        
    }
}