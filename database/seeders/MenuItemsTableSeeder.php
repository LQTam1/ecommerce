<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MenuItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('menu_items')->delete();
        
        \DB::table('menu_items')->insert(array (
            0 => 
            array (
                'id' => 4,
                'label' => '{"en":"Clothing","vi":"Quan ao"}',
                'slug' => 'clothing',
                'link' => '/clothing',
                'parent' => 0,
                'sort' => 1,
                'class' => 'fa fa-shopping-bag',
                'menu' => 1,
                'depth' => 0,
                'created_at' => '2021-08-26 15:12:49',
                'updated_at' => '2021-08-26 15:22:02',
                'role_id' => 0,
            ),
            1 => 
            array (
                'id' => 5,
                'label' => '{"en":"Dresses","vi":"Vay"}',
                'slug' => 'dresses',
                'link' => '/clothings/dress',
                'parent' => 15,
                'sort' => 3,
                'class' => 'fa fa-ban',
                'menu' => 1,
                'depth' => 2,
                'created_at' => '2021-08-26 15:13:46',
                'updated_at' => '2021-08-27 16:58:36',
                'role_id' => 0,
            ),
            2 => 
            array (
                'id' => 6,
                'label' => '{"en":"Shoes","vi":"Giay"}',
                'slug' => 'shoes',
                'link' => '/shoes',
                'parent' => 0,
                'sort' => 7,
                'class' => 'fa fa-paw',
                'menu' => 1,
                'depth' => 0,
                'created_at' => '2021-08-26 15:14:42',
                'updated_at' => '2021-08-27 16:58:33',
                'role_id' => 0,
            ),
            3 => 
            array (
                'id' => 7,
                'label' => '{"en":"Electronics","vi":"Dien tu"}',
                'slug' => 'electronics',
                'link' => '/electronics',
                'parent' => 0,
                'sort' => 9,
                'class' => 'fa fa-laptop',
                'menu' => 1,
                'depth' => 0,
                'created_at' => '2021-08-26 15:15:11',
                'updated_at' => '2021-08-27 16:58:33',
                'role_id' => 0,
            ),
            4 => 
            array (
                'id' => 8,
                'label' => '{"en":"Watches","vi":"Dong ho"}',
                'slug' => 'watches',
                'link' => '/watches',
                'parent' => 0,
                'sort' => 5,
                'class' => 'fa fa-clock-o',
                'menu' => 1,
                'depth' => 0,
                'created_at' => '2021-08-26 15:15:57',
                'updated_at' => '2021-08-27 16:58:33',
                'role_id' => 0,
            ),
            5 => 
            array (
                'id' => 9,
                'label' => '{"en":"Jewellery","vi":"Jewellery"}',
                'slug' => 'jewellery',
                'link' => '/jewellery',
                'parent' => 0,
                'sort' => 6,
                'class' => 'fa fa-diamond',
                'menu' => 1,
                'depth' => 0,
                'created_at' => '2021-08-26 15:16:13',
                'updated_at' => '2021-08-27 16:58:33',
                'role_id' => 0,
            ),
            6 => 
            array (
                'id' => 10,
                'label' => '{"en":"Health and Beauty","vi":"Suc khoe va sac dep"}',
                'slug' => 'health-and-beauty',
                'link' => '/health-beauty',
                'parent' => 0,
                'sort' => 4,
                'class' => 'fa fa-heartbeat',
                'menu' => 1,
                'depth' => 0,
                'created_at' => '2021-08-26 15:16:56',
                'updated_at' => '2021-08-27 16:58:33',
                'role_id' => 0,
            ),
            7 => 
            array (
                'id' => 11,
                'label' => '{"en":"Kids and Babies","vi":"Kids and Babies"}',
                'slug' => 'kids-and-babies',
                'link' => '/kids and babies',
                'parent' => 0,
                'sort' => 8,
                'class' => 'fa fa-paper-plane',
                'menu' => 1,
                'depth' => 0,
                'created_at' => '2021-08-26 15:17:19',
                'updated_at' => '2021-08-27 16:58:33',
                'role_id' => 0,
            ),
            8 => 
            array (
                'id' => 14,
                'label' => '{"en":"Home","vi":"Trang chu"}',
                'slug' => 'home',
                'link' => '/',
                'parent' => 0,
                'sort' => 0,
                'class' => 'fa fa-ban',
                'menu' => 1,
                'depth' => 0,
                'created_at' => '2021-08-26 15:19:19',
                'updated_at' => '2021-08-26 15:19:25',
                'role_id' => 0,
            ),
            9 => 
            array (
                'id' => 15,
                'label' => '{"en":"Men","vi":"Nam"}',
                'slug' => 'men',
                'link' => '#',
                'parent' => 4,
                'sort' => 2,
                'class' => 'fa fa-ban',
                'menu' => 1,
                'depth' => 1,
                'created_at' => '2021-08-27 16:58:29',
                'updated_at' => '2021-08-27 16:58:34',
                'role_id' => 0,
            ),
        ));
        
        
    }
}