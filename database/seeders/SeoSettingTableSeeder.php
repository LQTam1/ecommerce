<?php

namespace Database\Seeders;

use App\Models\SeoSetting;
use Illuminate\Database\Seeder;

class SeoSettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SeoSetting::create([
            'meta_title' => 'Meta Title',
            'meta_author' => 'Meta Author',
            'meta_keyword' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
            'google_analytics' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13047.58233027025!2d-98.45103495759341!3d35.15922478510743!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x87adb30af7b76acb%3A0xae2aac995c0e1ebe!2sNowhere%2C%20Oklahoma%2073038%2C%20Hoa%20K%E1%BB%B3!5e0!3m2!1svi!2s!4v1631552908216!5m2!1svi!2s',
        ]);
    }
}
