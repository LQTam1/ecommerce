<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $resources = [
            'roles',
            'permissions',
            'brands',
            'menus',
            'category_products_tab_home',
            'products',
            'stocks',
            'orders',
            'reports',
            'users',
            'admins',
            'sliders',
            'coupons',
            'countries',
            'post_categories',
            'posts',
            'siteSetting',
            'seoSetting',
            'reviews',
        ];
        $actions = [
            'view list',
            'create',
            'update',
            'delete'
        ];

        foreach ($resources as $resource) {
            foreach ($actions as $action) {
                Permission::create([
                    'name' => "$action $resource",
                    'guard_name' => 'admin'
                ]);
            }
        }
    }
}
