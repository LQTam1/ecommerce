<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Archibald Hauck III',
                'email' => 'user@gmail.com',
                'email_verified_at' => '2021-09-22 07:48:32',
                'password' => '$2y$10$nGOsmWo6ULnI5GjH.kv1WuT4RLJEt.HqqoWdb7I1v3wUKCXkK4t5C',
                'two_factor_secret' => NULL,
                'two_factor_recovery_codes' => NULL,
                'remember_token' => 'NtbhWrZsTRe77sTCwY3DaV66YxFq17ZWldKG7zqwNjGBVEVdztxLyuIklwfS',
                'current_team_id' => NULL,
                'profile_photo_path' => 'profile-photos/CJsfwntZfXbFQ9NPvo75bsvHZtWKO3qaicjHhFhf.jpg',
                'last_seen' => 1633014890,
                'created_at' => '2021-08-19 15:14:19',
                'updated_at' => '2021-09-30 15:14:50',
            ),
            1 => 
            array (
                'id' => 18,
                'name' => 'User 1',
                'email' => 'user1@gmail.com',
                'email_verified_at' => '2021-08-30 21:58:24',
                'password' => '$2y$10$.xNHZTSKhRHcqI8WYA.Dpu1v4Dhe6LaIZQQICpzYrLRakvDGpoq9S',
                'two_factor_secret' => NULL,
                'two_factor_recovery_codes' => NULL,
                'remember_token' => NULL,
                'current_team_id' => NULL,
                'profile_photo_path' => NULL,
                'last_seen' => 1632256716,
                'created_at' => '2021-08-30 21:58:14',
                'updated_at' => '2021-09-21 20:38:36',
            ),
            2 => 
            array (
                'id' => 19,
                'name' => 'Rae Stevenson',
                'email' => 'qagilo@mailinator.com',
                'email_verified_at' => '2021-09-21 20:40:31',
                'password' => '$2y$10$PEtK95MxF7z4UieEHHk4leH.zaC30eNkNbUYkHDUHAnr3TrMWLi/m',
                'two_factor_secret' => NULL,
                'two_factor_recovery_codes' => NULL,
                'remember_token' => NULL,
                'current_team_id' => NULL,
                'profile_photo_path' => NULL,
                'last_seen' => 1632256801,
                'created_at' => '2021-09-21 20:39:35',
                'updated_at' => '2021-09-21 20:40:31',
            ),
        ));
        
        
    }
}