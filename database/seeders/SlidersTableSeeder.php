<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SlidersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sliders')->delete();
        
        \DB::table('sliders')->insert(array (
            0 => 
            array (
                'id' => 1,
                'slider_img' => 'backend/assets/images//slider/1712336273834640-1709150573243265.jpg',
                'title' => '{"en":"Ratione inventore se","es":"In dolore consequatu","vi":"Pariatur Totam ut q"}',
                'description' => '{"en":"Eiusmod ut expedita","es":"Rerum consequuntur s","vi":"Ex quia et consectet"}',
                'status' => 0,
                'created_at' => '2021-09-30 12:35:58',
                'updated_at' => '2021-09-30 14:15:46',
            ),
        ));
        
        
    }
}