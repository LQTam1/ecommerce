<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductImagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product_images')->delete();
        
        \DB::table('product_images')->insert(array (
            0 => 
            array (
                'id' => 18,
                'product_id' => 263,
                'photo_name' => 'backend/assets/images/product/1711347698648237.jpg',
                'created_at' => '2021-09-19 16:19:06',
                'updated_at' => '2021-09-19 16:19:06',
            ),
            1 => 
            array (
                'id' => 19,
                'product_id' => 263,
                'photo_name' => 'backend/assets/images/product/1711347698866711.jpg',
                'created_at' => '2021-09-19 16:19:07',
                'updated_at' => '2021-09-19 16:19:07',
            ),
            2 => 
            array (
                'id' => 20,
                'product_id' => 263,
                'photo_name' => 'backend/assets/images/product/1711347699109885.jpg',
                'created_at' => '2021-09-19 16:19:07',
                'updated_at' => '2021-09-19 16:19:07',
            ),
            3 => 
            array (
                'id' => 21,
                'product_id' => 263,
                'photo_name' => 'backend/assets/images/product/1711347699281767.jpg',
                'created_at' => '2021-09-19 16:19:07',
                'updated_at' => '2021-09-19 16:19:07',
            ),
            4 => 
            array (
                'id' => 22,
                'product_id' => 264,
                'photo_name' => 'backend/assets/images/product/1711347867041506.jpg',
                'created_at' => '2021-09-19 16:21:47',
                'updated_at' => '2021-09-19 16:21:47',
            ),
            5 => 
            array (
                'id' => 23,
                'product_id' => 264,
                'photo_name' => 'backend/assets/images/product/1711347867189907.jpg',
                'created_at' => '2021-09-19 16:21:47',
                'updated_at' => '2021-09-19 16:21:47',
            ),
            6 => 
            array (
                'id' => 24,
                'product_id' => 264,
                'photo_name' => 'backend/assets/images/product/1711347867339552.jpg',
                'created_at' => '2021-09-19 16:21:47',
                'updated_at' => '2021-09-19 16:21:47',
            ),
            7 => 
            array (
                'id' => 25,
                'product_id' => 264,
                'photo_name' => 'backend/assets/images/product/1711347867493315.jpg',
                'created_at' => '2021-09-19 16:21:47',
                'updated_at' => '2021-09-19 16:21:47',
            ),
            8 => 
            array (
                'id' => 26,
                'product_id' => 264,
                'photo_name' => 'backend/assets/images/product/1711347867634695.jpg',
                'created_at' => '2021-09-19 16:21:47',
                'updated_at' => '2021-09-19 16:21:47',
            ),
            9 => 
            array (
                'id' => 27,
                'product_id' => 265,
                'photo_name' => 'backend/assets/images/product/1711548380315086.jpg',
                'created_at' => '2021-09-19 16:22:17',
                'updated_at' => '2021-09-21 21:28:51',
            ),
            10 => 
            array (
                'id' => 28,
                'product_id' => 265,
                'photo_name' => 'backend/assets/images/product/1711347898368029.jpg',
                'created_at' => '2021-09-19 16:22:17',
                'updated_at' => '2021-09-19 16:22:17',
            ),
            11 => 
            array (
                'id' => 29,
                'product_id' => 265,
                'photo_name' => 'backend/assets/images/product/1711347898515536.jpg',
                'created_at' => '2021-09-19 16:22:17',
                'updated_at' => '2021-09-19 16:22:17',
            ),
            12 => 
            array (
                'id' => 30,
                'product_id' => 265,
                'photo_name' => 'backend/assets/images/product/1711347898655977.jpg',
                'created_at' => '2021-09-19 16:22:17',
                'updated_at' => '2021-09-19 16:22:17',
            ),
            13 => 
            array (
                'id' => 31,
                'product_id' => 265,
                'photo_name' => 'backend/assets/images/product/1711548361355549.jpg',
                'created_at' => '2021-09-19 16:22:17',
                'updated_at' => '2021-09-21 21:28:33',
            ),
            14 => 
            array (
                'id' => 32,
                'product_id' => 266,
                'photo_name' => 'backend/assets/images/product/1711348009046862.jpg',
                'created_at' => '2021-09-19 16:24:02',
                'updated_at' => '2021-09-19 16:24:02',
            ),
            15 => 
            array (
                'id' => 33,
                'product_id' => 266,
                'photo_name' => 'backend/assets/images/product/1711348009317271.jpg',
                'created_at' => '2021-09-19 16:24:03',
                'updated_at' => '2021-09-19 16:24:03',
            ),
            16 => 
            array (
                'id' => 34,
                'product_id' => 266,
                'photo_name' => 'backend/assets/images/product/1711348009493906.jpg',
                'created_at' => '2021-09-19 16:24:03',
                'updated_at' => '2021-09-19 16:24:03',
            ),
            17 => 
            array (
                'id' => 35,
                'product_id' => 266,
                'photo_name' => 'backend/assets/images/product/1711348009647600.jpg',
                'created_at' => '2021-09-19 16:24:03',
                'updated_at' => '2021-09-19 16:24:03',
            ),
        ));
        
        
    }
}