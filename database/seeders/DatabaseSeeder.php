<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $super = \App\Models\Admin::factory()->create(['name' => 'Super Admin', 'email' => 'superadmin@gmail.com', 'password' => Hash::make('superadmin@gmail.com')]);
        // $admin = \App\Models\Admin::factory()->create(['name' => 'Admin', 'email' => 'admin@gmail.com', 'password' => Hash::make('admin@gmail.com')]);
        // $super->assignRole('Super-Admin');
        // $admin->assignRole('Administrator');
        $this->call([
            UsersTableSeeder::class,
            AdminsTableSeeder::class,
            BrandsTableSeeder::class,

            PersonalAccessTokensTableSeeder::class,

            PostCategoriesTableSeeder::class,
            PostsTableSeeder::class,

            ProductsTableSeeder::class,
            ProductImagesTableSeeder::class,
            ReviewsTableSeeder::class,

            RolesTableSeeder::class,
            PermissionsTableSeeder::class,
            RoleHasPermissionsTableSeeder::class,
            ModelHasPermissionsTableSeeder::class,
            ModelHasRolesTableSeeder::class,

            SeoSettingsTableSeeder::class,
            SessionsTableSeeder::class,

            OrdersTableSeeder::class,
            OrderItemsTableSeeder::class,
            ShippingsTableSeeder::class,


            ShoppingCartsTableSeeder::class,
            SiteSettingsTableSeeder::class,
            SlidersTableSeeder::class,
            CategoriesTableSeeder::class,
            CountriesTableSeeder::class,
            CouponsTableSeeder::class,
            FailedJobsTableSeeder::class,
            HomeCategoriesTableSeeder::class,
            JobsTableSeeder::class,

            MenusTableSeeder::class,
            MenuItemsTableSeeder::class,

            MigrationsTableSeeder::class,
            PasswordResetsTableSeeder::class,

            ProductTableSeeder::class
        ]);
    }
}
