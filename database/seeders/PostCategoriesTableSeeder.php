<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PostCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('post_categories')->delete();
        
        \DB::table('post_categories')->insert(array (
            0 => 
            array (
                'id' => 6,
                'name' => '{"en":"Marketing","es":"M\\u00e1rketing","vi":"Ti\\u1ebfp th\\u1ecb"}',
                'slug' => '{"en":"Marketing","es":"M\\u00e1rketing","vi":"Ti\\u1ebfp th\\u1ecb"}',
                'created_at' => '2021-09-12 10:03:10',
                'updated_at' => '2021-09-12 10:03:10',
            ),
            1 => 
            array (
                'id' => 11,
                'name' => '{"en":"Wealth","es":"Poder","vi":"Gi\\u00e0u c\\u00f3"}',
                'slug' => '{"en":"wealth","es":"poder","vi":"giau-co"}',
                'created_at' => '2021-09-12 10:19:20',
                'updated_at' => '2021-09-12 10:19:20',
            ),
            2 => 
            array (
                'id' => 12,
                'name' => '{"en":"Tech","es":"Tecnolog\\u00eda","vi":"C\\u00f4ng ngh\\u1ec7"}',
                'slug' => '{"en":"tech","es":"tecnologia","vi":"cong-nghe1"}',
                'created_at' => '2021-09-12 10:24:50',
                'updated_at' => '2021-09-12 14:37:15',
            ),
            3 => 
            array (
                'id' => 13,
                'name' => '{"en":"Technology","es":"Tecnolog\\u00eda","vi":"C\\u00f4ng ngh\\u1ec7"}',
                'slug' => '{"en":"technology","es":"tecnologia1","vi":"cong-nghe"}',
                'created_at' => '2021-09-12 10:25:40',
                'updated_at' => '2021-09-12 10:25:40',
            ),
        ));
        
        
    }
}