<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Super-Admin',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 13:55:41',
                'updated_at' => '2021-09-15 13:55:41',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Administrator',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 09:08:35',
                'updated_at' => '2021-09-15 09:15:31',
            ),
        ));
        
        
    }
}