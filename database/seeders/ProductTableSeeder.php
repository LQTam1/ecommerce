<?php

namespace Database\Seeders;

use App\Jobs\ProcessProductInsertData;
use App\Models\Brand;
use App\Models\MenuItems;
use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\DB;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        $faker = \Faker\Factory::create();
        $brands = collect(Brand::all()->modelKeys());
        $menuItems = collect(MenuItems::all()->modelKeys());
        $files =
            array_values(array_diff(scandir(public_path() . '/assets/images/products'), array('thumbnail', '.', '..')));
        $save_url = 'backend/assets/images/product/';

        for ($i = 0; $i < 10000; $i++) {
            $data[] = [
                'brand_id' => $brands->random(),
                'menu_item_id' => $menuItems->random(),
                'name' => json_encode(['en' => $faker->name()]),
                'slug' =>  $faker->unique()->slug(3) . '-' . time(),
                'product_code' => $faker->uuid(),

                'product_qty' => $faker->numberBetween(10, 100),
                'tags' => json_encode(['en' => $faker->word()]),
                'size' => json_encode(['en' => $faker->randomElement(['XS', 'S', 'M', 'L', 'XL', 'XXL'])]),
                'color' => json_encode(['en' => $faker->colorName()]),

                'selling_price' => $faker->numberBetween(100, 1000),
                'discount_price' => $faker->numberBetween(50, 250),
                'short_description' => json_encode(['en' => $faker->paragraph()]),
                'description' => json_encode(['en' => $faker->paragraph(3)]),

                'hot_deals' => $faker->randomElement([1, 0]),
                'featured' => $faker->randomElement([1, 0]),
                'special_offer' => $faker->randomElement([1, 0]),
                'special_deals' => $faker->randomElement([1, 0]),
                'product_thumbnail' => $save_url . $faker->randomElement($files),
                'status' => $faker->randomElement([1, 0]),
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ];
        }
        $jobs = [];
        $chunks = array_chunk($data, 3000);
        // DB::transaction(function () use ($chunks, $jobs) {
        foreach ($chunks as $chunk) {
            // DB::table('products')->insert($chunk);
            // ProcessProductInsertData::dispatch($chunk);
            $jobs[] = new ProcessProductInsertData($chunk);
        }
        // });
        $batch = Bus::batch($jobs)->onConnection('redis')->onQueue('import-products')->dispatch();
    }
}
