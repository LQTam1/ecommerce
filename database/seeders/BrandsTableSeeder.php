<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('brands')->delete();
        
        \DB::table('brands')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => '{"en":"Fletcher Cardenas","vi":"Dominic Palmer"}',
                'slug' => 'et-unde-cupiditate-n',
                'image' => 'backend/assets/images/brand/1709168957995311-brand1.png',
                'created_at' => '2021-08-26 15:08:57',
                'updated_at' => '2021-08-26 15:08:57',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => '{"en":"Wynne Norton","vi":"Devin Tyler"}',
                'slug' => 'possimus-sit-mollit',
                'image' => 'backend/assets/images/brand/1709168970527313-brand2.png',
                'created_at' => '2021-08-26 15:09:09',
                'updated_at' => '2021-08-26 15:09:09',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => '{"en":"Lael Bond","vi":"Dakota Talley"}',
                'slug' => 'nisi-sapiente-invent',
                'image' => 'backend/assets/images/brand/1709168979738715-brand3.png',
                'created_at' => '2021-08-26 15:09:18',
                'updated_at' => '2021-08-26 15:09:18',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => '{"en":"Joelle Fitzgerald","vi":"Florence Bailey"}',
                'slug' => 'aut-ut-et-id-aliquam',
                'image' => 'backend/assets/images/brand/1709168987102985-brand4.png',
                'created_at' => '2021-08-26 15:09:25',
                'updated_at' => '2021-09-22 18:49:15',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => '{"en":"Cynthia Summers","vi":"Carlos Sheppard"}',
                'slug' => 'placeat-a-dolorum-m',
                'image' => 'backend/assets/images/brand/1709168993032265-brand5.png',
                'created_at' => '2021-08-26 15:09:31',
                'updated_at' => '2021-08-26 15:09:31',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => '{"en":"Heather Spencer","vi":"Kaitlin Hall"}',
                'slug' => 'quo-provident-nihil',
                'image' => 'backend/assets/images/brand/1709168997859526-brand6.png',
                'created_at' => '2021-08-26 15:09:35',
                'updated_at' => '2021-08-26 15:09:35',
            ),
        ));
        
        
    }
}