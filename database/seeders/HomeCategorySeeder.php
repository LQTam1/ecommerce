<?php

namespace Database\Seeders;

use App\Models\HomeCategory;
use Illuminate\Database\Seeder;

class HomeCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        HomeCategory::create([
            'select_categories' =>\App\Models\MenuItems::inRandomOrder()->take(2)->get()->toArray(),
            'products_to_show' => 7
        ]);
    }
}
