<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ShoppingCartsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('shopping_carts')->delete();
        
        \DB::table('shopping_carts')->insert(array (
            0 => 
            array (
                'id' => '1_cart_conditions',
                'cart_data' => 'a:0:{}',
                'created_at' => '2021-09-02 17:34:36',
                'updated_at' => '2021-09-22 16:20:54',
            ),
            1 => 
            array (
                'id' => '1_cart_items',
                'cart_data' => 'O:32:"Darryldecode\\Cart\\CartCollection":1:{s:8:"' . "\0" . '*' . "\0" . 'items";a:1:{i:266;O:32:"Darryldecode\\Cart\\ItemCollection":2:{s:9:"' . "\0" . '*' . "\0" . 'config";a:6:{s:14:"format_numbers";b:1;s:8:"decimals";i:2;s:9:"dec_point";s:1:".";s:13:"thousands_sep";s:1:",";s:7:"storage";s:32:"App\\Services\\ShoppingCartService";s:6:"events";N;}s:8:"' . "\0" . '*' . "\0" . 'items";a:7:{s:2:"id";s:3:"266";s:4:"name";s:12:"Flynn Battle";s:5:"price";d:640;s:8:"quantity";i:2;s:10:"attributes";O:41:"Darryldecode\\Cart\\ItemAttributeCollection":1:{s:8:"' . "\0" . '*' . "\0" . 'items";a:3:{s:5:"color";s:20:"Anim eius sed sit of";s:4:"size";s:19:"At aliquid iusto ut";s:5:"image";s:60:"backend/assets/images/product/thumbnail/1711347996072390.jpg";}}s:10:"conditions";a:0:{}s:15:"associatedModel";a:3:{s:5:"color";a:1:{i:0;s:19:"At aliquid iusto ut";}s:4:"size";a:1:{i:0;s:20:"Anim eius sed sit of";}s:4:"slug";s:19:"praesentium-quo-non";}}}}}',
                'created_at' => '2021-09-02 17:33:35',
                'updated_at' => '2021-09-22 15:57:25',
            ),
            2 => 
            array (
                'id' => '18_cart_conditions',
                'cart_data' => 'a:0:{}',
                'created_at' => '2021-09-21 20:37:19',
                'updated_at' => '2021-09-21 20:37:19',
            ),
            3 => 
            array (
                'id' => '18_cart_items',
                'cart_data' => 'a:0:{}',
                'created_at' => '2021-08-30 21:58:44',
                'updated_at' => '2021-09-21 17:35:25',
            ),
            4 => 
            array (
                'id' => '19_cart_conditions',
                'cart_data' => 'a:0:{}',
                'created_at' => '2021-09-21 20:40:39',
                'updated_at' => '2021-09-21 20:40:39',
            ),
            5 => 
            array (
                'id' => 'details_cart_conditions',
                'cart_data' => 'a:0:{}',
                'created_at' => '2021-09-23 08:26:28',
                'updated_at' => '2021-09-23 08:26:28',
            ),
        ));
        
        
    }
}