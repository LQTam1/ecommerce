<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class OrderItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('order_items')->delete();
        
        \DB::table('order_items')->insert(array (
            0 => 
            array (
                'id' => 32,
                'order_id' => 22,
                'product_id' => 236,
                'color' => 'White',
                'size' => 'autem',
                'qty' => '1',
                'price' => 83.0,
                'created_at' => '2021-09-10 17:48:03',
                'updated_at' => '2021-09-10 17:48:03',
            ),
            1 => 
            array (
                'id' => 33,
                'order_id' => 22,
                'product_id' => 226,
                'color' => 'SaddleBrown',
                'size' => 'laudantium',
                'qty' => '1',
                'price' => 172.0,
                'created_at' => '2021-09-10 17:48:03',
                'updated_at' => '2021-09-10 17:48:03',
            ),
            2 => 
            array (
                'id' => 34,
                'order_id' => 22,
                'product_id' => 246,
                'color' => 'MediumPurple',
                'size' => 'velit',
                'qty' => '1',
                'price' => 234.0,
                'created_at' => '2021-09-10 17:48:03',
                'updated_at' => '2021-09-10 17:48:03',
            ),
            3 => 
            array (
                'id' => 35,
                'order_id' => 22,
                'product_id' => 249,
                'color' => 'DeepSkyBlue',
                'size' => 'laudantium',
                'qty' => '1',
                'price' => 135.0,
                'created_at' => '2021-09-10 17:48:03',
                'updated_at' => '2021-09-10 17:48:03',
            ),
            4 => 
            array (
                'id' => 36,
                'order_id' => 23,
                'product_id' => 226,
                'color' => 'laudantium,XS,S',
                'size' => 'SaddleBrown,Red,Green',
                'qty' => '1',
                'price' => 172.0,
                'created_at' => '2021-09-10 18:11:53',
                'updated_at' => '2021-09-10 18:11:53',
            ),
            5 => 
            array (
                'id' => 37,
                'order_id' => 24,
                'product_id' => 236,
                'color' => 'autem',
                'size' => 'White',
                'qty' => '1',
                'price' => 83.0,
                'created_at' => '2021-09-10 18:59:44',
                'updated_at' => '2021-09-10 18:59:44',
            ),
            6 => 
            array (
                'id' => 40,
                'order_id' => 27,
                'product_id' => 248,
                'color' => 'est',
                'size' => 'Silver',
                'qty' => '1',
                'price' => 80.0,
                'created_at' => '2021-09-10 19:51:00',
                'updated_at' => '2021-09-10 19:51:00',
            ),
            7 => 
            array (
                'id' => 41,
                'order_id' => 28,
                'product_id' => 236,
                'color' => 'autem',
                'size' => 'White',
                'qty' => '1',
                'price' => 83.0,
                'created_at' => '2021-09-13 17:47:38',
                'updated_at' => '2021-09-13 17:47:38',
            ),
            8 => 
            array (
                'id' => 42,
                'order_id' => 28,
                'product_id' => 248,
                'color' => 'est',
                'size' => 'Silver',
                'qty' => '1',
                'price' => 80.0,
                'created_at' => '2021-09-13 17:47:38',
                'updated_at' => '2021-09-13 17:47:38',
            ),
            9 => 
            array (
                'id' => 43,
                'order_id' => 29,
                'product_id' => 249,
                'color' => 'laudantium',
                'size' => 'DeepSkyBlue',
                'qty' => '1',
                'price' => 135.0,
                'created_at' => '2021-09-13 17:50:49',
                'updated_at' => '2021-09-13 17:50:49',
            ),
            10 => 
            array (
                'id' => 44,
                'order_id' => 30,
                'product_id' => 226,
                'color' => 'SaddleBrown',
                'size' => 'Laudantium',
                'qty' => '1',
                'price' => 172.0,
                'created_at' => '2021-09-19 16:33:03',
                'updated_at' => '2021-09-19 16:33:03',
            ),
            11 => 
            array (
                'id' => 45,
                'order_id' => 30,
                'product_id' => 266,
                'color' => 'At aliquid iusto ut',
                'size' => 'Anim eius sed sit of',
                'qty' => '3',
                'price' => 640.0,
                'created_at' => '2021-09-19 16:33:03',
                'updated_at' => '2021-09-19 16:33:03',
            ),
            12 => 
            array (
                'id' => 46,
                'order_id' => 31,
                'product_id' => 236,
                'color' => 'White',
                'size' => 'autem',
                'qty' => '3',
                'price' => 83.0,
                'created_at' => '2021-09-21 17:35:25',
                'updated_at' => '2021-09-21 17:35:25',
            ),
            13 => 
            array (
                'id' => 47,
                'order_id' => 31,
                'product_id' => 246,
                'color' => 'MediumPurple',
                'size' => 'velit',
                'qty' => '2',
                'price' => 234.0,
                'created_at' => '2021-09-21 17:35:25',
                'updated_at' => '2021-09-21 17:35:25',
            ),
            14 => 
            array (
                'id' => 48,
                'order_id' => 31,
                'product_id' => 226,
                'color' => 'SaddleBrown',
                'size' => 'laudantium',
                'qty' => '5',
                'price' => 172.0,
                'created_at' => '2021-09-21 17:35:25',
                'updated_at' => '2021-09-21 17:35:25',
            ),
        ));
        
        
    }
}