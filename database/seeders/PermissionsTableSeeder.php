<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 83,
                'name' => 'view list roles',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            1 => 
            array (
                'id' => 84,
                'name' => 'create roles',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            2 => 
            array (
                'id' => 85,
                'name' => 'update roles',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            3 => 
            array (
                'id' => 86,
                'name' => 'delete roles',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            4 => 
            array (
                'id' => 87,
                'name' => 'view list permissions',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            5 => 
            array (
                'id' => 88,
                'name' => 'create permissions',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            6 => 
            array (
                'id' => 89,
                'name' => 'update permissions',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            7 => 
            array (
                'id' => 90,
                'name' => 'delete permissions',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            8 => 
            array (
                'id' => 91,
                'name' => 'view list brands',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            9 => 
            array (
                'id' => 92,
                'name' => 'create brands',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            10 => 
            array (
                'id' => 93,
                'name' => 'update brands',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            11 => 
            array (
                'id' => 94,
                'name' => 'delete brands',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            12 => 
            array (
                'id' => 95,
                'name' => 'view list menus',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            13 => 
            array (
                'id' => 96,
                'name' => 'create menus',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            14 => 
            array (
                'id' => 97,
                'name' => 'update menus',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            15 => 
            array (
                'id' => 98,
                'name' => 'delete menus',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            16 => 
            array (
                'id' => 101,
                'name' => 'update category_products_tab_home',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            17 => 
            array (
                'id' => 103,
                'name' => 'view list products',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            18 => 
            array (
                'id' => 104,
                'name' => 'create products',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            19 => 
            array (
                'id' => 105,
                'name' => 'update products',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            20 => 
            array (
                'id' => 106,
                'name' => 'delete products',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            21 => 
            array (
                'id' => 107,
                'name' => 'view list stocks',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            22 => 
            array (
                'id' => 111,
                'name' => 'view list orders',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            23 => 
            array (
                'id' => 112,
                'name' => 'create orders',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            24 => 
            array (
                'id' => 113,
                'name' => 'update orders',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            25 => 
            array (
                'id' => 114,
                'name' => 'delete orders',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            26 => 
            array (
                'id' => 115,
                'name' => 'view list reports',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            27 => 
            array (
                'id' => 119,
                'name' => 'view list users',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            28 => 
            array (
                'id' => 120,
                'name' => 'create users',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            29 => 
            array (
                'id' => 121,
                'name' => 'update users',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            30 => 
            array (
                'id' => 122,
                'name' => 'delete users',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            31 => 
            array (
                'id' => 123,
                'name' => 'view list admins',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            32 => 
            array (
                'id' => 124,
                'name' => 'create admins',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            33 => 
            array (
                'id' => 125,
                'name' => 'update admins',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            34 => 
            array (
                'id' => 126,
                'name' => 'delete admins',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            35 => 
            array (
                'id' => 127,
                'name' => 'view list sliders',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            36 => 
            array (
                'id' => 128,
                'name' => 'create sliders',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            37 => 
            array (
                'id' => 129,
                'name' => 'update sliders',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            38 => 
            array (
                'id' => 130,
                'name' => 'delete sliders',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:20',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            39 => 
            array (
                'id' => 131,
                'name' => 'view list coupons',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            40 => 
            array (
                'id' => 132,
                'name' => 'create coupons',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            41 => 
            array (
                'id' => 133,
                'name' => 'update coupons',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            42 => 
            array (
                'id' => 134,
                'name' => 'delete coupons',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            43 => 
            array (
                'id' => 135,
                'name' => 'view list countries',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            44 => 
            array (
                'id' => 136,
                'name' => 'create countries',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            45 => 
            array (
                'id' => 137,
                'name' => 'update countries',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            46 => 
            array (
                'id' => 138,
                'name' => 'delete countries',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            47 => 
            array (
                'id' => 139,
                'name' => 'view list post_categories',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            48 => 
            array (
                'id' => 140,
                'name' => 'create post_categories',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            49 => 
            array (
                'id' => 141,
                'name' => 'update post_categories',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            50 => 
            array (
                'id' => 142,
                'name' => 'delete post_categories',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            51 => 
            array (
                'id' => 143,
                'name' => 'view list posts',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            52 => 
            array (
                'id' => 144,
                'name' => 'create posts',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            53 => 
            array (
                'id' => 145,
                'name' => 'update posts',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            54 => 
            array (
                'id' => 146,
                'name' => 'delete posts',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            55 => 
            array (
                'id' => 149,
                'name' => 'update siteSetting',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            56 => 
            array (
                'id' => 153,
                'name' => 'update seoSetting',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            57 => 
            array (
                'id' => 155,
                'name' => 'view list reviews',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            58 => 
            array (
                'id' => 156,
                'name' => 'create reviews',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            59 => 
            array (
                'id' => 157,
                'name' => 'update reviews',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
            60 => 
            array (
                'id' => 158,
                'name' => 'delete reviews',
                'guard_name' => 'admin',
                'created_at' => '2021-09-15 16:05:21',
                'updated_at' => '2021-09-15 17:38:15',
            ),
        ));
        
        
    }
}