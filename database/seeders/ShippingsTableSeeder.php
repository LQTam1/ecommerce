<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ShippingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('shippings')->delete();
        
        \DB::table('shippings')->insert(array (
            0 => 
            array (
                'id' => 2,
                'order_id' => 22,
                'user_id' => 1,
                'fname' => 'Amy',
                'lname' => 'Le',
            'phone' => '+1 (708) 477-6206',
                'email' => 'vovav@mailinator.com',
                'line1' => '468 Fabien Parkway',
                'line2' => 'Nisi aliquam excepte',
                'city' => 'Velit aliquip volupt',
                'state' => 'Neque mollitia volup',
                'country' => '6',
                'zip' => '33866',
                'created_at' => '2021-09-10 17:48:03',
                'updated_at' => '2021-09-10 17:48:03',
            ),
            1 => 
            array (
                'id' => 3,
                'order_id' => 23,
                'user_id' => 1,
                'fname' => 'Quentin',
                'lname' => 'Joyce',
            'phone' => '+1 (806) 393-4386',
                'email' => 'gymawy@mailinator.com',
                'line1' => '46 Oak Drive',
                'line2' => 'Laborum Suscipit au',
                'city' => 'Quas nobis omnis sun',
                'state' => 'Quibusdam deserunt u',
                'country' => '13',
                'zip' => '27417',
                'created_at' => '2021-09-10 18:11:53',
                'updated_at' => '2021-09-10 18:11:53',
            ),
            2 => 
            array (
                'id' => 4,
                'order_id' => 24,
                'user_id' => 1,
                'fname' => 'Amy',
                'lname' => 'Le',
            'phone' => '+1 (708) 477-6206',
                'email' => 'vovav@mailinator.com',
                'line1' => '468 Fabien Parkway',
                'line2' => 'Nisi aliquam excepte',
                'city' => 'Velit aliquip volupt',
                'state' => 'Neque mollitia volup',
                'country' => '6',
                'zip' => '33866',
                'created_at' => '2021-09-10 18:59:44',
                'updated_at' => '2021-09-10 18:59:44',
            ),
            3 => 
            array (
                'id' => 7,
                'order_id' => 27,
                'user_id' => 1,
                'fname' => 'Quentin',
                'lname' => 'Joyce',
            'phone' => '+1 (806) 393-4386',
                'email' => 'gymawy@mailinator.com',
                'line1' => '46 Oak Drive',
                'line2' => 'Laborum Suscipit au',
                'city' => 'Quas nobis omnis sun',
                'state' => 'Quibusdam deserunt u',
                'country' => '13',
                'zip' => '27417',
                'created_at' => '2021-09-10 19:51:00',
                'updated_at' => '2021-09-10 19:51:00',
            ),
            4 => 
            array (
                'id' => 8,
                'order_id' => 28,
                'user_id' => 1,
                'fname' => 'Quentin',
                'lname' => 'Joyce',
            'phone' => '+1 (806) 393-4386',
                'email' => 'gymawy@mailinator.com',
                'line1' => '46 Oak Drive',
                'line2' => 'Laborum Suscipit au',
                'city' => 'Quas nobis omnis sun',
                'state' => 'Quibusdam deserunt u',
                'country' => '13',
                'zip' => '27417',
                'created_at' => '2021-09-13 17:47:38',
                'updated_at' => '2021-09-13 17:47:38',
            ),
            5 => 
            array (
                'id' => 9,
                'order_id' => 29,
                'user_id' => 1,
                'fname' => 'Gillian',
                'lname' => 'Waller',
            'phone' => '+1 (822) 636-3092',
                'email' => 'somypu@mailinator.com',
                'line1' => '235 Green Cowley Parkway',
                'line2' => 'Nesciunt id id rep',
                'city' => 'Explicabo Eum ea eu',
                'state' => 'Laudantium inventor',
                'country' => '138',
                'zip' => '11434',
                'created_at' => '2021-09-13 17:50:49',
                'updated_at' => '2021-09-13 17:50:49',
            ),
            6 => 
            array (
                'id' => 10,
                'order_id' => 30,
                'user_id' => 1,
                'fname' => 'Gillian',
                'lname' => 'Waller',
            'phone' => '+1 (822) 636-3092',
                'email' => 'somypu@mailinator.com',
                'line1' => '235 Green Cowley Parkway',
                'line2' => 'Nesciunt id id rep',
                'city' => 'Explicabo Eum ea eu',
                'state' => 'Laudantium inventor',
                'country' => '138',
                'zip' => '11434',
                'created_at' => '2021-09-19 16:33:03',
                'updated_at' => '2021-09-19 16:33:03',
            ),
            7 => 
            array (
                'id' => 11,
                'order_id' => 31,
                'user_id' => 18,
                'fname' => 'Samantha',
                'lname' => 'Noel',
            'phone' => '+1 (526) 883-2791',
                'email' => 'nawebyjop@mailinator.com',
                'line1' => '591 Rocky Second Parkway',
                'line2' => 'Labore sint eu neque',
                'city' => 'Consequatur Magna n',
                'state' => 'Ut id quibusdam et',
                'country' => '187',
                'zip' => '86427',
                'created_at' => '2021-09-21 17:35:25',
                'updated_at' => '2021-09-21 17:35:25',
            ),
        ));
        
        
    }
}