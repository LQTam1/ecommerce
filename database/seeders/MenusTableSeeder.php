<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('menus')->delete();
        
        \DB::table('menus')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Main Menu',
                'created_at' => '2021-08-26 03:48:23',
                'updated_at' => '2021-08-26 04:26:46',
            ),
        ));
        
        
    }
}