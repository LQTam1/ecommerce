<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MigrationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('migrations')->delete();
        
        \DB::table('migrations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'migration' => '2014_10_12_000000_create_users_table',
                'batch' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'migration' => '2014_10_12_100000_create_password_resets_table',
                'batch' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'migration' => '2014_10_12_200000_add_two_factor_columns_to_users_table',
                'batch' => 1,
            ),
            3 => 
            array (
                'id' => 4,
                'migration' => '2019_08_19_000000_create_failed_jobs_table',
                'batch' => 1,
            ),
            4 => 
            array (
                'id' => 5,
                'migration' => '2019_12_14_000001_create_personal_access_tokens_table',
                'batch' => 1,
            ),
            5 => 
            array (
                'id' => 6,
                'migration' => '2021_02_02_203839_create_sessions_table',
                'batch' => 1,
            ),
            6 => 
            array (
                'id' => 7,
                'migration' => '2021_02_02_212221_create_admins_table',
                'batch' => 1,
            ),
            7 => 
            array (
                'id' => 8,
                'migration' => '2017_08_11_073824_create_menus_wp_table',
                'batch' => 2,
            ),
            8 => 
            array (
                'id' => 9,
                'migration' => '2017_08_11_074006_create_menu_items_wp_table',
                'batch' => 2,
            ),
            9 => 
            array (
                'id' => 10,
                'migration' => '2019_01_05_293551_add-role-id-to-menu-items-table',
                'batch' => 2,
            ),
            10 => 
            array (
                'id' => 11,
                'migration' => '2021_08_20_091114_create_brands_table',
                'batch' => 2,
            ),
            11 => 
            array (
                'id' => 17,
                'migration' => '2021_08_20_164834_create_categories_table',
                'batch' => 3,
            ),
            12 => 
            array (
                'id' => 18,
                'migration' => '2021_08_22_182017_create_products_table',
                'batch' => 3,
            ),
            13 => 
            array (
                'id' => 19,
                'migration' => '2021_08_22_195809_create_product_images_table',
                'batch' => 3,
            ),
            14 => 
            array (
                'id' => 20,
                'migration' => '2021_08_26_090310_create_sliders_table',
                'batch' => 3,
            ),
            15 => 
            array (
                'id' => 21,
                'migration' => '2021_08_26_175426_create_home_categories_table',
                'batch' => 3,
            ),
            16 => 
            array (
                'id' => 22,
                'migration' => '2021_08_29_082030_create_shopping_carts_table',
                'batch' => 4,
            ),
            17 => 
            array (
                'id' => 24,
                'migration' => '2021_08_31_155011_create_coupons_table',
                'batch' => 5,
            ),
            18 => 
            array (
                'id' => 25,
                'migration' => '2021_08_31_173225_create_countries_table',
                'batch' => 6,
            ),
            19 => 
            array (
                'id' => 26,
                'migration' => '2021_09_02_094942_add_type_column_to_coupons_table',
                'batch' => 7,
            ),
            20 => 
            array (
                'id' => 27,
                'migration' => '2021_09_09_153804_create_orders_table',
                'batch' => 8,
            ),
            21 => 
            array (
                'id' => 28,
                'migration' => '2021_09_09_154100_create_order_items_table',
                'batch' => 8,
            ),
            22 => 
            array (
                'id' => 29,
                'migration' => '2021_09_10_152027_create_jobs_table',
                'batch' => 9,
            ),
            23 => 
            array (
                'id' => 32,
                'migration' => '2021_09_10_164112_create_shippings_table',
                'batch' => 10,
            ),
            24 => 
            array (
                'id' => 35,
                'migration' => '2021_09_11_173843_add_return_order_column_to_orders_table',
                'batch' => 11,
            ),
            25 => 
            array (
                'id' => 37,
                'migration' => '2021_09_12_030902_add_last_seen_column_to_users_table',
                'batch' => 12,
            ),
            26 => 
            array (
                'id' => 38,
                'migration' => '2021_09_12_081043_create_post_categories',
                'batch' => 13,
            ),
            27 => 
            array (
                'id' => 42,
                'migration' => '2021_09_12_161521_create_posts_table',
                'batch' => 14,
            ),
            28 => 
            array (
                'id' => 43,
                'migration' => '2021_09_13_081203_create_site_settings_table',
                'batch' => 15,
            ),
            29 => 
            array (
                'id' => 44,
                'migration' => '2021_09_13_165956_create_seo_settings_table',
                'batch' => 16,
            ),
            30 => 
            array (
                'id' => 46,
                'migration' => '2021_09_14_145702_create_reviews_table',
                'batch' => 17,
            ),
            31 => 
            array (
                'id' => 47,
                'migration' => '2021_09_14_202248_create_permission_tables',
                'batch' => 18,
            ),
            32 => 
            array (
                'id' => 48,
                'migration' => '2021_09_17_195526_add_rating_column_for_reviews_table',
                'batch' => 19,
            ),
            33 => 
            array (
                'id' => 49,
                'migration' => '2021_09_19_100754_add_digital_file_column_to_products_table',
                'batch' => 20,
            ),
            34 => 
            array (
                'id' => 50,
                'migration' => '2021_09_19_152716_add_product_id_foreign_key_to_product_images_table',
                'batch' => 21,
            ),
            35 => 
            array (
                'id' => 53,
                'migration' => '2021_09_21_091502_change_select_categories_column_type_for_home_categories_table',
                'batch' => 22,
            ),
            36 => 
            array (
                'id' => 57,
                'migration' => '2021_09_21_175215_add_user_id_column_to_posts_table',
                'batch' => 23,
            ),
        ));
        
        
    }
}