<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Review;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReviewFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Review::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->randomElement(User::all()->modelKeys()),
            'product_id' => $this->faker->randomElement(Product::all()->modelKeys()),
            'comment' => $this->faker->paragraph(),
            'summary' => $this->faker->title(),
            'status' => 0,
            'rating' => random_int(1, 5)
        ];
    }
}
