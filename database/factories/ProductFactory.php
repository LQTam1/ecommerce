<?php

namespace Database\Factories;

use App\Models\Brand;
use App\Models\MenuItems;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $files =
            array_values(array_diff(scandir(public_path() . '/assets/images/products'), array('thumbnail', '.', '..')));
        $save_url = 'backend/assets/images/product/';
        $brands = collect(Brand::all()->modelKeys());
        $menuItems = collect(MenuItems::all()->modelKeys());
        return [
            'brand_id' => $brands->random(),
            'menu_item_id' => $menuItems->random(),
            'name' => $this->faker->name(),
            'slug' =>  $this->faker->unique()->slug(3),
            'product_code' => $this->faker->uuid(),

            'product_qty' => $this->faker->numberBetween(10, 100),
            'tags' => $this->faker->word(),
            'size' => $this->faker->randomElement(['XS', 'S', 'M', 'L', 'XL', 'XXL']),
            'color' => $this->faker->colorName(),

            'selling_price' => $this->faker->numberBetween(100, 1000),
            'discount_price' => $this->faker->numberBetween(50, 250),
            'short_description' => $this->faker->paragraph(),
            'description' => $this->faker->paragraphs(),

            'hot_deals' => $this->faker->randomElement([1, 0]),
            'featured' => $this->faker->randomElement([1, 0]),
            'special_offer' => $this->faker->randomElement([1, 0]),
            'special_deals' => $this->faker->randomElement([1, 0]),
            'product_thumbnail' => $save_url . $this->faker->randomElement($files),
            'status' => $this->faker->randomElement([1, 0]),
        ];
    }

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    // public function configure()
    // {
    //     return $this->afterCreating(function (Product $product) {
    //         $tags = [];
    //         $colors = [];
    //         $sizes = [];
    //         foreach (LaravelLocalization::getSupportedLocales() as $key => $lang) {
    //             $tags[$key] = $this->faker->word();
    //             $colors[$key] = $this->faker->colorName();
    //             $sizes[$key] = $this->faker->word();
    //         }
    //         $product->brand_id = Brand::inRandomOrder()->first()->id;
    //         $product->setTranslations('tags', $tags);
    //         $product->setTranslations('color', $colors);
    //         $product->setTranslations('size', $sizes);
    //         $product->menu_item_id = MenuItems::inRandomOrder()->first()->id;
    //         $product->save();
    //     });
    // }
}
