<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{
    EmailVerificationController,
    FrontendController,
};
use App\Http\Controllers\Frontend\ProductDetailController;
use App\Http\Controllers\User\{
    OrderController,
    ReviewController,
    ProfileInformationController,
    CheckoutController,
    CartController,
    CartCouponController,
    PasswordController,
    UserController,
    WishlistController,
};
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
], function () {
    Route::get('/', [FrontendController::class, 'home'])->name('home');
    Route::get('/details/{slug}', [ProductDetailController::class, 'show'])->name('product.details');
    Route::get('/product/tag/{tag}', [FrontendController::class, 'fetchProductByTag'])->name('product.tag');
    Route::get('/category/{slug}', [FrontendController::class, 'fetchProductByCategory'])->name('product.category');
    Route::get('/product/modal/{product}', [FrontendController::class, 'addToCartModal'])->name('product.modal');

    Route::get('/shop', [FrontendController::class, 'viewShop'])->name('shop');

    Route::group(['prefix' => 'carts', 'as' => 'carts.'], function () {
        Route::resource('coupon', CartCouponController::class)->only(['store', 'destroy']);
        Route::get('/mini-cart', [CartController::class, 'getMiniCart'])->name('mini.cart');
    });
    Route::resource('carts', CartController::class)->except(['create', 'edit'])->names(['show' => 'carts.details']);

    Route::get('/products/search', [FrontendController::class, 'productSearch'])->name('products.search');
    Route::post('/products/search-dropdown', [FrontendController::class, 'productSearchDropdown'])->name('products.search-dropdown');

    Route::group(['prefix' => 'blog', 'as' => 'blog.'], function () {
        Route::get('/', [FrontendController::class, 'blogList'])->name('index');
        Route::get('/{slug}/{id}/details', [FrontendController::class, 'blogDetails'])->name('details');
        Route::get('/{post_category}/category', [FrontendController::class, 'blogPostCategory'])->name('post.category');
    });

    Route::resource('wishlist', WishlistController::class)->only(['index', 'store', 'destroy']);

    Route::group(['middleware' => ['auth', 'user']], function () {
        Route::group(['middleware' => ['verified'], 'prefix' => 'user', 'as' => 'user.'], function () {
            Route::view('/', 'dashboard');
            Route::view('/dashboard', 'dashboard')->name('dashboard');

            Route::view('/profile', 'profile.show')->name('profile.show');
            Route::put('/profile-information', [ProfileInformationController::class, 'update'])->name('profile.update');
            Route::put('/password', [PasswordController::class, 'update'])->name('password.update');

            Route::post('reviews', [ReviewController::class, 'store'])->name('reviews.store');

            Route::resource('checkout', CheckoutController::class)->only(['index', 'store'])->names(['index' => 'checkout.view']);

            Route::group(['prefix' => 'orders'], function () {
                Route::get('/invoice/{order}/download', [OrderController::class, 'download'])->name('order.invoice.download');
                Route::get('/tracking', [OrderController::class, 'tracking'])->name('order.tracking');
                Route::post('/{order}/return', [OrderController::class, 'returnOrderRequest'])->name('order.return');
            });
            Route::resource('orders', OrderController::class)->only(['index', 'show']);
        });

        Route::group(['prefix' => 'email'], function () {
            Route::get('/verify/{id}/{hash}', [EmailVerificationController::class, 'handler'])->middleware(['signed'])->name('verification.verify');
            Route::view('/verify', 'auth.verify-email')->name('verification.notice');
            Route::post('/verification-notification', [EmailVerificationController::class, 'resend'])->middleware(['throttle:6,1'])->name('verification.send');
        });

        Route::post('/logout', [UserController::class, 'logout'])->name('logout');
    });
});