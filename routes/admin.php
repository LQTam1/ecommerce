<?php

use App\Http\Controllers\Admin\{
    AdminAuthenticatedSessionController,
    AdminController,
    BrandController,
    CategoryController,
    CountryController,
    CouponController,
    DashboardController,
    HomeProductCategoryController,
    OrderController,
    PasswordController,
    PermissionController,
    PostCategoryController,
    PostController,
    ProductController,
    ProductImageController,
    ProductStockController,
    ProfileInformationController,
    ReportController,
    ReviewController,
    RoleController,
    SeoSettingController,
    SiteSettingController,
    SliderController,
    UserController,
};
use Illuminate\Support\Facades\Route;


Route::prefix('admin')->group(function () {
    Route::middleware('guest:admin')->group(function () {
        Route::view('/', 'pages.admin.login');
        Route::view('/login', 'pages.admin.login');
        Route::post('/login', [AdminAuthenticatedSessionController::class, 'store'])->name('login');
    });

    Route::group([
        'middleware' => ['auth:sanctum,admin', 'verified', 'auth:admin'],
    ], function () {
        Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

        Route::view('/profile', 'profile.admin.show')->name('profile.show');
        Route::put('/profile-information', [ProfileInformationController::class, 'update'])->name('profile-information.update');
        Route::put('/password', [PasswordController::class, 'update'])->name('password.update');

        Route::resource('brands', BrandController::class)->except(['create', 'show']);
        Route::resource('categories', CategoryController::class)->except(['create', 'show']);

        Route::group(['prefix' => 'products', 'as' => 'products.'], function () {
            Route::get('/toggle-status/{product}', [ProductController::class, 'toggleStatus'])->name('toggle-status');
            Route::get('/images/{image}/delete', [ProductImageController::class, 'deleteProductImage'])->name('image.delete');
            Route::resource('images', ProductImageController::class)->only(['store'])->names(['store' => 'images.update']);
            Route::resource('stock', ProductStockController::class)->only(['index'])->names(['index' => 'stock.list']);
            Route::post('/import-data',[ProductController::class,'importData'])->name('import-data');
            Route::get('delete-all',function(){
                DB::table('products')->delete();
            return back()->with('success','table cleared.');
            })->name('delete-all');
        });
        Route::resource('products', ProductController::class)->except(['show']);

        Route::get('/coupons/toggle-status/{coupon}', [CouponController::class, 'toggleStatus'])->name('coupons.toggle-status');
        Route::resource('coupons', CouponController::class)->except(['show', 'create']);
        Route::resource('countries', CountryController::class)->except(['create', 'show']);
        
        Route::get('/sliders/toggle-status/{slider}', [SliderController::class, 'toggleStatus'])->name('sliders.toggle-status');
        Route::resource('sliders', SliderController::class)->except(['create', 'show']);

        Route::resource('home-category',HomeProductCategoryController::class)->only(['update','index'])->names([
            'index' => 'product.category.tabs',
            'update' => 'product.category.update',
        ]);

        Route::get('/orders/updateStatus/{order}', [OrderController::class, 'updateStatus'])->name('orders.updateStatus');
        Route::get('/return-orders/{order}/approve', [OrderController::class, 'returnApprove'])->name('return.order.approve');
        Route::get('/invoice/{order}/download', [OrderController::class, 'invoiceDownload'])->name('order.invoice.download');
        Route::resource('orders',OrderController::class)->only(['index','show']);


        Route::get('/reports', [ReportController::class, 'index'])->name('reports.index');

        Route::resource('users', UserController::class)->only(['index']);

        Route::group(['prefix' => 'blog', 'as' => 'blog.'], function () {
            Route::resource('postCategories', PostCategoryController::class)->except(['show', 'create']);
            Route::resource('posts', PostController::class)->except(['show']);
        });

        Route::group(['as' => 'settings.'], function () {
            Route::resource('siteSetting', SiteSettingController::class)->only(['edit', 'update']);
            Route::resource('seoSetting', SeoSettingController::class)->only(['edit', 'update']);
        });

        Route::get('/reviews/{review}/approve', [ReviewController::class, 'approve'])->name('reviews.approve');
        Route::resource('reviews', ReviewController::class)->only('index', 'destroy');

        Route::resource('roles', RoleController::class)->except(['create', 'show']);
        Route::resource('permissions', PermissionController::class)->except(['create', 'show']);

        Route::resource('admins', AdminController::class);

        Route::view('/menu', 'pages.admin.menu')->name('menu');

        Route::post('/logout', [AdminAuthenticatedSessionController::class, 'destroy'])->name('logout');
    });
});
