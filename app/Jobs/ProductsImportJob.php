<?php

namespace App\Jobs;

use App\Imports\ProductsImport;
use App\Models\Admin;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class ProductsImportJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;
    public $uploadFile;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Admin $user, $uploadFile)
    {
        $this->user = $user;
        $this->uploadFile = $uploadFile;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Redis::throttle('import-products')
        ->block(0)->allow(8)->every(1)
        ->then(function () {
            (new ProductsImport($this->user))->import($this->uploadFile);
        }, function () {

            return $this->release(5);
        });
    }
}
