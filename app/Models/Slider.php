<?php

namespace App\Models;

use App\Traits\HasSinglePhoto;
use App\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use HasFactory, HasTranslations, HasSinglePhoto;

    public $translatable = ['title', 'description'];

    protected $fillable = ['title', 'description', 'status', 'slider_img'];

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
