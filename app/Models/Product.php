<?php

namespace App\Models;

use App\Helpers\Helper;
use App\Traits\HasSinglePhoto;
use App\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

class Product extends Model
{
    use HasFactory, HasTranslations, HasSinglePhoto;

    public $translatable = ['name', 'tags', 'size', 'color', 'short_description', 'description'];

    protected $fillable = [
        'brand_id',
        'menu_item_id',
        'name',
        'slug',
        'product_code',
        'product_qty',
        'tags',
        'size',
        'color',
        'selling_price',
        'discount_price',
        'short_description',
        'description',
        'product_thumbnail',
        'hot_deals',
        'featured',
        'special_offer',
        'special_deals',
        'status',
        "digital_file"
    ];

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug($value);
    }

    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = $value == 'on' ? true : false;
    }

    public function setFeaturedAttribute($value)
    {
        $this->attributes['featured'] = $value == 'on' ? true : false;
    }

    public function setSpecialOfferAttribute($value)
    {
        $this->attributes['special_offer'] = $value == 'on' ? true : false;
    }

    public function setSpecialDealsAttribute($value)
    {
        $this->attributes['special_deals'] = $value == 'on' ? true : false;
    }

    public function getFinalPrice()
    {
        return $this->selling_price - $this->discount_price;
    }

    public function getDiscountPercentage()
    {
        return round($this->getFinalPrice() / $this->selling_price * 100);
    }

    public function setHotDealsAttribute($value)
    {
        $this->attributes['hot_deals'] = $value == 'on' ? true : false;
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeFeatured($query)
    {
        return $query->active()->where('featured', 1)->orderByDesc('id');
    }

    public function scopeHotDeals($query)
    {
        return $query->active()->where('hot_deals', 1)->orderByDesc('id');
    }

    public function scopeSpecialOffer($query)
    {
        return $query->active()->where('special_offer', 1)->orderByDesc('id');
    }

    public function scopeSpecialDeals($query)
    {
        return $query->active()->where('special_deals', 1)->orderByDesc('id');
    }

    public function category()
    {
        return $this->belongsTo(MenuItems::class, 'menu_item_id', 'id');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id', 'id');
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function orders()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function updateDigitalFile(UploadedFile $file, $columnName, $destination)
    {
        tap($this->$columnName, function ($previous) use ($file, $columnName, $destination) {
            $fileName = hexdec(uniqid()) . '.' . $file->getClientOriginalExtension();
            if (!is_dir($destination)) {
                mkdir($destination);
            }
            $file->move($destination, $fileName);

            $this->forceFill([
                $columnName => $destination . $fileName
            ]);

            if ($previous !== null && $this->isFileExist($previous)) {
                unlink($previous);
            }
        });
    }

    public function uploadFile()
    {
        if ($file = request()->product_thumbnail) {
            $destinationPath = Helper::IMG_BE_PATH . '/product/thumbnail';
            $this->updatePhoto($file, 'product_thumbnail', $destinationPath, [917, 1000]);
        }
        if ($digital_file = request()->digital_file) {
            $destinationPath = Helper::IMG_BE_PATH . '/product/file';
            $this->updateDigitalFile($digital_file, 'digital_file', $destinationPath);
        }
    }

    public function uploadImages()
    {
        if ($images = request()->file('images')) {
            foreach ($images as $img) {
                $productImg = new ProductImage();
                $destinationPath = Helper::IMG_BE_PATH . '/product';
                $productImg->updatePhoto($img, 'photo_name', $destinationPath, [917, 1000]);

                $productImg->forceFill(['product_id' => $this->id])->save();
            }
        }
    }
}
