<?php

namespace App\Models;

use App\Traits\HasSinglePhoto;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SiteSetting extends Model
{
    use HasFactory, HasSinglePhoto;

    protected $fillable = [
        'logo',
        'phone1',
        'phone2',
        'email',
        'company_name',
        'company_address',
        'facebook',
        'twitter',
        'linkedin',
        'youtube',
    ];
}
