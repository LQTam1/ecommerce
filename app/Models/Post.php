<?php

namespace App\Models;

use App\Traits\HasSinglePhoto;
use App\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory, HasTranslations,HasSinglePhoto;

    public $translatable = ['title', 'slug', 'details'];

    protected $fillable = ['title', 'slug', 'details', 'image', 'post_category_id'];

    public function category()
    {
        return $this->belongsTo(PostCategory::class,'post_category_id','id')->withDefault([
            'name' => 'Empty',
        ]);
    }

    public function author(){
        return $this->belongsTo(User::class,'user_id')->withDefault('Admin');
    }

    public function getImage($value){
        return !empty($value) ? asset($value) : asset('assets/images/no_image.jpg');
    }
}
