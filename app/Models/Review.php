<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;

    protected $fillable = ['product_id','user_id','comment','summary','status','rating'];

    public function scopeActive($query,$status=1){
        return $query->where('status',$status);
    }

    public function user()
    {
        return $this->belongsTo(User::class)->latest();
    }

    public function product()
    {
        return $this->belongsTo(Product::class)->latest();
    }
}
