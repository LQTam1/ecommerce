<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'value', 'type', 'expires', 'status'];

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = $value == 'on' ? true : false;
    }

    public function scopeIsNotExpired($query)
    {
        return $query->active()->where('expires', '>=', Carbon::now()->format('Y-m-d'));
    }

    public function setTypeAttribute($value)
    {
        $this->attributes['type'] = strtolower($value);
    }
}
