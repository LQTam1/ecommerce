<?php

namespace App\Models;

use App\Traits\HasSinglePhoto;
use App\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Str;
class Brand extends Model
{
    use HasFactory, HasTranslations, HasSinglePhoto;

    public $translatable = ['name'];

    protected $fillable = ['name', 'slug', 'image'];

    public function setSlugAttribute($value){
        $this->attributes['slug'] = Str::slug($value);
    }
}
