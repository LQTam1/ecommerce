<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomeCategory extends Model
{
    use HasFactory;

    protected $fillable = ['select_categories', 'products_to_show'];

    public function getSelectCategoriesAttribute($value){
        return json_decode($value);
    }
}
