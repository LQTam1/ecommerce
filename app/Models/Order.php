<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function scopeStatus($query, $condition = '=', $status = 'pending')
    {
        return $query->where('status', $condition, $status);
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function scopeReturnOrder($query, $status)
    {
        return $query->where('return_order', $status);
    }

    public function isDelivering(){
        return !in_array($this->status,['pending','processing','confirm']);
    }
}
