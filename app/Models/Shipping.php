<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    use HasFactory;

    protected $fillable = [
        'fname',
        'lname',
        'email',
        'line1',
        'line2',
        'city',
        'state',
        'zip',
        'country',
        'phone',
        'user_id'
    ];

    public function countryModel(){
        return $this->belongsTo(Country::class,'country','id');
    }

    public function getFullNameAttribute(){
        return $this->fname .' '.$this->lname;
    }
}
