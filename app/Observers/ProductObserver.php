<?php

namespace App\Observers;

use App\Models\Product;

class ProductObserver
{
    /**
     * Handle the Product "creating" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function creating(Product $product)
    {
        $product->product_thumbnail = \Helper::NO_IMAGE;

        $product->uploadFile();
    }

    /**
     * Handle the Product "created" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function created(Product $product)
    {
        $product->uploadImages();
    }

    /**
     * Handle the Product "updating" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function updating(Product $product)
    {
        $product->uploadFile();
        $product->uploadImages();
    }

    /**
     * Handle the Product "deleting" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function deleting(Product $product)
    {
        if ($product->isFileExist($product->product_thumbnail)) {
            unlink(public_path($product->product_thumbnail));
        }
        if ($product->isFileExist($product->digital_file)) {
            unlink(public_path($product->digital_file));
        }
        foreach ($product->images as $img) {
            if ($img->isFileExist($img->photo_name))
                unlink(public_path($img->photo_name));
        }
    }
}
