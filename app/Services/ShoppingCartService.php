<?php

namespace App\Services;

use App\Models\ShoppingCart;
use Darryldecode\Cart\CartCollection;

class ShoppingCartService
{
    public function has($key)
    {
        return ShoppingCart::find($key);
    }

    public function get($key)
    {
        if ($item = $this->has($key)) {
            return new CartCollection($item->cart_data);
        } else {
            return [];
        }
    }

    public function put($key, $value)
    {
        if ($row = ShoppingCart::find($key)) {
            // update
            $row->cart_data = $value;
            $row->save();
        } else {
            ShoppingCart::create([
                'id' => $key,
                'cart_data' => $value
            ]);
        }
    }
}
