<?php

namespace App\Services;

use App\Events\OrderSucceededEvent;
use App\Helpers\Helper;
use App\Models\Country;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Shipping;
use Carbon\Carbon;

class CheckoutService
{
    public $stripe;
    public $payment_info;
    public $billing_info;
    public $shipping_info;

    public function __construct(
        $payment_info,
        $billing_info,
        $shipping_info,
        $stripe = null
    ) {
        $this->stripe = $stripe;
        $this->billing_info = $billing_info;
        $this->payment_info = $payment_info;
        $this->shipping_info = $shipping_info;
    }
    public function stripePayment()
    {
        try {
            $token = $this->createToken();
            if (!isset($token['id'])) {
                return $this->responseWith('error', 'The stripe token was not generated correctly.');
            }

            $customer = $this->createCustomer($token);
            $orderId = uniqid();
            $charge = $this->createCharge($orderId, $customer);

            if ($charge['status'] === 'failed') {
                return $this->responseWith('error', 'Error in Transaction.');
            }

            $cart = \Cart::session(auth()->id());
            $cartAmount = $cart->getTotal();
            $order = $this->createOrder($cartAmount, $charge, $customer);

            $this->shipping_info['user_id'] = auth()->id();
            Shipping::updateOrCreate([
                'user_id' => $this->shipping_info['user_id'],
                'fname' => $this->shipping_info['fname'],
                'lname' => $this->shipping_info['lname'],
                'email' => $this->shipping_info['email'],
            ], [
                'line1' => $this->shipping_info['line1'],
                'line2' => $this->shipping_info['line2'],
                'city' => $this->shipping_info['city'],
                'state' => $this->shipping_info['state'],
                'zip' => $this->shipping_info['zip'],
                'country' => $this->shipping_info['country'],
                'phone' => $this->shipping_info['phone'],
            ]);

            event(new OrderSucceededEvent($order));

            $this->createOrderItems($cart, $order);

            $cart->clear();

            return response([
                'success' => 'Your order has been paid successfully! Please check mail for more information!',
                'data' => [
                    'charge' => $charge,
                    'customer' => $customer,
                    'token' => $token,
                    'stripe' => $this->stripe
                ]
            ]);
        } catch (\Exception $e) {
            return $this->responseWith('error', $e->getMessage(), 402);
        }
    }

    public function cashPayment()
    {
        try {
            $cart = \Cart::session(auth()->id());
            $cartAmount = $cart->getTotal();

            $order = $this->createOrder($cartAmount);
            $order->update(['order_number' => uniqid()]);

            $this->shipping_info['user_id'] = auth()->id();
            Shipping::updateOrCreate([
                'user_id' => $this->shipping_info['user_id'],
                'fname' => $this->shipping_info['fname'],
                'lname' => $this->shipping_info['lname'],
                'email' => $this->shipping_info['email'],
            ], [
                'line1' => $this->shipping_info['line1'],
                'line2' => $this->shipping_info['line2'],
                'city' => $this->shipping_info['city'],
                'state' => $this->shipping_info['state'],
                'zip' => $this->shipping_info['zip'],
                'country' => $this->shipping_info['country'],
                'phone' => $this->shipping_info['phone'],
            ]);

            event(new OrderSucceededEvent($order));

            $this->createOrderItems($cart, $order);

            $cart->clear();

            return $this->responseWith('success', 'Your order has been placed successfully! Please check mail for more information!', 200);
        } catch (\Exception $e) {
            return $this->responseWith('error', $e->getMessage(), 402);
        }
    }

    private function createOrder($amount, $charge = null, $customer = null)
    {
        $carbonNow = Carbon::now();
        return Order::create([
            'user_id' => auth()->id(),
            'name' => $this->getFullName(),
            'email' => $this->shipping_info['email'] ?? auth()->user()->email,
            'phone' => $this->shipping_info['phone'],
            'post_code' => $this->shipping_info['zip'],
            'notes' => $this->shipping_info['note'] ?? null,

            'payment_type' => 'Stripe',
            'payment_method' => $charge ? 'Stripe' : 'Cash On Delivery',
            'payment_type' => $charge->payment_method ?? 'Cash On Delivery',
            'transaction_id' => $charge->balance_transaction ?? null,
            'currency' => $charge->currency ?? 'usd',
            'amount' => Helper::formatThousandPrice($amount),
            'order_number' => $charge ? $charge->metadata->order_id : null,

            'invoice_no' => $customer['invoice_prefix'] ?? 'EOS' . mt_rand(10000000, 99999999),
            'order_date' => $carbonNow->format('d F Y'),
            'order_month' => $carbonNow->format('F'),
            'order_year' => $carbonNow->format('Y'),
            'status' => 'pending',
        ]);
    }

    private function createOrderItems($cart, $order)
    {
        $items = $cart->getContent();

        $items->each(function ($item) use ($order) {
            $orderItem = new OrderItem();
            $orderItem->product_id = $item->id;
            $orderItem->qty = $item->quantity; // the quantity
            $orderItem->price = $item->price; // the quantity
            $orderItem->order_id = $order->id;
            if ($item->attributes->has('size')) {
                $orderItem->size = $item->attributes['size'];
            }
            if ($item->attributes->has('color')) {
                $orderItem->color = $item->attributes['color'];
            }
            $orderItem->save();
        });
    }

    public function createToken()
    {
        return $this->stripe->tokens->create([
            'card' => [
                'name' => $this->payment_info['card_name'],
                'number' => $this->payment_info['card_no'],
                'exp_month' => $this->payment_info['exp_month'],
                'exp_year' => $this->payment_info['exp_year'],
                'cvc' => $this->payment_info['cvc'],
            ]
        ]);
    }

    public function createCustomer($token)
    {
        return
            $this->stripe->customers->create([
                'name' => $this->billing_info['fname'] . ' ' . $this->billing_info['lname'],
                'email' => $this->shipping_info['email'] ?? auth()->user()->email,
                'phone' => $this->billing_info['phone'],
                'address' => [
                    'line1' => $this->billing_info['line1'],
                    'postal_code' => $this->billing_info['zip'],
                    'city' => $this->billing_info['city'],
                    'state' => $this->billing_info['state'],
                    'country' => Country::find($this->billing_info['country'])->country_name
                ],
                'shipping' => [
                    'name' => $this->shipping_info['fname'] . ' ' . $this->shipping_info['lname'],
                    'address' => [
                        'line1' => $this->shipping_info['line1'],
                        'postal_code' => $this->shipping_info['zip'],
                        'city' => $this->shipping_info['city'],
                        'state' => $this->shipping_info['state'],
                        'country' => Country::find($this->shipping_info['country'])->country_name
                    ],
                ], 'source' => $token['id']
            ]);
    }

    public function createCharge($orderId, $customer)
    {
        return
            $this->stripe->charges->create([
                'customer' => $customer['id'],
                'currency' => 'USD',
                'amount' => Helper::formatThousandPrice(\Cart::session(auth()->id())->getTotal()) * 100,
                'description' => 'Stripe-php payment for order no.' . $orderId,
                'metadata' => ['order_id' => $orderId]
            ]);
    }

    private function responseWith($msgKey, $msgText, $code = 500)
    {
        return response()->json([
            $msgKey => $msgText,
        ], $code);
    }

    private function getFullName()
    {
        return $this->shipping_info['fname'] . ' ' . $this->shipping_info['lname'];
    }
}
