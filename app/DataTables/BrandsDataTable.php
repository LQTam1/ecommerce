<?php

namespace App\DataTables;

use App\Models\Brand;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class BrandsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn(
                'image',
                fn ($brand) => (view('datatable.single_image', [
                    'item' =>
                    [
                        'src' => $brand->image,
                        'name' => $brand->name,
                        'height' => 80,
                        'width' => 80,
                    ]
                ]))
            )
            ->addColumn('action', fn ($brand) => (view('datatable.brand.action', compact('brand'))));
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Brand $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Brand $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('brands-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('pageLength'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            )
            ->parameters([
                'lengthMenu' => [
                    [10, 25, 50, -1],
                    ['10', '25', '50', 'Show all']
                ],
                'searchDelay' => 350,
                'initComplete' => "function () {
                    this.api().columns().every(function () {
                        var column = this;
                        if(!$(column.header()).hasClass('searchable-disabled')){
                            var input = document.createElement(\"input\");
                            input.setAttribute('placeholder',$(column.header()).text())
                            $(input).appendTo($(column.footer()).empty())
                            .on('keyup', debounce( (e) => column.search(e.target.value, false, false, true).draw(), 500));
                        }
                    });
                }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex', 'id')->title('ID')->searchable(false)->addClass('searchable-disabled'),
            Column::make('name'),
            Column::make('slug'),
            Column::make('image')->searchable(false)->addClass('searchable-disabled'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center searchable-disabled'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Brands_' . date('YmdHis');
    }
}
