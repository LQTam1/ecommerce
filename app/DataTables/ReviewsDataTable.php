<?php

namespace App\DataTables;

use App\Models\Review;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class ReviewsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $currentLocale = app()->currentLocale();
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn('status', fn ($review) => (view('datatable.review.status', compact('review'))))
            ->editColumn('user_id', fn ($review) => ($review->user->name))
            ->editColumn('product_id', fn ($review) => ($review->product->name))
            ->filterColumn('user_id', fn ($query, $keyword) => ($query->whereHas('user', fn ($query) => ($query->where("name", 'like', "%$keyword%")))->get()))
            ->filterColumn('product_id', fn ($query, $keyword) => ($query->whereHas('product', fn ($query) => ($query->where("name->$currentLocale", 'like', "%$keyword%")))->get()))
            ->addColumn('action', fn ($review) => (view('datatable.review.action', compact('review'))));
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Review $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Review $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('reviews-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(0)
            ->buttons(
                Button::make('pageLength'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            )
            ->parameters([
                'lengthMenu' => [
                    [10, 25, 50, -1],
                    ['10', '25', '50', 'Show all']
                ],
                'searchDelay' => 350,
                'initComplete' => "function () {
                    this.api().columns().every(function () {
                        var column = this;
                        if(!$(column.header()).hasClass('searchable-disabled')){
                            var input = document.createElement(\"input\");
                            input.setAttribute('placeholder',$(column.header()).text())
                            $(input).appendTo($(column.footer()).empty())
                            .on('keyup', debounce( (e) => column.search(e.target.value, false, false, true).draw(), 500));
                        }
                    });
                }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex', 'id')->title('ID')->searchable(false)->addClass('searchable-disabled'),
            Column::make('summary'),
            Column::make('comment'),
            Column::make('user_id')->title('User'),
            Column::make('product_id')->title('Product'),
            Column::make('status'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center searchable-disabled'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Reviews_' . date('YmdHis');
    }
}
