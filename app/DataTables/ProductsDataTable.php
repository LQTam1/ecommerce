<?php

namespace App\DataTables;

use App\Exports\ProductsExport;
use App\Models\Product;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class ProductsDataTable extends DataTable
{
    protected $fastExcel = true;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $currentLocale = app()->currentLocale();
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn(
                'product_thumbnail',
                fn ($product) => (view('datatable.single_image', [
                    'item' =>
                    [
                        'src' => $product->product_thumbnail,
                        'name' => $product->name,
                    ]
                ]))
            )
            ->editColumn('discount_price', fn ($product) => (view('datatable.product.discount_price', compact('product'))))
            ->editColumn('status', fn ($item) => (view('datatable.status', compact('item'))))
            ->filterColumn('status', fn ($query, $keyword) => ($query->where('status', strtolower($keyword) === 'active' ? 1 : 0)))
            ->addColumn('action', fn ($product) => (view('datatable.product.action', compact('product'))));
    }

    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Product $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('dataTableBuilder')
            ->addIndex()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(0)
            ->buttons(
                Button::make('pageLength'),
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload'),
            )
            ->parameters([
                'lengthMenu' => [
                    [10, 25, 50, -1],
                    ['10', '25', '50', 'Show all']
                ],
                'searchDelay' => 350,
                'initComplete' => "function () {
                    this.api().columns().every(function () {
                        var column = this;
                        if(!$(column.header()).hasClass('searchable-disabled')){
                            var input = document.createElement(\"input\");
                            input.setAttribute('placeholder',$(column.header()).text())
                            $(input).appendTo($(column.footer()).empty())
                            .on('keyup', debounce( (e) => column.search(e.target.value, false, false, true).draw(), 500));
                        }
                    });
                }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex', 'id')->title('ID')->searchable(false)->addClass('searchable-disabled'),
            Column::computed('product_thumbnail')->addClass('searchable-disabled'),
            Column::make('name'),
            Column::make('selling_price'),
            Column::make('discount_price')->searchable(false)->addClass('searchable-disabled'),
            Column::make('status'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center searchable-disabled'),
        ];
    }

    public function fastExcelCallback()
    {
        return function ($row) {
            return [
                'brand_id' => $row['brand_id'],
                'menu_item_id' => $row['menu_item_id'],
                'name' => $row['name'],
                'slug' => $row['slug'],
                'product_code' => $row['product_code'],
                'product_qty' => $row['product_qty'],
                'tags' => $row['tags'],
                'size' => $row['size'],
                'color' => $row['color'],
                'selling_price' => $row['selling_price'],
                'discount_price' => $row['discount_price'],
                'short_description' => $row['short_description'],
                'description' => $row['description'],
                'product_thumbnail' => $row['product_thumbnail'],
                'hot_deals' => $row['hot_deals'],
                'featured' => $row['featured'],
                'special_offer' => $row['special_offer'],
                'special_deals' => $row['special_deals'],
                'status' => $row['status'],
                'digital_file' => $row['digital_file'],
            ];
        };
    }
}
