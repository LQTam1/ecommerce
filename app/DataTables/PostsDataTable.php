<?php

namespace App\DataTables;

use App\Models\Post;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PostsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $currentLocale = app()->currentLocale();
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn(
                'image',
                fn ($post) => (view('datatable.single_image', [
                    'item' =>
                    [
                        'src' => $post->image,
                        'name' => $post->name,
                    ]
                ]))
            )
            ->filterColumn('post_category_id', function ($query, $keyword) use ($currentLocale) {
                $query->whereHas('category', function ($query) use ($currentLocale, $keyword) {
                    return $query->where("name->$currentLocale", 'like', "%$keyword%");
                })->get();
            })
            ->editColumn('post_category_id', fn ($post) => ($post->category->name))
            ->addColumn('action', fn ($post) => (view('datatable.post.action', compact('post'))));
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Post $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('posts-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex','id')->title('ID')->searchable(false)->addClass('searchable-disabled'),
            Column::make('post_category_id'),
            Column::computed('image'),
            Column::make('title'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center searchable-disabled'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Posts_' . date('YmdHis');
    }
}
