<?php

namespace App\Providers;

use App\Facades\WMenu;
use App\Models\Brand;
use App\Models\HomeCategory;
use App\Models\SeoSetting;
use App\Models\SiteSetting;
use Harimayco\Menu\Facades\Menu;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('harimayco-menu', function () {
            return new WMenu();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
#        View::share('mainCategories', Menu::getByName('Main Menu'));
#        View::share('categoryProduct', HomeCategory::find(1));
#        View::share('brands',  Brand::latest()->get());
#        View::share('siteSetting',  SiteSetting::find(1));
#        View::share('seoSetting',  SeoSetting::find(1));
        // Paginator::defaultView('vendor.pagination.bootstrap-4');
        Paginator::defaultView('vendor.pagination.custom');

        // DB::listen(function ($query) {
        //     Log::info(
        //         $query->sql,
        //         $query->bindings,
        //         $query->time
        //     );
        // });
    }
}
