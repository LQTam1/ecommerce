<?php

namespace App\Listeners;

use App\Events\OrderSucceededEvent;
use App\Mail\OrderSucceededMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendOrderSucceededNotification implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  OrderSucceededEvent  $event
     * @return void
     */
    public function handle(OrderSucceededEvent $event)
    {
        $dataInvoice = [
            'order_id' => $event->order->id,
            'invoice_no' => $event->order->invoice_no,
            'amount' => $event->order->amount,
            'name' => $event->order->name,
            'email' => $event->order->email,
        ];
        Mail::to($event->order->email)->send(new OrderSucceededMail($dataInvoice));
    }
}
