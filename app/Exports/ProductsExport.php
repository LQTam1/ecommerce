<?php

namespace App\Exports;

use App\Helpers\Helper;
use Illuminate\Contracts\Translation\HasLocalePreference;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Yajra\DataTables\Exports\DataTablesCollectionExport;

class ProductsExport extends DataTablesCollectionExport implements
    WithMapping,
    WithDrawings,
    WithCustomStartCell,
    WithEvents,
    HasLocalePreference
{
    public function headings(): array
    {
        return [
            'brand_id',
            'menu_item_id',
            'name',
            'slug',
            'product_code',
            'product_qty',
            'tags',
            'size',
            'color',
            'selling_price',
            'discount_price',
            'short_description',
            'description',
            'product_thumbnail',
            'hot_deals',
            'featured',
            'special_offer',
            'special_deals',
            'status',
            'digital_file'
        ];
    }

    public function map($row): array
    {
        return [
            $row['brand_id'],
            $row['menu_item_id'],
            $row['name'],
            $row['slug'],
            $row['product_code'],
            $row['product_qty'],
            $row['tags'],
            $row['size'],
            $row['color'],
            $row['selling_price'],
            Helper::getAttributeOfFirstElement($row['discount_price'], 'span', 'data-discount-price'),
            $row['short_description'],
            $row['description'],
            Helper::getImgRelativePath(Helper::getAttributeOfFirstElement($row['product_thumbnail'], 'img', 'src')),
            $row['hot_deals'],
            $row['featured'],
            $row['special_offer'],
            $row['special_deals'],
            Helper::getAttributeOfFirstElement($row['status'], 'span', 'data-status'),
            $row['digital_file']
        ];
    }

    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Product Thumbnail');
        $drawing->setDescription('This is my logo');
        $drawing->setPath(public_path('assets/images/logo/1712347761317985-logo.png'));
        $drawing->setHeight(90);
        $drawing->setCoordinates('B3');

        return $drawing;
    }

    public function startCell(): string
    {
        return "A8";
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A8:T8')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            }
        ];
    }

    public function preferredLocale()
    {
        return app()->currentLocale();
    }
}
