<?php

namespace App\Helpers;

use DOMDocument;
use DOMXPath;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class Helper
{
    const NO_IMAGE = 'backend/assets/images/no_image.jpg';
    /**
     * Path to backend images folder
     */
    const IMG_BE_PATH = 'backend/assets/images/';

    /**
     * Group from single array to number(s) dimension arrays 
     *
     * @param Array|Collection $array
     * @param int $number
     * @return void
     */
    public static function groupBy(&$array, $number)
    {
        $group = [];
        for ($i = 0, $end = count($array) / $number; $i < $end; $i++) {
            $length = $i === 0 ? $number : (($i + 1) * $number) / ($i + 1);
            if (is_array($array)) {
                $subArray = array_slice($array, $i * $number, $length);
                array_push($group, $subArray);
            } else if (is_a($array, 'Illuminate\Database\Eloquent\Collection')) {
                $subArray = $array->slice($i * $number, $length);
                array_push($group, $subArray);
            }
        }
        return $group;
    }

    /**
     * removes some arithmetic signs (%,+,-) only
     *
     * @param $value
     * @return mixed
     */
    public static function cleanValue($value)
    {
        return str_replace(array('%', '-', '+'), '', $value);
    }

    /**
     * removes thousands_sep (,) 
     *
     * @param $value
     * @return mixed
     */
    public static function formatThousandPrice($value)
    {
        return str_replace(array(','), '', $value);
    }

    /**
     * Group the given permission collection by resource (last word in permission name)
     *
     * @param \Illuminate\Database\Eloquent\Collection  $permissions
     * @return array
     */
    public static function groupPermissionByResource($permissions)
    {
        return $permissions->groupBy(function ($item, $key) {
            $resource = explode(' ', $item->name);
            $resource = $resource[count($resource) - 1];
            return $resource;
        });
    }

    /**
     * Manual Paginator
     *
     * @param array $items
     * @param int $total
     * @param int|null $perPage
     * @param int $currentPage
     * @param array|null $options
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public static function paginator($items, $total, $perPage = 6, $currentPage, $options = [])
    {
        $offset = $perPage * ($currentPage - 1);
        $items = array_slice($items, $offset, $perPage, true);
        return new LengthAwarePaginator(
            $items,
            $total,
            $perPage,
            $currentPage,
            $options
        );
    }

    public static function filterProducts($query, $filters)
    {
        $perPage = request()->length ?? 6;
        $sortKey = request()->sortKey?? 'id';
        $order = request()->order?? 'desc';
        $currentLocale = app()->currentLocale();
        
        if (isset($filters['category'])) {
            array_map(fn ($cate) => ($query =  $query->where('menu_item_id', $cate)), $filters['category']);
        }
        if (isset($filters['brand'])) {
            array_map(fn ($brand) => ($query =  $query->where('brand_id', $brand)), $filters['brand']);
        }
        if (isset($filters['color'])) {
            array_map(fn ($color) => ($query =  $query->orWhere("color->$currentLocale", $color)), $filters['color']);
        }
        if (isset($filters['price'])) {
            $priceRange = explode(',', $filters['price']);
            $query =  $query->whereBetween("selling_price", $priceRange);
        }
        if (
            $sortKey &&
            $order
        ) {
            if ($sortKey === 'name') {
                $query =  $query->orderBy("$sortKey->$currentLocale", $order);
            } else {
                $query =  $query->orderBy($sortKey, $order);
            }
        }
        return $query->paginate($perPage);
    }

    public static function getImgRelativePath($fullPath){
        $array = explode('//',$fullPath);
        array_shift($array);
        $array = explode('/', $array[0]);
        array_shift($array);
        return implode('/',$array);
    }

    public static function getAttributeOfFirstElement($html, $expression, $attribute)
    {
        $doc = new DOMDocument();
        $doc->loadHTML($html);
        $xpath = new DOMXPath($doc);
        $query = "//$expression";
        return $xpath->query($query)[0]->getAttribute($attribute);
    }
}
