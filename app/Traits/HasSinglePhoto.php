<?php

namespace App\Traits;

use Illuminate\Http\UploadedFile;

trait HasSinglePhoto
{
    /**
     * Update the photo for table and store photo with Image Intervention Package.
     *
     * @param  \Illuminate\Http\UploadedFile  $photo
     * @param string $columnName Photo column in table
     * @param string $destination The path to photo saved
     * @param array $attributes This is the array include width and height of Image Intervention Resize
     * @return string Return path to the photo saved
     */
    public function updatePhoto(UploadedFile $photo, $columnName, $destination, $attributes)
    {
        tap($this->$columnName, function ($previous) use ($photo, $columnName, $attributes, $destination) {
            $filename = hexdec(uniqid()) . '-' . $photo->getClientOriginalName();
            if (!is_dir($destination)) {
                mkdir($destination);
            }

            $path = $destination . '/' . $filename;
            \Image::make($photo)->resize($attributes[0], $attributes[1])->save($path);
            $this->forceFill([
                $columnName => $path
            ]);

            if ($previous !== null && $this->isFileExist($previous)) {
                unlink($previous);
            }
        });
    }

    /**
     * Check file exist.
     *
     * @param string $filePath File Path to check
     * @return boolean
     */
    public function isFileExist($filePath)
    {
        return strpos($filePath,\Helper::NO_IMAGE) === false && file_exists($filePath);
    }
}
