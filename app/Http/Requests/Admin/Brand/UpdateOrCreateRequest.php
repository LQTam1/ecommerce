<?php

namespace App\Http\Requests\Admin\Brand;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateOrCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $slugUnique = Rule::unique('brands');
        if ($brand = $this->brand) {
            $slugUnique = $slugUnique->ignore($brand->id);
        }
        return [
            'name' => 'array|required',
            'name.*' => 'nullable|string|max:191',
            'name.en' => 'required|string|max:191',
            'slug' => ['required', $slugUnique],
            'image' => 'nullable|image|mimes:jpg,jpeg,png'
        ];
    }
}
