<?php

namespace App\Http\Requests\Admin\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateOrCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $slugUnique = Rule::unique('products');
        if($product = $this->product){
            $slugUnique = $slugUnique->ignore($product->id);
        }

        return
        [
            'status' => 'nullable',
            'brand_id' => 'required',
            'menu_item_id' => 'required|numeric',
            'name' => 'required',
            'slug' => ['required', $slugUnique],
            'product_code' => 'required',
            'product_qty' => 'required',
            'tags' => 'required|array',
            'size' => 'array',
            'color' => 'array',
            'selling_price' => 'required',
            'discount_price' => 'nullable',
            'short_description' => 'required|array',
            'description' => 'required|array',
            'hot_deals' => 'nullable',
            'featured' => 'nullable',
            'special_offer' => 'nullable',
            'special_deals' => 'nullable',
            'product_thumbnail' => 'nullable|image|mimes:jpg,png,jpeg',
            'digital_file' => 'nullable|file|mimes:pdf,xlx,csv'
        ];;
    }
}
