<?php

namespace App\Http\Requests\Admin\Country;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_code' => 'required|max:2',
            'country_name' => 'required|max:191',
            'phone_code' => 'required|numeric',
            'alpha_3' => 'nullable|max:3',
            'continent_code' => 'required|max:2',
            'continent_name' => 'required|max:191',
        ];
    }
}
