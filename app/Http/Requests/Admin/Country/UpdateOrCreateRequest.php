<?php

namespace App\Http\Requests\Admin\Country;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateOrCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $countryCodeUnique = Rule::unique('countries');
        $phoneCodeUnique = Rule::unique('countries');
        $continentCodeUnique = Rule::unique('countries');
        if ($country = $this->country) {
            $countryCodeUnique = $countryCodeUnique->ignore($country->id);
            $phoneCodeUnique = $phoneCodeUnique->ignore($country->id);
            $continentCodeUnique = $continentCodeUnique->ignore($country->id);
        }
        return [
            'country_code' => ['required', 'max:2', $countryCodeUnique],
            'country_name' => 'required|max:191',
            'phone_code' => ['required', 'numeric', $phoneCodeUnique],
            'alpha_3' => 'nullable|max:3',
            'continent_code' => ['required', 'max:2', $continentCodeUnique],
            'continent_name' => 'required|max:191',
        ];
    }
}
