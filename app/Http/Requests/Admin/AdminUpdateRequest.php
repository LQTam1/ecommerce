<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AdminUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $adminId = $this->admin->id;
        return [
            'name' => 'nullable|string|max:99',
            'email' => "required|email|unique:admins,email,$adminId|max:191",
            'password' => 'nullable|min:8|string|max:255',
            'newProfilePhoto' => 'nullable|image|mimes:jpg,jpeg,png|max:255',
            'permissions' => 'required|array',
            'permissions.*' => 'nullable|array',
        ];
    }

    public function messages()
    {
        return [
            'permissions.required' => 'The permissions field at least 1 permission selected.'
        ];
    }
}
