<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AdminStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|max:99',
            'email' => 'required|email|unique:admins,email|max:191',
            'profile_photo_path' => 'nullable|image|mimes:jpg,jpeg,png|max:255',
            'permissions' => 'required|array',
            'permissions.*' => 'nullable|array',
        ];
    }

    public function messages()
    {
        return [
            'permissions.required' => 'The permissions field at least 1 permission selected.'
        ];
    }
}
