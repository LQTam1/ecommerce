<?php

namespace App\Http\Requests\Admin\Setting;

use Illuminate\Foundation\Http\FormRequest;

class SiteUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logo' => 'nullable|image|mimes:jpg,jpeg,png',
            'phone1' => 'nullable',
            'phone2' => 'nullable',
            'email' => 'nullable|email|max:191',
            'company_name' => 'nullable|string|max:191',
            'company_address'  => 'nullable|string|max:191',
            'facebook'  => 'nullable|string|max:191',
            'twitter'  => 'nullable|string|max:191',
            'linkedin'  => 'nullable|string|max:191',
            'youtube'  => 'nullable|string|max:191',
        ];
    }
}
