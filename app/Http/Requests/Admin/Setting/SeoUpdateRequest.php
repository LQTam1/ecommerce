<?php

namespace App\Http\Requests\Admin\Setting;

use Illuminate\Foundation\Http\FormRequest;

class SeoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'meta_title' => 'nullable|max:191',
            'meta_author' => 'nullable|max:99',
            'meta_keyword' => 'nullable|max:191',
            'meta_description' => 'nullable',
            'google_analytics' => 'nullable',
        ];
    }
}
