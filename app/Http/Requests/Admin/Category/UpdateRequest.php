<?php

namespace App\Http\Requests\Admin\Category;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'array|required',
            'name.*' => 'nullable|string|max:191',
            'name.en' => 'required|string|max:191',
            'slug' => 'required|max:191|unique:categories,slug,' . $this->category->id,
            'newIcon' => 'nullable|string|max:99'
        ];
    }
}
