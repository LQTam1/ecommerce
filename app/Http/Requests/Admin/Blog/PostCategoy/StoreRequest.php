<?php

namespace App\Http\Requests\Admin\Blog\PostCategoy;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'array|required',
            'name.*' => 'nullable|string|max:191',
            'name.en' => 'required|string|max:191',
            'slug' => 'required|array|max:191',
            'slug.*' => 'nullable|unique:post_categories,slug->*|string|max:191',
            'slug.en' => 'required|unique:post_categories,slug->en|string|max:191',
        ];
    }
}
