<?php

namespace App\Http\Requests\Admin\Blog\Post;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $postId = $this->post->id;
        return [
            'post_category_id' => 'required|numeric',
            'newImage' => 'nullable|image|mimes:jpg,png,jpeg',
            'title' => 'required|array',
            'title.*' => 'nullable|string|max:191',
            'title.en' => 'required|string|max:191',
            'slug' => 'required|array',
            'slug.*' => "nullable|unique:posts,slug->*,$postId|string|max:191",
            'slug.en' => "required|string|unique:posts,slug->en,$postId|max:191",
            'details' => 'required|array',
            'details.*' => 'nullable|string',
            'details.en' => 'required|string',
        ];
    }

    public function messages()
    {
        return [
            'post_category_id.required' => 'The post category field is required.'
        ];
    }
}
