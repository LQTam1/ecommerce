<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserRedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->check()) {
            $userId = auth()->id();
            if (!session('user-is-online' . $userId)) {
                Session::put('user-is-online' . $userId, true);
                \App\Models\User::find($userId)->update(['last_seen' => \Carbon\Carbon::now()->timestamp]);
            }
        }
        return $next($request);
    }
}
