<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\MenuItems;
use App\Models\Post;
use App\Models\PostCategory;
use App\Models\Product;
use App\Models\Slider;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class FrontendController extends Controller
{
    public function home()
    {
        $sliders = Slider::active()->orderByDesc('id')->take(4)->get();

        $specialDeals = Product::specialDeals()->take(9)->get()->chunk(3);

        $specialOffer = Product::specialOffer()->take(9)->get()->chunk(3);

        $featured = Product::featured()->take(9)->get();

        $bestSeller = Product::withCount('orders')->withAvg('reviews', 'rating')->orderByDesc('orders_count')->take(9)->get()->chunk(2);

        $latestProducts = Product::latest()->take(8)->get();
        $skip_brand_1 = Brand::skip(1)->first();
        $skip_brand_product_1 = Product::active()->where('brand_id', $skip_brand_1->id)->orderByDesc('id')->take(8)->get();

        // dd(\App\Models\Product::active()->pluck("tags")->unique()->take(30));
        $posts = Post::with('author')->latest()->take(8)->get();
        return view('pages.home', compact(
            'sliders',
            'specialDeals',
            'specialOffer',
            'featured',
            'latestProducts',
            'bestSeller',
            'skip_brand_1',
            'skip_brand_product_1',
            'posts'
        ));
    }

    public function fetchProductByTag(Request $request, $tag)
    {
        $currentLocale = app()->currentLocale();
        $products = Product::active()->where("tags->$currentLocale", $tag)->orderByDesc('id')->paginate(3);
        return view('pages.tag.index', compact(
            'products',
        ));
    }


    public function fetchProductByCategory($slug)
    {
        $menuItem = MenuItems::where('slug', $slug)->first();
        $currentLocale = app()->currentLocale();
        $products = Product::active()->where("menu_item_id", $menuItem->id)->orderByDesc('id')->paginate(3);
        return view('pages.product.category', compact(
            'products',
        ));
    }

    public function addToCartModal(Product $product)
    {
        $product->load(['category', 'brand']);

        $color = $product->color;
        $product_color = explode(',', $color);

        $size = $product->size;
        $product_size = explode(',', $size);

        return response()->json(array(
            'product' => $product,
            'color' => $product_color,
            'size' => $product_size,
        ));
    }

    public function blogList()
    {
        $postCategories = PostCategory::latest()->get();
        $posts = Post::latest()->paginate(10);
        return view('pages.blog_list', compact('postCategories', 'posts'));
    }

    public function blogDetails($slug, $id)
    {
        $currentLocale = LaravelLocalization::getCurrentLocale();
        $postCategories = PostCategory::latest()->get();
        $post = Post::where("slug->$currentLocale", $slug)->where('id', $id)->first();
        return view('pages.blog_details', compact('postCategories', 'post'));
    }

    public function blogPostCategory(PostCategory $post_category)
    {
        $currentLocale = LaravelLocalization::getCurrentLocale();
        $postCategories = PostCategory::latest()->get();
        $posts = Post::where("post_category_id", $post_category->id)->get();
        return view('pages.blog_category_list', compact('postCategories', 'posts', 'post_category'));
    }

    public function productSearch(Request $request)
    {
        $currentLocale = app()->currentLocale();
        $fields = $request->only(['keyword', 'menu_item_id']);
        $menuItem = null;

        $products = Product::active();
        if (!empty($fields['menu_item_id'])) {
            $menuItem = MenuItems::find($fields['menu_item_id']);
            $products = $products->where("menu_item_id", $fields['menu_item_id']);
        }
        if (!empty($fields['keyword'])) {
            $products = $products
                ->where("name->$currentLocale", 'like', "%" . $fields['keyword'] . "%")
                ->orWhere("product_code", $fields['keyword'])
                ->orWhere("tags->$currentLocale", $fields['keyword'])
                ->orWhere("selling_price", $fields['keyword']);
        }
        if($filters = $request->filters){
            $products = \Helper::filterProducts($products, $filters)->withQueryString();
        }
        else{
            $perPage = $request->length ?? 6;
            $products = $products->orderByDesc('id')->paginate($perPage)->withQueryString();
        }
        return view('pages.product.search', compact(
            'products',
            'menuItem'
        ));
    }

    public function productSearchDropdown(Request $request)
    {
        $currentLocale = app()->currentLocale();
        $fields = $request->only(['keyword', 'menu_item_id']);

        $products = Product::active();
        if (!empty($fields['menu_item_id'])) {
            $products = $products->where("menu_item_id", $fields['menu_item_id']);
        }
        if (!empty($fields['keyword'])) {

            $products = $products
                ->where("name->$currentLocale", 'like', "%" . $fields['keyword'] . "%")
                ->orWhere("product_code", $fields['keyword'])
                ->orWhere("tags->$currentLocale", $fields['keyword'])
                ->orWhere("selling_price", $fields['keyword']);
        }

        $products = $products->orderByDesc('id')->get(['name', 'product_thumbnail', 'selling_price', 'id', 'slug'])->take(5);
        return view('pages.product.search-dropdown', compact('products'));
    }

    public function viewShop(Request $request)
    {
        $currentLocale = app()->currentLocale();
        $products = Product::active();
        $filters = $request->filters;
        $categories = MenuItems::orderBy('label')->get();
        $products = \Helper::filterProducts($products, $filters);
        $products = $products->withQueryString();
        return view('pages.shop_index', compact(
            'products',
        ));
    }
}
