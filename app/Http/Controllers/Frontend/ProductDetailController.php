<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;

class ProductDetailController extends Controller
{
    public function show($slug)
    {
        $product = Product::with(['images', 'reviews' => fn ($query) => ($query->active()), 'reviews.user'])
        ->withCount(['reviews' => fn($query) => ($query->active())])
        ->withAvg('reviews','rating')
        ->where('slug', $slug)->first();
        $relatedProducts = Product::active()->where('menu_item_id', $product->menu_item_id)->where('id', '!=', $product->id)->orderByDesc('id')->take(8)->get();
        return view('pages.product-details', compact('product', 'relatedProducts'));
    }
}
