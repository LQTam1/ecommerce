<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Shipping;
use App\Services\CheckoutService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CheckoutController extends Controller
{
    public function index()
    {
        $countries = Cache::remember('countries', 360, fn () => (Country::orderBy('country_name')->get()));
        $shippings = Shipping::where('user_id', auth()->id())->get()->unique('fname');
        return view('pages.checkout', compact('countries', 'shippings'));
    }

    public function store(Request $request)
    {
        $payment_info = $request->payment_info;
        $shipping_info = $request->shipping_info;
        $billing_info = $request->billing_info;
        if ($request->shipping_id) {
            $shipping_info = Shipping::find($request->shipping_id)->toArray();
            $billing_info = $shipping_info;
        }
        $payment_method = $request->payment_method;
        $stripe = new \Stripe\StripeClient(config('services.stripe.secret', 'sk_test_BmUW6GNJJQKV6gTRQKl55eyO00YEl8caza'));
        if ($payment_method === 'stripe') {
            $stripeService = new CheckoutService($payment_info, $billing_info, $shipping_info, $stripe);
            return $stripeService->stripePayment();
        }
        if ($payment_method === 'cash') {
            $cashService = new CheckoutService($payment_info, $billing_info, $shipping_info);
            return $cashService->cashPayment();
        }
    }
}
