<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function logout()
    {
        $userId = auth()->id();
        auth()->logout();
        session()->forget('user-is-online' . $userId);
        \App\Models\User::find($userId)->update(['last_seen' => \Carbon\Carbon::now()->timestamp]);
        return redirect(route('login'));
    }
}
