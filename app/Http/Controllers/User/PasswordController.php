<?php

namespace App\Http\Controllers\User;

use App\Actions\Fortify\UpdateUserPassword;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PasswordController extends Controller
{
    public function updatePassword(Request $request, UpdateUserPassword $updater)
    {
        $updater->update($request->user(), $request->all());
        return back()->with('success', 'User password has been updated successfully!');
    }
}
