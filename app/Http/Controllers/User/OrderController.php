<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Shipping;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::where('user_id', auth()->id())->orderByDesc('id')->get();
        return view('pages.user.orders.index', compact('orders'));
    }

    public function show(Order $order)
    {
        $order->load(['items.product']);
        $shipping = Shipping::where('id', $order->id)->orWhere('user_id', $order->user_id)->orWhere('email', $order->email)->orWhere('phone', $order->phone)->first();
        return view('pages.user.orders.show', compact('order', 'shipping'));
    }

    public function download(Order $order)
    {
        $order->load(['items', 'items.product']);
        $options     = config('datatables-buttons.snappy.options');
        $orientation = config('datatables-buttons.snappy.orientation');
        $shipping = Shipping::where('id', $order->id)->orWhere('user_id', $order->user_id)->orWhere('email', $order->email)->orWhere('phone', $order->phone)->first();
        return \PDF::loadHTML(view('pages.user.orders.pdf_invoice', compact('order', 'shipping')))
            ->setPaper('a4')
            ->setOptions($options)
            ->setOrientation($orientation)
            ->download("invoice_{$order->invoice_no}.pdf");
    }

    public function returnOrderRequest(Request $request, Order $order)
    {
        $validated = $request->validate(['return_reason' => 'required|string']);
        $order->update([
            'return_date' => \Carbon\Carbon::now()->format('d F Y'),
            'return_reason' => $validated['return_reason'],
            'return_order' => 1,
        ]);
        return redirect(route('user.orders.index'))->with('success', 'Your return order request has been sent successfully!');
    }

    public function tracking(Request $request)
    {
        $trackOrder = Order::with(['items', 'items.product'])->where('invoice_no', $request->invoice_no)->first();
        $shipping = Shipping::where('id', $trackOrder->id)->orWhere('user_id', $trackOrder->user_id)->orWhere('email', $trackOrder->email)->orWhere('phone', $trackOrder->phone)->first();
        if (!$trackOrder) {
            return back()->with('error', 'Invoice Code Is Invalid');
        }
        return view('pages.user.orders.tracking', compact('trackOrder','shipping'));
    }
}
