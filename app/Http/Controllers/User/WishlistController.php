<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class WishlistController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            $items = [];
            $wish_list = app('wishlist');

            $wish_list->getContent()->each(function ($item) use (&$items) {
                $items[] = $item;
            });

            return response(array(
                'success' => true,
                'data' => $items,
                'message' => 'Wishlist get items success'
            ), 200, []);
        } else {
            return view('pages.wishlist');
        }
    }

    public function store(Request $request)
    {
        $wish_list = app('wishlist');
        $id = $request->id;
        $product = Product::findOrFail($id);
        $data = [
            'id' => $id,
            'name' => $product->name,
            'price' => $product->selling_price,
            'quantity' => 1,
            'attributes' => [
                'image' => $product->product_thumbnail,
                'color' => $product->color,
                'size' => $product->size,
            ],
            'associatedModel' => [
                'color' => explode(',', $product->color),
                'size' => explode(',', $product->size),
                'slug' => $product->slug,
                'discount_price' => $product->discount_price
            ]
        ];

        $wish_list->add($data);
        return response()->json(['success' => 'Product has been added to your Wishlist']);
    }

    public function destroy($rowId)
    {
        $wish_list = app('wishlist');

        $wish_list->remove($rowId);

        return response(array(
            'success' => true,
            'data' => $rowId,
            'message' => "Product item {$rowId} has been removed from Wishlist"
        ), 200, []);
    }
}
