<?php

namespace App\Http\Controllers\User;

use App\Actions\Fortify\PasswordValidationRules;
use App\Actions\Fortify\UpdateUserProfileInformation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileInformationController extends Controller
{
    use PasswordValidationRules;

    public function update(Request $request,UpdateUserProfileInformation $updater)
    {
        $updater->update($request->user(), $request->all());
        return back()->with('success', 'User profile has been updated successfully!');
    }
}
