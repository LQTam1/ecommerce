<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function store(Request $request)
    {
        $validated = $request->validate([
            'summary' => 'required|max:191',
            'comment' => 'required',
            'product_id' => 'required|numeric',
            'rating' => 'required|numeric'
        ]);
        auth()->user()->reviews()->create($validated);
        return back()->with('success', 'Your review has been sent successfully!');
    }
}
