<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use App\Models\Product;
use Illuminate\Http\Request;
use Cart;
use Darryldecode\Cart\CartCondition;
use Darryldecode\Cart\Helpers\Helpers;

class CartController extends Controller
{
    public function index()
    {
        $userId = auth()->id(); // get this from session or wherever it came from

        if (request()->ajax()) {
            $items = [];

            Cart::session($userId)->getContent()->each(function ($item) use (&$items) {
                $item['attributes']['image'] = asset($item['attributes']['image']);
                $items[] = $item;
            });

            return response(array(
                'success' => true,
                'data' => $items,
                'message' => 'Cart get items success'
            ), 200, []);
        } else {
            return view('pages.cart');
        }
    }

    public function store(Request $request)
    {
        $id = $request->id;
        $userId = auth()->id(); // get this from session or wherever it came from
        $product = Product::findOrFail($id);
        $price = $product->selling_price;
        if ($product->discount_price !== null) {
            $price = $product->discount_price;
        }
        Cart::session($userId)->add([
            'id' => $id,
            'name' => $request->name,
            'price' => $price,
            'quantity' => $request->quantity,
            'attributes' => [
                'color' => $request->color,
                'size' => $request->size,
                'image' => $product->product_thumbnail,
            ],
            'associatedModel' => [
                'color' => explode(',', $product->color),
                'size' => explode(',', $product->size),
                'slug' => $product->slug,
            ]
        ]);
        if($request->ajax()){
            return response()->json(['success' => 'Product has been added on Your Cart']);
        }
        return back()->with('success','Product has been added on your cart.');
    }

    public function update(Request $request, $rowId)
    {
        $userId = auth()->id();
        $data = $request->only(['attributes', 'quantity']);
        $cart = Cart::session($userId);
        $cart->update($rowId, [
            'attributes' => $data['attributes'],
            'quantity' => [
                'relative' => false,
                'value' => $data['quantity']
            ]
        ]);
        return response(array(
            'success' => true,
            'data' => [
                'sub_total' => $cart->getSubTotal(),
                'total' => $cart->getTotal()
            ],
            'message' => "Cart item {$rowId} updated successfully."
        ), 200, []);
    }

    public function getMiniCart()
    {
        $userId = auth()->id();
        $carts = Cart::session($userId)->getContent();
        $cartQty = $carts->count();
        $cartTotal = Cart::session($userId)->getTotal();

        return response()->json(array(
            'carts' => $carts,
            'cartQty' => $cartQty,
            'cartTotal' => round($cartTotal),
        ));
    }

    public function destroy($rowId)
    {
        Cart::session(auth()->id())->remove($rowId);
        return response()->json(['success' => "Cart item {$rowId} has been removed from Cart"]);
    }

    public function show($userId)
    {
        // $userId = auth()->id(); // get this from session or wherever it came from
        $cart = Cart::session($userId);
        // get subtotal applied condition amount
        $conditions = $cart->getConditions();
        $cartTotalQuantity = $cart->getTotalQuantity();
        if ($cartTotalQuantity === 0) {
            session()->forget('coupon');
            $cart->clearCartConditions();
        }

        // get conditions that are applied to cart sub totals
        $subTotalConditions = $conditions->filter(function (CartCondition $condition) {
            return $condition->getTarget() == 'subtotal';
        })->map(function (CartCondition $c) use ($userId) {
            return [
                'name' => $c->getName(),
                'type' => $c->getType(),
                'target' => $c->getTarget(),
                'value' => $c->getValue(),
            ];
        });

        $subTotal = $cart->getSubTotal(false);
        // get conditions that are applied to cart totals
        $totalConditions = $conditions->filter(function (CartCondition $condition) {
            return $condition->getTarget() == 'total';
        })->map(function (CartCondition $c) use ($subTotal) {
            $discount = Helpers::formatValue($c->getCalculatedValue($subTotal), true, config('shopping_cart'));
            return [
                'name' => $c->getName(),
                'type' => $c->getType(),
                'target' => $c->getTarget(),
                'value' => $c->getValue(),
                'discount' => $discount
            ];
        });

        return response(array(
            'success' => true,
            'data' => array(
                'cart_sub_total_conditions_count' => $subTotalConditions->count(),
                'cart_total_conditions_count' => $totalConditions->count(),
                'cart_sub_total_conditions' => $subTotalConditions,
                'cart_total_conditions' => $totalConditions,
                'total_quantity' => $cartTotalQuantity,
                'sub_total' => number_format($subTotal,2),
                'total' => $cart->getTotal(),
            ),
            'message' => "Get cart details success."
        ), 200, []);
    }
}
