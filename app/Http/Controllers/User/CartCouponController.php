<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use Illuminate\Http\Request;
use Cart;
use Darryldecode\Cart\CartCondition;

class CartCouponController extends Controller
{
    public function store(Request $request)
    {
        $coupon = Coupon::where('name', $request->coupon_name)->isNotExpired()->first();
        if (!$coupon) {
            return response(['message' => 'Invalid coupon or Coupon has been expired!'], 405);
        }
        $cart = Cart::session(auth()->id());
        $condition = new CartCondition([
            'name' => $coupon->name,
            'type' => $coupon->type,
            'target' => 'total', // this condition will be applied to cart's subtotal when getTotal() is called.
            'value' => $coupon->value,
        ]);
        $taxCondition =
            new CartCondition([
                'name' => 'T10P',
                'type' => 'tax',
                'target' => 'total', // this condition will be applied to cart's subtotal when getTotal() is called.
                'value' => '+10%',
            ]);
        $cart->condition([$condition, $taxCondition]);
        return response(['message' => 'Coupon has been applied successfully!'], 200);
    }

    public function destroy($coupon_name)
    {
        $coupon = Coupon::where('name', $coupon_name)->isNotExpired()->first();
        if (!$coupon) {
            return response(['message' => 'Invalid coupon or Coupon has been expired!'], 405);
        }
        $cart = Cart::session(auth()->id());

        $cart->clearCartConditions();

        session()->forget('coupon');
        return response(['message' => 'Coupon has been removed successfully!'], 200);
    }
}
