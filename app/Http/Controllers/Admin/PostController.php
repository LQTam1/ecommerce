<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PostsDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Blog\Post\StoreRequest;
use App\Http\Requests\Admin\Blog\Post\UpdateRequest;
use App\Models\Post;
use App\Models\PostCategory;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Support\Str;

class PostController extends Controller
{
    public function index(PostsDataTable $dataTable)
    {
        return $dataTable->render('pages.admin.blog.post_index');
    }

    public function create()
    {
        $this->authorize('create posts');
        $postCategories = PostCategory::all(['id', 'name']);
        return view('pages.admin.blog.post_create', compact('postCategories'));
    }

    public function store(StoreRequest $request)
    {
        $this->authorize('create posts');
        $validated = $request->validated();
        $post = new Post();

        foreach (LaravelLocalization::getSupportedLocales() as $localeCode => $properties) {
            $validated['slug'][$localeCode] =  Str::slug($validated['slug'][$localeCode]);
        }

        $validated['image'] = 'assets/images/no_image.jpg';
        if ($image = $request->image) {
            $post->updatePhoto($image, 'image', 'assets/images/blog-post', [780, 433]);
            unset($validated['image']);
        }

        $post->forceFill($validated)->save();
        return back()->with('success', 'Blog Post has been created successfully!');
    }

    public function edit(Post $post)
    {
        $postCategories = PostCategory::all(['id', 'name']);
        return view('pages.admin.blog.post_edit', compact('post', 'postCategories'));
    }

    public function update(UpdateRequest $request, Post $post)
    {
        $this->authorize('update posts');
        $validated = $request->validated();
        $post->post_category_id = $validated['post_category_id'];
        $post->setTranslations('title', $validated['title']);
        $post->setTranslations('details', $validated['details']);
        foreach (LaravelLocalization::getSupportedLocales() as $localeCode => $properties) {
            $post->setTranslation('slug', $localeCode, Str::slug($validated['slug'][$localeCode]));
        }

        if ($image = $request->newImage) {
            $post->updatePhoto($image, 'image', 'assets/images/blog-post', [780, 433]);
        }

        $post->save();
        return back()->with('success', 'Blog Post has been updated successfully!');
    }

    public function destroy(Post $post)
    {
        $this->authorize('delete posts');
        if ($post->isFileExist($post->image)) {
            unlink(public_path($post->image));
        }
        $post->delete();
        return back()->with('success', 'Blog Post has been deleted successfully.');
    }
}
