<?php

namespace App\Http\Controllers\Admin;

use App\Models\MenuItems;
use Harimayco\Menu\Controllers\MenuController as HarimaycoMenuController;
use Harimayco\Menu\Models\Menus;

class MenuController extends HarimaycoMenuController
{
    public function createnewmenu()
    {
        $menu = new Menus();
        $menu->name = request()->input("menuname");
        $menu->save();
        return json_encode(array("resp" => $menu->id));
    }

    public function deleteitemmenu()
    {
        $menuitem = MenuItems::find(request()->input("id"));

        $menuitem->delete();
    }

    public function deletemenug()
    {
        $menus = new MenuItems();
        $getall = $menus->getall(request()->input("id"));
        if (count($getall) == 0) {
            $menudelete = Menus::find(request()->input("id"));
            $menudelete->delete();

            return json_encode(array("resp" => "you delete this item"));
        } else {
            return json_encode(array("resp" => "You have to delete all items first", "error" => 1));
        }
    }

    private function get_numerics($str)
    {
        preg_match_all('/\d+/', $str, $matches);
        return $matches[0];
    }

    public function updateitem()
    {
        $arraydata = request()->input("arraydata");
        if (is_array($arraydata)) {
            foreach ($arraydata as $value) {
                $menuitem = MenuItems::find($value['id']);
                foreach ($value['label'] as $labels) {
                    foreach ($labels as $label => $v) {
                        $id = $this->get_numerics($label);
                        if ($value['id'] == $id[0]) {
                            $keyName = str_replace("idlabelmenu_" . $value['id'] . "[", '', $label);
                            $menuitem
                                ->setTranslation('label', $keyName, $v);
                        }
                    }
                }
                // $menuitem->label = $value['label'];
                $menuitem->link = $value['link'];
                $menuitem->class = $value['class'];
                if (config('menu.use_roles')) {
                    $menuitem->role_id = $value['role_id'] ? $value['role_id'] : 0;
                }
                $menuitem->save();
            }
        } else {
            $menuitem = MenuItems::find(request()->input("id"));
            $menuitem->label = request()->input("label");
            $menuitem->link = request()->input("url");
            $menuitem->class = request()->input("clases");
            if (config('menu.use_roles')) {
                $menuitem->role_id = request()->input("role_id") ? request()->input("role_id") : 0;
            }
            $menuitem->save();
        }
    }

    public function addcustommenu()
    {
        $menuitem = new MenuItems();
        $labelMenus = request()->input("labelmenu");
        foreach ($labelMenus as $value) {
            foreach ($value as $label => $v) {
                $keyName = str_replace('label[', '', $label);
                $menuitem
                    ->setTranslation('label', $keyName, $v);
            }
        }
        $menuitem->slug = \Illuminate\Support\Str::slug(request()->slugmenu);
        $menuitem->link = strtolower(request()->input("linkmenu"));
        if (!empty(request()->input("classmenu"))) {
            $menuitem->class = request()->input("classmenu");
        }
        if (config('menu.use_roles')) {
            $menuitem->role_id = request()->input("rolemenu") ? request()->input("rolemenu")  : 0;
        }
        $menuitem->menu = request()->input("idmenu");
        $menuitem->sort = MenuItems::getNextSortRoot(request()->input("idmenu"));
        $menuitem->save();
    }

    public function generatemenucontrol()
    {
        $menu = Menus::find(request()->input("idmenu"));
        $menu->name = request()->input("menuname");

        $menu->save();
        if (is_array(request()->input("arraydata"))) {
            foreach (request()->input("arraydata") as $value) {

                $menuitem = MenuItems::find($value["id"]);
                $menuitem->parent = $value["parent"];
                $menuitem->sort = $value["sort"];
                $menuitem->depth = $value["depth"];
                if (config('menu.use_roles')) {
                    $menuitem->role_id = request()->input("role_id");
                }
                $menuitem->save();
            }
        }
        echo json_encode(array("resp" => 1));
    }
}
