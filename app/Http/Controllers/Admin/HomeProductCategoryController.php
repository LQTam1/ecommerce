<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\HomeCategory;
use App\Models\MenuItems;
use Illuminate\Http\Request;

class HomeProductCategoryController extends Controller
{
    public function index()
    {
        $categories = MenuItems::all('id', 'label');
        return view('pages.admin.home-product-category', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->authorize('update home categories');
        $validated = $request->validate([
            'select_categories' => 'required|array',
            'products_to_show' => 'numeric|required|min:1|max:50'
        ]);
        $validated['select_categories'] = MenuItems::whereIn('id',$validated['select_categories'])->get();
        HomeCategory::create($validated);
        return back()->with('success', 'Product Category has been created successfully!');
    }

    public function update(Request $request, HomeCategory $homeCategory)
    {
        $this->authorize('update home categories');
        $validated = $request->validate([
            'select_categories' => 'required|array',
            'products_to_show' => 'numeric|required|min:1|max:50'
        ]);
        $validated['select_categories'] = MenuItems::whereIn('id',$validated['select_categories'])->get();
        $homeCategory->update($validated);
        return back()->with('success', 'Product Category has been updated successfully!');
    }
}
