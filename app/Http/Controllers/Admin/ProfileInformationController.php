<?php

namespace App\Http\Controllers\Admin;

use App\Actions\Fortify\UpdateUserProfileInformation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileInformationController extends Controller
{
    public function update(Request $request,UpdateUserProfileInformation $updater)
    {
        $updater->update($request->user(), $request->all());
        return back()->with('success', 'Admin Profile has been updated successfully!');
    }
}
