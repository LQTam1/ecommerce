<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $date = date('d-m-y');
        $today = Order::where('order_date', $date)->sum('amount');

        $month = date('F');
        $month = Order::where('order_month', $month)->sum('amount');

        $year = date('Y');
        $year = Order::where('order_year', $year)->sum('amount');

        $pendingOrders = Order::status('pending')->get();
        return view('pages.admin.dashboard', compact('today', 'month', 'year', 'pendingOrders'));
    }
}
