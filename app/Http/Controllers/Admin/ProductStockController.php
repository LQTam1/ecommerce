<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class ProductStockController extends Controller
{
    public function index()
    {
        return view('pages.admin.product.stock_list');
    }
}
