<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ReviewsDataTable;
use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function index(ReviewsDataTable $dataTable)
    {
        return $dataTable->render('pages.admin.review.index');
    }

    public function approve(Review $review)
    {
        $review->update(['status' => 1]);
        return back()->with('success', 'Review has been approved successfully!');
    }

    public function destroy(Review $review)
    {
        $review->delete();
        return response(['success' => 'Review has been deleted successfully.', 'status' => 200]);
    }
}
