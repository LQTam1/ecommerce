<?php

namespace App\Http\Controllers\Admin;

use App\Actions\Fortify\UpdateUserPassword;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PasswordController extends Controller
{
    public function update(Request $request, UpdateUserPassword $updater)
    {
        $updater->update($request->user(), $request->all());
        return back()->with('success', 'Admin password has been updated successfully!');
    }
}
