<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ProductImage;
use Illuminate\Http\Request;

class ProductImageController extends Controller
{
    public function store(Request $request)
    {
        $this->authorize('create product images');
        $imgs = $request->multi_img;

        foreach ($imgs as $id => $img) {
            $productImg = ProductImage::findOrFail($id);
            if ($productImg->isFileExist($productImg->photo_name))
                unlink(public_path($productImg->photo_name));

            $destinationPath = \Helper::IMG_BE_PATH . '/product';
            $productImg->updatePhoto($img, 'photo_name', $destinationPath, [917, 1000]);

            $productImg->save();
        } // end foreach

        return redirect()->back()->with('success', 'Product Image Updated Successfully');
    } // end mehtod 

    public function deleteProductImage(ProductImage $image)
    {
        $this->authorize('delete product image');
        if ($image->isFileExist($image->photo_name))
            unlink(public_path($image->photo_name));
        $image->delete();

        return redirect()->back()->with('success', 'Product Image Deleted Successfully');
    } // end method /
}
