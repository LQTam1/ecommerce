<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Setting\SeoUpdateRequest;
use App\Models\SeoSetting;

class SeoSettingController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SeoSetting  $seoSetting
     * @return \Illuminate\Http\Response
     */
    public function edit(SeoSetting $seoSetting)
    {
        return view('pages.admin.settings.seo_edit',compact('seoSetting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Http\Requests\Admin\Setting\SeoUpdateRequest  $seoSetting
     * @return \Illuminate\Http\Response
     */
    public function update(SeoUpdateRequest $request, SeoSetting $seoSetting)
    {
        $this->authorize('update seo settings');
        $seoSetting->update($request->validated());
        return back()->with('success','Seo Setting has been updated successfully!');
    }
}
