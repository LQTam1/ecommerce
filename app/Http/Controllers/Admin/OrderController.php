<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\OrdersDataTable;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Shipping;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(OrdersDataTable $dataTable)
    {
        return $dataTable->render('pages.admin.order.index');
    }

    public function updateStatus(Request $request, Order $order)
    {
        $status = $request->status;
        switch ($status) {
            case 'delivered':
                $statusDateColumn = 'delivered_date';
                break;
            case 'confirm':
                $statusDateColumn = 'confirmed_date';
                break;
            case 'processing':
                $statusDateColumn = 'processing_date';
                break;
            case 'picked':
                $statusDateColumn = 'picked_date';
                break;
            case 'shipped':
                $statusDateColumn = 'shipped_date';
                break;
            case 'cancel':
                $statusDateColumn = 'cancel_date';
                break;
            default:
                $statusDateColumn = 'order_date';
                break;
        }

        $date = \Carbon\Carbon::now()->format('d F Y');
        if ($status === 'pending') $date = $order->order_date;
        if ($status === 'delivered') {
            $order->items()->each(fn ($orderItem) => ($orderItem->product()->decrement('product_qty', $orderItem->qty)));
        }

        $order->status = $status;
        $order->$statusDateColumn = $date;
        $order->save();
        return back()->with('success', "Order status has been updated to $status successfully!");
    }

    public function show(Order $order)
    {
        $order->load(['items', 'items.product']);
        $shipping = Shipping::where('id', $order->id)->orWhere('user_id', $order->user_id)->orWhere('email', $order->email)->orWhere('phone', $order->phone)->first();
        return view('pages.admin.order.show', compact('order','shipping'));
    }

    public function returnApprove(Order $order)
    {
        $order->update([
            'return_date' => \Carbon\Carbon::now()->format('d F Y'),
            'return_order' => 2,
        ]);
        return back()->with('success', 'Return order request has been approved successfully!');
    }

    public function invoiceDownload(Order $order)
    {
        $order->load(['items', 'items.product']);
        $options     = config('datatables-buttons.snappy.options');
        $orientation = config('datatables-buttons.snappy.orientation');
        $shipping = Shipping::where('id', $order->id)->orWhere('user_id', $order->user_id)->orWhere('email', $order->email)->orWhere('phone', $order->phone)->first();

        return \PDF::loadHTML(view('pages.user.orders.pdf_invoice', compact('order','shipping')))
            ->setPaper('a4')
            ->setOptions($options)
            ->setOrientation($orientation)
            ->download("invoice_{$order->invoice_no}.pdf");
    }
}
