<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\SlidersDataTable;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Slider\UpdateOrCreateRequest;
use App\Models\Slider;

class SliderController extends Controller
{
    public function index(SlidersDataTable $dataTable)
    {
        return $dataTable->render('pages.admin.slider.index');
    }


    public function store(UpdateOrCreateRequest $request)
    {
        $this->authorize('create sliders');
        $validated = $request->validated();
        $validated['slider_img'] = Helper::NO_IMAGE;

        $slider = new Slider();
        if ($image = $request->slider_img) {
            $destination = Helper::IMG_BE_PATH . '/slider';
            $slider->updatePhoto($image, 'slider_img', $destination, [870, 370]);
            unset($validated['slider_img']);
        }

        $slider->forceFill($validated)->save();

        return redirect()->back()->with('success', 'Slider Inserted Successfully');
    }

    public function edit(Slider $slider)
    {
        return view('pages.admin.slider.edit', compact('slider'));
    }


    public function update(UpdateOrCreateRequest $request, Slider $slider)
    {
        $this->authorize('update sliders');
        $validated = $request->validated();

        if ($file = $request->slider_img) {
            $destination = Helper::IMG_BE_PATH . '/slider';
            $slider->updatePhoto($file, 'slider_img', $destination, [870, 370]);
            unset($validated['slider_img']);
        }
        $slider->update($validated);
        return back()->with('success', 'Slider Updated Successfully');
    }


    public function destroy(Slider $slider)
    {
        $this->authorize('delete sliders');
        $img = $slider->slider_img;
        if ($slider->isFileExist($img))
            unlink(public_path($img));
        $slider->delete();

        return back()->with('success', 'Slider Delectd Successfully',);
    }


    public function toggleStatus(Slider $slider)
    {
        $this->authorize('toggle-status sliders');
        $status = $slider->status === 1 ? 0 : 1;
        $slider->update(['status' => $status]);
        return back()->with('success', 'Slider status has been updated successfully!');
    }
}
