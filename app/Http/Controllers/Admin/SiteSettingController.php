<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Setting\SiteUpdateRequest;
use App\Models\SiteSetting;

class SiteSettingController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SiteSetting  $siteSetting
     * @return \Illuminate\Http\Response
     */
    public function edit(SiteSetting $siteSetting)
    {
        return view('pages.admin.settings.site_edit', compact('siteSetting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Admin\Setting\SiteUpdateRequest $request
     * @param  \App\Models\SiteSetting  $siteSetting
     * @return \Illuminate\Http\Response
     */
    public function update(SiteUpdateRequest $request, SiteSetting $siteSetting)
    {
        $this->authorize('update site settings');
        $validated = $request->validated();
        if ($logo = $request->logo) {
            $siteSetting->updatePhoto($logo, 'logo', 'assets/images/logo', [139, 36]);
            unset($validated['logo']);
        }
        $siteSetting->update($validated);
        return back()->with('success', 'Site setting has been updated successfully!');
    }
}
