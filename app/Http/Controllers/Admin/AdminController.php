<?php

namespace App\Http\Controllers\Admin;

use App\Actions\Fortify\UpdateUserProfileInformation;
use App\DataTables\AdminsDataTable;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AdminStoreRequest;
use App\Http\Requests\Admin\AdminUpdateRequest;
use App\Models\Admin;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AdminsDataTable $dataTable)
    {
        return $dataTable->render('pages.admin.admin_index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create admins');
        $permissions = Permission::all(['id', 'name']);
        $permissionGroup = Helper::groupPermissionByResource($permissions);
        return view('pages.admin.admin_user_create', compact('permissionGroup'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\AdminStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminStoreRequest $request)
    {
        $this->authorize('create admins');
        $validated = $request->validated();
        $admin = new Admin();
        if ($image = $request->profile_photo_path) {
            $admin->forceFill(['profile_photo_path' => $image->storePublicly(
                'profile-photos',
                ['disk' => 'public']
            )]);
        }
        $admin->forceFill([
            'name' => $validated['name'],
            'email' => $validated['email'],
            'password' => Hash::make('password'),
        ])->save();

        $admin->givePermissionTo(array_values($validated['permissions']));
        return back()->with('success', 'Account has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        $permissions = $admin->getAllPermissions();
        if ($admin->hasRole('Super-Admin'))
            $permissions = Permission::all(['id', 'name']);
        $permissionGroup = Helper::groupPermissionByResource($permissions);
        return view('pages.admin.admin_user_show', compact('admin', 'permissionGroup'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        $this->authorize('update admins');
        if (Gate::denies('update admins')) abort(403);
        $permissions = Permission::all(['id', 'name']);
        $permissionGroup = Helper::groupPermissionByResource($permissions);
        return view('pages.admin.admin_user_edit', compact('admin', 'permissionGroup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\AdminUpdateRequest  $request
     * @param  \App\Models\Admin $admin
     * @return \Illuminate\Http\Response
     */
    public function update(AdminUpdateRequest $request, Admin $admin)
    {
        $this->authorize('update admins');
        $validated = $request->validated();
        if ($image = $request->newProfilePhoto) {
            $admin->updateProfilePhoto($image);
        }
        $admin->update($validated);
        $admin->syncPermissions(array_values($validated['permissions']));
        return back()->with('success', 'Account has been updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        $this->authorize('delete admins');
        $admin->deleteProfilePhoto();
        $admin->delete();
        return back()->with('success', 'Account has been deleted successfully!');
    }
}
