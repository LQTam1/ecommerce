<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PostCategoriesDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Blog\PostCategoy\StoreRequest;
use App\Http\Requests\Admin\Blog\PostCategoy\UpdateRequest;
use App\Models\PostCategory;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Support\Str;

class PostCategoryController extends Controller
{
    public function index(PostCategoriesDataTable $dataTable)
    {
        return $dataTable->render('pages.admin.blog.category_index');
    }

    public function store(StoreRequest $request)
    {
        $this->authorize('create post categories');
        $validated = $request->validated();
        $postCategory = new PostCategory();
        $postCategory->setTranslations('name', $validated['name']);
        foreach (LaravelLocalization::getSupportedLocales() as $localeCode => $properties) {
            $postCategory->setTranslation('slug', $localeCode, Str::slug($validated['slug'][$localeCode]));
        }
        $postCategory->save();
        return back()->with('success', 'Post Category has been created successfully!');
    }

    public function edit(PostCategory $postCategory)
    {
        return view('pages.admin.blog.category_edit', compact('postCategory'));
    }

    public function update(UpdateRequest $request, PostCategory $postCategory)
    {
        $this->authorize('update post categories');
        $validated = $request->validated();
        $postCategory->setTranslations('name', $validated['name']);
        foreach (LaravelLocalization::getSupportedLocales() as $localeCode => $properties) {
            $postCategory->setTranslation('slug', $localeCode, Str::slug($validated['slug'][$localeCode]));
        }
        $postCategory->save();
        return back()->with('success', 'Post Category has been updated successfully!');
    }

    public function destroy(PostCategory $postCategory)
    {
        $this->authorize('delete post categories');
        $postCategory->delete();
        return response(['success' => 'Post Category has been deleted successfully.', 'status' => 200]);
    }
}
