<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ProductsDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Product\UpdateOrCreateRequest;
use App\Imports\ProductsImport;
use App\Jobs\NotifyUserOfCompleteImport;
use App\Jobs\ProcessProductInsertData;
use App\Jobs\ProductsImportJob;
use App\Models\Brand;
use App\Models\Product;
use Harimayco\Menu\Facades\Menu;
use Illuminate\Bus\Batch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\HeadingRowImport;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \App\DataTables\ProductsDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index(ProductsDataTable $dataTable)
    {
        return $dataTable->render('pages.admin.product.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create products');
        $categories = Menu::getByName('Main Menu');
        $brands = Brand::latest()->get();
        return view('pages.admin.product.create', compact('categories', 'brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\Product\UpdateOrCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdateOrCreateRequest $request)
    {
        $this->authorize('create products');
        $validated = $request->validated();
        $product = new Product();

        $product->forceFill($validated)->save();
        return redirect()->route('admin.products.index')->with('success', 'Product Inserted Successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Menu::getByName('Main Menu');
        $brands = Brand::latest()->get(['id', 'name']);
        return view('pages.admin.product.edit', compact('categories', 'brands', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\Product\UpdateOrCreateRequest $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOrCreateRequest $request, Product $product)
    {
        $this->authorize('update products');
        $validated = $request->validated();
        $product->update($validated);

        return back()->with('success', 'Product Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->authorize('delete products');
        $product->delete();

        return response([
            'success' => 'Product deleted successfully!',
            'status' => 200
        ]);
    }

    public function toggleStatus(Product $product)
    {
        $this->authorize('toggle-status');
        $status = $product->status === 1 ? false : true;
        $product->forceFill(['status' => $status])->save();

        return redirect()->back()->with('success', 'Product status has been updated successfully!');
    }

    public function importData(Request $request)
    {
        if ($request->has('header')) {

            // $file = $request->import_file->store('imports');
            // $batch = Bus::batch([new ProductsImportJob($request->user(), $file)])
            //     ->onQueue('import-products')
            //     ->onConnection('redis')
            //     ->dispatch();
            // if ($batch->finished()) {
            //     (new NotifyUserOfCompleteImport($request->user()))->dispatch();
            //     Storage::delete($file);
            // }
            (new ProductsImport($request->user()))
                ->queue($request->file('import_file'))
                ->chain([
                    new NotifyUserOfCompleteImport($request->user()),
                ])
            ;
            return back()->with('success', 'Import on processing.We\'ll send you mail when import done.');
        } else {
            $data = array_map('str_getcsv', file($request->file('import_file')->getRealPath()));
        }
    }
}
