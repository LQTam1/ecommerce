<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\BrandsDataTable;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Brand\UpdateOrCreateRequest;
use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param \App\DataTables\BrandsDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index(BrandsDataTable $dataTable)
    {
        return $dataTable->render('pages.admin.brand.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\Brand\UpdateOrCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdateOrCreateRequest $request)
    {
        $this->authorize('create brands');
        $validated = $request->validated();
        $validated['image'] = \Helper::NO_IMAGE;
        $brand = new Brand();
        if ($file = $request->image) {
            $brand->updatePhoto($file, 'image', Helper::IMG_BE_PATH . '/brand', [300, 300]);
            unset($validated['image']);
        }

        $brand->forceFill($validated)->save();
        return back()->with('success', 'Brand has been created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        return view('pages.admin.brand.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\Brand\UpdateOrCreateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOrCreateRequest $request, Brand $brand)
    {
        $this->authorize('update brands');
        $validated = $request->validated();
        if ($file = $request->image) {
            $brand->updatePhoto($file, 'image', Helper::IMG_BE_PATH . '/brand', [300, 300]);
            unset($validated['image']);
        }

        $brand->update($validated);
        return back()->with('success', 'Brand has been updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        $this->authorize('delete brands');
        if ($brand->isFileExist($brand->image)) {
            unlink(public_path($brand->image));
        }
        $brand->delete();
        return back()->with('success', 'Brand has been deleted successfully!');
    }
}
