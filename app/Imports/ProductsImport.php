<?php

namespace App\Imports;

use App\Models\Admin;
use App\Models\Product;
use App\Notifications\ImportHasFailedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithUpserts;
use Maatwebsite\Excel\Events\ImportFailed;

// WithBatchInserts,
class ProductsImport implements
    ToModel,
    WithHeadingRow,
    WithUpserts,
    WithChunkReading,
    ShouldQueue
{
    use Importable;

    public function __construct(Admin $importedBy)
    {
        $this->importedBy = $importedBy;
    }

    public function model(array $row)
    {
        return new Product([
            'brand_id' => $row['brand_id'],
            'menu_item_id' => $row['menu_item_id'],
            'name' => $row['name'],
            'slug' => $row['slug'],
            'product_code' => $row['product_code'],
            'product_qty' => $row['product_qty'],
            'tags' => $row['tags'],
            'size' => $row['size'],
            'color' => $row['color'],
            'selling_price' => $row['selling_price'],
            'discount_price' => $row['discount_price'],
            'short_description' => $row['short_description'],
            'description' => $row['description'],
            'product_thumbnail' => $row['product_thumbnail'],
            'hot_deals' => $row['hot_deals'],
            'featured' => $row['featured'],
            'special_offer' => $row['special_offer'],
            'special_deals' => $row['special_deals'],
            'status' => $row['status'],
            'digital_file' => $row['digital_file'],
        ]);
    }

    public function uniqueBy()
    {
        return 'slug';
    }

    // public function batchSize(): int
    // {
    //     return 2700;
    // }

    public function chunkSize(): int
    {
        return 3000;
    }

    public function registerEvents(): array
    {
        return [
            ImportFailed::class => function (ImportFailed $event) {
                $this->importedBy->notify(new ImportHasFailedNotification);
            }
        ];
    }
}
