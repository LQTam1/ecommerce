"use strict";

$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
});
// Start Product View with Modal
function productView(id) {
    $.ajax({
        type: "GET",
        url: "/product/modal/" + id,
        dataType: "json",
        success: function (data) {
            // console.log(data)
            $("#pname").text(data.product.name);
            $("#price").text(data.product.selling_price);
            $("#pcode").text(data.product.product_code);
            $("#pcategory").text(data.product.category.label);
            $("#pbrand").text(data.product.brand.name);
            $("#pimage").attr("src", "/" + data.product.product_thumbnail);
            $("#product_id").val(id);
            $("#qty").val(1);
            // Product Price
            if (data.product.discount_price == null) {
                $("#pprice").text("");
                $("#oldprice").text("");
                $("#pprice").text(data.product.selling_price);
            } else {
                $("#pprice").text(data.product.discount_price);
                $("#oldprice").text(data.product.selling_price);
            } // end prodcut price
            // Start Stock opiton
            if (data.product.product_qty > 0) {
                $("#aviable").text("");
                $("#stockout").text("");
                $("#aviable").text("aviable");
            } else {
                $("#aviable").text("");
                $("#stockout").text("");
                $("#stockout").text("stockout");
            } // end Stock Option
            // Color
            $('select[name="color"]').empty();
            $.each(data.color, function (key, value) {
                $('select[name="color"]').append(
                    '<option value=" ' + value + ' ">' + value + " </option>"
                );
            }); // end color
            // Size
            $('select[name="size"]').empty();
            $.each(data.size, function (key, value) {
                $('select[name="size"]').append(
                    '<option value=" ' + value + ' ">' + value + " </option>"
                );
                if (data.size == "") {
                    $("#sizeArea").hide();
                } else {
                    $("#sizeArea").show();
                }
            }); // end size
        },
    });
}
// Eend Product View with Modal

function increaseQty() {
    let quantity = document.querySelector("#quantity");
    quantity.value = parseInt(quantity.value) + 1;
}
function decreaseQty() {
    let quantity = document.querySelector("#quantity");
    if (parseInt(quantity.value) > 1)
        quantity.value = parseInt(quantity.value) - 1;
}

// Start Add To Cart Product
function addToCart() {
    var name = $("#pname").text();
    var id = $("#product_id").val();
    var color = $("#color option:selected").text();
    var size = $("#size option:selected").text();
    var quantity = $("#qty").val();

    let toastOptions = {
        text: "success",
        position: "top-right",
        loaderBg: "#ff6849",
        icon: "success",
        hideAfter: 3500,
        stack: 6,
    };

    $.ajax({
        url: "/carts",
        type: "POST",
        dataType: "json",
        data: {
            color,
            size,
            quantity,
            name,
            id,
        },
        success: function (data) {
            miniCart();
            $("#closeModal").click();
            // Start Message
            if ($.isEmptyObject(data.error)) {
                $.toast({ ...toastOptions, text: data.success });
            } else {
                $.toast({ ...toastOptions, icon: "error", text: data.error });
            }
            // End Message
        },
    });
}
// End Add To Cart Product

// Start Add To Cart Product
function add2Wishlist(id) {
    let toastOptions = {
        text: "success",
        position: "top-right",
        loaderBg: "#ff6849",
        icon: "success",
        hideAfter: 3500,
        stack: 6,
    };

    $.ajax({
        url: "/wishlist",
        type: "POST",
        dataType: "json",
        data: {
            id,
        },
        success: function (data) {
            // Start Message
            if ($.isEmptyObject(data.error)) {
                $.toast({ ...toastOptions, text: data.success });
            } else {
                $.toast({ ...toastOptions, icon: "error", text: data.error });
            }
            // End Message
        },
    });
}
// End Add To Cart Product

function miniCart() {
    $.ajax({
        type: "GET",
        url: "/carts/mini-cart",
        dataType: "json",
        success: function (response) {
            $('span[id="cartSubTotal"]').text(response.cartTotal);
            $("#cartQty").text(response.cartQty);
            var miniCart = "";
            $.each(response.carts, function (key, value) {
                miniCart += `<div class="cart-item product-summary">
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="image"> <a href="/product/details/${value.associatedModel.slug}"><img src="/${value.attributes.image}" alt=""></a> </div>
                        </div>
                        <div class="col-xs-7">
                            <h3 class="name"><a href="/product/details/${value.associatedModel.slug}">${value.name}</a></h3>
                            <div class="price"> ${value.price} * ${value.quantity} </div>
                        </div>
                        <div class="col-xs-1 action"> 
                            <button type="submit" id="${value.id}" onclick="miniCartRemove(this.id)"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                </div>
                <!-- /.cart-item -->
                <div class="clearfix"></div>
                <hr>`;
            });

            $("#miniCart").html(miniCart);
        },
    });
}
miniCart();
/// mini cart remove Start
function miniCartRemove(rowId) {
    let toastOptions = {
        text: "Oops! Something went wrong!",
        position: "top-right",
        loaderBg: "#ff6849",
        icon: "error",
        hideAfter: 3500,
        stack: 6,
    };
    $.ajax({
        type: "DELETE",
        url: "/minicart/product-remove/" + rowId,
        dataType: "json",
        success: function (data) {
            miniCart();
            // Start Message

            if ($.isEmptyObject(data.error)) {
                $.toast({
                    ...toastOptions,
                    icon: "success",
                    text: data.success,
                });
            } else {
                $.toast({ ...toastOptions, text: data.error });
            }
            // End Message
        },
        error: function () {
            $.toast(toastOptions);
        },
    });
}
//  end mini cart remove

function searchResultHide() {
    $("#productSearchDropdown").slideUp();
}

function searchResultShow() {
    $("#productSearchDropdown").slideDown();
}

(function ($) {
    "use strict";
    var MERCADO_JS = {
        init: function () {
            this.mercado_chosen();
        },
        mercado_chosen: function () {
            if ($(".search-area .categories-filter").length > 0) {
                $(".search-area .categories-filter").on(
                    "click",
                    ".dropdown>.link-control",
                    function (event) {
                        event.preventDefault();
                        $(this).siblings("ul").slideToggle();
                    }
                );
                $(
                    ".search-area .categories-filter .dropdown>.dropdown-menu"
                ).on("click", "li", function (event) {
                    var _this = $(this),
                        _content = _this.text(),
                        _title = _this.text();

                    _content = _content.slice(0, 12);
                    _this
                        .parent()
                        .siblings("a")
                        .text(_content)
                        .attr("title", _title);
                    _this
                        .parents(".dropdown")
                        .siblings('input[name="menu_item_id"]')
                        .val(_this.data("id"));
                    _this.parent().slideUp();
                });
            }
        },
    };
    /* ---------------------------------------------
	 Scripts on load
	 --------------------------------------------- */
    window.onload = function () {
        MERCADO_JS.init();
    };
})(window.Zepto || window.jQuery, window, document);
