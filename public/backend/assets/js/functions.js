function debounce(func, timeout = 350) {
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            func.apply(this, args);
        }, timeout);
    };
}

function formatDatedFY(date) {
    var options = { day: "numeric", year: "numeric", month: "long" };
    date = new Date(date);
    let arr = date
        .toLocaleDateString("en-US", options)
        .replace(",", "")
        .split(" ");
    let day = arr[1];
    arr[1] = arr[0];
    arr[0] = day;
    return arr.join(" ");
}

function deleteDatatableRow(table, _this) {
    let url = $(_this).attr("data-href");
    swal(
        {
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
        },
        function () {
            $.ajax({
                url,
                method: "delete",
                data: {
                    _token: $('meta[name="csrf-token"]').attr("content"),
                },
                dataType: "json",
                success: function (data, status, xhr) {
                    if (data.status === 200) {
                        $.toast({
                            text: data.success,
                            position: "top-right",
                            loaderBg: "#ff6849",
                            icon: "success",
                            hideAfter: 3500,
                            stack: 6,
                        });
                        table.row($(_this).parents("tr")).remove().draw();
                        swal.close()
                    }
                },
            });
        }
    );
}
